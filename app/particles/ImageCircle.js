import React from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';
import { Icon } from 'native-base';
import Image from 'react-native-remote-svg'
const ImageCircle = (props) => {
    return (
        <View>
            <Image source={{ uri: props.pictProfile }} style={[styles.profile, props.style]} />
            <TouchableWithoutFeedback onPress={props.onPressUpload}>
                <View style={[styles.profile2, { backgroundColor: GLOBAL.mainColor }]}>
                    <Icon name="ios-camera-outline" style={{ color: 'white' }} />
                </View>
            </TouchableWithoutFeedback>
        </View>
    );
};

//require('../assets/images.jpg')

const styles = {
    profile: {
        width: 140,
        height: 140,
        borderRadius: 70
    },
    profile2: {
        width: 33,
        height: 33,
        borderRadius: 50,
        alignSelf: 'flex-end',
        marginTop: -35,
        justifyContent: 'center',
        alignItems: 'center'

    }

};

export { ImageCircle };
