import React from 'react';
import { View, Dimensions, TouchableWithoutFeedback } from 'react-native';
import { Input, Icon, Text } from 'native-base';
import Image from 'react-native-remote-svg'
const { width, height } = Dimensions.get('window');


const CustomeCardBig = (props) => {
  return (
    <View style={[styles.containerStyle, {
      alignSelf: props.isCenter,
      marginTop: props.marginTopCard,
      height: props.heightCard,
      width: props.widthCard
    }]}>
      {props.children}
    </View>
  );
};

const CustomeCardSmall2 = (props) => {
  return (
    <View style={[styles.cardSmall, {
      width: width * props.widthCard,
      height: props.heightCard,
      marginBottom: props.marginBottomCard
    }]}>
      {props.children}
    </View>
  );
};


const InputWhiteBg = (props) => {
  return (
    <View style={styles.viewSearch}>
      <View style={styles.inputSearch}>
        <Input
          placeholder={props.pHolder}
          value={props.valueInput}
          onChangeText={props.onChangeInput}
          secureTextEntry={props.sec}
        />
      </View>
    </View>
  );
};

const InputWhiteBgStyle = (props) => {
  return (
    <View style={props.style}>
      <View style={props.style}>
        <Input
          placeholder={props.pHolder}
          value={props.valueInput}
          onChangeText={props.onChangeInput}
          secureTextEntry={props.sec}
        />
      </View>
    </View>
  );
};



const NewCard = (props) => {
  return (
    <View style={[styles.newCardStyle, props.style]}>
      {props.children}
    </View>
  );
};

const NewCardShadow = (props) => {
  return (
    <View style={[styles.ncShadow, props.style]}>
      {props.children}
    </View>
  );
};


const SmallBtn = (props) => {
  return (
    <View style={[props.style, { borderRadius: 3, paddingTop: 5, paddingRight: 10, paddingBottom: 5, paddingLeft: 10, backgroundColor: GLOBAL.mainColor }]}>
      {props.children}
    </View>
  );
};

const Tabikon = (props) => {
  return (
    <View style={{ flexDirection: 'row' }}>
      <Icon type="FontAwesome" name="shopping-cart" style={{ color: 'grey' }} />
      <View style={{ height: 20, width: 20, marginLeft: -10, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center', borderRadius: 30 }}>
        <Text style={{ fontSize: 14, color: 'white' }}>9</Text>
      </View>
    </View>
  );
};

const InputWhiteBgIcon = (props) => {
  return (
    <View style={styles.viewSearch2}>
      <View style={styles.inputSearch}>

        <Input
        
          placeholder={props.pHolder}
          value={props.valueInput}
          onChangeText={props.onChangeInput}
          style={styles.formInputText2}

        />
        {/* <TouchableWithoutFeedback onPress={props.toSemua}>
         
          <Image
            source={require('../assets/glassSearch.jpg')} resizeMode="contain"
            style={{ width: 25, height: 25, marginTop: 10, marginRight: 30 }} />
        </TouchableWithoutFeedback> */}
      </View>
    </View>
  );
};

const styles = {
  formInputText: {
    width: 250,
    borderRadius: 15,
    paddingLeft: 20,
    marginTop: -5
  },
  formInputText2: {
    // width: width * 0.7,
    borderRadius: 15,
    // paddingLeft: 30,
    // backgroundColor:'red',
    marginTop: -5
  },
  viewSearch: {
    height: 60,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: width * 0.97,
    backgroundColor: 'white'
  },
  viewSearch2: {
    flex:1,
    height: 40,
    marginLeft: 10,
    borderRadius: 15,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    // width: width * 0.6,
    backgroundColor: 'white'
  },
  viewSearchOutline: {
    height: 42,
    alignSelf: 'center',
    justifyContent: 'space-between',
    width: width * 0.89,
    height: 42,
    borderRadius: 15,
    borderWidth: 2,
    borderColor: "#660099",
    backgroundColor: "#ffffff"
  },
  inputSearch: {
    flex:1,
    flexDirection: 'row',
    alignSelf: 'center',
    //width: width * 0.95,
    height: 40,
    // backgroundColor:'yellow'
  },
  containerStyle: {
    borderRadius: 15,
    backgroundColor: "#ffffff",
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 4,
  },
  cardSmall: {
    borderWidth: 1,
    borderStyle: "solid",
    borderRadius: 15,
    borderColor: "#9b9b9b",
    elevation: 1,
    backgroundColor: "#ffffff",
  },
  newCardStyle: {
    borderBottomWidth: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative'
  },
  ncShadow: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: 'black',
    shadowOffset: { width: 10, height: 10, },
    shadowRadius: 2,
    shadowOpacity: 1.0,
    elevation: 3
  }
};

export { NewCard, SmallBtn, NewCardShadow, InputWhiteBg, InputWhiteBgIcon, Tabikon, InputWhiteBgStyle };
