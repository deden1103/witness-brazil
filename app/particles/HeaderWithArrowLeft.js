import React from 'react';
import { Text, View, TouchableWithoutFeedback } from 'react-native';
import { Button, Icon } from 'native-base'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Tabikon from './Tabikon'
const HeaderWithArrowLeft = (props) => {
    const { textStyle, viewStyle, left, body, right, icon } = styles;

    return (
        <View style={[viewStyle, { backgroundColor: props.colornya ? props.colornya : GLOBAL.mainColor }]}>
            <View style={left}>
                <Button transparent onPress={props.goBack}>
                    <Ionicons name="md-arrow-back" size={30} style={icon} />
                </Button>
            </View>
            <View style={body}>
                <Text style={textStyle}>{props.txtTitle}</Text>
            </View>
            <View style={right}>

                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    <View style={{ marginRight: 15 }}>
                        <Tabikon gocart={props.kecart} />
                    </View>
                    <TouchableWithoutFeedback transparent onPress={props.openDrawer}>
                        <View>
                            <Ionicons name="md-menu" size={30} style={icon} />
                        </View>
                    </TouchableWithoutFeedback>
                    {/* <Text style={textStyle}>{props.kembaliText}</Text> */}
                </View>

            </View>

        </View>
    );
};

const styles = {
    viewStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 60,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        elevation: 9,
        position: 'relative'
    },
    textStyle: {
        fontSize: 20,
        color: '#fff',
        alignSelf: 'center',
        // textDecorationLine: 'underline'
    },
    left: {
        paddingLeft: 10,
        flex: 0.3
    },
    body: {
        flex: 0.4
    },
    right: {
        flex: 0.3
    },
    icon: {
        color: '#fff',
        alignSelf: 'flex-end',
        marginRight: 10
    }
};


export { HeaderWithArrowLeft };
