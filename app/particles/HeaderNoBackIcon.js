import React from 'react';
import { Text, View } from 'react-native';
import { Button, Icon } from 'native-base';

const HeaderNoBackIcon = (props) => {
    const { textStyle, viewStyle, left, body, right, icon } = styles;

    return (
        <View style={[viewStyle, { backgroundColor: GLOBAL.mainColor }]}>
            <View style={left}>
                
            </View>
            <View style={body}>
                <Text style={textStyle}>{props.txtTitle}</Text>
            </View>
            <View style={right}>
                <Button transparent onPress={props.goBack} >
                    {/* <Icon type="FontAwesome" name="ellipsis-v" style={icon} /> */}
                    {/* <Ionicons name='ios-more' size={30} style={styles.icon} /> */}
                </Button>
            </View>

        </View>
    );
};

const styles = {
    viewStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 60,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        elevation: 9,
        position: 'relative'
    },
    textStyle: {
        fontSize: 20,
        color: '#fff',
        alignSelf: 'center'
    },
    left: {
        paddingLeft: 10,
        flex: 0.1
    },
    body: {
        flex: 0.8
    },
    right: {
        flex: 0.1
    },
    icon: {
        color: '#fff'
    }
};


export { HeaderNoBackIcon };
