import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import { Icon } from 'native-base';
import { connect } from 'react-redux';
import { emptycart } from '../rdx/actions';


class Tabikon extends Component {
    constructor(props) {
        super(props)
    }


    render() {

        return (
            <TouchableWithoutFeedback onPress={this.props.gocart}>
                <View style={{ flexDirection: 'row' }}>

                    <Icon type="FontAwesome" name="shopping-cart" style={{ color: 'white' }} />
                    {this.props.cart.length > 0 && <View style={{ height: 19, width: 19, marginLeft: -10, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center', borderRadius: 30 }}>
                        <Text style={{ fontSize: 14, color: 'white' }}>{this.props.cart.length}</Text>
                    </View>}
                </View>
            </TouchableWithoutFeedback>
        );
    }
}




const mapStateToProps = (state) => {
    const { cart } = state.cartpersist;

    return { cart };
};

export default connect(mapStateToProps, { emptycart })(Tabikon);
