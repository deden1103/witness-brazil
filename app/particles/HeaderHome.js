import React from 'react';
import { Text, View, TouchableWithoutFeedback, Dimensions, ImageBackground } from 'react-native';
import { Button, Icon } from 'native-base'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Image from 'react-native-remote-svg';
const { width, height } = Dimensions.get('window');
const HeaderHome = (props) => {
    const { textStyle, viewStyle, left, body, right, icon } = styles;

    return (
        <ImageBackground source={require('../assets/bg.png')} resizeMode="stretch" style={ viewStyle }>

            <View style={body}>
                <Image source={require('../assets/dcomart.png')} resizeMode="stretch" style={{ width: 140, height: 60 }} />
            </View>


        </ImageBackground>
    );
};

const styles = {
    viewStyle: {
        justifyContent: 'center',
        alignItems: 'center'
        // alignItems: 'center',
        // height: 60,
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 1,
        // elevation: 9,
        // position: 'relative'
    },
    textStyle: {
        fontSize: 20,
        color: '#fff',
        alignSelf: 'center',
        //  textDecorationLine: 'underline'
    },
    left: {
        paddingLeft: 10,
        flex: 0.3
    },
    body: {
        alignSelf: 'center',
        width: 140,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center'
    }
    // right: {
    //     flex: 0.3
    // },
    // icon: {
    //     color: '#fff'
    // }
};


export { HeaderHome };
/*

 <View style={[viewStyle, { backgroundColor: GLOBAL.mainColor }]}>
            <View style={left}>


            </View>
            <View style={body}>
                <Image source={require('../assets/dcomart.png')}  style={{ height:40,width:100, alignSelf: 'center' }} />
            </View>
            <View style={right}>
                <TouchableWithoutFeedback transparent onPress={props.goBack}>
                    <View>
                        <Text style={textStyle}>{props.kembaliText}</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>

        </View>
 <Image source={require('../assets/logo.png')} resizeMode="stretch" style={{ width,height:100}} />
*/