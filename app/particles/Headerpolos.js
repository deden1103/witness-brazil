import React from 'react';
import { Text, View } from 'react-native';

const Headerpolos = (props) => {
    const { textStyle, viewStyle, left, body, right, icon } = styles;

    return (
        <View style={[viewStyle, { backgroundColor: GLOBAL.mainColor }]}>
            <View style={left}></View>
            <View style={body}>
                <Text style={textStyle}>{props.txtTitle}</Text>
            </View>
            <View style={right}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                </View>
            </View>
        </View>
    );
};

const styles = {
    viewStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 60,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        elevation: 9,
        position: 'relative'
    },
    textStyle: {
        fontSize: 20,
        color: '#fff',
        alignSelf: 'center'
    },
    left: {
        paddingLeft: 10,
        flex: 0.3
    },
    body: {
        flex: 0.4
    },
    right: {
        flex: 0.3
    },
    icon: {
        color: '#fff',
        alignSelf: 'flex-end',
        marginRight: 10
    }
};


export { Headerpolos };
