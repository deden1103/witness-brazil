import axios from 'axios';
import { Alert } from 'react-native';

export const loginUser = (email, password) => {
	return (dispatch) => {
		dispatch({ type: 'wait_respons' })
		axios.post(GLOBAL.APIURL, { username: email, password }).then((res) => {
			if (res.data.success == true) {
				dispatch({ type: 'wait_respons_success' })
			} else {
				dispatch({ type: 'wait_respons_success' })
				Alert.alert('Seu e-mail ou senha está errado')
			}
		}).catch((error) => {
			if (error) {
				Alert.alert(error.response)
			} else {
				Alert.alert('sem conexão')
			}
		})
	}
}


export const testing = () => {
	return (dispatch) => {
		dispatch({ type: 'test_persist' })
	}
}

export const inputArticleList = (data) => {
	return (dispatch) => {
		dispatch({ type: 'input_article_list', payload: JSON.parse(data) })
	}
}

export const addqty = (data, id) => {
	return (dispatch) => {
		let carts = JSON.parse(data)
		for (var i = 0; i < carts.length; i++) {
			if (carts[i]['barangid'] == id) {
				carts[i]['hargaanggota'] = parseInt(carts[i]['hargaanggota']) + parseInt(carts[i]['hargaanggotaasli']);
				carts[i]['hargabiasa'] = parseInt(carts[i]['hargabiasa']) + parseInt(carts[i]['hargabiasaasli']);
				carts[i]['quantity']++;
			}
		}

		dispatch({ type: 'add_qty', payload: carts })
	}
}

export const minqty = (data, id) => {
	return (dispatch) => {
		let carts = JSON.parse(data)
		for (var i = 0; i < carts.length; i++) {
			if (carts[i]['barangid'] == id) {
				if (carts[i]['quantity'] > 1) {
					carts[i]['hargaanggota'] = parseInt(carts[i]['hargaanggota']) - parseInt(carts[i]['hargaanggotaasli']);
					carts[i]['hargabiasa'] = parseInt(carts[i]['hargabiasa']) - parseInt(carts[i]['hargabiasaasli']);
					carts[i]['quantity']--;
				}
			}
		}

		dispatch({ type: 'min_qty', payload: carts })
	}
}



export const removeone = (data, one) => {
	return (dispatch) => {
		let items = []
		let carts = JSON.parse(data)
		let oneitem = JSON.parse(one)
		for (var i = 0; i < carts.length; i++) {
			if (JSON.stringify(carts[i]) !== JSON.stringify(oneitem)) {
				items.push(carts[i]);
			}
		}
		dispatch({ type: 'remove_one', payload: items })
	}
}


export const emptycart = () => {
	return (dispatch) => {
		dispatch({ type: 'empty_cart' })
	}
}


export const loginsuccess = (nama) => {
	return (dispatch) => {
		dispatch({ type: 'login_success', payload: nama })
	}
}

export const loginsuccess2 = (email) => {
	return (dispatch) => {
		dispatch({ type: 'login_success2', payload: email })
	}
}

export const loginsuccess3 = (id) => {
	return (dispatch) => {
		dispatch({ type: 'login_success3', payload: id })
	}
}

export const loginsuccess4 = (foto) => {
	return (dispatch) => {
		dispatch({ type: 'login_success4', payload: foto })
	}
}



export const loginsuccess5 = (token) => {
	return (dispatch) => {
		dispatch({ type: 'login_success5', payload: token })
	}
}

export const loginsuccess6 = (isanggota) => {
	return (dispatch) => {
		dispatch({ type: 'login_success6', payload: isanggota })
	}
}


export const loginsuccess7 = (fasilitator) => {
	return (dispatch) => {
		dispatch({ type: 'login_success7', payload: fasilitator })
	}
}

export const loginsuccess8 = (jabatan) => {
	return (dispatch) => {
		dispatch({ type: 'login_success8', payload: jabatan })
	}
}

export const loginsuccess9 = (eposko) => {
	return (dispatch) => {
		dispatch({ type: 'login_success9', payload: eposko })
	}
}


export const searchshow = () => {
	return (dispatch) => {
		dispatch({ type: 'search_show' })
	}
}
export const searchhide = () => {
	return (dispatch) => {
		dispatch({ type: 'search_hide' })
	}
}

export const logout = () => {
	return (dispatch) => {
		dispatch({ type: 'log_out' })
	}
}

export const broadcastcount = (token, lastid) => {
	return (dispatch) => {
		axios.get(`${GLOBAL.apiUrl}relawan/getcount/${lastid}/1`, {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		}).then((res) => {
			if (res.data.success == true) {
				dispatch({ type: 'broadcast_count', payload: res.data.data.total })
			}
		}).catch((error) => {
			if (error) {
				console.log(error)
			} else {
				console.log(error)
			}
		})
	}
}

export const savelastidbroadcast = (broadcastid) => {
	return (dispatch) => {
		dispatch({ type: 'save_id_broadcast', payload: broadcastid })
	}
}


export const clearbroadcastcount = () => {
	return (dispatch) => {
		dispatch({ type: 'clear_broadcast' })
	}
}

// export const countcomment = (token, lastid = 0) => {
// 	return (dispatch) => {
// 		axios.get(`${GLOBAL.apiUrl}relawan/getcountnotif/${lastid}`, {
// 			headers: {
// 				'Authorization': `Bearer ${token}`
// 			}
// 		}).then((res) => {
// 			if (res.data.success == true) {
// 				dispatch({ type: 'comment_count', payload: res.data.data.total })
// 			}
// 		}).catch((error) => {
// 			if (error) {
// 				console.log(error)
// 			} else {
// 				console.log(error)
// 			}
// 		})
// 	}
// }

export const countcomment = (no) => {
	return (dispatch) => {
		dispatch({ type: 'comment_count', payload: no })
	}
}

export const savelastidcomment = (commentid) => {
	return (dispatch) => {
		dispatch({ type: 'save_id_comment', payload: commentid })
	}
}


export const clearcommentcount = () => {
	return (dispatch) => {
		dispatch({ type: 'clear_comment' })
	}
}

export const save_uri_movie = (uri) => {
	return (dispatch) => {
		dispatch({ type: 'save_uri_movie',payload:uri })
	}
}

export const remove_uri_movie = () => {
	return (dispatch) => {
		dispatch({ type: 'remove_uri_movie' })
	}
}

export const save_uri_video = (uri) => {
	return (dispatch) => {
		dispatch({ type: 'save_uri_video', payload: uri })
	}
}

export const save_latitude = (par) => {
	return (dispatch) => {
		dispatch({ type: 'save_latitude', payload: par })
	}
}

export const save_longitude = (par) => {
	return (dispatch) => {
		dispatch({ type: 'save_longitude', payload: par })
	}
}

export const update_list_article = (data) => {
	return (dispatch) => {
		dispatch({ type: 'update_list_article', payload: data })
	}
}

export const model_video_open = (data) => {
	return (dispatch) => {
		dispatch({ type: 'model_video_open', payload: data })
	}
}

export const model_video_open2 = (data) => {
	return (dispatch) => {
		dispatch({ type: 'model_video_open2', payload: data })
	}
}


export const savephone = (par) => {
	return (dispatch) => {
		dispatch({ type: 'savephone', payload: par })
	}
}
