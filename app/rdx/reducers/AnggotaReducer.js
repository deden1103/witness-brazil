import {Alert} from 'react-native';

const INITIAL_STATE = {
  alert: '',
  relawanid: null,
  isanggota: 0,
  nama: 'Silahkan Login',
  email: '',
  photo: '',
  phone: '',
  token: '',
  fasilitator: '',
  jabatan: '',
  eposko: '',
  lastidbroadcast: 0,
  lastidcomment: 0,
  komen: 7,
  uriMovie: '',
  myArticle: [],
  userid: '',
  latitude: '',
  longitude: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'input_cart':
      return {...state, alert: 'naon'};
    case 'login_success':
      return {
        ...state,
        nama: action.payload,
      };
    case 'login_success2':
      return {...state, email: action.payload};
    case 'login_success3':
      return {...state, relawanid: action.payload, userid: action.payload};
    case 'login_success4':
      return {...state, photo: action.payload};
    case 'login_success5':
      return {...state, token: action.payload};
    case 'login_success6':
      return {...state, isanggota: action.payload};
    case 'login_success7':
      return {...state, fasilitator: action.payload};
    case 'login_success8':
      return {...state, jabatan: action.payload};
    case 'login_success9':
      return {...state, eposko: action.payload};
    case 'savephone':
      return {...state, phone: action.payload};
    case 'log_out':
      return {
        ...state,
        photo: '',
        relawanid: null,
        nama: 'Silahkan Login',
        email: '',
        token: '',
        isanggota: 0,
        fasilitator: '',
        eposko: 0,
        jabatan: 0,
      };
    case 'save_id_broadcast':
      return {...state, lastidbroadcast: action.payload};
    case 'save_id_comment':
      return {...state, lastidcomment: action.payload};
    case 'save_uri_movie':
      return {...state, uriMovie: action.payload};
    case 'remove_uri_movie':
      return {...state, uriMovie: ''};
    case 'input_article_list':
      return {...state, myArticle: action.payload};
    case 'save_latitude':
      return {...state, latitude: action.payload};
    case 'save_longitude':
      return {...state, longitude: action.payload};

    default:
      return state;
  }
};
