import { Alert } from 'react-native';


const INITIAL_STATE = {
    cart: [],
    alert: '',
    broadcastcount: 0,
    commentcount: 0,


};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'input_cart':
            // return { ...state, cart: action.payload, alert: Alert.alert('Barang Berhasil Dimasukan Ke keranjang') }
            return { ...state, cart: action.payload }
        case 'empty_cart':
            return { ...state, cart: [] }
        case 'add_qty':
            return { ...state, cart: action.payload }
        case 'min_qty':
            return { ...state, cart: action.payload }
        case 'remove_one':
            return { ...state, cart: action.payload }
        case 'broadcast_count':
            return { ...state, broadcastcount: action.payload }
        case 'comment_count':
            return { ...state, commentcount: action.payload }
        case 'clear_broadcast':
            return { ...state, broadcastcount: 0 }
        case 'clear_comment':
            return { ...state, commentcount: 0 }


        default:
            return state;
    }
};
