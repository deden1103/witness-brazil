import { Alert } from 'react-native';


const INITIAL_STATE = {
  al: '',
  pjr: 'testing',
  count: null,
  uriVid: '',
  modalVideoIsOpen: false,
  modalVideoIsOpen2: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'test_aja':
      return { ...state, al: Alert.alert('test berhasil') }
    case 'test_persist':
      return { ...state, count: state.count + 1 }
    case 'save_uri_video':
      return { ...state, uriVid: action.payload }
    case 'model_video_open':
      return { ...state, modalVideoIsOpen: action.payload }
    case 'model_video_open2':
      return { ...state, modalVideoIsOpen2: action.payload }
    default:
      return state;
  }
};
