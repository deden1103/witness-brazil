import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import CartReducer from './CartReducer';
import Data from './Data';
import AnggotaReducer from './AnggotaReducer';
export default combineReducers({
  auth: AuthReducer,
  data: Data,
  cartpersist: CartReducer,
  anggota:AnggotaReducer
});
