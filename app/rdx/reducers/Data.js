import { Alert } from 'react-native';


const INITIAL_STATE = {
  al: '',
  issearchshow: false,
  listArticleRedux: []
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'test_aja':
      return { ...state, al: Alert.alert('test berhasil') }
    case 'search_show':
      return { ...state, issearchshow: true }
    case 'search_hide':
      return { ...state, issearchshow: false }
    case 'update_list_article':
      return { ...state, listArticleRedux: action.payload }
    default:
      return state;
  }
};