import React, { Component } from "react";
import {
    StyleSheet,
    View,
    TextInput,
    TouchableOpacity,
    Alert,
    Text,
    Dimensions,
    processColor,
    ScrollView
} from "react-native";

import {
    Container,
    Content,
} from "native-base";
const { width, height } = Dimensions.get('window')
import { connect } from "react-redux";
import Image from 'react-native-remote-svg';
import axios from 'axios';
// import { PieChart } from 'react-native-charts-wrapper';
import { StackNavigator, SafeAreaView } from 'react-navigation';
class Piechart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            legend: {
                enabled: false,
                textSize: 15,
                form: 'CIRCLE',
                horizontalAlignment: "RIGHT",
                verticalAlignment: "TOP",
                orientation: "VERTICAL",
                wordWrapEnabled: true
            },
            data: {
                dataSets: [{
                    values: [
                        { value: 45, label: 'Positive', id: 2 },
                        { value: 21, label: 'Negative', id: 4 }
                    ],
                    label: '',
                    config: {
                        colors: [processColor('#8CEAFF'), processColor('#FF8C9D')],
                        valueTextSize: 20,
                        valueTextColor: processColor('green'),
                        sliceSpace: 5,
                        selectionShift: 13,
                        // xValuePosition: "OUTSIDE_SLICE",
                        // yValuePosition: "OUTSIDE_SLICE",
                        valueFormatter: "#.#'%'",
                        valueLineColor: processColor('green'),
                        valueLinePart1Length: 0.5
                    }
                }],
            },
            highlights: [{ x: 2 }],
            description: {
                text: ' ',
                textSize: 15,
                textColor: processColor('white')
            }
        }
    }

    componentDidMount() {

    }

    handleSelect(event) {
        let entry = event.nativeEvent
        if (entry == null) {
            this.setState({ ...this.state, selectedEntry: null })
        } else {
            this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
        }

        // console.log(event.nativeEvent)
    }



    render() {
        return (
            <SafeAreaView style={{ flex: 1, paddingTop: 20 }}>
                {/* <View>
                    <Text>selected:</Text>
                    <Text> {this.state.selectedEntry}</Text>
                </View> */}

                <View style={styles.container}>
                    {/* <PieChart
                        style={styles.chart}
                        logEnabled={true}
                        //chartBackgroundColor={processColor('pink')}
                        chartDescription={this.state.description}
                        data={this.props.datanya}
                        legend={this.state.legend}
                        highlights={this.state.highlights}

                        entryLabelColor={processColor('green')}
                        entryLabelTextSize={12}
                        drawEntryLabels={true}

                        rotationEnabled={false}
                        rotationAngle={45}
                        // usePercentValues={true}
                        //styledCenterText={{ text: 'Pie center text!', color: processColor('pink'), size: 20 }}
                        centerTextRadiusPercent={2}
                        holeRadius={2}
                        holeColor={processColor('#f0f0f0')}
                        transparentCircleRadius={0}
                        transparentCircleColor={processColor('#f0f0f088')}
                        maxAngle={360}
                        onSelect={this.props.selectPie}
                        onChange={(event) => console.log(event.nativeEvent)}
                    /> */}
                </View>
            </SafeAreaView>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        
       
    },
    chart: {
        flex: 1,
       
      
    }
});

const mapStateToProps = (state) => {
    const { token, fasilitator } = state.anggota;
    return { token, fasilitator };
};

export default connect(mapStateToProps, {})(Piechart);
