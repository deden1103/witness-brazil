import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native';
import { Icon } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons'
import Image from 'react-native-remote-svg';
const { width, height } = Dimensions.get('window');


class ProductItem extends Component {
    constructor(props) {
        super(props)

    }

    formatMoney = (num) => {
        var p = num.toFixed().split(".");
        return "" + p[0].split("").reverse().reduce(function (acc, num, i, orig) {
            return num == "-" ? acc : num + (i && !(i % 3) ? "." : "") + acc;
        }, "");
    }


    render() {
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.props.toDetail}>
                    <View style={{ height: 170 }}>
                        <Image
                            source={{ uri: this.props.propsimg }}
                            style={{ flex: 1 }}
                            resizeMode="stretch"
                        />
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={this.props.toDetail}>
                    <View style={{ marginTop: 10 }}><Text style={{ color: 'black' }}>{this.props.propsnama}</Text></View>
                </TouchableWithoutFeedback>
                <View><Text>{this.props.propscategory}</Text></View>
                <View style={{ flexDirection: 'row' }}>
                    <View><Text>Harga Anggota Koperasi</Text></View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Icon type="FontAwesome" name="tag" style={{ color: 'red', fontSize: 20, marginRight: 10 }} />
                    <View><Text>Rp. {this.formatMoney(parseInt(this.props.propshargaanggota))}</Text></View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <View><Text>Harga Non Anggota</Text></View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Icon type="FontAwesome" name="tag" style={{ color: 'red', fontSize: 20, marginRight: 10 }} />
                    <View><Text>Rp. {this.formatMoney(parseInt(this.props.propshargabiasa))} </Text></View>
                </View>
                {/* <View><Text>SHU : </Text></View> */}
            </View >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: width * 0.465,
        minHeight: 320,
        backgroundColor: 'white',
        marginBottom: 10,
        padding: 4
    }
});

export default ProductItem