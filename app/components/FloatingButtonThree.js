import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions, Button, TouchableWithoutFeedback, Alert } from 'react-native';
import { Icon } from 'native-base';
import Image from 'react-native-remote-svg';
import { InputWhiteBgIcon } from '../particles/SmallParticle';
const { width, height } = Dimensions.get('window');


class FloatingButtonThree extends Component {

    render() {
        return (
            <View style={[styles.mainStyle, { backgroundColor: GLOBAL.mainColor }]}>
                <View style={{
                    width, alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-around',
                    // borderBottomLeftRadius: 6,
                    // borderBottomRightRadius: 6
                }}>
                    <TouchableWithoutFeedback onPress={this.props.cancel}>
                        <View><Text style={{ fontSize: 18, color: 'white' }}>Batal</Text></View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={this.props.kirim}>
                        <View><Text style={{ fontSize: 18, color: 'white' }}>Enviar</Text></View>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        )
    }

}

const styles = {
    mainStyle: {
        height: 50,

        // borderBottomLeftRadius: 6,
        // borderBottomRightRadius: 6,
        //backgroundColor: '#F2F3F5',
        //backgroundColor: 'grey',
        //alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
        flexDirection: 'row'
    },
    leftStyle: {
        flex: 0.5,
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30,
        borderColor: '#CCCCCC',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rightStyle: {
        flex: 0.5,
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30,
        borderColor: '#CCCCCC',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'

    }
}
export default FloatingButtonThree;
