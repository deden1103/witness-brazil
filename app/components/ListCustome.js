import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableWithoutFeedback, ImageBackground
} from 'react-native';
import { Icon } from 'native-base';
import Image from 'react-native-remote-svg';
import { NewCard, SmallBtn } from '../particles/SmallParticle';
const { width, height } = Dimensions.get('window');


class ListCustome extends Component {
    constructor(props) {
        super(props)

    }
    render() {
        return (

            <ImageBackground
                style={[styles.cardThree, {}]}
                source={{ uri: this.props.imgUri }}
            >
                <TouchableWithoutFeedback onPress={this.props.toEvent}>
                    <View style={styles.cardThree}>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', paddingRight: 15, paddingTop: 15 }}>
                            <Icon type="FontAwesome" name="comment" style={{ color: 'white', fontSize: 20, marginRight: 6 }} />
                            <Text style={{ color: 'white', fontSize: 15 }}>{this.props.comments}</Text>
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'white', fontSize: 20 }}>{this.props.title}</Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </ImageBackground >

        );
    }
}

const styles = StyleSheet.create({
    cardThree: {
        height: 200,
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
        marginBottom: 5
    }

});

export default ListCustome
/**
 *  <Image source={{ uri: 'https://i.imgur.com/yxovJ4S.jpg' }} resizeMode="stretch" style={{ flex: 1, width }} />
 * https://i.imgur.com/Xulubox.jpg
 * https://i.imgur.com/sduLRvf.jpg
 * https://i.imgur.com/yxovJ4S.jpg
 */