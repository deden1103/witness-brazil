import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions, Button } from 'react-native';
import Image from 'react-native-remote-svg';
const { width, height } = Dimensions.get('window');
const SCREEN_WIDTH = Dimensions.get('window').width;


class Slides extends Component {
    renderLastSlide(index) {
        if (index === this.props.data.length - 1) {
            return (
                <Button title="More"
                    raised
                    buttonStyle={styles.buttonStyle}
                    onPress={this.props.onComplete}
                />
            )
        }
    }

    renderSlides() {
        return this.props.data.map((slide, index) => {
            if (index !== this.props.data.length - 1) {
                return (
                    <View
                        key={index}
                        style={[styles.slide, { backgroundColor: slide.color }]}>
                        <View style={{ height: 200, width, padding: 5 }}>
                            <Image
                                source={{ uri: "https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg" }}
                                style={{ flex: 1 }}
                                resizeMode="stretch"
                            />
                        </View>
                       
    
                    </View>
                )
            }else{
                return (
                    <View
                        key={index}
                        style={[styles.slide, { backgroundColor: slide.color }]}>
                      
                        {this.renderLastSlide(index)}
    
                    </View>
                )
            }
           
        })
    }

    render() {
        return (
            <ScrollView
                horizontal
                style={{ flex: 1 }}
                pagingEnabled
            >
                {this.renderSlides()}
            </ScrollView>
        )
    }


}

const styles = {
    slideText: {
        fontSize: 30,
        color: 'white',
        textAlign: 'center'

    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: SCREEN_WIDTH
    },
    buttonStyle: {
        backgroundColor: '#0288D1',

    }
}
export default Slides;