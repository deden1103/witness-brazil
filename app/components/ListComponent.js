import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native';
import { Icon } from 'native-base';
import { NewCard, SmallBtn } from '../particles/SmallParticle';
const { width, height } = Dimensions.get('window');


class ListComponent extends Component {
    constructor(props) {
        super(props)

    }
    render() {
        return (
            <TouchableWithoutFeedback onPress={this.props.toForm}>
                <View style={[styles.cardThree, {}]}>
                    <View style={{ width: width * 0.9 }}><Text style={{ color: GLOBAL.mainColor,fontSize:18 }}>{this.props.nameList}</Text></View>
                    <View><Icon name="ios-arrow-forward" style={{ color: GLOBAL.mainColor }} /></View>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    cardThree: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20,
        paddingBottom: 20,
        marginTop: 5
    }

});

export default ListComponent