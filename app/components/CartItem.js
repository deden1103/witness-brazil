import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native';
import { Icon } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons'
import Image from 'react-native-remote-svg';
const { width, height } = Dimensions.get('window');


class CartItem extends Component {
    constructor(props) {
        super(props)

    }

    formatMoney = (num) => {
        var p = num.toFixed().split(".");
        return "" + p[0].split("").reverse().reduce(function (acc, num, i, orig) {
            return num == "-" ? acc : num + (i && !(i % 3) ? "." : "") + acc;
        }, "");
    }


    render() {
        return (
            <View style={[GLOBAL.applyShadow, styles.container]}>
                <View style={{ flex: 0.3, margin: 20 }}>
                    <TouchableWithoutFeedback>
                        <View style={{ height: 100, justifyContent: 'center', alignItems: 'center' }}>
                            <Image
                                source={{ uri: this.props.imagebrg }}
                                style={{ width: 80, height: 80 }}
                                resizeMode="stretch"
                            />
                        </View>
                    </TouchableWithoutFeedback>
                </View>

                <View style={{ flex: 0.7, padding: 20, justifyContent: 'space-around' }}>
                    <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                        <View style={{ flexWrap: 'wrap', width: width * 0.5 }}>
                            <Text style={{ color: 'black', fontSize: 20 }}>{this.props.namabrg}</Text>
                        </View>
                        <TouchableWithoutFeedback onPress={this.props.removeOne}>
                            <View>
                                <Icon type="FontAwesome" name="trash" style={{ color: 'red', fontSize: 20 }} />
                            </View>
                        </TouchableWithoutFeedback>

                    </View>

                    <View>
                        <View>
                            <Text style={{ color: 'grey', fontSize: 18 }}>Rp. {this.props.hargaAngg}</Text>
                        </View>
                    </View>

                    <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                        <View style={{ width: width * 0.2, justifyContent: 'space-between', flexDirection: 'row' }}>
                            <TouchableWithoutFeedback onPress={this.props.minQty}>
                                <View>
                                    <Text style={{ color: 'black', fontSize: 20 }}>--</Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <View>
                                <Text style={{ color: 'black', fontSize: 18 }}>{this.props.qty}</Text>
                            </View>
                            <TouchableWithoutFeedback onPress={this.props.addQty}>
                                <View>
                                    <Text style={{ color: 'black', fontSize: 20 }}>+</Text>
                                </View>
                            </TouchableWithoutFeedback>

                        </View>
                        <View>
                            {/* <Text>Show Details</Text> */}
                        </View>
                    </View>
                </View>


            </View >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 150,
        backgroundColor: 'white',
        flexDirection: 'row',
        margin: 4
    }
});

export default CartItem