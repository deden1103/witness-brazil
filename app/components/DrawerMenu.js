import React from 'react';
import { StyleSheet, Image, ImageBackground, Alert, TouchableOpacity, View, TouchableWithoutFeedback } from 'react-native';
import { DrawerActions } from 'react-navigation';
import { connect } from 'react-redux';
import {
    Content,
    Text,
    List,
    ListItem,
    Icon,
    Container,
    Left,
    Grid,
    Row
} from 'native-base';

import { logout, loginsuccess, loginsuccess2, loginsuccess3, loginsuccess4, loginsuccess5 } from '../rdx/actions';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import axios from 'axios';



class DrawerMenu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoggedIn: true
        };

        this.togle = this.props.navigation

    }


    handleLogout = async () => {

        Alert.alert(
            'Atenção!',
            'Tem a certeza de que vai sair da aplicação?',
            [
                { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' },
                { text: 'Yes', onPress: () => {
                    this.props.logout()
                    this.props.navigation.navigate('login')
                }}
            ]
        )

       
    }


    handleUploadImage = () => {
        const options = {
            title: 'Foto de no máximo 4MB ',
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            //console.log('Response = ', response);
            if (response.didCancel) {
                //console.log('User cancelled image picker');
            }
            else if (response.error) {
                //console.log('ImagePicker Error: ', response.error);
            } else if (response.fileSize > 1000000000000) {//1000000
                Alert.alert('arquivo de foto é muito grande')
            }
            else {
                console.log(response)
                this.uplod(response)
            }
        });
    }


    uplod = (datanya) => {
        // console.log(datanya)
        RNFetchBlob.fetch('POST', `${GLOBAL.apiUrl}user/updatepict`, {
            Accept: 'application/json',
            'Authorization': `Bearer ${this.props.token}`,
            'Content-Type': 'multipart/form-data',
        }, [
                {
                    name: 'pict',
                    filename: datanya.fileName,
                    type: datanya.type,
                    data: datanya.data
                }
            ]
        ).then((resp) => {
            let rspUpload = JSON.parse(resp.data);
            if (rspUpload.success == true) {
                // console.log(rspUpload)
                this.props.loginsuccess4(rspUpload.photoRelawan)
            } else {
                Alert.alert('Falha ao atualizar a foto')
            }
        }).catch((err) => {
            // console.log(JSON.parse(err))
            // Alert.alert('Kesalahan Server')
        })
    }


    handleLoginGoogle = async (email) => {
        try {
            this.setState({ loading: true })

            let data = JSON.stringify({
                email: email
            })
            let loginResponse = await axios.post(`${GLOBAL.apiUrl}api/authanggota/logingoogle`, data, {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            if (loginResponse.data.success == true) {
                this.setState({ loading: false })
                this.props.loginsuccess4(loginResponse.data.userPict)
                // console.log(loginResponse)
            } else {
                Alert.alert('Falha no upload. Tente novamente mais tarde')
                this.setState({ loading: false })
                // console.log(loginResponse)
            }


        } catch (e) {
            // console.log(e)
            Alert.alert('Falha no upload. Tente novamente mais tarde')
            this.setState({ loading: false })
        }
    }



    render() {
        const { profile, contentProfile, profileName } = styles;
        return (
            <Container style={styles.containerStyle}>
                <Grid>
                    <Row size={2} style={[contentProfile, { backgroundColor: GLOBAL.mainColor }]}>

                        <Image source={{ uri: this.props.photo }} style={profile} />
                        {this.props.relawanid > 0 && <TouchableWithoutFeedback onPress={this.handleUploadImage}>
                            <View style={[styles.profile2, { backgroundColor: 'green' }]}>
                                <Icon name="ios-camera" style={{ color: 'white' }} />
                            </View>
                        </TouchableWithoutFeedback>}


                        <View style={{ marginTop: 35, alignItems: 'center' }}>
                            <Text style={profileName}>{this.props.nama}</Text>
                            <Text style={{ color: 'white' }}>{this.props.email} </Text>
                        </View>


                    </Row>
                    <Row size={4} style={{ backgroundColor: '#fff' }}>
                        <Content bounces={false} style={{ flex: 1, top: -1 }}>
                            {/* <List dataArray={this.state.buttons}
                                renderRow={data =>
                                    <ListItem button noBorder onPress={() => {
                                        this.props.navigation.navigate(`${data.route}`),
                                            this.props.navigation.dispatch(DrawerActions.closeDrawer())
                                    }}>
                                        <Left>
                                            <Icon active name={data.icon} style={{ color: 'black', fontSize: 26, width: 30 }} />
                                            <Text style={styles.text}>{data.name}</Text>
                                        </Left>
                                    </ListItem>}>

                            </List> */}


                            {this.props.relawanid > 0 && <TouchableWithoutFeedback onPress={() => {
                                this.props.navigation.navigate(`profilescreen`),
                                    this.props.navigation.dispatch(DrawerActions.closeDrawer())
                            }}>
                                <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 20 }}>
                                    <View>
                                        <Icon active name="ios-person" style={{ color: 'black', fontSize: 26, width: 30 }} />
                                    </View>
                                    <View>
                                        <Text style={styles.text}>Perfil</Text>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>}


                            {
                            //     this.props.relawanid > 0 && <TouchableWithoutFeedback onPress={() => {
                            //     this.props.navigation.navigate(`home`),
                            //         this.props.navigation.dispatch(DrawerActions.closeDrawer())
                            // }}>
                            //     <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 20 }}>
                            //         <View>
                            //             <Icon active name="ios-home-outline" style={{ color: 'black', fontSize: 26, width: 30 }} />
                            //         </View>
                            //         <View>
                            //             <Text style={styles.text}>Home</Text>
                            //         </View>
                            //     </View>
                            // </TouchableWithoutFeedback>
                        }
                            {/* <View><Text>{this.props.isanggota}</Text></View> */}
                            {/* <TouchableWithoutFeedback onPress={() => {
                                this.props.navigation.navigate(`cart`),
                                    this.props.navigation.dispatch(DrawerActions.closeDrawer())
                            }}>
                                <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 20 }}>
                                    <View>
                                        <Icon active name="cart" style={{ color: 'black', fontSize: 26, width: 30 }} />
                                    </View>
                                    <View>
                                        <Text style={styles.text}>Cart</Text>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback> */}

                            {
                                // this.props.anggotaid > 0 && <TouchableWithoutFeedback onPress={() => {
                                //     this.props.navigation.navigate(`listalamat`),
                                //         this.props.navigation.dispatch(DrawerActions.closeDrawer())
                                // }}>
                                //     <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 20 }}>
                                //         <View>
                                //             <Icon active name="ios-card-outline" style={{ color: 'black', fontSize: 26, width: 30 }} />
                                //         </View>
                                //         <View>
                                //             <Text style={styles.text}>Alamat Saya</Text>
                                //         </View>
                                //     </View>
                                // </TouchableWithoutFeedback>
                            }

                            {
                                // this.props.anggotaid > 0 &&
                                // <TouchableWithoutFeedback onPress={() => {
                                //     this.props.navigation.navigate(`changepassword`),
                                //         this.props.navigation.dispatch(DrawerActions.closeDrawer())
                                // }}>
                                //     <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 20 }}>
                                //         <View>
                                //             <Icon active name="key" style={{ color: 'black', fontSize: 26, width: 30 }} />
                                //         </View>
                                //         <View>
                                //             <Text style={styles.text}>Ganti Password</Text>
                                //         </View>
                                //     </View>
                                // </TouchableWithoutFeedback>
                            }



                            {this.props.relawanid > 0 ? <TouchableWithoutFeedback onPress={this.handleLogout}>
                                <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 20 }}>
                                    <View>
                                        <Icon active name="ios-power" style={{ color: 'black', fontSize: 26, width: 30 }} />
                                    </View>
                                    <View>
                                        <Text style={styles.text}>Sair (da sessão)</Text>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback> : <TouchableWithoutFeedback onPress={() => {
                                this.props.navigation.navigate(`login`),
                                    this.props.navigation.dispatch(DrawerActions.closeDrawer())
                            }}>
                                    <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 20 }}>
                                        <View>

                                        </View>
                                        <View>
                                            <Text style={styles.text}>Iniciar sessão (Entrar)</Text>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>}



                            {/* <TouchableWithoutFeedback onPress={this.handleLogout}>
                                <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 20 }}>
                                    <View>
                                        <Icon active name="power" style={{ color: 'black', fontSize: 26, width: 30 }} />
                                    </View>
                                    <View>
                                        <Text style={styles.text}>Sair (da sessão)</Text>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback> */}


                        </Content>
                    </Row>
                </Grid>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    profile: {
        width: 120,
        height: 120,
        borderRadius: 60,
       // marginBottom: 30,
        marginVertical: 30,

    },

    contentProfile: {
        flexDirection: 'column',
        // backgroundColor: '#1a40e2',
        justifyContent: 'center',
        alignItems: 'center',
        //borderBottomWidth: 6,
        //borderBottomColor:'red'
    },

    profileName: {
        color: 'white',
        fontSize: 20,
        marginTop: -30
    },
    containerStyle: {
        //borderBottomWidth: 3,
        //borderBottomColor:'red'
    },
    profile2: {
        width: 33,
        height: 33,
        borderRadius: 50,
        alignSelf: 'center',
        marginTop: -70,
        marginRight: -80,
        justifyContent: 'center',
        alignItems: 'center'

    }

});
const mapStateToProps = (state) => {
    const { nama, email, photo, anggotaid, token, isanggota, relawanid } = state.anggota;
    return { nama, email, photo, anggotaid, token, isanggota, relawanid };
};

export default connect(mapStateToProps, { logout, loginsuccess, loginsuccess2, loginsuccess3, loginsuccess4, loginsuccess5 })(DrawerMenu);