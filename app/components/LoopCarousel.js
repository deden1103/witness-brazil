import React, { Component } from 'react';
import { Text, View, Dimensions } from 'react-native';
import Carousel from 'react-native-looped-carousel';
import Image from 'react-native-remote-svg';
const { width, height } = Dimensions.get('window');

export default class LoopCarousel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            size: { width, height },
        };
    }

    _onLayoutDidChange = e => {
        const layout = e.nativeEvent.layout;
        this.setState({ size: { width: layout.width, height: layout.height } });
    };

    render() {
        return (
            <View style={{ flex: 1 }} onLayout={this._onLayoutDidChange}>
                <Carousel
                    delay={5000}
                    bullets
                    style={this.state.size}
                    autoplay
                    onAnimateNextPage={p => console.log(p)}>
                    {this.props.arrimg.map((res, i) => (
                        <View key={i} style={[{}, this.state.size]}>
                            <Image
                                source={{ uri: res.image }}
                                style={{ flex: 1 }}
                                resizeMode="stretch"
                            />
                        </View>
                    ))}
                    {/* <View style={[{}, this.state.size]}>
                        <Image
                            source={{ uri: "https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg" }}
                            style={{ flex: 1 }}
                            resizeMode="stretch"
                        />
                    </View>
                    <View style={[{}, this.state.size]}>
                        <Image
                            source={{ uri: "https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg" }}
                            style={{ flex: 1 }}
                            resizeMode="stretch"
                        />
                    </View>
                    <View style={[{}, this.state.size]}>
                        <Image
                            source={{ uri: "https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg" }}
                            style={{ flex: 1 }}
                            resizeMode="stretch"
                        />
                    </View> */}
                </Carousel>
            </View>
        );
    }
}