import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableWithoutFeedback, ImageBackground
} from 'react-native';
import { Icon } from 'native-base';
import Image from 'react-native-remote-svg';
import { NewCard, SmallBtn } from '../particles/SmallParticle';
const { width, height } = Dimensions.get('window');


class CommentComponent extends Component {
    constructor(props) {
        super(props)

    }
    render() {
        return this.props.datanya.map((val, i) => {
            return (
                <View key={i} style={{ marginTop: 20, minHeight: 80, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                    <View style={{ width: 55, height: 55, borderRadius: 60, alignSelf: 'flex-start' }}>
                        <View>
                            <Image source={{ uri: val.relawan.photoRelawan }} style={styles.profile} />
                        </View>
                    </View>
                    <View>
                        <View style={{ minHeight: 60, backgroundColor: '#2b8fef', width: width * 0.8, borderRadius: 15, justifyContent: 'center', padding: 5 }}>
                            <Text style={{ color: 'white' }}> {val.relawan.namaRelawan} :  {val.commentDesc}</Text>

                            <Text style={{ alignSelf: 'flex-end', color: 'white' }}>{val.createDate}</Text>
                        </View>

                        {this.renderReplay(val.replays)}
                        {this.state.isShowReplay == val.commentId && <View style={{ marginTop: 5, marginBottom: 5, minHeight: 50, width: width * 0.8, borderRadius: 10, justifyContent: 'center', padding: 5 }}>
                            <InputWhiteBgStyle
                                valueInput={this.state.rep}
                                onChangeInput={(rep) => this.setState({ rep })}
                                pHolder="balas komentar..."
                                style={{ backgroundColor: 'transparent', borderColor: 'grey', borderWidth: 1, borderRadius: 30, height: 45 }} />
                        </View>}

                        <TouchableOpacity onPress={() => this.setState({ isShowReplay: val.commentId })}>
                            <View style={{ marginLeft: 20, paddingBottom: 10, marginTop: 5 }}>
                                <Text style={{ alignSelf: 'flex-end', marginRight: 10, fontSize: 18, color: 'black' }}>Balas</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        })
    }
}

const styles = StyleSheet.create({


});

export default CommentComponent
