import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback, Alert } from 'react-native';
import { Icon } from 'native-base';
import { connect } from 'react-redux';
import { emptycart } from '../rdx/actions';


class Tabiconnotif extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {

    }


    render() {

        return (
            <TouchableWithoutFeedback onPress={() => console.log('noo')}>
                <View style={{ flexDirection: 'row', width: 180, justifyContent: 'center' }}>

                    <Icon name={`ios-notifications-outline`} color={this.props.color} style={{ color: this.props.warna, paddingRight: 5 }} />
                    {this.props.commentcount > 0 && <View style={{ height: 14, width: 14, marginLeft: -16, marginTop: 2, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center', borderRadius: 30 }}>
                        <Text style={{ fontSize: 10, color: 'white' }}>{this.props.commentcount}</Text>
                    </View>}


                </View>
            </TouchableWithoutFeedback>
        );
    }
}




const mapStateToProps = (state) => {
    const { commentcount } = state.cartpersist;
    const { komen } = state.anggota;
    return { commentcount, komen };
};

export default connect(mapStateToProps, { emptycart })(Tabiconnotif);
