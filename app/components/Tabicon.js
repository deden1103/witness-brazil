import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import { Icon } from 'native-base';
import { connect } from 'react-redux';
import { emptycart } from '../rdx/actions';


class Tabicon extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {

    }


    render() {

        return (
            <TouchableWithoutFeedback onPress={() => console.log('noo')}>
                <View style={{ flexDirection: 'row', width: 180, justifyContent: 'center' }}>

                    <Icon name={`ios-megaphone-outline`} color={this.props.color} style={{ color: this.props.warna, paddingRight: 5 }} />
                    {this.props.broadcastcount > 0 && <View style={{ height: 14, width: 14, marginLeft: -16, marginTop: 2, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center', borderRadius: 30 }}>
                        <Text style={{ fontSize: 10, color: 'white' }}>{this.props.broadcastcount}</Text>
                    </View>}

                    {
                        // this.props.cart.length > 0 && <View style={{ height: 19, width: 19, marginLeft: -10, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center', borderRadius: 30 }}>
                        //     <Text style={{ fontSize: 14, color: 'white' }}>{this.props.cart.length}</Text>
                        // </View>
                    }
                </View>
            </TouchableWithoutFeedback>
        );
    }
}




const mapStateToProps = (state) => {
    const { broadcastcount } = state.cartpersist;

    return { broadcastcount };
};

export default connect(mapStateToProps, { emptycart })(Tabicon);
