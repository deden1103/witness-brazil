import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import {withNavigation} from 'react-navigation';
import {Thumbnail} from 'native-base';
import {connect} from 'react-redux';

class HeaderTwiter extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          this.props.navigation.openDrawer();
          // this.props.navigation.closeDrawer();
        }}>
        <View style={{flexDirection: 'row'}}>
          <Thumbnail
            small
            source={{uri: this.props.photo}}
            style={{marginLeft: 20}}
          />
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 18,
              color: 'black',
              marginLeft: 15,
              marginTop: 4,
            }}>
            {this.props.nama}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({});

const mapStateToProps = state => {
  const {photo, nama} = state.anggota;
  return {photo, nama};
};

export default connect(
  mapStateToProps,
  {},
)(withNavigation(HeaderTwiter));
