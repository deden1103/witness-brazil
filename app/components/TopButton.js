import React, { Component } from 'react';
import {
    View, Text, TouchableWithoutFeedback, StyleSheet,
    TouchableOpacity, Dimensions, ImageBackground
} from 'react-native';
import { Icon } from 'native-base';
import { connect } from 'react-redux';
import { emptycart } from '../rdx/actions';
import { ratio } from '../global/styles';
const { width, height } = Dimensions.get('window')
class TopButton extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {

    }


    render() {

        return (
            <View style={{
                flex: 1,
                justifyContent: 'space-around'
            }}>


             <View style={[GLOBAL.applyShadow, {
                    margin: 10,
                    // padding: 5,
                    // backgroundColor: 'red'
                    borderColor: '#D1D1D1',
                    borderWidth: 0.5
                }]}>
                    <TouchableOpacity onPress={this.props.openVideo}>
                        <ImageBackground source={require('../assets/btnvideo.jpeg')}
                            style={{
                                width: '100%',
                                height: 100,
                                // marginTop: 10,
                            }}>
                            <View style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                // backgroundColor: 'rgba(0,0,0, 0.2)',
                                padding: 5
                            }}>

                                <View style={[styles.fabitem, {}]}>

                                </View>
                            </View>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>


                <View style={[GLOBAL.applyShadow, {
                    margin: 10,
                    // padding: 5,
                    // backgroundColor: 'red'
                    borderColor: '#D1D1D1',
                    borderWidth: 0.5
                }]}>
                    <TouchableOpacity onPress={this.props.openImage}>
                        <ImageBackground source={require('../assets/btnphoto.jpeg')}
                            style={{
                                width: '100%',
                                height: 100,
                                // marginTop: 10,
                            }}>
                            <View style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                // backgroundColor: 'rgba(0,0,0, 0.2)',
                                padding: 5
                            }}>

                                <View style={[styles.fabitem, {}]}>

                                </View>

                            </View>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>

               



                <View style={[GLOBAL.applyShadow, {
                    margin: 10,
                    // padding: 5,
                    // backgroundColor: 'red'
                    borderColor: '#D1D1D1',
                    borderWidth: 0.5
                }]}>
                 <TouchableOpacity onPress={this.props.openText}>
                    <ImageBackground source={require('../assets/btntext.jpeg')}
                        style={{
                            width: '100%',
                            height: 100,
                            // marginTop: 10,
                        }}>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            // backgroundColor: 'rgba(0,0,0, 0.2)',
                            padding: 5
                        }}>
                           
                                <View style={[styles.fabitem, {}]}>

                                </View>
                            
                        </View>
                    </ImageBackground>
                    </TouchableOpacity>
                </View>


                                <View style={[GLOBAL.applyShadow, {
                    margin: 10,
                    // padding: 5,
                    // backgroundColor: 'red'
                    borderColor: '#D1D1D1',
                    borderWidth: 0.5
                }]}>
                <TouchableOpacity onPress={this.props.openVoice}>
                    <ImageBackground source={require('../assets/btnaudio.jpeg')}
                        style={{
                            width: '100%',
                            height: 100,
                            // marginTop: 10,
                        }}>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            // backgroundColor: 'rgba(0,0,0, 0.2)',
                            padding: 5
                        }}>
                            
                                <View style={[styles.fabitem, {}]}>

                                </View>
                           
                        </View>
                    </ImageBackground>
                    </TouchableOpacity>
                </View>

                {/* <View style={{ flexDirection: 'column', justifyContent: 'space-around' }}> */}


                {/* <View style={{
                        justifyContent: 'center', alignItems: 'center', marginTop: 20,
                        backgroundColor: 'rgba(89,180,255, 0.5)',
                        padding: 5
                    }}>
                        <TouchableOpacity onPress={this.props.openImage}>
                            <View style={[styles.fabitem, {}]}>
                                <Icon name="image" style={{
                                    // color: '#A6A6A4',
                                    color: 'white',
                                    fontSize: 45
                                }} />
                                <Text style={{
                                    // color: '#A6A6A4'
                                    color: 'white',
                                }}>Buat Laporan Photo</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                        <TouchableOpacity onPress={this.props.openVideo}>
                            <View style={[styles.fabitem, {}]}><Icon name="videocam" style={{ color: '#A6A6A4', fontSize: 45 }} />
                                <Text style={{ color: '#A6A6A4' }}>Buat Laporan Video</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                        <TouchableOpacity onPress={this.props.openVoice}>
                            <View style={[styles.fabitem, {}]}><Icon name="mic" style={{ color: '#A6A6A4', fontSize: 45 }} />
                                <Text style={{ color: '#A6A6A4' }}>Buat Laporan Suara</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                        <TouchableOpacity onPress={this.props.openText}>
                            <View style={[styles.fabitem, {}]}><Icon name="create" style={{ color: '#A6A6A4', fontSize: 45 }} />
                                <Text style={{ color: '#A6A6A4' }}>Buat Laporan Teks</Text>
                            </View>
                        </TouchableOpacity>
                    </View> */}

                {/* </View> */}
            </View>
        );
    }
}


const styles = StyleSheet.create({
    fabitem: {
        height: 100,
        width: width * 0.95,
        // borderColor: 'white',
        // borderWidth: 1,
        // borderRadius: 200,
        // position: 'absolute',
        // bottom: 20,
        // left: 20,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#3498db',
    }
})

const mapStateToProps = (state) => {
    const { broadcastcount } = state.cartpersist;

    return { broadcastcount };
};

export default connect(mapStateToProps, { emptycart })(TopButton);
