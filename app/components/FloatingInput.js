import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions, Button, TouchableWithoutFeedback, Alert } from 'react-native';
import { Icon } from 'native-base';
import Image from 'react-native-remote-svg';
import { InputWhiteBgIcon } from '../particles/SmallParticle';
const { width, height } = Dimensions.get('window');


class FloatingInput extends Component {

    render() {
        return (
            <View style={[styles.mainStyle, { backgroundColor: GLOBAL.mainColor }]}>
                <View style={{
                    alignSelf: 'center', flexDirection: 'row',
                    // borderBottomLeftRadius: 6,
                    // borderBottomRightRadius: 6,
                    justifyContent: 'space-around'
                }}>
                    <View style={{ flex: 0.8 }}>
                        <InputWhiteBgIcon
                            valueInput={this.props.value}
                            onChangeInput={this.props.onChange} pHolder={this.props.holder} />
                    </View>
                    <View style={{ flex: 0.2 }}>
                        <TouchableWithoutFeedback onPress={this.props.toSend}>
                            <Icon type="FontAwesome" name="paper-plane" style={{ marginTop: 5, color: 'white', alignSelf: 'center' }} />
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </View>
        )
    }

}

const styles = {
    mainStyle: {
        height: 50,
        flex: 1,
        //backgroundColor: '#F2F3F5', width: width * 0.958, 
        //backgroundColor: 'grey',
        //alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
        flexDirection: 'row',
        // borderBottomLeftRadius: 6,
        // borderBottomRightRadius: 6
    },
    leftStyle: {
        flex: 0.5,
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30,
        borderColor: '#CCCCCC',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rightStyle: {
        flex: 0.5,
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30,
        borderColor: '#CCCCCC',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'

    }
}
export default FloatingInput;
