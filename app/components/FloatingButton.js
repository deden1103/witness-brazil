import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions, Button, TouchableWithoutFeedback } from 'react-native';
import { Icon } from 'native-base';
import Image from 'react-native-remote-svg';
import { View as Vw } from 'react-native-animatable';
const { width, height } = Dimensions.get('window');


class FloatingButton extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Vw
                animation={this.props.tipe}
               
                duration={500}
                ref={(ref) => { this.animationView = ref; }}
                style={styles.mainStyle}>
                <TouchableWithoutFeedback onPress={this.props.openSort}>
                    <View style={styles.leftStyle}>
                        <Image
                            source={require('../assets/sort.png')}
                            style={{ flex: 1 }}
                            resizeMode="contain"
                        />

                    </View>
                </TouchableWithoutFeedback>

                <TouchableWithoutFeedback onPress={this.props.openFilter}>
                    <View style={styles.rightStyle}>
                        <Image
                            source={require('../assets/filter.png')}
                            style={{ flex: 1 }}
                            resizeMode="contain"
                        />
                    </View>
                </TouchableWithoutFeedback>
            </Vw>
        )
    }

}

const styles = {
    mainStyle: {
        height: 50,
        width: width * 0.6,
        backgroundColor: 'white',
        alignSelf: 'center',
        borderRadius: 30,
        position: 'absolute',
        bottom: 50,
        flexDirection: 'row'
    },
    leftStyle: {
        flex: 0.5,
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30,
        borderColor: '#CCCCCC',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rightStyle: {
        flex: 0.5,
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30,
        borderColor: '#CCCCCC',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'

    }
}
export default FloatingButton;
