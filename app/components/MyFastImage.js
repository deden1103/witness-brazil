
import React, { Component } from 'react';
import FastImage from 'react-native-fast-image'
import {
    Text,
    View,
    StatusBar,
    Alert,
    TouchableWithoutFeedback,
    ActivityIndicator, AppState, Image, TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';

class MyFastImage extends Component {


    constructor() {
        super()
        this.state = {
            progress: 0,
            isOpenImage: false
        }
    }

    handleOpenImage = () => {
        this.setState({ isOpenImage: true })
    }
    // shouldComponentUpdate(np, ns) {
    //     if (this.state.progress !== ns.progress) {
    //         return true
    //     }
    // }
    // console.log(
    //     'Loading Progress ' +
    //     e.nativeEvent.loaded / e.nativeEvent.total
    // )
    render() {
        return (
            <View>
                {
                    (this.state.progress === 100 || this.state.progress == '100') ? null
                        : <View style={{
                            height: 20,
                            // flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            // backgroundColor: 'red',
                            position: 'absolute',
                            // top: 140,
                            // left: 50
                        }}>
                            <Text style={{
                                fontSize: 20,
                                color: this.state.isOpenImage ? 'gray' : 'white'
                            }}>Loading photo... {this.state.progress} %</Text>
                        </View>
                }
                {
                    this.state.isOpenImage == true &&
                    <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('imageviewer', { imglempar: this.props.urlPict })}>
                        <FastImage
                            onProgress={e => {
                                this.setState({ progress: Math.ceil(e.nativeEvent.loaded / e.nativeEvent.total * 100) })
                            }}
                            style={this.props.style}
                            source={{
                                uri: this.props.urlPict,
                                cache: FastImage.cacheControl.web,
                                priority: FastImage.priority.normal,
                            }}
                            resizeMode={FastImage.resizeMode.contain}
                        />
                    </TouchableWithoutFeedback>
                }



                {
                    this.state.isOpenImage == false &&
                    <View style={{
                        height: 180,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0, 0, 0, 0.6)'
                    }}>
                        <Image
                            resizeMode="contain"
                            style={{ height: 100, width: 300, position: 'absolute', opacity: 0.2 }}
                            source={require('../assets/witness-logo.png')}
                        />
                        <TouchableOpacity onPress={this.handleOpenImage}>
                            <Text style={{ color: 'white', fontSize: 20 }}>Tampilkan Photo</Text>
                        </TouchableOpacity>
                    </View>
                }


            </View>
        );
    }
}

export default withNavigation(MyFastImage)
