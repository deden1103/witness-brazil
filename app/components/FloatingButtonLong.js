import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions, Button, TouchableWithoutFeedback, Alert } from 'react-native';
import { Icon } from 'native-base';
import Image from 'react-native-remote-svg';
const { width, height } = Dimensions.get('window');


class FloatingButtonLong extends Component {

    render() {
        return (
            <View style={styles.mainStyle}>
                <View style={{ width: width * 0.97, alignSelf: 'center' }}>
                    <TouchableWithoutFeedback onPress={this.props.gotoCheckout}>
                        <View style={{ backgroundColor: GLOBAL.mainColor, height: 60, justifyContent: 'center', alignItems: 'center' }} onPress={() => Alert.alert('tik')}>
                            <Text style={{ fontSize: 20, color: 'white' }}>Total Rp. {this.props.nominal} CHECKOUT</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        )
    }

}

const styles = {
    mainStyle: {
        height: 50,
        backgroundColor: 'black',
        alignSelf: 'center',
        borderRadius: 30,
        position: 'absolute',
        bottom: 5,
        flexDirection: 'row'
    },
    leftStyle: {
        flex: 0.5,
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30,
        borderColor: '#CCCCCC',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rightStyle: {
        flex: 0.5,
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30,
        borderColor: '#CCCCCC',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'

    }
}
export default FloatingButtonLong;
