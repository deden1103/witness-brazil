import React from "react";
import { Modal, Dimensions, View, TouchableOpacity, TouchableWithoutFeedback, StyleSheet, Text } from "react-native";
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
// import {
//     login_success_nama,
//     login_success_email,
//     login_success_userid,
//     login_success_photo,
//     login_success_token,
//     login_success_total_donasi
// } from '../rdx/actions';
const { width, height } = Dimensions.get('window')



class MyModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false

        };
    }

    componentDidMount() {

    }

    shouldComponentUpdate(np,ns){
        return true
    }


    handleCloseModal = () => {
        this.setState({ modalVisible: !this.state.modalVisible })
    }

    render() {
        return (
            <Modal
                closeOnClick={true}
                animationType="slide"
                visible={this.props.visible}
                transparent
               // animationType="fade"
                onRequestClose={() => {
                    this.handleCloseModal()
                }}>
                {/* <TouchableWithoutFeedback onPress={this.props.outClick}> */}
                <View style={styles.containerStyle}>

                    <View style={{
                        flex: 1,
                        backgroundColor: 'white',
                        borderTopLeftRadius:20,
                        borderTopRightRadius:20,
                        marginTop:30,
                        // borderColor:'red',
                        // borderWidth:1
                    }}>
                        {/* <TouchableOpacity onPress={this.props.outClick}>
                            <Text style={{ fontSize: 20, marginLeft: 20, marginTop: 20 }}>Tutup</Text>
                        </TouchableOpacity> */}

                        <Button transparent style={{ width:50}} onPress={this.props.outClick}>
                            <Icon name="close" style={{ color: "black", fontSize: 32 }} />
                        </Button>

                        {this.props.children}
                    </View>
                </View>
                {/* </TouchableWithoutFeedback> */}
            </Modal>


        );
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
        position: 'relative',
        flex: 1,
        justifyContent: 'center',
    }
});

const mapStateToProps = (state) => {
    const { count } = state.auth;
    const { nama, email, token, userid, photo } = state.anggota;
    return { count, nama, email, token, userid, photo };
};

export default connect(mapStateToProps, null)(MyModal);