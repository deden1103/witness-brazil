
class StyleGlobal {
    loadStyle() {
        GLOBAL.mainColor = '#2498e8';
        GLOBAL.blueColor = '#4267b2';
        GLOBAL.dedede = '#DEDEDE';
        GLOBAL.buttonBlue = '#0028FF';

        GLOBAL.greyBg = '#F4F4F4';
        GLOBAL.greyBg2 = '#CCCCCC';
        GLOBAL.greyBg3 = '#EFEEF3';
        GLOBAL.borderColor = '#F3F3F3';
        GLOBAL.blueLight = '#77A7F7';
        GLOBAL.yellowOld = '#FFCD43';
        GLOBAL.textGrey = "#4D4D4D";
        GLOBAL.applyShadow = {
            shadowColor: '#000000',
            shadowOffset: {
                width: 0,
                height: 3
            },
            shadowRadius: 5,
            shadowOpacity: 1.0,
            elevation: 5
        }
    }
    // loadBgLg() {
    //     const ran = Math.floor(Math.random() * (9 - 1 + 1) + 1)
    //     if (ran <= 3) {
    //         GLOBAL.bgLg = require('../assets/bglogin2.jpg')
    //     } else if (ran > 6) {
    //         GLOBAL.bgLg = require('../assets/bglogin3.jpg')
    //     } else {
    //         GLOBAL.bgLg = require('../assets/bglogin.jpg')
    //     }
    // }

}
export default new StyleGlobal()