export const language = {
    indo:{
        login:'masuk',
        erorLoginInfo:'Login falhou, e-mail / palavras-passe erradas',
        forgotPassword:'Lupa Password ?',
        noHaveAccount:'Tidak punya akun ?',
        isHaveAccount:'Sudah punya akun ?',
        register:'DAFTAR',
        fullname:'Nome completo',
        phone:'Nomor Telpon',
        email:'Email',
        password:'Password',
        conPassword:'Konfirm Password',
        homeTitle:'Riwayat Berobat',
        polisaraf:'Poli Saraf',
        cancelText:'Batal',
        doneText:'Selesai',
        queueNo:'No Antrian',
        detail:'Detail',
        detailTicket:'Lihat Tiket',
        detailTitle:'Detail Tiket',
        finishText:'Selesai',
        onProccess:'Dalam Proses',
        comeTo:'Datang ke Klinik',
        registerSuccess:'Pendaftaran Anda Berhasil',
        registerInfoDesc:'Pastikan Anda siap pada jadwal yang Anda tentukan',
        pictCare:'Foto Perawatan',
        doCancel:'BATALKAN',
        myAccount:'My Profile',
        nameLabel:'nama',
        medicalLabel:'No Rekam Medis',
        phoneLabel:'No Telpon',
        emailLabel:'Email',
        birthLabel:'Tanggal Lahir',
        bloodLabel:'Gol Darah',
        bpjsNumber:'Nomor BPJS',
        editAccountTitle:'Ubah Informasi Akun',
        editPasswordTitle:'Ubah Password',
        saveBtn:'Save',
        bookDataTitle:'Buku Data Diri Pasien',
        editPasswordLabel:'Masukan Password Lama',
        editPasswordNewLabel:'Masukan Password Baru',
        confirmPasswordLabel:'Konfirmasi Password Baru',
        btnSendText:'Kirim'

    },
    eng:{
        login:'login',
        erorLoginInfo:'Login Failed Username / Password are wrong',
        forgotPassword:'Forgot Password ?',
        noHaveAccount:'Dont have account ?',
        isHaveAccount:'Already Have Account ?',
        register:'REGISTER',
        lname:'Fullname',
        phone:'Phone',
        email:'Email',
        password:'Password',
        conPassword:'Confirm Password',
        homeTitle:'Medical History',
        polisaraf:'Poli Saraf',
        cancelText:'Batal',
        doneText:'Done',
        queueNo:'Queue Number',
        detail:'Detail',
        detailTicket:'Lihat Tiket',
        detailTitle:'Ticket Detail',
        finishText:'Finish',
        onProccess:'On Proccess',
        comeTo:'Come to clinic',
        registerSuccess:'Register Successfull',
        registerInfoDesc:'Make sure you ready for schedule you made up',
        pictCare:'Medical Result',
        doCancel:'CANCEL',
        myAccount:'My Account',
        nameLabel:'name',
        medicalLabel:'Medical Number',
        phoneLabel:'Phone',
        emailLabel:'Email',
        birthLabel:'Birth Date',
        bloodLabel:'Blood Type',
        bpjsNumber:'BPJS Number',
        editAccountTitle:'Change Account Information',
        editPasswordTitle:'Ubah Password',
        saveBtn:'Save',
        bookDataTitle:'Buku Data Diri Anda',
        editPasswordLabel:'Masukan Password Lama',
        editPasswordNewLabel:'Masukan Password Baru',
        confirmPasswordLabel:'Konfirmasi Password',
        btnSendText:'Send'

    }
}


class myLanguage{
    loadLang(){
        GLOBAL.myLang = language.eng
    }

    loadLangIndo(){
        GLOBAL.myLang = language.indo
    }
}
export default new myLanguage()