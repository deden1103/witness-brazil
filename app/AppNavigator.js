import React, {Component} from 'react';
import {Icon} from 'native-base';
import {
  createStackNavigator,
  createSwitchNavigator,
  createMaterialTopTabNavigator,
  createDrawerNavigator,
} from 'react-navigation';
import Splash from './screens/Splash';
import Loginscreen from './screens/Loginscreen';
import Otp from './screens/Otp';
import Register from './screens/Register';
import Homescreennolike from './screens/Homescreennolike';
import Draftscreen from './screens/Draftscreen';
import Recordvideo from './screens/Recordvideo';
import RecordvideoEditOnline from './screens/RecordVideoEditOnline';
import Eventdetail from './screens/Eventdetail';
import Chatscreen from './screens/Chatscreen';
import Profilescreen from './screens/Profilescreen';
import Profile from './screens/Profile';
import Changepassword from './screens/Changepassword';
import Forgotpassword from './screens/Forgotpassword';
import Notificationeposko from './screens/Notificationeposko';
import Imageviewer from './screens/Imageviewer';
import DrawerMenu from './components/DrawerMenu';
import HeaderTwiter from './components/HeaderTwiter';
import Tabiconnotif from '../app/components/Tabiconnotif';

const MyTab = createMaterialTopTabNavigator(
  {
    a: {screen: Homescreennolike},
    b: {screen: Draftscreen},
    d: {screen: Notificationeposko},
  },
  {
    initialRouteName: 'b',
    tabBarOptions: {
      labelStyle: {
        fontSize: 12,
      },
      style: {
        backgroundColor: 'white',
      },
      showIcon: true,
      showLabel: false,
      indicatorStyle: {backgroundColor: 'red', height: 4},
    },
    navigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, tintColor}) => {
        const {routeName} = navigation.state;
        isColor = focused ? 'red' : 'grey';
        if (routeName === 'a') {
          iconName = `ios-home`;
          isColor = focused ? 'red' : 'grey';
          return (
            <Icon name={iconName} color={tintColor} style={{color: isColor}} />
          );
        } else if (routeName === 'b') {
          iconName = `ios-filing`;
          isColor = focused ? 'red' : 'grey';
          return (
            <Icon name={iconName} color={tintColor} style={{color: isColor}} />
          );
        } else if (routeName === 'd') {
          iconName = `ios-notifications-outline`;
          isColor = focused ? 'red' : 'grey';
          return <Tabiconnotif color={tintColor} warna={isColor} />;
        }
      },
    }),
  },
);

const stt = createStackNavigator({
  e: {
    screen: MyTab,
    navigationOptions: ({navigation}) => ({
      headerStyle: {backgroundColor: '#fff', elevation: 0},
      headerLeft: <HeaderTwiter />,
    }),
  },
});

const AppNavigator = createStackNavigator(
  {
    login: {screen: Loginscreen},
    register: {screen: Register},
    home: {screen: stt},
    chat: {screen: Chatscreen},
    recordvideo: {screen: Recordvideo},
    recordvideoTwo: {screen: RecordvideoEditOnline},
    eventdetail: {screen: Eventdetail},
    profile: {screen: Profile},
    profilescreen: {screen: Profilescreen},
    forgotpassword: {screen: Forgotpassword},
    changepassword: {screen: Changepassword},
    splash: {screen: Splash},
    imageviewer: {screen: Imageviewer},
  },
  {
    initialRouteName: 'home',
    headerMode: 'none',

    transitionConfig: () => ({
      transitionSpec: {
        duration: 0,
      },
    }),
  },
);

const MyDrawer = createDrawerNavigator(
  {
    Home: {
      screen: AppNavigator,
    },
  },
  {
    drawerWidth: 270,
    drawerPosition: 'left',
    contentComponent: (props) => <DrawerMenu {...props} />,
  },
);

const Sw = createSwitchNavigator(
  {
    sp: Splash,
    lo: Loginscreen,
    otp: Otp,
    app: MyDrawer,
  },
  {
    initialRouteName: 'sp',
  },
);

export default Sw;
