import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Dimensions,
    StatusBar, Alert, View
} from 'react-native';
import {
    Container,
    Content,
    Form,
    Item,
    Label,
    Input,
    Button,
    Icon, Text,
    Footer,
    Header, Right, Left, Body
} from 'native-base';
import axios from 'axios';
const { width, height } = Dimensions.get('window');


class Forgotpassword extends Component {
    constructor(props) {
        super(props)
        this.state = {
            active: false,
            email: ''
        }

    }


    handleSubmit = async () => {
        try {
            let data = JSON.stringify({
                email: this.state.email
            })
            let response = await axios.post(`${GLOBAL.apiUrl}authrelawan/sendemail`, data, {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            if (response.data.success == true) {
                // console.log(response)
                Alert.alert('email untuk reset password Berhasil dikirim')
            } else {
                Alert.alert('gagal')
                // console.log(response)
            }
        } catch (e) {
            // console.log(e)
            Alert.alert('erro de servidor')
        }
    }


    render() {
        return (
            <Container>
                {/* <StatusBar
                    translucent
                    backgroundColor={GLOBAL.mainColor}
                    animated
                /> */}
                {/* <View style={{ marginTop: 5, backgroundColor: GLOBAL.mainColor }} /> */}
                <Header noShadow style={{ backgroundColor: "white" }}>
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="md-arrow-back" style={{ color: GLOBAL.mainColor }} />
                        </Button>
                    </Left>
                    <Body style={{ flex: 1 }}>
                        {/* <Icon
                            name="logo-twitter"
                            style={{ alignSelf: "center", color: "#4286f4" }}
                        /> */}
                    </Body>
                    <Right style={{ flex: 1 }}>
                        {/* <Button transparent>
                            <Text style={{ color: "#4286f4" }}>Sign up</Text>
                        </Button>
                        <Button transparent>
                            <Icon name="more" style={{ color: "#4286f4" }} />
                        </Button> */}
                    </Right>
                </Header>
                <Content>

                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
                        <View><Text style={{ fontSize: 20, color: GLOBAL.textGrey }}>Lupa Password ?</Text></View>
                        <View><Text style={{ color: GLOBAL.textGrey }}>Jangan Panik silahkan lengkapi</Text></View>
                        <View><Text style={{ color: GLOBAL.textGrey }}>form dibawah ini untuk reset password Anda</Text></View>
                    </View>

                    <View style={{ width: width * 0.9 }}>
                        <Form>
                            <Item inlineLabel>
                                <Label>Correio eletrónico</Label>
                                <Input value={this.state.email} onChangeText={(email) => this.setState({ email })} />
                            </Item>

                        </Form>

                    </View>

                    {/* <Button block style={{ margin: 15, marginTop: 50, backgroundColor: GLOBAL.mainColor }} onPress={this.handleSubmit}>
                        <Text style={{ color: 'white' }}>Send</Text>
                    </Button> */}

                </Content>

                <Footer style={styles.footer}>
                    <Button
                        onPress={this.handleSubmit}
                        rounded
                        style={{ backgroundColor: "#4286f4", marginLeft: 20 }}
                    >
                        {this.state.loading ? <Spinner color="white" /> : <Text>Enviar</Text>}
                    </Button>
                </Footer>


            </Container>
        );
    }
}

const styles = StyleSheet.create({
    footer: {
        backgroundColor: "white",
        elevation: 3,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0.2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        height: 60,
        padding: 5,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-end"
    }

});

export default Forgotpassword

