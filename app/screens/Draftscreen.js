import React, {Component} from 'react';
import Modal from 'react-native-modalbox';
import Dimensions from 'Dimensions';
// import Notification from 'react-native-android-local-notification';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Platform,
  Alert,
  ScrollView,
  TouchableOpacity,
  StatusBar,
  Image,
  Picker,
  Linking,
  LayoutAnimation,
  ActivityIndicator,
  BackHandler,
  ToastAndroid,
  RefreshControl,
  AppState,
  TextInput,
  Switch,
  PermissionsAndroid,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import {
  Container,
  Header,
  Body,
  Content,
  Left,
  Title,
  Thumbnail,
  Col,
  Row,
  Grid,
  Icon,
  Textarea,
  Fab,
  Button,
  Footer,
  Input,
  Right,
} from 'native-base';
import {connect} from 'react-redux';
// import Image from 'react-native-remote-svg';
import OneSignal from 'react-native-onesignal';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import {withNavigationFocus} from 'react-navigation';
// import ScrollableTabView, {
//     ScrollableTabBar
// } from "react-native-scrollable-tab-view";
// import AutoHeightImage from 'react-native-auto-height-image';
import FastImage from 'react-native-fast-image';
import Hyperlink from 'react-native-hyperlink';
// import YouTube, { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';
// import VideoPlayer from './VideoPlayer';
// import VideoPlayer from 'react-native-video-player';
import Video from 'react-native-af-video-player';
import {Thumbnail as Thm} from 'react-native-thumbnail-video';
import TopButton from '../components/TopButton';
import Sound from 'react-native-sound';
import {AudioRecorder, AudioUtils} from 'react-native-audio';
import {inputArticleList, save_uri_video} from '../rdx/actions';
// import Video from 'react-native-video';
import Spinner from 'react-native-loading-spinner-overlay';
// const audioRecorderPlayer = new AudioRecorderPlayer();
import Upload from 'react-native-background-upload';
import {
  update_list_article,
  model_video_open,
  save_latitude,
  save_longitude,
} from '../rdx/actions';
import MyModal from '../components/MyModal';
import Geolocation from '@react-native-community/geolocation';
import firebase from 'react-native-firebase';
var RNFS = require('react-native-fs');
const {width, height} = Dimensions.get('window');
// var RNFS = require('react-native-fs');

class Draftscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      myModalVisible: true,
      refreshing: false,
      active: false,
      newTweetContent: '',
      isModalOpen: false,
      isModalOpen2: false,
      isModalOpen3: false,
      modalTextVisible: false,
      daftarevent: [],
      eventname: '',
      imgforupload: '',
      typeevent: '',
      osid: '',
      page: 1,
      loadingMore: false,
      alleventforconcat: [],
      kabupaten: '0',
      kecamatan: '0',
      organisasi: '0',
      usulan: '',
      kabupatenlist: [],
      kabupatenlist2: [],
      kecamatanlist: [],
      kecamatanlist2: [],
      organisasilist: [],
      backClickCount: 0,
      gambarUri: '',
      gambarType: '',
      gambarFilename: '',
      voiceFileName: '',
      letsPlay: false,
      isply: false,

      currentTime: 0.0,
      startRecord: false,
      recording: false,
      paused: false,
      stoppedRecording: false,
      finished: false,
      audioPath: '', //AudioUtils.MusicDirectoryPath + '/reportaudio.aac',
      hasPermission: undefined,

      descript: 'tempo',
      listArticle: [],
      listArticleForPush: {},
      idForEdit: '',
      audioIsPlaying: true,
      audioIsPlayingIndex: '',
      voiceExist: false,
      judul: '',
      desk: '',
      videoUriForSend: '',
      isLoadingForSpinner: false,
      appState: AppState.currentState,
      playSound: false,
      isInternetOk: null,
      idForDeleteAfterSuccess: null,
      isHideUser: false,
      showOrHideUser: '1',
      typeModalForDelete: '',

      objectPhoto: {},
      arrayPhoto: [],
    };
    this._scrollOffset = 0;
  }

  requestPermissionFoto = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Tempo Witness Location Permission',
          message:
            'Tempo Witness access to your locations ' +
            'so you can send location.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the foto file');
        // this.getLotLang()
      } else {
        console.log('GPS permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  requestPermissionVideo = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'Tempo Witness Location Permission',
          message:
            'Tempo Witness access to your locations ' +
            'so you can send location.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the foto file');
        // this.getLotLang()
      } else {
        console.log('GPS permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  requestPermissionSound = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        {
          title: 'Tempo Witness Location Permission',
          message:
            'Tempo Witness access to your locations ' +
            'so you can send location.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the foto file');
        // this.getLotLang()
      } else {
        console.log('GPS permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  requestPermissionRead = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'Tempo Witness Location Permission',
          message:
            'Tempo Witness access to your locations ' +
            'so you can send location.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the foto file');
        // this.getLotLang()
      } else {
        console.log('GPS permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  _onRefresh = async () => {
    try {
      this.setState({refreshing: true});
      await this.fetchingEvent();
      await this.setState({refreshing: false, page: 1});
    } catch (e) {}
  };

  shouldComponentUpdate(np, ns) {
    // if (np.isFocused) {
    //     if(this.props.latitude == '' && this.props.longitude == ''){
    //         this.getLotLang()
    //     }
    //     return true
    // }
    return true;
  }

  handleCheckNetwork = () => {
    NetInfo.getConnectionInfo().then((connectionInfo) => {
      if (connectionInfo.type == 'none') {
        Alert.alert('Você não está conectado à internet');
        this.setState({isInternetOk: false});
      } else {
        this.setState({isInternetOk: true});
      }
    });
    NetInfo.addEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange,
    );
  };

  handleFirstConnectivityChange = (connectionInfo) => {
    if (connectionInfo.type == 'none') {
      Alert.alert('Você não está conectado à internet');
      this.setState({isInternetOk: false});
    } else {
      console.log('ok');
      this.setState({isInternetOk: true});
    }
  };

  async componentDidMount() {
    this.requestCameraPermission();
    this.requestPermissionFoto();
    this.requestPermissionRead();
    this.requestPermissionSound();
    this.requestPermissionVideo();
    this.handleCheckNetwork();
    //  this.getLotLang()
    // if(this.props.token){
    //     this.fetchingListArticle()
    // }

    //   BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    // this.fetchingEvent()
    this.fetchingDetail();
    // this.fetchDetailKabupaten()
    // this.fetchDetailOrganisasi()
    // this.setState({ letsPlay: '' })
    OneSignal.init('04f752ea-ae43-42a1-8488-75b945eb9447', {
      kOSSettingsKeyAutoPrompt: true,
    });

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.configure(); // triggers the ids event

    // AppState.addEventListener('change', this._handleAppStateChange);

    //  firebase.crashlytics().log('Test Message!');
    // firebase.crashlytics().recordError(37,"Test Error");
  }

  fetchingListArticle = async () => {
    try {
      this.setState({loading: false});
      let response = await fetch(GLOBAL.apiUrl + 'article/list', {
        headers: {
          Authorization: 'Bearer ' + this.props.token,
          Accept: 'application/json',
        },
      });
      let responseJson = await response.json();
      await this.setState({listArticle: responseJson.detail});
      this.props.update_list_article(this.state.listArticle);
    } catch (error) {
      this.setState({loading: false});
      console.error(error);
    }
  };

  handleBackgroundUploadVideo = (arid) => {
    let file = this.state.videoUriForSend;
    if (Platform.OS == 'android') {
      file = this.state.videoUriForSend.replace('file://', '');
    }
    const options = {
      url: 'http://witness-api.hahabid.com/article/add_background',
      path: file,
      method: 'POST',
      // type: 'raw',
      field: 'file',
      type: 'multipart',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        //   'content-type': 'application/octet-stream', // Customize content-type
        Authorization: `Bearer ${this.props.token}`,
        arid: `${arid}`,
      },
      // Below are options only supported on Android
      notification: {
        enabled: true,
      },
    };

    Upload.startUpload(options)
      .then((uploadId) => {
        console.log('Upload started');
        Upload.addListener('progress', uploadId, (data) => {
          console.log(`Progress: ${data.progress}%`);
        });
        Upload.addListener('error', uploadId, (data) => {
          console.log(data);
        });
        Upload.addListener('cancelled', uploadId, (data) => {
          console.log(`Cancelled!`);
          console.log(data);
        });
        Upload.addListener('completed', uploadId, (data) => {
          console.log('Completed!');
          console.log(JSON.parse(data.responseBody));
          let dataResponse = JSON.parse(data.responseBody);
          this.fetchingListArticle();
          this.removeArticle(this.state.idForDeleteAfterSuccess);
          this.handleDeletFileVideo(this.state.videoUriForSend);
        });
      })
      .catch((err) => {
        console.log('Upload error!', err);
      });
  };

  handleBackgroundUploadPhoto = (arid) => {
    let file = this.state.gambarUri;
    if (Platform.OS == 'android') {
      file = this.state.gambarUri.replace('file://', '');
    }
    const options = {
      url: 'http://witness-api.hahabid.com/article/add_background',
      path: file,
      method: 'POST',
      // type: 'raw',
      field: 'file',
      type: 'multipart',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        //   'content-type': 'application/octet-stream', // Customize content-type
        Authorization: `Bearer ${this.props.token}`,
        arid: `${arid}`,
      },
      // Below are options only supported on Android
      notification: {
        enabled: true,
      },
    };

    Upload.startUpload(options)
      .then((uploadId) => {
        console.log('Upload started');
        Upload.addListener('progress', uploadId, (data) => {
          console.log(`Progress: ${data.progress}%`);
        });
        Upload.addListener('error', uploadId, (data) => {
          console.log(data);
        });
        Upload.addListener('cancelled', uploadId, (data) => {
          console.log(`Cancelled!`);
          console.log(data);
        });
        Upload.addListener('completed', uploadId, (data) => {
          console.log('Completed!');
          console.log(JSON.parse(data.responseBody));
          let dataResponse = JSON.parse(data.responseBody);
          this.fetchingListArticle();
          this.removeArticle(this.state.idForDeleteAfterSuccess);
          this.handleDeletFileFoto(this.state.gambarUri);
        });
      })
      .catch((err) => {
        console.log('Upload error!', err);
      });
  };

  handleBackgroundUploadSound = (arid) => {
    /* /storage/emulated/0/Music/PHw05.aac*/
    let file = this.state.suaraTukKirim;
    const options = {
      url: 'http://witness-api.hahabid.com/article/add_background',
      path: file,
      method: 'POST',
      // type: 'raw',
      field: 'file',
      type: 'multipart',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        //   'content-type': 'application/octet-stream', // Customize content-type
        Authorization: `Bearer ${this.props.token}`,
        arid: `${arid}`,
      },
      // Below are options only supported on Android
      notification: {
        enabled: true,
      },
    };

    Upload.startUpload(options)
      .then((uploadId) => {
        console.log('Upload started');
        Upload.addListener('progress', uploadId, (data) => {
          console.log(`Progress: ${data.progress}%`);
        });
        Upload.addListener('error', uploadId, (data) => {
          console.log(data);
        });
        Upload.addListener('cancelled', uploadId, (data) => {
          console.log(`Cancelled!`);
          console.log(data);
        });
        Upload.addListener('completed', uploadId, (data) => {
          console.log('Completed!');
          console.log(JSON.parse(data.responseBody));
          let dataResponse = JSON.parse(data.responseBody);
          this.fetchingListArticle();
          this.removeArticle(this.state.idForDeleteAfterSuccess);
          this.handleDeletFileSuara(this.state.soundPath);
        });
      })
      .catch((err) => {
        console.log('Upload error!', err);
      });
  };

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
    // AppState.removeEventListener('change', this._handleAppStateChange);
  }

  onReceived(notification) {
    console.log('Notification received: ', notification);
  }

  onOpened = (openResult) => {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
    this.toChatWithNav(openResult.notification.payload.additionalData.artcid);
  };

  onIds(device) {
    console.log('Device info: ', device);
  }

  toChatWithNav = (artc) => {
    //   Alert.alert(artc)
    if (artc == null || artc == '') {
      console.log('no artc');
    } else {
      this.props.navigation.push('chat', {artcidlempar: artc});
    }
  };

  fetchingDetail = async () => {
    try {
      const response = await axios.get(`${GLOBAL.apiUrl}user/detail`, {
        headers: {
          Authorization: `Bearer ${this.props.token}`,
        },
      });
      if (response.data.success == true) {
        this.setEmailOneSignal();
      } else {
        Alert.alert('Por favor entre');
      }
    } catch (e) {}
  };

  handleSubmit = async (id, modalName) => {
    let newarrartc = await this.props.myArticle.filter(
      (item) => item.articleId == id,
    );

    this.setState({idForDeleteAfterSuccess: id});

    if (modalName == 'image') {
      this.setState(
        {
          //  idForEdit:id,
          judul: newarrartc[0].title,
          desk: newarrartc[0].longDesc,
          showOrHideUser: newarrartc[0].isshow,
          tinggiImage: newarrartc[0].tinggi,
          gambarUri: newarrartc[0].media,
          gambarType: 'image/' + newarrartc[0].ext,
          gambarFilename: 'report.' + newarrartc[0].ext,
          imgforupload: newarrartc[0].isExist,
        },
        this.handleSendArticleImage,
      );
      //    },()=>this.handleDeletFile(this.state.gambarUri))
    } else if (modalName == 'sound') {
      this.setState(
        {
          // idForEdit:id,
          judul: newarrartc[0].title,
          desk: newarrartc[0].longDesc,
          showOrHideUser: newarrartc[0].isshow,
          soundPath: newarrartc[0].media,
          audioPath: newarrartc[0].media2,
          voiceExist: newarrartc[0].isExist,
        },
        this.handleSendArticleAudio,
      );
      //   },()=>this.handleDeletFileSuara(this.state.soundPath))
    } else {
      this.setState(
        {
          //  idForEdit:id,
          judul: newarrartc[0].title,
          desk: newarrartc[0].longDesc,
          showOrHideUser: newarrartc[0].isshow,
          videoUriForSend: newarrartc[0].media,
          videoUriForSendExist: newarrartc[0].isExist,
        },
        this.handleSendArticleVideo,
      );
      //   },()=>this.handleDeletFileVideo(this.state.videoUriForSend))
      // },this.handleBackgroundUploadVideo)
    }
  };

  handleDeletFileFoto = (gambarUri) => {
    return RNFS.unlink(gambarUri)
      .then(() => {
        console.log('FILE DELETED');
      })
      .catch((err) => {
        console.log(err.message);
      });
  };

  handleDeletFileSuara = (uri) => {
    return RNFS.unlink(uri)
      .then(() => {
        console.log('FILE DELETED');
      })
      .catch((err) => {
        console.log(err.message);
      });
  };

  handleDeletFileVideo = (uri) => {
    return RNFS.unlink(uri)
      .then(() => {
        console.log('FILE DELETED');
      })
      .catch((err) => {
        console.log(err.message);
      });
  };

  handleSendArticleAudio = () => {
    if (this.state.judul == '' || this.state.desk == '') {
      Alert.alert('Por favor, preencha o que não foi preenchido');
      return false;
    }
    const data = new FormData();
    data.append('title', this.state.judul);
    data.append('desc', this.state.desk);
    data.append('isshow', this.state.showOrHideUser);
    data.append('latitude', this.props.latitude);
    data.append('longitude', this.props.longitude);

    fetch(`${GLOBAL.apiUrl}article/add`, {
      headers: {
        Authorization: 'Bearer ' + this.props.token,
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      method: 'post',
      body: data,
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        this.handleBackgroundUploadSound(responseJson.id);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  handleSendArticleImage = () => {
    if (this.state.judul == '' || this.state.desk == '') {
      Alert.alert('Por favor, preencha o que não foi preenchido');
      return false;
    }
    const data = new FormData();
    data.append('title', this.state.judul);
    data.append('desc', this.state.desk);
    data.append('isshow', this.state.showOrHideUser);
    data.append('hg', this.state.tinggiImage);
    data.append('latitude', this.props.latitude);
    data.append('longitude', this.props.longitude);
    fetch(`${GLOBAL.apiUrl}article/add`, {
      headers: {
        Authorization: 'Bearer ' + this.props.token,
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      method: 'post',
      body: data,
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (this.state.imgforupload) {
          this.handleBackgroundUploadPhoto(responseJson.id);
        } else {
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  handleSendArticleVideo = () => {
    if (this.state.judul == '' || this.state.desk == '') {
      Alert.alert('Por favor, preencha o que não foi preenchido');
      return false;
    }
    const data = new FormData();
    data.append('title', this.state.judul);
    data.append('desc', this.state.desk);
    data.append('isshow', this.state.showOrHideUser);
    data.append('latitude', this.props.latitude);
    data.append('longitude', this.props.longitude);
    fetch(`${GLOBAL.apiUrl}article/add`, {
      headers: {
        Authorization: 'Bearer ' + this.props.token,
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      method: 'post',
      body: data,
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoadingForSpinner: false,
        });
        if (this.state.typeModalForDelete == 'text') {
          // this.removeArticle(this.state.idForDeleteAfterSuccess)
        }
        console.log(responseJson);
        if (this.state.videoUriForSendExist) {
          this.handleBackgroundUploadVideo(responseJson.id);
        } else {
          // this.removeArticle(this.state.idForDeleteAfterSuccess)
        }
        // Alert.alert(JSON.stringify(responseJson))
      })
      .catch((e) => {
        this.setState({
          isLoadingForSpinner: false,
        });
        console.log(e);
        // Alert.alert(JSON.stringify(e))
        // Notification.create({ subject: 'Tempo Witness', message: 'Upload Gagal' });
      });
  };

  processorAudio = async () => {
    await this.setState({voiceFileName: this.makeid(5)});
    await this.setState({
      audioPath:
        AudioUtils.MusicDirectoryPath + '/' + this.state.voiceFileName + '.aac',
    });
    AudioRecorder.requestAuthorization().then((isAuthorised) => {
      this.setState({hasPermission: isAuthorised});

      if (!isAuthorised) return;

      this.prepareRecordingPath(this.state.audioPath);

      AudioRecorder.onProgress = (data) => {
        this.setState({currentTime: Math.floor(data.currentTime)});
      };

      AudioRecorder.onFinished = (data) => {
        // Android callback comes in the form of a promise instead.
        if (Platform.OS === 'ios') {
          this._finishRecording(
            data.status === 'OK',
            data.audioFileURL,
            data.audioFileSize,
          );
        }
      };
    });
  };

  //=============================== image prossess ===========================
  addArticle = () => {
    console.log(this.state.gambarUri);
    // this.setState({idForEdit:''})
    var artikel = this.state.listArticleForPush;
    artikel['articleId'] = this.makeid(5);
    artikel['title'] = this.state.judul;
    artikel['shortDesc'] = this.state.desk.substring(0, 20);
    artikel['longDesc'] = this.state.desk;
    artikel['isshow'] = this.state.isHideUser ? `0` : `1`;
    artikel['type'] = 'image';
    artikel['media'] =
      'file:///storage/emulated/0/Pictures/images/' + this.state.gambarFilename;
    // artikel['media'] = this.state.gambarUri;
    artikel['tinggi'] = this.state.tinggiImage;
    artikel['ext'] = this.state.gambarFilename.split('.').pop();
    if (this.state.gambarFilename !== '') {
      artikel['isExist'] = true;
    } else {
      artikel['isExist'] = false;
    }
    var items = this.props.myArticle;
    if (this.state.idForEdit !== '') {
      objIndex = items.findIndex(
        (obj) => obj.articleId == this.state.idForEdit,
      );
      items[objIndex].title = this.state.judul;
      items[objIndex].shortDesc = this.state.desk.substring(0, 20);
      items[objIndex].longDesc = this.state.desk;

      if (this.state.gambarFilename !== '') {
        items[objIndex].media =
          'file:///storage/emulated/0/Pictures/images/' +
          this.state.gambarFilename;
        items[objIndex].isExist = true;
      } else {
        //  items[objIndex].isExist = false;
        items[objIndex].isExist = items[objIndex].isExist
          ? items[objIndex].isExist
          : false;
      }
      this.props.inputArticleList(JSON.stringify(items));
    } else {
      items.push(artikel);
      this.props.inputArticleList(JSON.stringify(items));
    }

    this.closeModal();
  };

  removeArticle = async (id) => {
    let newarrartc = await this.props.myArticle.filter(
      (item) => item.articleId !== id,
    );
    this.props.inputArticleList(JSON.stringify(newarrartc));
    this.closeModal();
  };

  removeArticleTwo = async (id) => {
    let oneArr = await this.props.myArticle.filter(
      (item) => item.articleId == id,
    );
    let newarrartc = await this.props.myArticle.filter(
      (item) => item.articleId !== id,
    );
    this.props.inputArticleList(JSON.stringify(newarrartc));
    this.closeModal();
    this.handleDeletFileFoto(oneArr[0].media);
  };

  removeOnePressed = (par) => {
    Alert.alert('Atenção!', 'Tem a certeza de que vai apagar este relatório??', [
      {text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel'},
      {text: 'Yes', onPress: () => this.removeArticleTwo(par)},
    ]);
  };

  // removeOnePressed = (par) => {
  //     Alert.alert(
  //         'Atenção !',
  //         'Anda Yakin Akan Menghapus Barang ini ?',
  //         [
  //             { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' },
  //             { text: 'Yes', onPress: () => this.handleRemoveone(par) }
  //         ]
  //     )
  // }

  submitOnPressed = (id, modalname) => {
    this.setState({typeModalForDelete: modalname});
    if (!this.state.isInternetOk) {
      Alert.alert('Você não está conectado à Internet');
      return false;
    }
    if (this.props.latitude == '' && this.props.longitude == '') {
      Alert.alert('Ative seu GPS');
      this.requestCameraPermission();
      return false;
    } else {
      Alert.alert(
        'Atenção!',
        'Tem certeza de que deseja enviar este relatório?',
        [
          {
            text: 'No',
            onPress: () => console.log('No Pressed'),
            style: 'cancel',
          },
          {text: 'Yes', onPress: () => this.handleSubmit(id, modalname)},
        ],
      );
    }
  };

  getLotLang = async () => {
    try {
      const tesloc = Geolocation.getCurrentPosition(
        (position) => {
          this.props.save_latitude(position.coords.latitude);
          this.props.save_longitude(position.coords.longitude);
        },
        (error) => {},
        {
          enableHighAccuracy: false,
          timeout: 65000,
          // maximumAge: 1000
        },
      );
    } catch (err) {
      // this.props.navigation.navigate('app')
      console.log(err);
    }
  };

  openModalEdit = async (id, modalName) => {
    let newarrartc = await this.props.myArticle.filter(
      (item) => item.articleId == id,
    );
    this.setState({
      idForEdit: id,
      judul: newarrartc[0].title,
      desk: newarrartc[0].longDesc,
      isHideUser: false,
    });

    if (modalName == 'image') {
      this.openModaldua();
    } else if (modalName == 'video') {
      this.openModal();
    } else if (modalName == 'text') {
      this.openModalEmpat();
    } else {
      this.openModaltiga();
    }
  };

  //=============================== voice prossess ===========================

  addArticleVoice = () => {
    if (this.state.recording) {
      Alert.alert('Você ainda está gravando, pressione Parar primeiro');
      return false;
    }
    if (this.state.paused) {
      Alert.alert('Continue gravando e pare para salvar');
      return false;
    }
    // this.setState({idForEdit:''})
    var artikel = this.state.listArticleForPush;
    artikel['articleId'] = this.state.voiceFileName;
    artikel['title'] = this.state.judul;
    artikel['shortDesc'] = this.state.desk.substring(0, 20);
    artikel['longDesc'] = this.state.desk;
    artikel['isshow'] = this.state.isHideUser ? `0` : `1`;
    artikel['type'] = 'sound';
    // artikel['media'] = 'file:///storage/emulated/0/Music/'+this.state.voiceFileName+'.aac';
    artikel['media'] = this.state.soundPath;
    artikel['media2'] = this.state.audioPath;

    artikel['isExist'] = this.state.voiceExist;
    artikel['ext'] = 'aac';

    var items = this.props.myArticle;
    if (this.state.idForEdit !== '') {
      objIndex = items.findIndex(
        (obj) => obj.articleId == this.state.idForEdit,
      );
      items[objIndex].title = this.state.judul;
      items[objIndex].shortDesc = this.state.desk.substring(0, 20);
      items[objIndex].longDesc = this.state.desk;
      if (this.state.soundPath !== undefined || this.state.soundPath !== '') {
        items[objIndex].media = this.state.soundPath;
        items[objIndex].media2 = this.state.soundPath
          ? this.state.audioPath
          : items[objIndex].media2;
        items[objIndex].isExist = true;
      } else {
        items[objIndex].isExist = items[objIndex].isExist
          ? items[objIndex].isExist
          : this.state.voiceExist;
      }

      this.props.inputArticleList(JSON.stringify(items));
    } else {
      items.push(artikel);
      this.props.inputArticleList(JSON.stringify(items));
    }

    this.closeModal();
  };

  //================================ video process ====================================

  addArticleVideo = async () => {
    // this.setState({idForEdit:''})
    var artikel = this.state.listArticleForPush;
    artikel['articleId'] = this.makeid(5);
    artikel['title'] = this.state.judul;
    artikel['shortDesc'] = this.state.desk.substring(0, 20);
    artikel['longDesc'] = this.state.desk;
    artikel['isshow'] = this.state.isHideUser ? `0` : `1`;
    artikel['type'] = 'video';
    // artikel['media'] = 'file:///storage/emulated/0/Music/'+this.state.voiceFileName+'.aac';
    artikel['media'] = this.props.uriVid;
    artikel['ext'] = 'mp4';
    if (this.props.uriVid !== '') {
      artikel['isExist'] = true;
    } else {
      artikel['isExist'] = false;
    }
    var items = this.props.myArticle;
    if (this.state.idForEdit !== '') {
      objIndex = items.findIndex(
        (obj) => obj.articleId == this.state.idForEdit,
      );
      items[objIndex].title = this.state.judul;
      items[objIndex].shortDesc = this.state.desk.substring(0, 20);
      items[objIndex].longDesc = this.state.desk;

      if (this.props.uriVid !== '') {
        items[objIndex].media = this.props.uriVid;
        items[objIndex].isExist = true;
      } else {
        // items[objIndex].isExist = false;
        items[objIndex].isExist = items[objIndex].isExist
          ? items[objIndex].isExist
          : false;
      }
      await this.props.inputArticleList(JSON.stringify(items));
      this.props.save_uri_video('');
    } else {
      items.push(artikel);
      await this.props.inputArticleList(JSON.stringify(items));
      this.props.save_uri_video('');
    }

    this.closeModal();
  };

  addArticleText = () => {
    // this.setState({idForEdit:''})
    var artikel = this.state.listArticleForPush;
    artikel['articleId'] = this.makeid(5);
    artikel['title'] = this.state.judul;
    artikel['shortDesc'] = this.state.desk.substring(0, 20);
    artikel['longDesc'] = this.state.desk;
    artikel['isshow'] = this.state.isHideUser ? `0` : `1`;
    artikel['type'] = 'text';
    // artikel['media'] = 'file:///storage/emulated/0/Music/'+this.state.voiceFileName+'.aac';
    artikel['media'] = this.state.soundPath;
    artikel['media2'] = this.state.audioPath;

    artikel['isExist'] = this.state.voiceExist;
    artikel['ext'] = 'aac';

    var items = this.props.myArticle;
    if (this.state.idForEdit !== '') {
      objIndex = items.findIndex(
        (obj) => obj.articleId == this.state.idForEdit,
      );
      items[objIndex].title = this.state.judul;
      items[objIndex].shortDesc = this.state.desk.substring(0, 20);
      items[objIndex].longDesc = this.state.desk;
      // if(this.state.soundPath !== undefined || this.state.soundPath !== ''){
      //     items[objIndex].media = this.state.soundPath;
      //     items[objIndex].media2 = this.state.soundPath ? this.state.audioPath :  items[objIndex].media2  ;
      //     items[objIndex].isExist = true
      // }else{
      //     items[objIndex].isExist = items[objIndex].isExist ? items[objIndex].isExist : this.state.voiceExist;
      // }

      this.props.inputArticleList(JSON.stringify(items));
    } else {
      items.push(artikel);
      this.props.inputArticleList(JSON.stringify(items));
    }

    this.closeModal();
  };

  //=======================================================================================

  makeid = (length) => {
    var result = '';
    var characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  };

  setEmailOneSignal = () => {
    OneSignal.init('04f752ea-ae43-42a1-8488-75b945eb9447', {
      kOSSettingsKeyAutoPrompt: true,
    });
    //apikey YTEyNmNhYTgtOTI0Yy00NGNjLWFmMmUtNzQyMWJiYzE4N2M5
    // OneSignal.setEmail(this.props.email, (error) => {
    //console.log("Sent email with error: ", error);
    // });
    OneSignal.getPermissionSubscriptionState((subscriptionState) => {
      this.setState({osid: subscriptionState.userId}, this.saveOneSignalId);
    });
  };

  saveOneSignalId = () => {
    let data = JSON.stringify({
      osid: this.state.osid,
    });
    axios
      .post(`${GLOBAL.apiUrl}user/updateonesignal`, data, {
        headers: {
          Authorization: `Bearer ${this.props.token}`,
          'Content-Type': 'application/json',
        },
      })
      .then((res) => {
        // console.log(res)
      })
      .catch((e) => {
        // console.log(e)
      });
  };

  handleUploadImage = () => {
    // if(this.state.arrayPhoto.length == 5){
    //     Alert.alert("Atenção!","Maksimal 5 Photo")
    //     return false
    // }
    const options = {
      title: 'Foto, máximo de 4 MB ',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.fileSize > 10000000000000000) {
        Alert.alert('arquivo de foto é muito grande');
      } else {
        // console.log(response)
        // this.uplod(response)
        // if(this.state.arrayPhoto.length > 0){
        //     this.handleCollectPhoto(response)
        // }else{
        // this.handleCollectPhoto(response)
        this.setState({
          imgforupload: response,
          gambarFilename: response.fileName,
          gambarUri: response.uri,
          gambarType: response.type,
        });
        // }
      }
    });
  };

  handleCollectPhoto = (dataPhoto) => {
    var photoLaporan = {};
    var arrayPhoto = this.state.arrayPhoto;
    photoLaporan['photo'] = dataPhoto;
    arrayPhoto.push(photoLaporan);
    this.setState({arrayPhoto}, () => console.log(this.state.arrayPhoto));
  };

  handleDeletePhotoInArrayPhoto = (par) => {
    Alert.alert('Atenção!', 'Tem certeza de que deseja excluir esta foto?', [
      {text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel'},
      {
        text: 'Yes',
        onPress: () => {
          const valueToRemove = par;
          const arrayPhoto = this.state.arrayPhoto.filter(
            (item) => item !== valueToRemove,
          );
          this.setState({arrayPhoto});
        },
      },
    ]);
  };

  uplod = (datanya, ei) => {
    // console.log(datanya)
    RNFetchBlob.fetch(
      'POST',
      `${GLOBAL.apiUrl}event/imgnext`,
      {
        Accept: 'application/json',
        Authorization: `${ei}`,
        'Content-Type': 'multipart/form-data',
      },
      [
        {
          name: 'pict',
          filename: datanya.fileName,
          type: datanya.type,
          data: datanya.data,
        },
      ],
    )
      .then((resp) => {
        let rspUpload = JSON.parse(resp.data);
        if (rspUpload.success == true) {
          this.fetchingEvent();
          this.setState({eventname: ''});
        } else {
          Alert.alert('Falha ao atualizar a foto');
        }
      })
      .catch((err) => {
        // console.log(JSON.parse(err))
        // Alert.alert('Kesalahan Server')
      });
  };

  openModal = () => {
    //canvasing
    this.setState({
      isHideUser: false,
      // isModalOpen: !this.state.isModalOpen,
      // typeevent: '3',
      // usulan: '',
      // // imgforupload: '',
      // // gambarUri: '',
      // // gambarFilename: '',
      // // gambarType: '',
      // letsPlay: ''
    });

    setTimeout(() => {
      this.props.navigation.navigate('recordvideo');
    }, 200);
  };
  openModaldua = () => {
    //isu
    this.handleUploadImage();
    this.setState({
      isModalOpen2: !this.state.isModalOpen2,
      typeevent: '1',
      usulan: '',
      // imgforupload: '',
      // gambarUri: '',
      // gambarFilename: '',
      // gambarType: '',
      letsPlay: '',
      isHideUser: false,
    });
  };

  openModaltiga = async () => {
    //broadcast
    await this.setState({
      isModalOpen3: !this.state.isModalOpen3,
      typeevent: '',
      usulan: '',
      // imgforupload: '',
      // gambarUri: '',
      // gambarFilename: '',
      // gambarType: '',
      letsPlay: '',
      isHideUser: false,
      //voiceFileName:this.makeid(5),
      // audioPath: AudioUtils.MusicDirectoryPath + '/'+this.makeid(5)+'.aac',
    });

    this.processorAudio();

    // await this.setState({
    //     audioPath: AudioUtils.MusicDirectoryPath + '/'+this.state.voiceFileName+'.aac',
    // })
    // this.fetchDetailKabupaten2()
    //this.fetchDetailKecamatan2()
  };

  openModalEmpat = () => {
    //text
    this.setState({
      modalTextVisible: !this.state.modalTextVisible,
      isHideUser: false,
      // typeevent: '',
      // usulan: '',
      // imgforupload: '',
      // gambarUri: '',
      // gambarFilename: '',
      // gambarType: '',
      // letsPlay: ''
    });
  };

  closeModal = () => {
    this.setState({
      isModalOpen: false,
      isModalOpen2: false,
      isModalOpen3: false,
      modalTextVisible: false,
      gambarFilename: '',
      gambarUri: '',
      voiceFileName: '',
      voiceExist: false,
      judul: '',
      desk: '',
      idForEdit: '',
      soundPath: '',
      audioPath: '',
    });
    this.props.model_video_open(false);
  };

  openLinking = (ph) => {
    // Linking.openURL(`tel:021${800800}`)
    Linking.openURL(`tel:${ph}`);
  };

  // openLinking2 = (ur) => {
  //     Alert.alert(
  //         'Atenção',
  //         'Kunjungi Link',
  //         [
  //             { text: 'Cancel', onPress: () => console.log('no') },
  //             { text: 'Ok', onPress: () => Linking.openURL(ur) }
  //         ] //, { cancelable: false }
  //     )

  // }

  handleScroll = (event) => {
    const CustomLayoutLinear = {
      duration: 100,
      create: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity,
      },
      update: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity,
      },
      delete: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity,
      },
    };

    const currentOffset = event.nativeEvent.contentOffset.y;
    let shouldShow;

    if (currentOffset > this._scrollOffset || currentOffset === 0) {
      //0
      // scroll down or on top of ScrollView
      shouldShow = false;
      if (
        this.isCloseToBottom(
          event.nativeEvent.layoutMeasurement,
          event.nativeEvent.contentOffset,
          event.nativeEvent.contentSize,
        )
      ) {
        this.setState({page: this.state.page + 1}, this.handleConcat);
        //Alert.alert('sampai bawah')
        //this.handleConcat()
      }
    } else {
      // scroll up
      shouldShow = true;
    }

    if (
      shouldShow !== this.state.shouldShow &&
      (!shouldShow || currentOffset > 250)
    ) {
      //250
      LayoutAnimation.configureNext(CustomLayoutLinear);
      this._scrollOffset = currentOffset;
      this.setState({shouldShow});
    }

    this._scrollOffset = currentOffset;
  };

  handleConcat = async () => {
    try {
      this.setState({loadingMore: true});
      const response = await axios.get(
        `${GLOBAL.apiUrl}relawan/geteventbyrule?p=${this.state.page}`,
        {
          headers: {
            Authorization: `Bearer ${this.props.token}`,
          },
        },
      );
      if (response.data.success == true) {
        await this.setState({
          alleventforconcat: response.data.data,
        });
        await this.setState({
          daftarevent: this.state.daftarevent.concat(
            this.state.alleventforconcat,
          ),
        });
        this.setState({loadingMore: false});

        // console.log(response)
      } else {
        // console.log(response)
        this.setState({loadingMore: false});
      }
    } catch (e) {
      // console.log(e)
      this.setState({loadingMore: false});
    }
  };

  isCloseToBottom = (layoutMeasurement, contentOffset, contentSize) => {
    const paddingToBottom = 10;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  openLinking2 = async (ur) => {
    let cek = await this.isYoutubeUrl(ur);
    if (cek == true) {
      let vId = await this.getId(ur);
      Alert.alert(
        'Atenção',
        'Lihat Video ?',
        [
          {text: 'Cancel', onPress: () => console.log('no')},
          {text: 'Ok', onPress: () => Linking.openURL(ur)},
        ], //, { cancelable: false }
      );
    } else {
      Alert.alert(
        'Atenção',
        'Kunjungi Link ?',
        [
          {text: 'Cancel', onPress: () => console.log('no')},
          {text: 'Ok', onPress: () => Linking.openURL(ur)},
        ], //, { cancelable: false }
      );
    }
  };

  isYoutubeUrl = (url) => {
    //var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/
    var p =
      /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    if (url.match(p)) {
      return true;
    }
    return false;
  };

  getId = (url) => {
    var regExp =
      /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
      return match[2];
    } else {
      return 'error';
    }
  };

  handleIconYoutubeClick = (pa) => {
    if (pa == 'paused') {
      this.setState({letsPlay: ''});
    }
  };

  //==================================== function audios=================

  prepareRecordingPath(audioPath) {
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: 'Low',
      AudioEncoding: 'aac',
      AudioEncodingBitRate: 32000,
    });
  }

  _renderButton(title, onPress, active) {
    var style = active ? styles.activeButtonText : styles.buttonText;

    return (
      <TouchableHighlight style={styles.button} onPress={onPress}>
        <Text style={style}>{title}</Text>
      </TouchableHighlight>
    );
  }

  _renderPauseButton(onPress, active) {
    var style = active ? styles.activeButtonText : styles.buttonText;
    var title = this.state.paused ? 'RESUME' : 'PAUSE';
    return (
      <TouchableHighlight style={styles.button} onPress={onPress}>
        <Text style={style}>{title}</Text>
      </TouchableHighlight>
    );
  }

  async _pause() {
    if (!this.state.recording) {
      console.warn("Can't pause, not recording!");
      return;
    }

    try {
      const filePath = await AudioRecorder.pauseRecording();
      this.setState({paused: true, recording: false});
    } catch (error) {
      console.error(error);
    }
  }

  async _resume() {
    if (!this.state.paused) {
      console.warn("Can't resume, not paused!");
      return;
    }

    try {
      await AudioRecorder.resumeRecording();
      this.setState({paused: false, recording: true});
    } catch (error) {
      console.error(error);
    }
  }

  async _stop() {
    if (!this.state.recording) {
      console.warn("Can't stop, not recording!");
      return;
    }

    this.setState({
      stoppedRecording: true,
      recording: false,
      paused: false,
      startRecord: false,
    });

    try {
      const filePath = await AudioRecorder.stopRecording();

      if (Platform.OS === 'android') {
        this._finishRecording(true, filePath);
      }
      return filePath;
    } catch (error) {
      console.error(error);
    }
  }

  async _play() {
    this.setState({playSound: true});
    if (this.state.recording) {
      await this._stop();
    }

    // These timeouts are a hacky workaround for some issues with react-native-sound.
    // See https://github.com/zmxv/react-native-sound/issues/89.
    setTimeout(() => {
      var sound = new Sound(this.state.audioPath, '', (error) => {
        if (error) {
          console.log('failed to load the sound', error);
        }
      });

      setTimeout(() => {
        sound.play((success) => {
          if (success) {
            console.log('successfully finished playing');
          } else {
            console.log('playback failed due to audio decoding errors');
          }
        });
      }, 100);
    }, 100);
  }

  async _record() {
    if (this.state.recording) {
      console.warn('Already recording!');
      return;
    }

    if (!this.state.hasPermission) {
      console.warn("Can't record, no permission granted!");
      return;
    }

    if (this.state.stoppedRecording) {
      this.prepareRecordingPath(this.state.audioPath);
    }

    this.setState({
      recording: true,
      paused: false,
      startRecord: true,
    });

    try {
      const filePath = await AudioRecorder.startRecording();
    } catch (error) {
      console.error(error);
    }
  }

  _finishRecording(didSucceed, filePath, fileSize) {
    this.setState({
      finished: didSucceed,
      soundPath: `file://${AudioUtils.MusicDirectoryPath}/${this.state.voiceFileName}.aac`,
      voiceExist: true,
      suaraTukKirim: `${filePath}`,
    });
    console.log(
      `Finished recording of duration ${
        this.state.currentTime
      } seconds at path: ${filePath} and size of ${fileSize || 0} bytes`,
    );
  }

  handlePlayAudio = (uri, ind) => {
    if (this.state.audioIsPlaying) {
      this.handleStopAudio();
    } else {
      Sound.setCategory('Playback');
      this.whoosh = new Sound(uri, Sound.MAIN_BUNDLE, (error) => {
        if (error) {
          console.log('failed to load the sound', error);
          return;
        }
        this.whoosh.play((success) => {
          if (success) {
            console.log('successfully finished playing');
            this.setState({audioIsPlaying: true, audioIsPlayingIndex: ''});
          } else {
            console.log('playback failed due to audio decoding errors');
          }
        });
      });
    }
  };

  handlePlayAudioPress = async (uri, ind) => {
    await this.setState({audioIsPlaying: !this.state.audioIsPlaying});
    this.setState({audioIsPlayingIndex: ind});
    this.handlePlayAudio(uri, ind);
  };

  handleStopAudio = () => {
    this.whoosh.pause();
    this.setState({audioIsPlayingIndex: ''});
  };

  //==================================== function audios end ===============
  fancyTimeFormat = (time) => {
    // Hours, minutes and seconds
    var hrs = ~~(time / 3600);
    var mins = ~~((time % 3600) / 60);
    var secs = ~~time % 60;

    // Output like "1:01" or "4:03:59" or "123:03:59"
    var ret = '';

    if (hrs > 0) {
      ret += '' + hrs + ':' + (mins < 10 ? '0' : '');
    }

    ret += '' + mins + ':' + (secs < 10 ? '0' : '');
    ret += '' + secs;
    return ret;
  };

  requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Tempo Witness Location Permission',
          message:
            'Tempo Witness access to your locations ' +
            'so you can send location.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the GPS');
        this.getLotLang();
      } else {
        console.log('GPS permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  render() {
    if (this.props.tweetPosted === 'success') {
      this.closeModal();
    }
    return (
      <Container>
        <Spinner
          visible={this.state.isLoadingForSpinner}
          textContent={'Loading...'}
          textStyle={{color: 'white'}}
        />

        <Content>
          <View
            contentContainerStyle={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          />

          <View style={{justifyContent: 'flex-start', flex: 1}}>
            {this.props.myArticle.length == 0 && (
              <TopButton
                openImage={this.openModaldua}
                openVideo={this.openModal}
                openVoice={this.openModaltiga}
                openText={this.openModalEmpat}
              />
            )}

            {/* =============================== */}
            {this.props.myArticle &&
              this.props.myArticle.map((r, i) => (
                <View key={i} style={styles.tweet}>
                  <TouchableHighlight
                    underlayColor="white"
                    activeOpacity={0.75}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <Thumbnail
                        source={{uri: this.props.photo}}
                        style={{marginBottom: 15}}
                      />
                      <View
                        style={{
                          flexDirection: 'column',
                          justifyContent: 'flex-start',
                        }}>
                        <Text
                          style={{
                            paddingLeft: 15,
                          }}
                        />
                        <Text
                          style={{
                            paddingLeft: 15,
                            fontWeight: 'bold',
                            fontSize: 20,
                            color: 'black',
                          }}>
                          {r.title}
                        </Text>

                        <Text
                          style={{
                            paddingLeft: 15,
                            color: '#aaa',
                            fontSize: 16,
                          }}
                        />

                        <Text
                          style={{
                            paddingLeft: 15,
                            color: '#aaa',
                            fontSize: 16,
                          }}
                        />
                      </View>
                    </View>
                  </TouchableHighlight>

                  {r.type == 'image' && r.isExist && (
                    <TouchableWithoutFeedback
                      onPress={() =>
                        this.props.navigation.navigate('imageviewer', {
                          imglempar: r.media,
                        })
                      }>
                      <View style={{minHeight: 250, paddingBottom: 10}}>
                        <FastImage
                          style={{height: r.tinggi}}
                          source={{
                            uri: r.media,

                            priority: FastImage.priority.high,
                          }}
                          resizeMode={FastImage.resizeMode.cover}
                        />
                      </View>
                    </TouchableWithoutFeedback>
                  )}

                  {r.type == 'video' && r.isExist && <Video url={r.media} />}

                  {r.type == 'sound' && r.isExist && (
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        alignItems: 'flex-end',
                        width: width * 0.7,
                        alignSelf: 'center',
                        marginTop: 10,
                      }}>
                      <View>
                        <TouchableOpacity
                          onPress={() =>
                            this.handlePlayAudioPress(r.media2, r.articleId)
                          }>
                          <View
                            style={{
                              height: 80,
                              width: 80,
                              backgroundColor: 'red',
                              borderRadius: 50,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Icon
                              type="FontAwesome"
                              name={
                                this.state.audioIsPlayingIndex == r.articleId
                                  ? 'stop'
                                  : 'play'
                              }
                            />
                          </View>
                        </TouchableOpacity>
                      </View>
                    </View>
                  )}

                  <View
                    style={{
                      flexDirection: 'column',
                      justifyContent: 'flex-start',
                    }}>
                    <Text
                      style={{
                        paddingLeft: 15,
                        fontSize: 16,
                        fontWeight: 'bold',
                        color: 'black',
                      }}>
                      Detail :
                    </Text>
                    <Hyperlink
                      linkStyle={{color: '#2980b9'}}
                      onPress={() => console.log('no')}>
                      <Text
                        selectable={true}
                        style={{
                          paddingLeft: 25,
                          color: '#aaa',
                          fontSize: 16,
                        }}>
                        {r.longDesc}
                      </Text>
                      {/* <View style={{ marginLeft: 25 }}>
                                            <Text style={{ color: 'black', fontWeight: 'bold' }}>Read More</Text>
                                        </View> */}
                    </Hyperlink>
                  </View>

                  <Text style={styles.tweetText}>{}</Text>
                  <View style={styles.tweetFooter}>
                    <View style={styles.footerIcons}>
                      <Button
                        style={{
                          backgroundColor: 'green',
                          height: 40,
                          width: 50,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                        rounded
                        onPress={() => this.openModalEdit(r.articleId, r.type)}>
                        <Icon name="ios-create" style={{width: 30}} />
                      </Button>
                    </View>
                    <View style={styles.footerIcons}>
                      <Button
                        onPress={() =>
                          this.submitOnPressed(r.articleId, r.type)
                        }
                        rounded
                        style={{
                          color: '#4286f4',
                          height: 40,
                          width: 94,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text style={{color: 'white'}}>Send</Text>
                      </Button>
                    </View>

                    <View style={styles.footerIcons}>
                      <Button
                        onPress={() => this.removeOnePressed(r.articleId)}
                        rounded
                        style={{
                          backgroundColor: 'red',
                          height: 40,
                          width: 50,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Icon name="ios-trash" />
                      </Button>
                    </View>
                  </View>
                </View>
              ))}

            {/* =============================== */}
          </View>

          <View style={{height: 100}} />
        </Content>
        {
          // modal canvasing
        }
        <Modal
          backdropPressToClose={false}
          swipeToClose={false}
          ref={'newTweetModal'}
          backdrop={true}
          style={styles.modal}
          // isOpen={this.state.isModalOpen}
          isOpen={false}
          onClosed={this.closeModal.bind(this)}>
          <ScrollView keyboardShouldPersistTaps="always">
            <View
              style={{
                alignSelf: 'flex-start',
                alignItems: 'center',
                flexDirection: 'row',
                padding: 5,
                paddingRight: 10,
              }}>
              <Button transparent onPress={this.closeModal.bind(this)}>
                <Icon name="close" style={{color: 'black', fontSize: 32}} />
              </Button>
              <View style={{flex: 1}} />
              <Thumbnail
                small
                source={{
                  uri: 'https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg',
                }}
              />
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                width: '100%',
              }}>
              <Input
                style={{
                  width: width * 0.95,
                  marginLeft: 10,
                  color: 'grey',
                  borderBottomWidth: 0.5,
                  borderBottomColor: 'grey',
                  // borderBottomColor: 'grey',
                  // borderBottomWidth: 0.5
                }}
                multiline
                placeholder="Judul... "
                value={this.state.judul}
                onChangeText={(judul) => this.setState({judul})}
              />
              <View
                style={{
                  borderTopColor: 'grey',
                  borderTopWidth: 0.5,
                }}
              />

              <Input
                style={{
                  width: width * 0.95,
                  marginLeft: 10,
                  color: 'grey',
                }}
                multiline
                value={this.state.desk}
                placeholder="Descrição... "
                onChangeText={(desk) => this.setState({desk})}
              />
            </View>

            <Video url={this.props.uriVid} />
          </ScrollView>
        </Modal>

        {
          //===========================================================================
        }
        <Modal
          // photo
          // coverScreen={true}
          backdropPressToClose={false}
          swipeToClose={false}
          ref={'newTweetModal2'}
          backdrop={true}
          style={styles.modal}
          // isOpen={this.state.isModalOpen2}
          isOpen={false}
          onClosed={this.closeModal.bind(this)}>
          <ScrollView keyboardShouldPersistTaps="always">
            <View
              style={{
                alignSelf: 'flex-start',
                alignItems: 'center',
                flexDirection: 'row',
                padding: 5,
                paddingRight: 10,
              }}>
              <Button transparent onPress={this.closeModal.bind(this)}>
                <Icon name="close" style={{color: 'black', fontSize: 32}} />
              </Button>
              <View style={{flex: 1}} />
              <Thumbnail
                small
                source={{
                  uri: 'https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg',
                }}
              />
            </View>

            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                width: '100%',
              }}>
              <View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Judul
                  </Text>
                </View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 5,
                    marginBottom: 10,
                    height: 40,
                    alignContent: 'center',
                    backgroundColor: '#F7F7F7',
                    //height: 50,
                    width: width * 0.9,
                    borderColor: '#A6A6A4',
                    borderWidth: 1,
                    borderRadius: 8,
                  }}>
                  <TextInput
                    // textAlign={'left'}
                    style={{
                      // paddingTop: 5,
                      // paddingRight: 16,
                      // fontStyle:'italic',
                      flex: 1,
                      // alignSelf: 'center',
                      fontSize: 10,
                      // fontWeight: 'bold',
                      // fontFamily: "Lato-Bold",
                      color: GLOBAL.greyText,
                    }}
                    // onBlur={this.handleEndEditing}
                    // onEndEditing={this.handleEndEditing}
                    value={this.state.judul}
                    onChangeText={(judul) => this.setState({judul})}
                    autoFocus={this.state.autoFo}
                    underlineColorAndroid="transparent"
                  />
                </View>
              </View>

              <View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Descrição
                  </Text>
                </View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    width: width * 0.9,
                    marginTop: 5,
                    minHeight: 150,
                    backgroundColor: '#F7F7F7',
                    borderColor: '#A6A6A4',
                    borderWidth: 1,
                    borderRadius: 8,
                  }}>
                  <Textarea
                    style={{
                      fontSize: 15,
                      // fontWeight: 'bold',
                      // fontFamily: "Lato-Bold",
                      color: GLOBAL.greyText,
                    }}
                    value={this.state.desk}
                    onChangeText={(desk) => this.setState({desk})}
                    rowSpan={4}
                    placeholder=""
                  />
                </View>
              </View>
            </View>

            <View style={{paddingBottom: 10}}>
              <FastImage
                style={{minHeight: 250}}
                source={{
                  uri: this.state.gambarUri,

                  priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.cover}
                onLoad={(e) => {
                  const height = e.nativeEvent.height;
                  console.log(e.nativeEvent.height);
                  this.setState({tinggiImage: height});
                }}
              />
            </View>
          </ScrollView>
        </Modal>

        <Modal
          //Audio
          keyboardTopOffset={-100}
          backdropPressToClose={false}
          swipeToClose={false}
          ref={'newTweetModal3'}
          backdrop={true}
          style={styles.modal}
          // isOpen={this.state.isModalOpen3}
          isOpen={false}
          onClosed={this.closeModal.bind(this)}>
          <ScrollView keyboardShouldPersistTaps="always">
            <View
              style={{
                alignSelf: 'flex-start',
                alignItems: 'center',
                flexDirection: 'row',
                padding: 5,
                paddingRight: 10,
              }}>
              <Button transparent onPress={this.closeModal.bind(this)}>
                <Icon name="close" style={{color: 'black', fontSize: 32}} />
              </Button>
              <View style={{flex: 1}} />
              <Thumbnail
                small
                source={{
                  uri: 'https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg',
                }}
              />
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                width: '100%',
              }}>
              <View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Judul
                  </Text>
                </View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 5,
                    marginBottom: 10,
                    height: 40,
                    alignContent: 'center',
                    backgroundColor: '#F7F7F7',
                    //height: 50,
                    width: width * 0.9,
                    borderColor: '#A6A6A4',
                    borderWidth: 1,
                    borderRadius: 8,
                  }}>
                  <TextInput
                    // textAlign={'left'}
                    style={{
                      // paddingTop: 5,
                      // paddingRight: 16,
                      // fontStyle:'italic',
                      flex: 1,
                      // alignSelf: 'center',
                      fontSize: 10,
                      // fontWeight: 'bold',
                      // fontFamily: "Lato-Bold",
                      color: GLOBAL.greyText,
                    }}
                    // onBlur={this.handleEndEditing}
                    // onEndEditing={this.handleEndEditing}
                    value={this.state.judul}
                    onChangeText={(judul) => this.setState({judul})}
                    autoFocus={this.state.autoFo}
                    underlineColorAndroid="transparent"
                  />
                </View>
              </View>

              <View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Descrição
                  </Text>
                </View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    width: width * 0.9,
                    marginTop: 5,
                    minHeight: 150,
                    backgroundColor: '#F7F7F7',
                    borderColor: '#A6A6A4',
                    borderWidth: 1,
                    borderRadius: 8,
                  }}>
                  <Textarea
                    style={{
                      fontSize: 15,
                      // fontWeight: 'bold',
                      // fontFamily: "Lato-Bold",
                      color: GLOBAL.greyText,
                    }}
                    value={this.state.desk}
                    onChangeText={(desk) => this.setState({desk})}
                    rowSpan={1}
                    placeholder=""
                  />
                </View>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'flex-end',
                width: width * 0.7,
                alignSelf: 'center',
                marginTop: 10,
              }}>
              <TouchableOpacity onPress={() => this._play()}>
                <View>
                  <Icon name="play" />
                </View>
              </TouchableOpacity>

              <View>
                <View style={{alignSelf: 'center'}}>
                  <Text
                    style={{
                      color: 'red',
                      fontWeight: 'bold',
                      fontSize: 20,
                    }}>
                    {this.fancyTimeFormat(this.state.currentTime)}
                  </Text>
                </View>
                <TouchableOpacity onPress={() => this._record()}>
                  <View
                    style={{
                      height: 80,
                      width: 80,
                      backgroundColor: 'red',
                      borderRadius: 50,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon name="mic" />
                  </View>
                </TouchableOpacity>
              </View>

              <TouchableOpacity onPress={() => this._stop()}>
                <View>
                  <Icon type="FontAwesome" name="stop" />
                </View>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </Modal>

        {this.state.isModalOpen == true ||
        this.state.isModalOpen2 == true ||
        this.state.isModalOpen3 == true ? (
          <View style={[styles.modalFooter, styles.floatingButton]}>
            <Button transparent small>
              <Icon
                name={
                  this.state.isModalOpen == true
                    ? 'videocam'
                    : this.state.isModalOpen2 == true
                    ? 'ios-image'
                    : ''
                }
              />
            </Button>

            {this.state.isModalOpen == true ? (
              <Button
                transparent
                small
                onPress={() => this.props.navigation.navigate('recordvideo')}>
                <Text>Anexar vídeo</Text>
              </Button>
            ) : this.state.isModalOpen2 == true ? (
              <Button transparent small onPress={this.handleUploadImage}>
                <Text>Anexar Photo</Text>
              </Button>
            ) : (
              <Button transparent small onPress={this.onStartRecord}>
                <Text> </Text>
              </Button>
            )}

            <View style={{flex: 1}} />

            {this.state.isModalOpen == true ? (
              <Button
                onPress={this.addArticleVideo}
                rounded
                style={{
                  color: '#4286f4',
                  height: 40,
                  width: 94,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: 'white'}}>Guardar </Text>
              </Button>
            ) : this.state.isModalOpen2 == true ? (
              <Button
                onPress={this.addArticle}
                rounded
                style={{
                  color: '#4286f4',
                  height: 40,
                  width: 94,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: 'white'}}>Guardar </Text>
              </Button>
            ) : (
              <Button
                onPress={this.addArticleVoice}
                rounded
                style={{
                  color: '#4286f4',
                  height: 40,
                  width: 94,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: 'white'}}>Guardar </Text>
              </Button>
            )}
          </View>
        ) : null}
        {this.props.myArticle.length > 0 ? (
          <View style={styles.fab}>
            <TouchableOpacity onPress={this.openModaldua}>
              <View style={[styles.fabitem, {backgroundColor: '#3F51B5'}]}>
                <Icon name="image" style={{color: 'white'}} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.openModal}>
              <View style={[styles.fabitem, {backgroundColor: 'red'}]}>
                <Icon name="videocam" style={{color: 'white'}} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.openModaltiga}>
              <View style={[styles.fabitem, {backgroundColor: 'green'}]}>
                <Icon name="mic" style={{color: 'white'}} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.openModalEmpat}>
              <View
                style={[styles.fabitem, {backgroundColor: GLOBAL.mainColor}]}>
                <Icon name="create" style={{color: 'white'}} />
              </View>
            </TouchableOpacity>
          </View>
        ) : null}

        <MyModal
          //Text
          outClick={this.closeModal}
          visible={this.state.modalTextVisible}>
          <ScrollView keyboardShouldPersistTaps="always">
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                width: '100%',
              }}>
              <View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Título do relatório de texto
                  </Text>
                </View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 5,
                    marginBottom: 10,
                    height: 40,
                    alignContent: 'center',
                    backgroundColor: '#F7F7F7',
                    //height: 50,
                    width: width * 0.9,
                    borderColor: '#A6A6A4',
                    borderWidth: 1,
                    borderRadius: 8,
                  }}>
                  <TextInput
                    // textAlign={'left'}
                    style={{
                      // paddingTop: 5,
                      // paddingRight: 16,
                      // fontStyle:'italic',
                      flex: 1,
                      // alignSelf: 'center',
                      fontSize: 10,
                      // fontWeight: 'bold',
                      // fontFamily: "Lato-Bold",
                      color: GLOBAL.greyText,
                    }}
                    // onBlur={this.handleEndEditing}
                    // onEndEditing={this.handleEndEditing}
                    value={this.state.judul}
                    onChangeText={(judul) => this.setState({judul})}
                    autoFocus={this.state.autoFo}
                    underlineColorAndroid="transparent"
                  />
                </View>
              </View>

              <View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Descrição
                  </Text>
                </View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    width: width * 0.9,
                    marginTop: 5,
                    minHeight: 150,
                    backgroundColor: '#F7F7F7',
                    borderColor: '#A6A6A4',
                    borderWidth: 1,
                    borderRadius: 8,
                  }}>
                  <Textarea
                    style={{
                      fontSize: 16,
                      // fontWeight: 'bold',
                      // fontFamily: "Lato-Bold",
                      color: GLOBAL.greyText,
                    }}
                    value={this.state.desk}
                    onChangeText={(desk) => this.setState({desk})}
                    rowSpan={6}
                    placeholder=""
                  />
                </View>
              </View>

              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    marginLeft: 12,
                    // marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Esconder o meu nome
                  </Text>
                </View>
                <View
                  style={{
                    // marginLeft: 12,
                    // marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Switch
                    onValueChange={(val) => this.setState({isHideUser: val})}
                    style={{marginLeft: 6}}
                    value={this.state.isHideUser ? true : false}
                    // thumbTintColor="#0000ff"
                    // tintColor="#ff0000"
                  />
                </View>
              </View>
            </View>
          </ScrollView>
          <View style={[styles.modalFooter, styles.floatingButton]}>
            <View style={{flex: 1}} />
            <Button
              onPress={this.addArticleText}
              rounded
              style={{
                color: '#4286f4',
                height: 40,
                width: 94,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: 'white'}}>Guardar </Text>
            </Button>
          </View>
        </MyModal>

        <MyModal
          //photo
          outClick={this.closeModal}
          visible={this.state.isModalOpen2}>
          <ScrollView keyboardShouldPersistTaps="always">
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                width: '100%',
              }}>
              <View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Título do relatório fotográfico
                  </Text>
                </View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 5,
                    marginBottom: 10,
                    height: 40,
                    alignContent: 'center',
                    backgroundColor: '#F7F7F7',
                    //height: 50,
                    width: width * 0.9,
                    borderColor: '#A6A6A4',
                    borderWidth: 1,
                    borderRadius: 8,
                  }}>
                  <TextInput
                    // textAlign={'left'}
                    style={{
                      // paddingTop: 5,
                      // paddingRight: 16,
                      // fontStyle:'italic',
                      flex: 1,
                      // alignSelf: 'center',
                      fontSize: 10,
                      // fontWeight: 'bold',
                      // fontFamily: "Lato-Bold",
                      color: GLOBAL.greyText,
                    }}
                    // onBlur={this.handleEndEditing}
                    // onEndEditing={this.handleEndEditing}
                    value={this.state.judul}
                    onChangeText={(judul) => this.setState({judul})}
                    autoFocus={this.state.autoFo}
                    underlineColorAndroid="transparent"
                  />
                </View>
              </View>

              <View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Descrição
                  </Text>
                </View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    width: width * 0.9,
                    marginTop: 5,
                    minHeight: 150,
                    backgroundColor: '#F7F7F7',
                    borderColor: '#A6A6A4',
                    borderWidth: 1,
                    borderRadius: 8,
                  }}>
                  <Textarea
                    style={{
                      fontSize: 15,
                      // fontWeight: 'bold',
                      // fontFamily: "Lato-Bold",
                      color: GLOBAL.greyText,
                    }}
                    value={this.state.desk}
                    onChangeText={(desk) => this.setState({desk})}
                    rowSpan={4}
                    placeholder=""
                  />
                </View>
              </View>

              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    marginLeft: 12,
                    // marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Esconder o meu nome
                  </Text>
                </View>
                <View
                  style={{
                    // marginLeft: 12,
                    // marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Switch
                    onValueChange={(val) => this.setState({isHideUser: val})}
                    style={{marginLeft: 6}}
                    value={this.state.isHideUser ? true : false}
                    // thumbTintColor="#0000ff"
                    // tintColor="#ff0000"
                  />
                </View>
              </View>
            </View>

            <View
              style={{
                paddingBottom: 10,
                marginTop: 10,
                position: 'relative',
              }}>
              <FastImage
                style={{minHeight: 250}}
                source={{
                  uri: this.state.gambarUri,
                  // uri:'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/nature-quotes-1557340276.jpg?crop=0.666xw:1.00xh;0.168xw,0&resize=640:*',
                  priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.cover}
                onLoad={(e) => {
                  const height = e.nativeEvent.height;
                  console.log(e.nativeEvent.height);
                  this.setState({tinggiImage: height});
                }}
              />

              <View
                style={{
                  minHeight: 80,
                  width: '100%',
                  position: 'absolute',
                  bottom: 20,
                  flexDirection: 'row',
                  backgroundColor:
                    this.state.arrayPhoto.length > 1
                      ? 'rgba(0,0,0,0.4)'
                      : 'transparent',
                  // justifyContent:'space-around'
                }}>
                <ScrollView horizontal></ScrollView>
              </View>
            </View>
          </ScrollView>

          {this.state.isModalOpen == true ||
          this.state.isModalOpen2 == true ||
          this.state.isModalOpen3 == true ? (
            <View style={[styles.modalFooter, styles.floatingButton]}>
              <Button transparent small>
                <Icon
                  name={
                    this.state.isModalOpen == true
                      ? 'videocam'
                      : this.state.isModalOpen2 == true
                      ? 'ios-image'
                      : ''
                  }
                />
              </Button>

              {this.state.isModalOpen == true ? (
                <Button
                  transparent
                  small
                  onPress={() => this.props.navigation.navigate('recordvideo')}>
                  <Text>Anexar vídeo</Text>
                </Button>
              ) : this.state.isModalOpen2 == true ? (
                <Button transparent small onPress={this.handleUploadImage}>
                  {/* <Text>{this.state.arrayPhoto.length > 0 ? "Tambah Photo" : "Attachment Photo"}</Text> */}
                  <Text>Anexar Photo</Text>
                </Button>
              ) : (
                <Button transparent small onPress={this.onStartRecord}>
                  <Text> </Text>
                </Button>
              )}

              <View style={{flex: 1}} />

              {this.state.isModalOpen == true ? (
                <Button
                  onPress={this.addArticleVideo}
                  rounded
                  style={{
                    color: '#4286f4',
                    height: 40,
                    width: 94,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{color: 'white'}}>Guardar </Text>
                </Button>
              ) : this.state.isModalOpen2 == true ? (
                <Button
                  onPress={this.addArticle}
                  rounded
                  style={{
                    color: '#4286f4',
                    height: 40,
                    width: 94,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{color: 'white'}}>Guardar </Text>
                </Button>
              ) : (
                <Button
                  onPress={this.addArticleVoice}
                  rounded
                  style={{
                    color: '#4286f4',
                    height: 40,
                    width: 94,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{color: 'white'}}>Guardar </Text>
                </Button>
              )}
            </View>
          ) : null}
        </MyModal>

        <MyModal
          //video
          outClick={this.closeModal}
          visible={this.props.modalVideoIsOpen}>
          <ScrollView keyboardShouldPersistTaps="always">
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                width: '100%',
                marginBottom: 10,
              }}>
              <View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Título do relatório video
                  </Text>
                </View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 5,
                    marginBottom: 10,
                    height: 40,
                    alignContent: 'center',
                    backgroundColor: '#F7F7F7',
                    //height: 50,
                    width: width * 0.9,
                    borderColor: '#A6A6A4',
                    borderWidth: 1,
                    borderRadius: 8,
                  }}>
                  <TextInput
                    // textAlign={'left'}
                    style={{
                      // paddingTop: 5,
                      // paddingRight: 16,
                      // fontStyle:'italic',
                      flex: 1,
                      // alignSelf: 'center',
                      fontSize: 10,
                      // fontWeight: 'bold',
                      // fontFamily: "Lato-Bold",
                      color: GLOBAL.greyText,
                    }}
                    // onBlur={this.handleEndEditing}
                    // onEndEditing={this.handleEndEditing}
                    value={this.state.judul}
                    onChangeText={(judul) => this.setState({judul})}
                    autoFocus={this.state.autoFo}
                    underlineColorAndroid="transparent"
                  />
                </View>
              </View>

              <View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Descrição
                  </Text>
                </View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    width: width * 0.9,
                    marginTop: 5,
                    minHeight: 150,
                    backgroundColor: '#F7F7F7',
                    borderColor: '#A6A6A4',
                    borderWidth: 1,
                    borderRadius: 8,
                  }}>
                  <Textarea
                    style={{
                      fontSize: 15,
                      // fontWeight: 'bold',
                      // fontFamily: "Lato-Bold",
                      color: GLOBAL.greyText,
                    }}
                    value={this.state.desk}
                    onChangeText={(desk) => this.setState({desk})}
                    rowSpan={4}
                    placeholder=""
                  />
                </View>
              </View>

              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    marginLeft: 12,
                    // marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Esconder o meu nome
                  </Text>
                </View>
                <View
                  style={{
                    // marginLeft: 12,
                    // marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Switch
                    onValueChange={(val) => this.setState({isHideUser: val})}
                    style={{marginLeft: 6}}
                    value={this.state.isHideUser ? true : false}
                    // thumbTintColor="#0000ff"
                    // tintColor="#ff0000"
                  />
                </View>
              </View>
            </View>
            {this.props.uriVid !== '' && <Video url={this.props.uriVid} />}
          </ScrollView>
          <View style={[styles.modalFooter, styles.floatingButton]}>
            <Button
              transparent
              small
              onPress={() => {
                this.props.navigation.navigate('recordvideo');
                this.props.model_video_open(false);
              }}>
              <Icon name="videocam" />
            </Button>
            <Button
              transparent
              small
              onPress={() => {
                this.props.navigation.navigate('recordvideo');
                this.props.model_video_open(false);
              }}>
              <Text>Anexar vídeo</Text>
            </Button>
            <View style={{flex: 1}} />
            <Button
              onPress={this.addArticleVideo}
              rounded
              style={{
                color: '#4286f4',
                height: 40,
                width: 94,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: 'white'}}>Guardar </Text>
            </Button>
          </View>
        </MyModal>

        <MyModal
          //Sound
          outClick={this.closeModal}
          visible={this.state.isModalOpen3}>
          <ScrollView keyboardShouldPersistTaps="always">
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                width: '100%',
                marginBottom: 10,
              }}>
              <View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Título do relatório de voz
                  </Text>
                </View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 5,
                    marginBottom: 10,
                    height: 40,
                    alignContent: 'center',
                    backgroundColor: '#F7F7F7',
                    //height: 50,
                    width: width * 0.9,
                    borderColor: '#A6A6A4',
                    borderWidth: 1,
                    borderRadius: 8,
                  }}>
                  <TextInput
                    // textAlign={'left'}
                    style={{
                      // paddingTop: 5,
                      // paddingRight: 16,
                      // fontStyle:'italic',
                      flex: 1,
                      // alignSelf: 'center',
                      fontSize: 10,
                      // fontWeight: 'bold',
                      // fontFamily: "Lato-Bold",
                      color: GLOBAL.greyText,
                    }}
                    // onBlur={this.handleEndEditing}
                    // onEndEditing={this.handleEndEditing}
                    value={this.state.judul}
                    onChangeText={(judul) => this.setState({judul})}
                    autoFocus={this.state.autoFo}
                    underlineColorAndroid="transparent"
                  />
                </View>
              </View>

              <View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Descrição
                  </Text>
                </View>
                <View
                  style={{
                    marginLeft: 12,
                    marginRight: 12,
                    width: width * 0.9,
                    marginTop: 5,
                    minHeight: 150,
                    backgroundColor: '#F7F7F7',
                    borderColor: '#A6A6A4',
                    borderWidth: 1,
                    borderRadius: 8,
                  }}>
                  <Textarea
                    style={{
                      fontSize: 15,
                      // fontWeight: 'bold',
                      // fontFamily: "Lato-Bold",
                      color: GLOBAL.greyText,
                    }}
                    value={this.state.desk}
                    onChangeText={(desk) => this.setState({desk})}
                    rowSpan={4}
                    placeholder=""
                  />
                </View>
              </View>

              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    marginLeft: 12,
                    // marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Text
                    style={{
                      color: GLOBAL.mainColor,
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}>
                    Esconder o meu nome
                  </Text>
                </View>
                <View
                  style={{
                    // marginLeft: 12,
                    // marginRight: 12,
                    marginTop: 10,
                    marginBottom: 5,
                  }}>
                  <Switch
                    onValueChange={(val) => this.setState({isHideUser: val})}
                    style={{marginLeft: 6}}
                    value={this.state.isHideUser ? true : false}
                    // thumbTintColor="#0000ff"
                    // tintColor="#ff0000"
                  />
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'flex-end',
                  width: width * 0.7,
                  alignSelf: 'center',
                  marginTop: 10,
                }}>
                <TouchableOpacity onPress={() => this._play()}>
                  <View>
                    <Icon name="play" />
                    <Text>Tocar</Text>
                  </View>
                </TouchableOpacity>

                <View>
                  <View style={{alignSelf: 'center'}}>
                    <Text
                      style={{
                        color: 'red',
                        fontWeight: 'bold',
                        fontSize: 20,
                      }}>
                      {this.fancyTimeFormat(this.state.currentTime)}
                    </Text>
                  </View>
                  {this.renderButtonRecord()}
                  {/* <TouchableOpacity onPress={() => this._record()}>
                                    <View style={{
                                        height: 80,
                                        width: 80,
                                        backgroundColor: 'red',
                                        borderRadius: 50,
                                        justifyContent: 'center',
                                        alignItems: 'center'

                                    }}>
                                        <Icon name="mic" />
                                    </View>
                                </TouchableOpacity> */}
                  {/* <TouchableOpacity onPress={() => this._stop()}>
                                    <View style={{alignSelf:'center'}}>
                                        <Icon type="FontAwesome" name="times" />
                                    </View>
                                </TouchableOpacity> */}
                </View>

                <TouchableOpacity onPress={() => this._stop()}>
                  <View>
                    <Icon type="FontAwesome" name="stop" />
                    <Text>Parar</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>

          <View style={[styles.modalFooter, styles.floatingButton]}>
            <View style={{flex: 1}} />

            <Button
              onPress={this.addArticleVoice}
              rounded
              style={{
                color: '#4286f4',
                height: 40,
                width: 94,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: 'white'}}>Guardar </Text>
            </Button>
          </View>
        </MyModal>
      </Container>
    );
  }

  renderButtonRecord = () => {
    if (
      !this.state.startRecord &&
      !this.state.recording &&
      !this.state.paused
    ) {
      return (
        <TouchableOpacity onPress={() => this._record()}>
          <View
            style={{
              height: 80,
              width: 80,
              backgroundColor: 'red',
              borderRadius: 50,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon name="mic" />
          </View>
        </TouchableOpacity>
      );
    } else if (
      this.state.startRecord &&
      this.state.recording &&
      !this.state.paused
    ) {
      return (
        <TouchableOpacity onPress={() => this._pause()}>
          <View
            style={{
              height: 80,
              width: 80,
              backgroundColor: 'red',
              borderRadius: 50,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon name="pause" />
          </View>
        </TouchableOpacity>
      );
    } else if (
      this.state.startRecord &&
      !this.state.recording &&
      this.state.paused
    ) {
      return (
        <TouchableOpacity onPress={() => this._resume()}>
          <View
            style={{
              height: 80,
              width: 80,
              backgroundColor: 'red',
              borderRadius: 50,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon name="mic" />
          </View>
        </TouchableOpacity>
      );
    }
  };
}

const styles = StyleSheet.create({
  topMargin: {
    backgroundColor: 'white',
    zIndex: -1,
    borderBottomColor: 'grey',
    borderBottomWidth: 0.4,
  },
  content: {
    padding: 10,
    backgroundColor: 'white',
  },
  heading: {
    fontSize: 32,
    fontWeight: '400',
    marginBottom: 30,
  },
  tweet: {
    paddingTop: 20,
    paddingBottom: 5,
    paddingLeft: 5,
    paddingRight: 5,
    borderBottomColor: '#bbb',
    borderBottomWidth: StyleSheet.hairlineWidth,
    flexDirection: 'column',
  },
  tweetText: {
    marginTop: 10,
    fontSize: 18,
    color: '#555',
  },
  tweetFooter: {
    flexDirection: 'row',
    // justifyContent: "flex-start",
    justifyContent: 'space-around',
    padding: 0,
  },
  badgeCount: {
    fontSize: 12,
    paddingLeft: 5,
  },
  footerIcons: {
    flexDirection: 'row',
    alignItems: 'center', //center
  },
  modalFooter: {
    backgroundColor: 'white',
    borderTopWidth: 0.4,
    borderTopColor: 'grey',
    // elevation: 3,
    // shadowColor: "#000",
    // shadowOffset: { width: 0, height: 0.2 },
    //  shadowOpacity: 0.3,
    // shadowRadius: 2,
    height: 54,
    width,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 5,
  },
  modal: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: 'absolute',
    zIndex: 4,
    elevation: 4,
    // paddingBottom: 145
    // height: Dimensions.get("window").height //- Expo.Constants.statusBarHeight,
    // marginTop: Expo.Constants.statusBarHeight / 2
  },
  inputContainer: {
    borderBottomColor: 'grey',
    borderBottomWidth: 0.5,
  },
  floatingButton: {
    position: 'absolute',
    bottom: 0,
    zIndex: 99,
  },
  fab: {
    height: 50,
    width: width * 0.7,
    alignSelf: 'center',
    // borderRadius: 200,
    position: 'absolute',
    bottom: 20,
    // left: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  fabitem: {
    height: 50,
    width: 50,
    borderRadius: 200,
    // position: 'absolute',
    // bottom: 20,
    // left: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

//export default HomeScreen
const mapStateToProps = (state) => {
  const {
    relawanid,
    token,
    email,
    fasilitator,
    eposko,
    jabatan,
    myArticle,
    photo,
    latitude,
    longitude,
  } = state.anggota;
  const {uriVid, modalVideoIsOpen} = state.auth;
  return {
    modalVideoIsOpen,
    relawanid,
    token,
    email,
    fasilitator,
    eposko,
    jabatan,
    myArticle,
    photo,
    uriVid,
    latitude,
    longitude,
  };
};

export default connect(mapStateToProps, {
  inputArticleList,
  save_uri_video,
  update_list_article,
  model_video_open,
  save_latitude,
  save_longitude,
})(withNavigationFocus(Draftscreen));
