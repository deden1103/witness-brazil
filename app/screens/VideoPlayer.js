
import React, { Component } from 'react';
import {
    View, Text, WebView, PixelRatio, Dimensions, TouchableOpacity
} from 'react-native';
// import YouTube, { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';
const { width, height } = Dimensions.get('window')
class VideoPlayer extends Component {
    static navigationOptions = {
        header: null
    };

    constructor() {
        super()
        this.state = {
            isReady: false,
            status: null,
            quality: null,
            error: null,
            isPlaying: true,
            isLooping: true,
            duration: 0,
            currentTime: 0,
            fullscreen: false,
            containerMounted: false,
            containerWidth: null,
            height: 249
        }
    }



    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.url !== nextProps.url) {
            return true
        }
        return true
    }

    handleReady = () => {
        setTimeout(() => this.setState({ height: 250 }), 500);
    }

    //hia2D22WQfM

    handlePress = (e) => {
        if (typeof this.props.onPress === 'function') {
          this.props.onPress(e)
        }
      }

    render() {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                {/* <YouTube
                    ref={component => {
                        this._youTubeRef = component;
                    }}
                    apiKey="AIzaSyAdOug4XfveBAwd_i8gAMV2ZOJbHz7cRjk"
                    videoId={this.props.url}//"hia2D22WQfM"//{this.props.navigation.getParam('vidId')}
                    play={this.state.isPlaying}
                    loop={this.state.isLooping}
                    fullscreen={this.state.fullscreen}
                    controls={1}
                    showinfo={true}
                    showFullscreenButton={true}
                    fullscreen={this.state.fullscreen}
                    style={{ alignSelf: 'stretch', height: this.state.height, backgroundColor: 'black' }}
                    onError={e => this.setState({ error: e.error })}
                    onReady={this.handleReady}
                    onChangeState={e => this.handlePress(e)}
                    onChangeQuality={e => this.setState({ quality: e.quality })}
                    onChangeFullscreen={e => this.setState({ fullscreen: e.isFullscreen })}
                    onProgress={e => this.setState({ duration: e.duration, currentTime: e.currentTime })}
                /> */}

            </View>
        );
    }
}

export default VideoPlayer

/**
  <TouchableOpacity onPress={() => this.setState({ fullscreen: !this.state.fullscreen })}>
                    <View style={{
                        width: width * 0.3,
                        alignSelf: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 40,
                        backgroundColor: 'green',
                        padding: 10,
                        borderRadius: 15
                    }}>
                        <Text style={{ color: 'white' }}>
                            Fullscreen
                    </Text>
                    </View>
                </TouchableOpacity>
 */