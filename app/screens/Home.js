
import React, { Component } from 'react';
import {
    Container,
    Content
} from 'native-base';

import {
    Text,
    View,
    StatusBar,
    Alert, Dimensions
} from 'react-native';
import axios from 'axios';
import ListCustome from '../components/ListCustome';
import { Headerpolos } from '../particles/Headerpolos';
const { width, height } = Dimensions.get('window')

class Home extends Component {

    constructor() {
        super()
        this.state = {
            daftarevent: [],
            no: 1
        }
    }


    componentDidMount() {
        // this.fetchingEvent()
    }



    fetchingEvent = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}event/getallevent`)
            if (response.data.success == true) {
                this.setState({
                    daftarevent: response.data.data
                })
                // console.log(response)
            } else {
                // console.log(response)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }


    render() {
        return (

            <Container style={styles.container}>
                {/* <StatusBar
                    backgroundColor={GLOBAL.mainColor}
                    animated
                /> */}
                <Headerpolos
                    txtTitle="Home"
                />
                <Content style={{ flex: 1 }}>
                    <View style={{ flex: 1, margin: 5 }}>

                        {this.state.daftarevent.map((res, i) => {
                            return (
                                <ListCustome
                                    key={i}
                                    comments={10}
                                    toEvent={() => this.props.navigation.navigate('event', { eventIdLempar: res.eventId })}
                                    title={res.eventName}
                                    imgUri={`https://i.imgur.com/yxovJ4S.jpg`}
                                />
                            )
                        })}

                    </View>
                </Content>

            </Container>
        );
    }
}

export default Home

const styles = {
    container: {
        flex: 1,
        backgroundColor: 'white'
    }
}

/**
 * res.eventPhoto
 * https://i.imgur.com/Xulubox.jpg
 * https://i.imgur.com/sduLRvf.jpg
 * https://i.imgur.com/yxovJ4S.jpg
 */





















