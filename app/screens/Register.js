import React, { Component } from "react";
import {
    StyleSheet, Alert, StatusBar, Animated, Easing, View,
    TextInput,
    TouchableOpacity,
    Dimensions, Picker
} from "react-native";
// import getTheme from "../native-base-theme/components";
// import platform from "../native-base-theme/variables/platform";
import material from "../native-base-theme/variables/material";
import { NavigationActions } from "react-navigation";
import {
    Container,
    Button,
    Text,
    Header,
    Left,
    Body,
    Right,
    Icon,
    Content,
    Spinner
} from "native-base";
import { connect } from "react-redux";
import { loginsuccess3, loginsuccess4, loginsuccess, loginsuccess5, loginsuccess7 } from '../rdx/actions';
import axios from 'axios';
import Image from 'react-native-remote-svg';
import AwesomeAlert from 'react-native-awesome-alerts';
import OneSignal from 'react-native-onesignal';
const { width, height } = Dimensions.get('window')
class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showAlert: false,
            loading: false,
            namalengkap: '',
            email: '',
            kabupaten: '',
            kecamatan: '',
            organisasi: '',
            handphone: '',
            password: '',
            confirmpassword: '',
            kabupatenlist: [],
            kecamatanlist: [],
            organisasilist: [],
            osid: ''
        }
        this.animatedValue = new Animated.Value(0)
    }

    componentDidMount() {
        this.animate()
        this.fetchDetailKabupaten()
        this.fetchDetailOrganisasi()
        // this.getosid()
    }

    onValueChange = async  value => {
        await this.setState({ kabupaten: value })
        await this.fetchDetailKecamatan(this.state.kabupaten)
    }


    // getosid = () => {
    //     OneSignal.init("6169230c-f70c-4ecc-88f7-0828c79945ff", { kOSSettingsKeyAutoPrompt: true });
    //     OneSignal.getPermissionSubscriptionState((subscriptionState) => {
    //         this.setState({ osid: subscriptionState.userId })//, () => Alert.alert(this.state.osid)
    //     })
    // }


    handleRegister = async () => {
        try {
            this.setState({ loading: true })
            //  if (this.state.emailorphone !== '' && this.state.password !== '') {
            let data = JSON.stringify({
                namalengkap: this.state.namalengkap,
                email: this.state.email,
                kabupaten: this.state.kabupaten,
                kecamatan: this.state.kecamatan,
                organisasi: this.state.organisasi,
                handphone: this.state.handphone,
                password: this.state.password,
                osid:this.state.osid,
                confirmpassword: this.state.confirmpassword
            })
            let loginResponse = await axios.post(`${GLOBAL.apiUrl}authrelawan/register`, data, {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            if (loginResponse.data.success == true) {
                // console.log(loginResponse)
                this.setState({ loading: false })
                Alert.alert('Register Berhasil')
                this.props.navigation.navigate('login')
            } else {
                // console.log(loginResponse)
                Alert.alert('Register Gagal')
                this.setState({ loading: false })
            }

            // } else {
            //     this.setState({ showAlert: !this.state.showAlert, loading: false })
            // }
        } catch (e) {
            // console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }
    }


    fetchDetailKecamatan = async (id) => {
        try {
            const response = await axios.get(`http://cms.deltaone.id/data_kecamatan/api/${id}`)
            if (response) {
                this.setState({
                    kecamatanlist: response.data
                })
            } else {
                // console.log(response)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }

    fetchDetailKabupaten = async () => {
        try {
            const response = await axios.get(`http://cms.deltaone.id/data_kecamatan/api_kabupaten`)
            if (response) {
                this.setState({
                    kabupatenlist: response.data
                })
                // console.log(response)
            } else {
                // console.log(response)
            }
            //console.log(response)
        }
        catch (e) {
            // console.log(e)
        }
    }

    //http://cms.deltaone.id/data_kecamatan/api_organisasi

    fetchDetailOrganisasi = async () => {
        try {
            const response = await axios.get(`http://cms.deltaone.id/data_kecamatan/api_organisasi`)
            if (response) {
                this.setState({
                    organisasilist: response.data
                })
                // console.log(response)
            } else {
                // console.log(response)
            }
            //console.log(response)
        }
        catch (e) {
            // console.log(e)
        }
    }


    animate() {
        this.animatedValue.setValue(0)
        Animated.timing(
            this.animatedValue,
            {
                toValue: 1,
                duration: 500,
                easing: Easing.linear
            }
        ).start()
    }

    toHome = () => {
        this.props.navigation.navigate('ho')
    }
    handleLogin2 = async () => {
        try {
            this.setState({ loading: true })
            if (this.state.emailorphone !== '' && this.state.password !== '') {
                let data = JSON.stringify({
                    emailorphone: this.state.emailorphone,
                    password: this.state.password
                })
                let loginResponse = await axios.post(`${GLOBAL.apiUrl}authrelawan/login`, data, {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
                if (loginResponse.data.success == true) {
                    // console.log(loginResponse)
                    this.setState({ loading: false })
                    this.props.loginsuccess3(loginResponse.data.relawanId)
                    this.props.loginsuccess4(loginResponse.data.photoRelawan)
                    this.props.loginsuccess(loginResponse.data.namaRelawan)
                    this.props.loginsuccess5(loginResponse.data.token)
                    this.props.loginsuccess7(loginResponse.data.fasilitator)
                    if (loginResponse.data.fasilitator == null) {
                        this.props.navigation.navigate('home2')
                    } else {
                        this.props.navigation.navigate('home')
                    }
                    // this.props.navigation.navigate('home')
                    //setTimeout(() => { this.props.navigation.navigate('home') }, 1000)
                } else {
                    // console.log(loginResponse)
                    Alert.alert('Login falhou, e-mail / palavras-passe erradas')
                    this.setState({ loading: false })
                }

            } else {
                this.setState({ showAlert: !this.state.showAlert, loading: false })
            }
        } catch (e) {
            // console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }
    }

    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };

    render() {
        const marginLeft = this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [-50, 10]
        })
        return (
            <Container style={styles.topMargin}>
                {/* <StatusBar

                    backgroundColor='transparent'
                    animated
                /> */}
                <Header noShadow style={{ backgroundColor: "white" }}>
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="md-arrow-back" style={{ color: GLOBAL.mainColor }} />
                        </Button>
                    </Left>
                    <Body style={{ flex: 1 }}>
                        {/* <Icon
                            name="logo-twitter"
                            style={{ alignSelf: "center", color: "#4286f4" }}
                        /> */}
                    </Body>
                    <Right style={{ flex: 1 }}>
                        {/* <Button transparent>
                            <Text style={{ color: "#4286f4" }}>Sign up</Text>
                        </Button>
                        <Button transparent>
                            <Icon name="more" style={{ color: "#4286f4" }} />
                        </Button> */}
                    </Right>
                </Header>
                <Content style={styles.content}>
                    <View >
                        <Animated.Text style={[{ translateY: marginLeft }, styles.heading]}>Register Delta One</Animated.Text>
                    </View>
                    <View>
                        <View style={styles.inputContainer}>
                            <TextInput style={styles.inputs}
                                value={this.state.namalengkap}
                                onChangeText={(namalengkap) => this.setState({ namalengkap })}
                                placeholder="Nome completop"
                                keyboardType="email-address"
                                underlineColorAndroid='transparent' />
                        </View>
                        {/* <View style={styles.inputContainer}>
                            <TextInput style={styles.inputs}
                                value={this.state.email}
                                onChangeText={(email) => this.setState({ email })}
                                placeholder="Email"
                                keyboardType="email-address"
                                underlineColorAndroid='transparent' />
                        </View> */}
                        <View style={styles.inputContainer}>
                            <Picker
                                selectedValue={this.state.kabupaten}
                                style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue)}>
                                <Picker.Item label="Pilih Kabupaten" value="0" />
                                {this.state.kabupatenlist.map((res, i) => (
                                    <Picker.Item key={i} label={res.msrgName} value={res.msrgId} />
                                ))}
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            {/* <TextInput style={styles.inputs}
                                value={this.state.kecamatan}
                                onChangeText={(kecamatan) => this.setState({ kecamatan })}
                                placeholder="Kecamatan"
                                keyboardType="email-address"
                                underlineColorAndroid='transparent' /> */}
                            <Picker
                                selectedValue={this.state.kecamatan}
                                style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                onValueChange={(itemValue, itemIndex) => this.setState({ kecamatan: itemValue })}>
                                <Picker.Item label="Pilih Kecamatan" value="0" />
                                {this.state.kecamatanlist.map((res, i) => (
                                    <Picker.Item key={i} label={res.msdiName} value={res.msdiId} />
                                ))}
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            {/* <TextInput style={styles.inputs}
                                value={this.state.organisasi}
                                onChangeText={(organisasi) => this.setState({ organisasi })}
                                placeholder="Organisasi"
                                keyboardType="email-address"
                                underlineColorAndroid='transparent' /> */}
                            <Picker
                                selectedValue={this.state.organisasi}
                                style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                onValueChange={(itemValue, itemIndex) => this.setState({ organisasi: itemValue })}>
                                <Picker.Item label="Pilih Organisasi" value="0" />
                                {this.state.organisasilist.map((res, i) => (
                                    <Picker.Item key={i} label={res.namaOrganisasi} value={res.organisasiId} />
                                ))}
                            </Picker>
                        </View>
                        <View style={styles.inputContainer}>
                            <TextInput style={styles.inputs}
                                value={this.state.handphone}
                                onChangeText={(handphone) => this.setState({ handphone })}
                                placeholder="Celular"
                                keyboardType="email-address"
                                underlineColorAndroid='transparent' />
                        </View>
                        <View style={styles.inputContainer}>
                            <TextInput style={styles.inputs}
                                secureTextEntry={true}
                                value={this.state.password}
                                onChangeText={(password) => this.setState({ password })}
                                placeholder="Senha (Palavra-passe)"
                                // keyboardType="email-address"
                                underlineColorAndroid='transparent' />
                        </View>
                        <View style={styles.inputContainer}>
                            <TextInput style={styles.inputs}
                                secureTextEntry={true}
                                value={this.state.confirmpassword}
                                onChangeText={(confirmpassword) => this.setState({ confirmpassword })}
                                placeholder="Confirm Password"
                                // keyboardType="email-address"
                                underlineColorAndroid='transparent' />
                        </View>

                        <TouchableOpacity
                            style={[styles.buttonContainer, styles.loginButton, { alignSelf: 'center' }]}
                            onPress={this.handleRegister}>
                            {this.state.loading ? <Spinner color="white" /> : <Text style={styles.loginText}>Submit</Text>}
                        </TouchableOpacity>

                    </View>
                </Content>
                {/* <Footer style={styles.footer}>
                    <Button
                        rounded
                        style={{ backgroundColor: "#4286f4", marginLeft: 20 }}
                        onPress={this.handleLogin2}
                    >
                        {this.state.loading ? <Spinner color="white" /> : <Text>Submit</Text>}
                    </Button>
                </Footer> */}
                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title="Aviso"
                    message="E-mail / Senha Necessário!"
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={false}
                    showConfirmButton={true}
                    cancelText="OK"
                    confirmText="OK"
                    confirmButtonColor="#DD6B55"
                    onCancelPressed={() => {
                        this.hideAlert()
                    }}
                    onConfirmPressed={() => {
                        this.hideAlert()
                    }}
                />
            </Container >
        );
    }
}


const styles = StyleSheet.create({
    topMargin: {
        // marginTop: 25
    },
    content: {
        padding: 10,
        backgroundColor: "white"
    },
    heading: {
        fontSize: 32,
        fontWeight: "400",
        marginBottom: 30
    },
    footer: {
        backgroundColor: "white",
        elevation: 3,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0.2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        height: 60,
        padding: 5,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-end"
    },



    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#B0E0E6',
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#E6ECF0',
        borderRadius: 30,
        borderBottomWidth: 1,
        height: 45,
        marginBottom: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#FFFFFF',
        flex: 1,
    },
    icon: {
        width: 30,
        height: 30,
    },
    inputIcon: {
        marginLeft: 15,
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: width * 0.945,
        borderRadius: 30,
    },
    loginButton: {
        backgroundColor: '#3498db',
    },
    fabookButton: {
        backgroundColor: "#3b5998",
    },
    googleButton: {
        backgroundColor: "#ff0000",
    },
    loginText: {
        color: 'white',
    },
    restoreButtonContainer: {
        width: 250,
        marginBottom: 15,
        alignItems: 'flex-end'
    },
    socialButtonContent: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    socialIcon: {
        color: "#FFFFFF",
        marginRight: 5
    }
});

const mapStateToProps = (state) => {
    const { token, fasilitator } = state.anggota;
    return { token, fasilitator };
};

export default connect(mapStateToProps, { loginsuccess3, loginsuccess4, loginsuccess, loginsuccess5, loginsuccess7 })(Register);
