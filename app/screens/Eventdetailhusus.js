import React, { Component } from "react";
//import moment from "moment";
import Modal from "react-native-modalbox";
import {
    StyleSheet,
    View,
    Text,
    FlatList,
    TouchableHighlight,
    Platform, StatusBar, Dimensions, Alert
} from "react-native";
import {
    Container,
    Header,
    Body,
    Content,
    Left,
    Title,
    Thumbnail,
    Icon,
    Spinner,
    Fab,
    Button,
    Footer,
    Input,
    Right
} from "native-base";
import axios from 'axios';
import { connect } from "react-redux";
// import ScrollableTabView, {
//     ScrollableTabBar
// } from "react-native-scrollable-tab-view";
const { width, height } = Dimensions.get('window')



class Eventdetailhusus extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            dataevent: {},
            comments: [],
            relawan: {},
            isShowReplay: 0,
            isModalOpen: false,
            isModalOpenReplay: false,
            commentid: 0,
            comm: '',
            rep: ''
        }

    }


    componentDidMount() {
        this.fetchDetail()
    }



    fetchDetail = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}event/getoneevent?eventid=${this.props.navigation.getParam('eventIdLempar')}`)
            if (response.data.success == true) {//this.props.navigation.getParam('eventIdLempar')
                this.setState({
                    dataevent: response.data.data,
                    comments: response.data.data.comments,
                    relawan: response.data.data.relawan
                })
            } else {
                // console.log(response)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }


    handlePostComment = async (evi) => {
        try {
            let data = JSON.stringify({
                eventId: evi,
                commentDesc: this.state.comm,
                relawanId: this.props.relawanid

            })
            if (this.state.comm !== '') {
                let loginResponse = await axios.post(`${GLOBAL.apiUrl}event/sendcomment`, data, {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
                if (loginResponse.data.success == true) {
                    this.fetchDetail()
                    // console.log(loginResponse)
                    this.setState({ comm: '' })
                    this.closeModal()

                    // this.component._root.scrollToEnd()
                } else {
                    Alert.alert('Kirim komentar Gagal / Anda Belum Login')
                    // console.log(loginResponse)
                }
            } else {
                Alert.alert('A mensagem não pode estar vazia')
            }
        } catch (e) {
            // console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }
    }


    handlePostReplay = async () => {
        try {
            let data = JSON.stringify({
                commentId: this.state.commentid,
                replayDesc: this.state.rep,
                relawanId: this.props.relawanid

            })
            if (this.state.rep !== '') {
                let loginResponse = await axios.post(`${GLOBAL.apiUrl}event/sendreplay`, data, {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
                if (loginResponse.data.success == true) {
                    this.fetchDetail()
                    this.closeModal2()
                    // console.log(loginResponse)
                    this.setState({ rep: '' })
                } else {
                    Alert.alert('Kirim komentar Gagal / Anda Belum Login')
                    // console.log(loginResponse)
                }
            } else {
                Alert.alert('A mensagem não pode estar vazia')
            }
        } catch (e) {
            // console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }
    }


    renderReplay = (replay) => {
        return replay.map((r, i) => (
            <View key={i} style={{ marginTop: 5, marginLeft: 20 }}>
                <Text style={{ fontWeight: "bold" }}>{r.relawan.namaRelawan} : </Text>
                <Text>{r.replayDesc}</Text>
            </View>
        ))
    }


    openModal = () => {
        this.setState({ isModalOpen: !this.state.isModalOpen })
    }

    closeModal = () => {
        this.setState({ isModalOpen: false })
    }

    openModal2 = (cid) => {
        this.setState({ isModalOpenReplay: !this.state.isModalOpenReplay, commentid: cid })
    }

    closeModal2 = () => {
        this.setState({ isModalOpenReplay: false })
    }



    render() {
        const res = this.state.dataevent
        const rel = this.state.relawan
        return (
            <Container>
                {/* <StatusBar
                    backgroundColor='white'
                    animated
                /> */}
                <Header style={{
                    backgroundColor: "white", borderBottomColor: 'black',
                    borderBottomWidth: 0.6
                }}>
                    <Left>
                        <Button
                            onPress={() => this.props.navigation.goBack()}
                            transparent>
                            <Icon name="arrow-back" style={{ color: 'blue' }} />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={{ color: 'black' }}>Relatório</Title>
                    </Body>
                    <Right />
                </Header>
                <Content style={{ backgroundColor: "white" }}>
                    <View style={styles.tweetHead}>
                        <Thumbnail source={{ uri: rel.photoRelawan }} />
                        <View
                            style={{
                                flex: 1,
                                justifyContent: "center",
                                paddingLeft: 10,
                                height: 56
                            }}
                        >
                            <Text style={{ fontWeight: "bold", fontSize: 20 }}>
                                {res.eventName}
                            </Text>

                            <Text style={{ color: "#999", fontSize: 18 }}>
                                {rel.namaRelawan}
                            </Text>
                        </View>
                    </View>
                    <View>
                        <Text style={{ fontSize: 22, padding: 10 }}>
                            {}
                        </Text>
                    </View>
                    <View style={styles.timeStamp}>
                        <Text style={{ color: "#888", fontSize: 16 }}>
                            {}
                        </Text>
                    </View>
                    <View style={styles.timeStamp}>
                        {/* <Text style={{ fontWeight: "bold", fontSize: 16, paddingRight: 5 }}>
                            {}
                        </Text>
                        <Text style={{ color: "#888", fontSize: 16, paddingRight: 20 }}>
                            Retweets
                         </Text>
                        <Text style={{ fontWeight: "bold", fontSize: 16, paddingRight: 5 }}>
                            {}
                        </Text>
                        <Text style={{ color: "#888", fontSize: 16 }}>Likes</Text> */}
                    </View>
                    <View style={styles.tweetFooter}>
                        <View>
                            <Button
                                transparent
                                dark
                                style={{ paddingBottom: 0, paddingTop: 0 }}
                            >
                                <Icon name="ios-text-outline" />

                            </Button>
                        </View>
                        {/*  <View>
                            <Button transparent dark onPress={this.openModal}>
                                <Text>Komentari</Text>
                            </Button>
                        </View>
                         
                        <View>
                            <Button transparent dark>
                                <Icon name="ios-repeat" />
                            </Button>
                        </View>
                        <View>
                            <Button transparent dark>
                                <Icon name="ios-heart-outline" />
                            </Button>
                        </View>
                        <View>
                            <Button transparent dark>
                                <Icon name="ios-mail-outline" />
                            </Button>
                        </View> */}
                    </View>
                    <View>


                        {this.state.comments.map((val, i) => (
                            <View key={i} style={styles.tweetReply}>
                                <Thumbnail small source={{ uri: '' }} />
                                <View
                                    style={{
                                        justifyContent: "flex-start",
                                        alignItems: "flex-start",
                                        paddingLeft: 10,
                                        paddingRight: 10,
                                        width: "93%"
                                    }}
                                >
                                    <View style={{ flexDirection: "row", maxHeight: 22 }}>
                                        <Text style={{ fontWeight: "bold" }}>
                                            {val.relawan.namaRelawan} :
                                        </Text>
                                        {/* <Text
                                            style={{ color: "#888", flex: 1, paddingLeft: 5 }}
                                        >
                                            {}
                                        </Text> */}
                                    </View>
                                    <View
                                        style={{
                                            flexDirection: "row",
                                            //paddingTop: 5,
                                            maxHeight: 22
                                        }}
                                    >
                                        {/* <Text style={{ color: "#888" }}>Replying to</Text> */}
                                        <Text style={{ color: "#4286f4", flex: 1 }}>
                                            {}
                                        </Text>
                                    </View>
                                    <Text style={{ paddingTop: 5 }}>{val.commentDesc}</Text>

                                    {
                                        //this.renderReplay(val.replays)
                                    }
                                    <View
                                        style={StyleSheet.flatten([
                                            styles.tweetFooter,
                                            { width: "100%", justifyContent: 'flex-start' }
                                        ])}
                                    >
                                        <View style={styles.footerIcons}>

                                            {/* <Button transparent dark>
                                                <Icon
                                                    name="ios-text-outline"
                                                    style={{ fontSize: 20 }}
                                                />
                                                <Text style={{ fontSize: 14 }}>{}</Text>
                                            </Button> */}
                                        </View>
                                        {
                                            //      <View style={{ flexDirection: 'row' }}>
                                            //      <Text>Nama relawan : </Text>
                                            //      <Text>ini isi balasan</Text>
                                            //  </View>
                                            // this.state.isShowReplay == val.commentId &&
                                            // <View style={{
                                            //     width: width * 0.7,
                                            //     borderRadius: 10,
                                            //     justifyContent: 'center',
                                            //     padding: 5
                                            // }}>
                                            //     <InputWhiteBgStyle
                                            //         valueInput={this.state.rep}
                                            //         onChangeInput={(rep) => this.setState({ rep })}
                                            //         pHolder="balas komentar..."
                                            //         style={{
                                            //             backgroundColor: 'transparent',
                                            //             borderColor: 'grey', borderWidth: 1,
                                            //             borderRadius: 30, height: 45
                                            //         }} />
                                            // </View>
                                        }

                                        {/* <View>

                                            <Button
                                                transparent
                                                dark
                                                onPress={() => this.openModal2(val.commentId)}
                                            >
                                                <Text>Balas</Text>
                                            </Button>

                                        </View> */}

                                        {/* <View style={styles.footerIcons}>
                                            <Button transparent dark>
                                                <Icon name="ios-repeat" style={{ fontSize: 20 }} />
                                                <Text style={{ fontSize: 14 }}>
                                                    {}
                                                </Text>
                                            </Button>
                                        </View>
                                        <View style={styles.footerIcons}>
                                            <Button transparent dark>
                                                <Icon
                                                    name="ios-heart-outline"
                                                    style={{ fontSize: 20 }}
                                                />
                                                <Text style={{ fontSize: 14 }}>{}</Text>
                                            </Button>
                                        </View>
                                        <View style={styles.footerIcons}>
                                            <Button transparent dark>
                                                <Icon
                                                    name="ios-mail-outline"
                                                    style={{ fontSize: 20 }}
                                                />
                                            </Button>
                                        </View> */}
                                    </View>
                                </View>
                            </View>
                        ))}





                    </View>
                </Content>
                {
                    // this.state.isShowReplay ? <FloatingButtonThree
                    //     cancel={() => this.setState({ isShowReplay: 0 })}
                    //     kirim={this.handlePostReplay} /> : <FloatingInput
                    //         // value={this.state.comm}

                    //         onChange={(comm) => this.setState({ comm })}
                    //         holder="Tambah Komentar"
                    //         toSend={() => this.handlePostComment(res.eventId)} />
                }

                <Modal
                    ref={"newTweetModal"}
                    backdrop={true}
                    position={"bottom"}
                    style={{ height: 300 }}
                    isOpen={this.state.isModalOpen}
                    onClosed={this.closeModal.bind(this)}>
                    <View
                        style={{
                            alignSelf: "flex-start",
                            alignItems: "center",
                            flexDirection: "row",
                            padding: 5,
                            paddingRight: 10
                        }}>
                        <Button transparent onPress={this.closeModal.bind(this)}>
                            <Icon name="close" style={{ color: "black", fontSize: 32 }} />
                        </Button>
                        <View style={{ flex: 1 }} />
                        <Thumbnail
                            small
                            source={{
                                uri:
                                    "https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg"
                            }} />
                    </View>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: "flex-start",
                            alignItems: "flex-start",
                            width: "100%",

                        }} >
                        <Input
                            style={{
                                flex: 1,
                                width: "100%",
                                fontSize: 24,
                                alignContent: "flex-start",
                                justifyContent: "flex-start",
                                textAlignVertical: "top",
                                margin: 5,

                            }}
                            multiline
                            placeholder="Tambah Komentar"
                            onChangeText={comm => this.setState({ comm })} />
                    </View>
                    <View>
                        <View style={{ flex: 1 }} />

                        <Button
                            onPress={() => this.handlePostComment(this.props.navigation.getParam('eventIdLempar'))}
                            rounded
                            style={{ color: "#4286f4", height: 40, width: 94, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', margin: 6 }}>
                            <Text style={{ color: "white" }}>Enviar</Text>
                        </Button>
                    </View>
                </Modal>

                <Modal
                    ref={"newTweetModal"}
                    backdrop={true}
                    position={"bottom"}
                    style={{ height: 300 }}
                    isOpen={this.state.isModalOpenReplay}
                    onClosed={this.closeModal2.bind(this)}>
                    <View
                        style={{
                            alignSelf: "flex-start",
                            alignItems: "center",
                            flexDirection: "row",
                            padding: 5,
                            paddingRight: 10
                        }}>
                        <Button transparent onPress={this.closeModal2.bind(this)}>
                            <Icon name="close" style={{ color: "black", fontSize: 32 }} />
                        </Button>

                        <View style={{ flex: 1 }} />
                        <Thumbnail
                            small
                            source={{
                                uri:
                                    "https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg"
                            }} />
                    </View>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: "flex-start",
                            alignItems: "flex-start",
                            width: "100%",

                        }} >
                        <Input
                            style={{
                                flex: 1,
                                width: "100%",
                                fontSize: 24,
                                alignContent: "flex-start",
                                justifyContent: "flex-start",
                                textAlignVertical: "top",
                                margin: 5,

                            }}
                            multiline
                            placeholder="Balas Komentar"
                            onChangeText={rep => this.setState({ rep })} />
                    </View>
                    <View>
                        <View style={{ flex: 1 }} />

                        <Button
                            onPress={this.handlePostReplay}
                            rounded
                            style={{ color: "#4286f4", height: 40, width: 94, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', margin: 6 }}>
                            <Text style={{ color: "white" }}>Enviar</Text>
                        </Button>
                    </View>
                </Modal>


                {this.state.isModalOpen || this.props.fasilitator == null ? null : <Fab
                    position="bottomRight"
                    style={{ backgroundColor: "#4286f4" }}
                    onPress={this.openModal}
                    ref={"FAB"}
                >
                    <Icon name="md-create" />
                </Fab>}


            </Container>
        )
    }
}


const styles = StyleSheet.create({
    tweetHead: {
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        padding: 10,
        paddingBottom: 0
    },
    timeStamp: {
        flexDirection: "row",
        justifyContent: "flex-start",
        padding: 10,
        borderBottomColor: "#CCC",
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    tweetFooter: {
        flexDirection: "row",//justifyContent:'flex-start'
        justifyContent: "flex-start",
        borderBottomColor: "#CCC",
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    footerIcons: {
        flexDirection: "row",
        alignItems: "center"
    },
    tweetReply: {
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "flex-start",
        padding: 10,
        paddingBottom: 0
    }
});


const mapStateToProps = (state) => {
    const { relawanid, fasilitator } = state.anggota;
    return { relawanid, fasilitator };
};

export default connect(mapStateToProps, {})(Eventdetailhusus);