import React, { Component } from 'react';
import axios from 'axios';
import {
    StyleSheet, Dimensions,
    Text,
    View,
    TouchableOpacity,
    Image,
    Alert,
    ScrollView,
    TextInput,
    Linking,
    AsyncStorage,
    AppState
} from 'react-native';
import { Container, Content, Icon, Header, Left, Right, Body, Title, Button  } from 'native-base';
import { connect } from 'react-redux';
// import Header from '../components/Header';
import Hyperlink from 'react-native-hyperlink';
import io from "socket.io-client";
// import MyModal from '../components/MyModal';
//https://witness-so.tempo.co/
const { width, height } = Dimensions.get('window');
class KonsultasiChat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            textchat: '',
            titleChat:'',
            appState: AppState.currentState,
            data: [
                { id: 1, date: "9:50 am", type: 'in', message: "Lorem ipsum dolor sit amet" },
                { id: 2, date: "9:50 am", type: 'out', message: "Lorem ipsum dolor sit amet" },
                { id: 3, date: "9:50 am", type: 'in', message: "Lorem ipsum dolor sit a met" },
                { id: 4, date: "9:50 am", type: 'in', message: "Lorem ipsum dolor sit a met" },
                { id: 5, date: "9:50 am", type: 'out', message: "Lorem ipsum dolor sit a met" },
                { id: 6, date: "9:50 am", type: 'out', message: "Lorem ipsum dolor sit a met" },
                { id: 7, date: "9:50 am", type: 'in', message: "Lorem ipsum dolor sit a met" },
                { id: 8, date: "9:50 am", type: 'in', message: "Lorem ipsum dolor sit a met" },
                { id: 9, date: "9:50 am", type: 'in', message: "Lorem ipsum dolor sit a met" },
            ],
            cha: []
        };
        this.ar = [
            { nama: 'deden', pesan: 'hai', time: '12.35', isShowAvatar: '' },
            // { nama: 'deden', pesan: 'dsf', time: '5.35', isShowAvatar: '' },
            // { nama: 'anu', pesan: 'hfhgjai', time: '3.35', isShowAvatar: '' },
            // { nama: 'deden', pesan: 'kjk', time: '2.35', isShowAvatar: '' },
            // { nama: 'anu', pesan: 'gfhdf', time: '1.35', isShowAvatar: '' },
            // { nama: 'deden', pesan: 'jjk', time: '3.35', isShowAvatar: '' },
            // { nama: 'deden', pesan: 'jjk', time: '7.35', isShowAvatar: '' },
            // { nama: 'anu', pesan: 'jjk', time: '2.35', isShowAvatar: '' },
            // { nama: 'anu', pesan: 'oo', time: '4.35', isShowAvatar: '' }
        ]

    }
    //http://192.168.0.27:3000 //103.253.107.37
    componentDidMount() {
        console.log(this.props.userid + " dan "+ this.props.navigation.getParam('artcidlempar'))
        // this._retrieveData()
        // this.socket = io(`http://witness-so.hahabid.com`, { transports: ['websocket'] });
        this.socket = io(`https://witness-websocket-frgdn.ondigitalocean.app`, { transports: ['websocket'] });
        this.socket.emit('join',this.props.userid,this.props.userid,this.props.navigation.getParam('artcidlempar'));
        // this.socket.emit('join',1,1,77);
         this.socket.on('updatechat', (room = "", date = "", fullname = "", datatext = "", image="") => {
            let arra = []
            let chatitem = { nama: fullname, pesan: datatext, time: date, isShowAvatar: '', artcid : this.props.navigation.getParam('artcidlempar') , image: image }
            arra.push(chatitem)
            console.log(arra)
            this.setState({ cha: this.state.cha.concat(arra) })
        });
        // this.socket.emit('join',this.props.userid,this.props.userid,this.props.navigation.getParam('artcidlempar'));
        // this.socket.emit('join', "consultation_chat", this.props.userid, this.props.userid, this.props.nama);

        // this.socket.on('updatechat', (room = "", date = "", fullname = "", datatext = "", image = "") => {
        //     let arra = []
        //     let chatitem = { nama: fullname, pesan: datatext, time: date, isShowAvatar: '', image: image }
        //     arra.push(chatitem)
        //     console.log(arra)
        //     this.setState({ cha: this.state.cha.concat(arra) }, this._storeData)
        // });

        this.fetchingListChat()
        AppState.addEventListener('change', this._handleAppStateChange);

    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
          console.log('App has come to the foreground!');
          //  this.socket = io(`http://witness-so.hahabid.com`, { transports: ['websocket'] });
          this.socket = io(`https://witness-websocket-frgdn.ondigitalocean.app`, { transports: ['websocket'] });
          this.socket.emit('join',this.props.userid,this.props.userid,this.props.navigation.getParam('artcidlempar'));
          // this.socket.emit('join',1,1,77);
           this.socket.on('updatechat', (room = "", date = "", fullname = "", datatext = "", image="") => {
              let arra = []
              let chatitem = { nama: fullname, pesan: datatext, time: date, isShowAvatar: '', artcid : this.props.navigation.getParam('artcidlempar') , image: image }
              arra.push(chatitem)
              console.log(arra)
              this.setState({ cha: this.state.cha.concat(arra) })
          });
          this.fetchingListChat()

        }else{
            console.log('App has background! coy');
            this.socket.close()
        }
        this.setState({appState: nextAppState});
      };

      componentWillUnmount() {
        // AppState.removeEventListener('change', this._handleAppStateChange);
        this.socket.close()
      }

    fetchingListChat = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}chat/list?artcid=${this.props.navigation.getParam('artcidlempar')}`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })
            if (response.data.success == true) {
                this.setState({
                   cha : response.data.detail,
                   titleChat:response.data.title
                })
                // if (response.data.detail.osid == '' || response.data.detail.osid == null) {
                //     // console.log(response)
                //     this.setEmailOneSignal()

                // } else {
                //     // console.log('tidak dijalankan coy')
                    console.log(response)
                // }
            } else {
                Alert.alert('response failed')
            }
        }
        catch (e) {
            //  Alert.alert('Kesalahan Server Detail')
            // console.log(e)
        }
    }

    deleteChatServer = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}chat/chat_delete?id=${this.props.navigation.getParam('artcidlempar')}`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })
            if (response.data.success == true) {
                this.setState({
                   cha : []
                })
                // if (response.data.detail.osid == '' || response.data.detail.osid == null) {
                //     // console.log(response)
                //     this.setEmailOneSignal()

                // } else {
                //     // console.log('tidak dijalankan coy')
                    console.log(response)
                // }
            } else {
                Alert.alert('response failed')
            }
        }
        catch (e) {
            //  Alert.alert('Kesalahan Server Detail')
            // console.log(e)
        }
    }

    _storeData = async () => {
        try {
            await AsyncStorage.setItem('chatlist', JSON.stringify(this.state.cha));

        } catch (error) {
            // Error saving data
        }
    };

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('chatlist');
            if (value !== null) {
                this.setState({ cha: JSON.parse(value) })
            }
        } catch (error) {
            // Error retrieving data
        }
    };

    _clearChat1 = async () => {
        try {
            const value = await AsyncStorage.getItem('chatlist');
            if (value !== null) {
                var arrchatnya = await JSON.parse(value)
                var filteredArray = await arrchatnya.filter((r)=>{
                    if(r.artcid !== this.props.navigation.getParam('artcidlempar')){
                      return r
                    }
                  })

                  console.log(filteredArray)
                  this.setState({ cha:filteredArray },this._storeData)
            }
        } catch (error) {
            // Error retrieving data
        }
        // await AsyncStorage.removeItem('chatlist')
        // this.setState({ cha: [] })
    }



    _clearChat = () => {
        Alert.alert(
            'Perhatian !',
            'Anda Yakin Akan Menghapus Histori Chat ini ?',
            [
                { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' },
                { text: 'Yes', onPress: () => this.deleteChatServer() }
            ]
        )
    }

    renderDate = (date) => {
        return (
            <Text style={styles.time}>
                {date}
            </Text>
        );
    }

    handleSendChat = () => {
        this.socket.emit('sendchat', this.state.textchat);
        this.setState({ textchat: '' })
        this.refs.scrollView.scrollToEnd({ animated: true })
    }

    newArr = (arry) => {
        if (arry.length == 1) {
            arry[0].isShowAvatar = 'yes'
            return arry
        } else if (arry.length == 0) {
            return []
        } else {
            var last = arry.length - 1
            arry.map(function (value, index, elements) {
                if (elements[index - 1] !== undefined) {
                    if ((elements[index].nama == elements[index - 1].nama)) {
                        elements[0].isShowAvatar = 'yes'
                        elements[index].isShowAvatar = 'no'
                        elements[last].isShowAvatar = 'no'
                    } else {
                        elements[0].isShowAvatar = 'yes'
                        elements[index].isShowAvatar = 'yes'
                        elements[last].isShowAvatar = 'yes'
                    }
                } else {
                    if ((elements[last].nama == elements[last - 1].nama)) {
                        elements[0].isShowAvatar = 'yes'
                        elements[last].isShowAvatar = 'yes'
                    } else {
                        elements[0].isShowAvatar = 'yes'
                        elements[last].isShowAvatar = 'no'
                    }
                }
            })
            return arry
        }

    }

    openLinking2 = async (ur) => {
        let cek = await this.isYoutubeUrl(ur)
        if (cek == true) {
            let vId = await this.getId(ur)
            Alert.alert(
                'Perhatian',
                'Lihat Video ?',
                [
                    { text: 'Cancel', onPress: () => console.log('no') },
                    { text: 'Ok', onPress: () => this.props.navigation.navigate('videoPlayer', { vidId: vId }) }
                ] //, { cancelable: false }
            )
        } else {
            Alert.alert(
                'Perhatian',
                'Kunjungi Link ?',
                [
                    { text: 'Cancel', onPress: () => console.log('no') },
                    { text: 'Ok', onPress: () => Linking.openURL(ur) }
                ] //, { cancelable: false }
            )
        }
    }



    isYoutubeUrl = (url) => {
        //var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/
        var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        if (url.match(p)) {
            return true;
        }
        return false;
    }



    getId = (url) => {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }


    render() {
        if (this.props.userid == '') {
            return (
                <Container style={styles.container}>
                    {/* <Header
                        openProfile={() => this.props.navigation.navigate('profile')}
                        openMenu={() => this.props.navigation.navigate('login')}
                        goBack={() => this.props.navigation.goBack()}
                        txtTitle="Konsultasi Chat"
                    /> */}
                    <Content>

                        <Image source={require('../assets/iconchat.png')} style={{ alignSelf: 'center', marginTop: 200 }} />


                    </Content>
                    <View style={{
                        flexDirection: 'row',
                        height: 60,
                        backgroundColor: '#eeeeee',
                        paddingHorizontal: 10,
                        padding: 5,
                        // alignSelf:'center',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text>Silahkan Login Untuk Mulai Chat</Text>
                    </View>

                </Container>
            )

        }
        return (
            <Container style={styles.container}>

             <Header style={{
                    backgroundColor: "white", borderBottomColor: 'black',
                    borderBottomWidth: 0.6
                }}>
                    <Left>
                        <Button
                            onPress={() => this.props.navigation.goBack()}
                            transparent>
                            <Icon name="arrow-back" style={{ color: 'blue' }} />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={{ color: 'black' }}>Back</Title>
                    </Body>
                    <Right />
                </Header>


                {/* <Header
                    openProfile={() => this.props.navigation.navigate('profile')}
                    openMenu={() => this.props.navigation.navigate('login')}
                    goBack={() => this.props.navigation.goBack()}
                    txtTitle="Konsultasi Chat"
                /> */}

                <ScrollView
                    ref="scrollView"
                    onContentSizeChange={(contentWidth, contentHeight) => {
                        this.refs.scrollView.scrollToEnd({ animated: true })
                    }} >
                    <View style={{ padding: 10, marginBottom: 100 }}>

                    <View style={{justifyContent:'center', alignItems:'center', marginBottom:5}}>
                        <Text style={{color:'black'}}>Title Report : </Text>
                        <Text style={{fontSize:24, color:'black',textAlign:'center'}}>{this.state.titleChat}</Text>
                    </View>

                        {/* <View style={{ marginBottom: 20 }}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{}}>
                                    <Image source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvpiHcFhKUBokBe7xP-rx85UDdqpGKIZhwzj1D1C9v6TdyJLkW' }} style={{ height: 50, width: 50, borderRadius: 300, }} />
                                </View>

                                <View style={[styles.item]}>
                                    <View style={{
                                        width: 0,
                                        height: 0,
                                        position: 'absolute',
                                        marginLeft: -20,
                                        backgroundColor: 'transparent',
                                        borderStyle: 'solid',
                                        borderRightWidth: 30,
                                        borderTopWidth: 30,
                                        borderRightColor: 'transparent',
                                        borderTopColor: "#eeeeee",
                                        transform: [
                                            { rotate: '90deg' }
                                        ]
                                    }}></View>
                                    <View style={[styles.balloon]}>
                                        <View style={{ flex: 0.8 }}>
                                            <Text style={{}}>
                                                Ada masalah dengan usaha pertanian dan peternakan Anda? Temukan jawabannya di sini. Anda akan mendapatkan solusinya dari para pakar dibidangnya
                                            </Text>
                                        </View>
                                        <View style={{ flex: 0.2, alignSelf: 'flex-end' }}>
                                            <Text style={{ alignSelf: 'flex-end', color: 'black' }}></Text>
                                        </View>

                                    </View>
                                </View>
                            </View>
                        </View> */}

                        {
                            this.newArr(this.state.cha).map((row, i) => {
                                // this.state.cha.map((row, i) => {
                            // if(this.props.navigation.getParam('artcidlempar') == row.artcid){
                                if (row.nama == this.props.nama) {
                                    return (
                                        <View key={i} style={{ marginBottom: 20 }}>
                                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <View style={{
                                                    flex: 0.95,
                                                    flexDirection: 'row',
                                                    backgroundColor: "#99dfff",
                                                    borderRadius: 5
                                                }}>
                                                    <View style={[styles.balloon]}>
                                                        <View style={{ flex: 0.8 }}>
                                                            <Hyperlink
                                                                linkStyle={{ color: '#2980b9' }}
                                                                onPress={(url, text) => this.openLinking2(url)}>
                                                                <Text style={{}}>{row.nama} : {row.pesan} </Text>
                                                            </Hyperlink>
                                                        </View>
                                                        <View style={{ flex: 0.2, alignSelf: 'flex-end' }}>
                                                            <Text style={{ alignSelf: 'flex-end', color: 'black' }}>{row.time}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{
                                                        width: 0,
                                                        height: 0,
                                                        position: 'absolute',
                                                        right: -20,
                                                        backgroundColor: 'transparent',
                                                        borderStyle: 'solid',
                                                        borderRightWidth: 30,
                                                        borderTopWidth: 30,
                                                        borderRightColor: 'transparent',
                                                        borderTopColor: "#99dfff"
                                                    }}></View>
                                                </View>
                                                {row.isShowAvatar == 'yes' ? <View style={{}}>
                                                    <Image source={{ uri: row.image }} style={{ height: 50, width: 50, borderRadius: 300, }} />
                                                </View> : <View style={{}}>
                                                        <Image source={{ uri: '' }} style={{ height: 50, width: 50, borderRadius: 300, }} />
                                                    </View>}

                                            </View>
                                        </View>
                                    )
                                } else {
                                    return (
                                        <View key={i} style={{ marginBottom: 20 }}>
                                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                                {row.isShowAvatar == 'yes' ? <View style={{}}>
                                                    <Image source={{ uri: row.image }} style={{ height: 50, width: 50, borderRadius: 300, }} />
                                                </View> : <View style={{}}>
                                                        <Image source={{ uri: '' }} style={{ height: 50, width: 50, borderRadius: 300, }} />
                                                    </View>}

                                                <View style={[styles.item]}>
                                                    <View style={{
                                                        width: 0,
                                                        height: 0,
                                                        position: 'absolute',
                                                        marginLeft: -20,
                                                        backgroundColor: 'transparent',
                                                        borderStyle: 'solid',
                                                        borderRightWidth: 30,
                                                        borderTopWidth: 30,
                                                        borderRightColor: 'transparent',
                                                        borderTopColor: "#eeeeee",
                                                        transform: [
                                                            { rotate: '90deg' }
                                                        ]
                                                    }}></View>
                                                    <View style={[styles.balloon]}>
                                                        <View style={{ flex: 0.8 }}>
                                                            <Hyperlink
                                                                linkStyle={{ color: '#2980b9' }}
                                                                onPress={(url, text) => this.openLinking2(url)}>
                                                                <Text style={{}}>{row.nama} : {row.pesan} </Text>
                                                            </Hyperlink>
                                                        </View>
                                                        <View style={{ flex: 0.2, alignSelf: 'flex-end' }}>
                                                            <Text style={{ alignSelf: 'flex-end', color: 'black' }}>{row.time}</Text>
                                                        </View>

                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    )
                                }

                         //   }

                            })
                        }

                        {
                            this.state.cha.length > 0 && <TouchableOpacity onPress={this._clearChat}>
                                <View style={{ alignSelf: 'center' }}>
                                    <Text style={{ color: 'red' }}>Clear Chat</Text>
                                </View>
                            </TouchableOpacity>
                        }


                    </View>
                </ScrollView>

                <View style={styles.footer}>
                    <View style={styles.inputContainer}>
                        <TextInput style={styles.inputs}
                            value={this.state.textchat}
                            placeholder="Tulis pesan..."
                            underlineColorAndroid='transparent'
                            onChangeText={(textchat) => this.setState({ textchat })} />
                    </View>

                    <TouchableOpacity style={styles.btnSend} onPress={this.handleSendChat}>

                        <Icon type="FontAwesome" style={{ color: 'white', fontSize: 18 }} name="paper-plane" />
                        {/* <Image source={{ uri: "https://png.icons8.com/small/75/ffffff/filled-sent.png" }} style={styles.iconSend} /> */}
                    </TouchableOpacity>
                </View>



                {/* <MyModal
                    onPressClose={() => this.setState({ modalVisible: false })}
                    onPressLogin={() => {
                        this.props.navigation.navigate('login')
                        this.setState({ modalVisible: false })
                    }}
                    onPressRegister={() => {
                        this.setState({ modalVisible: false })
                        this.props.navigation.navigate('register')
                    }}
                    visible={this.state.modalVisible}>

                </MyModal> */}

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    list: {
        paddingHorizontal: 17,
    },
    footer: {
        flexDirection: 'row',
        height: 60,
        backgroundColor: '#eeeeee',
        paddingHorizontal: 10,
        padding: 5,
    },
    btnSend: {
        backgroundColor: "#00BFFF",
        width: 40,
        height: 40,
        borderRadius: 360,
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconSend: {
        width: 30,
        height: 30,
        alignSelf: 'center',
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius: 30,
        borderBottomWidth: 1,
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        marginRight: 10,
    },
    inputs: {
        height: 40,
        marginLeft: 16,
        borderBottomColor: '#FFFFFF',
        flex: 1,
    },
    balloon: {
        //flexDirection: 'row',
        // justifyContent:'space-between',#99dfff
        //minWidth: 250,
        padding: 10,
        flex: 1
    },
    itemIn: {
        alignSelf: 'flex-start'
    },
    itemOut: {
        alignSelf: 'flex-end'
    },
    time: {
        alignSelf: 'flex-end',
        margin: 15,
        fontSize: 12,
        color: "#808080",
    },
    item: {
        flex: 0.95,
        flexDirection: 'row',
        backgroundColor: "#eeeeee",
        borderRadius: 5
    },
    item2: {
        flex: 0.11,
        marginVertical: 14,
        flexDirection: 'row',
        backgroundColor: "#eeeeee",
    },
});
const mapStateToProps = (state) => {
    const { userid, nama } = state.anggota;
    return { userid, nama };
};

export default connect(mapStateToProps, {})(KonsultasiChat);
// export default KonsultasiChat


{/* <View style={{ marginBottom: 20 }}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{}}>
                                    <Image source={{ uri: 'http://i-pangan.com/demo/images/news01.jpg' }} style={{ height: 50, width: 50, borderRadius: 300, }} />
                                </View>

                                <View style={[styles.item]}>
                                    <View style={{
                                        width: 0,
                                        height: 0,
                                        position: 'absolute',
                                        marginLeft: -20,
                                        backgroundColor: 'transparent',
                                        borderStyle: 'solid',
                                        borderRightWidth: 30,
                                        borderTopWidth: 30,
                                        borderRightColor: 'transparent',
                                        borderTopColor: "#eeeeee",
                                        transform: [
                                            { rotate: '90deg' }
                                        ]
                                    }}></View>
                                    <View style={[styles.balloon]}>
                                        <View style={{ flex: 0.8 }}>
                                            <Text style={{}}>Nama Konsultan : hai juga </Text>
                                        </View>
                                        <View style={{ flex: 0.2, alignSelf: 'flex-end' }}>
                                            <Text style={{ alignSelf: 'flex-end', color: 'black' }}>12.00</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>

                        <View style={{ marginBottom: 20 }}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{
                                    flex: 0.95,
                                    flexDirection: 'row',
                                    backgroundColor: "#99dfff",
                                    borderRadius: 5
                                }}>
                                    <View style={[styles.balloon]}>
                                        <View style={{ flex: 0.8 }}>
                                            <Text style={{}}>Nama Anda : hai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagihai lagi </Text>
                                        </View>
                                        <View style={{ flex: 0.2, alignSelf: 'flex-end' }}>
                                            <Text style={{ alignSelf: 'flex-end', color: 'black' }}>12.00</Text>
                                        </View>
                                    </View>
                                    <View style={{
                                        width: 0,
                                        height: 0,
                                        position: 'absolute',
                                        right: -20,
                                        backgroundColor: 'transparent',
                                        borderStyle: 'solid',
                                        borderRightWidth: 30,
                                        borderTopWidth: 30,
                                        borderRightColor: 'transparent',
                                        borderTopColor: "#99dfff"
                                    }}></View>
                                </View>
                                <View style={{}}>
                                    <Image source={{ uri: 'http://i-pangan.com/demo/images/news01.jpg' }} style={{ height: 50, width: 50, borderRadius: 300, }} />
                                </View>
                            </View>
                        </View> */}
