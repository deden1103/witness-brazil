
import React, { Component } from 'react';
import {
    Button,
    Container,
    Content
} from 'native-base';

import {
    Text,
    View,
    StatusBar,
    Alert, Dimensions
} from 'react-native';
import axios from 'axios';
import { HeaderRightBack } from '../particles/HeaderRightBack';
const { width, height } = Dimensions.get('window')
class Listdc extends Component {

    constructor() {
        super()
        this.state = {
            daftaroutlet: [],
            no: 1
        }
    }


    componentDidMount() {
        this.fetchingDetail()
    }


    fetchingDetail = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}api/apidc/listdc`)
            if (response.data.success == true) {
                this.setState({
                    daftaroutlet: response.data.data
                })
                console.log(response)
            } else {
                console.log(response)
            }
        }
        catch (e) {
            console.log(e)
        }
    }


    render() {
        return (

            <Container style={styles.container}>
                {/* <StatusBar
                    backgroundColor={GLOBAL.mainColor}
                    animated
                /> */}
                <HeaderRightBack
                    goBack={() => this.props.navigation.goBack()}
                    txtTitle="List DC"
                    kembaliText="Kembali"
                    openDrawer={()=>this.props.navigation.openDrawer()}
                    kecart={()=>this.props.navigation.navigate('cart')}
                />
                <Content style={{ flex: 1 }}>
                    <View style={{ flex: 1,margin:5 }}>

                        {this.state.daftaroutlet.map((res, i) => (
                            <View key={i} style={{ flex: 1, backgroundColor: 'white', padding: 20, marginTop: 5, borderColor: 'grey', borderWidth: 1, borderRadius: 7 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View><Text>No : </Text></View>
                                    <View><Text style={{ fontSize: 15, fontWeight: 'bold' }}>{this.state.no++}</Text></View>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <View><Text>Nama DC : </Text></View>
                                    <View><Text style={{ fontSize: 15, fontWeight: 'bold' }}>{res.dcName}</Text></View>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <View><Text>Alamat : </Text></View>
                                    <View><Text style={{ fontSize: 15, fontWeight: 'bold' }}>{res.dcAddress}</Text></View>
                                </View>

                            </View>
                        ))}



                    </View>
                </Content>
            </Container>
        );
    }
}

export default Listdc

const styles = {
    container: {
        flex: 1,
        backgroundColor: 'white'
    }
}























