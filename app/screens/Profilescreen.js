import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  FlatList,
  TouchableHighlight,
  Animated,
  Alert,
  Easing,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Picker,
  PermissionsAndroid,
} from 'react-native';
import {
  Container,
  Header,
  Body,
  Content,
  Left,
  Title,
  Thumbnail,
  Col,
  Row,
  Grid,
  Icon,
  Button,
  Spinner,
} from 'native-base';
import {connect} from 'react-redux';
import axios from 'axios';
import RNSimData from 'react-native-sim-data';
const {width, height} = Dimensions.get('window');
const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
      detail: null,
      namalengkap: '',
      email: '',
      kabupaten: '',
      kecamatan: '',
      organisasi: '',
      namakabupaten: '',
      namakecamatan: '',
      namaorganisasi: '',
      handphone: '',
      password: '',
      confirmpassword: '',
      kabupatenlist: [],
      kecamatanlist: [],
      organisasilist: [],
      iccid: '',
      iccid2: '',
    };
  }

  componentDidMount() {
    this.fetchingDetail();
    this.requestSimPermission();
    // this.fetchDetailKabupaten()
    // this.fetchDetailOrganisasi()
    //this.fetchDetailKecamatan()
  }

  requestSimPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
        {
          title: 'Tempo Witness Location Permission',
          message:
            'Tempo Witness access to your locations ' +
            'so you can send location.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the Sim');
        console.log(RNSimData.getSimInfo().simSerialNumber0);
        this.setState({iccid: RNSimData.getSimInfo().simSerialNumber0});
        if (
          RNSimData.getSimInfo().simSerialNumber1 !== undefined ||
          RNSimData.getSimInfo().simSerialNumber1 !== 'undefined'
        ) {
          this.setState({iccid2: RNSimData.getSimInfo().simSerialNumber1});
        }
      } else {
        console.log('GPS permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.relawanid !== nextProps.relawanid) {
      this.fetchingDetail();
    }
    return true;
  }

  onValueChange = async value => {
    await this.setState({kabupaten: value});
    await this.fetchDetailKecamatan(this.state.kabupaten);
  };

  fetchingDetail = async () => {
    try {
      const response = await axios.get(`${GLOBAL.apiUrl}user/detail`, {
        headers: {
          Authorization: `Bearer ${this.props.token}`,
        },
      });
      if (response.data.success == true) {
        this.setState({
          //detail: response.data.detail,
          namalengkap: response.data.detail.ukomName,
          email: response.data.detail.ukomEmail,
          organisasi: response.data.detail.ukomKomtId,
          handphone: response.data.detail.ukomPhone,
          namaorganisasi: response.data.detail.ukomKomtId,
        });
        // console.log(response.data.detail)
      } else {
        // console.log(response)
      }
    } catch (e) {
      // console.log(e)
    }
  };

  handleChangePassword = async () => {
    try {
      if (this.state.password != '' || this.state.confirmpassword != '') {
        if (this.state.password != this.state.confirmpassword) {
          Alert.alert('Password dan Konfirmasi Password Tidak Sama');
        } else {
          this.setState({loading: true});
          let data = JSON.stringify({
            password: this.state.password,
          });
          let loginResponse = await axios.post(
            `${GLOBAL.apiUrl}user/changepassword`,
            data,
            {
              headers: {
                Authorization: `Bearer ${this.props.token}`,
                'Content-Type': 'application/json',
              },
            },
          );
          if (loginResponse.data.success == true) {
            // console.log(loginResponse)
            this.setState({loading: false});
            Alert.alert('Atualização bem sucedida');
            this.props.navigation.goBack();
          } else {
            // console.log(loginResponse)
            Alert.alert('Atualização falhou');
            this.setState({loading: false});
          }
        }
      } else {
        Alert.alert('Password dan konfirm password masih kosong');
      }
    } catch (e) {
      // console.log(e)
      Alert.alert('erro de servidor');
      this.setState({loading: false});
    }
  };

  handleUpdate = async () => {
    try {
      if (this.state.password != '' || this.state.confirmpassword != '') {
        if (this.state.password != this.state.confirmpassword) {
          Alert.alert('Password dan Konfirmasi Password Tidak Sama');
        } else {
          this.setState({loading: true});
          let data = JSON.stringify({
            namaRelawan: this.state.namalengkap,
            emailRelawan: this.state.email,
            kabupaten: this.state.kabupaten,
            kecamatan: this.state.kecamatan,
            organisasiRelawan: this.state.organisasi,
            handphone: this.state.handphone,
            password: this.state.password,
            // confirmpassword: this.state.confirmpassword
          });
          let loginResponse = await axios.post(
            `${GLOBAL.apiUrl}relawan/update`,
            data,
            {
              headers: {
                Authorization: `Bearer ${this.props.token}`,
                'Content-Type': 'application/json',
              },
            },
          );
          if (loginResponse.data.success == true) {
            // console.log(loginResponse)
            this.setState({loading: false});
            Alert.alert('Atualização bem sucedida');
            this.props.navigation.goBack();
          } else {
            // console.log(loginResponse)
            Alert.alert('Atualização falhou');
            this.setState({loading: false});
          }
        }
      } else {
        this.setState({loading: true});
        let data = JSON.stringify({
          namaRelawan: this.state.namalengkap,
          emailRelawan: this.state.email,
          kabupaten: this.state.kabupaten,
          kecamatan: this.state.kecamatan,
          organisasiRelawan: this.state.organisasi,
          handphone: this.state.handphone,
          password: this.state.password,
          // confirmpassword: this.state.confirmpassword
        });
        let loginResponse = await axios.post(
          `${GLOBAL.apiUrl}relawan/update`,
          data,
          {
            headers: {
              Authorization: `Bearer ${this.props.token}`,
              'Content-Type': 'application/json',
            },
          },
        );
        if (loginResponse.data.success == true) {
          // console.log(loginResponse)
          this.setState({loading: false});
          Alert.alert('Atualização bem sucedida');
          this.props.navigation.goBack();
        } else {
          // console.log(loginResponse)
          Alert.alert('Atualização falhou');
          this.setState({loading: false});
        }
      }
    } catch (e) {
      // console.log(e)
      Alert.alert('erro de servidor');
      this.setState({loading: false});
    }
  };

  fetchDetailKabupaten = async () => {
    try {
      const response = await axios.get(
        `http://cms.deltaone.id/data_kecamatan/api_kabupaten`,
      );
      if (response) {
        this.setState({
          kabupatenlist: response.data,
        });
        // console.log(response)
        await this.fetchDetailKecamatan(this.state.kabupaten);
      } else {
        // console.log(response)
      }
      //console.log(response)
    } catch (e) {
      // console.log(e)
    }
  };

  fetchDetailKecamatan = async id => {
    try {
      const response = await axios.get(
        `http://cms.deltaone.id/data_kecamatan/api/${id}`,
      );
      if (response) {
        this.setState({
          kecamatanlist: response.data,
        });
      } else {
        // console.log(response)
      }
    } catch (e) {
      // console.log(e)
    }
  };

  fetchDetailOrganisasi = async () => {
    try {
      const response = await axios.get(
        `http://cms.deltaone.id/data_kecamatan/api_organisasi`,
      );
      if (response) {
        this.setState({
          organisasilist: response.data,
        });
        // console.log(response)
      } else {
        // console.log(response)
      }
      //console.log(response)
    } catch (e) {
      // console.log(e)
    }
  };

  _goBack() {
    // console.log("Back button pressed");
    this.props.navigation.goBack();
  }

  _keyExtractor = (item, index) => item.id;

  render() {
    var headMov = this.state.scrollY.interpolate({
      inputRange: [0, 390, 391],
      outputRange: [0, -390, -390],
    });
    var coverMov = this.state.scrollY.interpolate({
      inputRange: [0, 94, 95],
      outputRange: [0, -94, -94],
    });
    var avatarMov = this.state.scrollY.interpolate({
      inputRange: [0, 150, 151],
      outputRange: [0, -150, -150],
    });
    var avatarOp = this.state.scrollY.interpolate({
      inputRange: [0, 94, 95],
      outputRange: [1, 0, 0],
    });
    var headerOp = this.state.scrollY.interpolate({
      inputRange: [95, 180, 181],
      outputRange: [0, 0.75, 0.75],
    });
    var headerContentOp = this.state.scrollY.interpolate({
      inputRange: [0, 180, 210],
      outputRange: [0, 0, 1],
    });
    // const cover = this.state.detail == null ? 'http://relawan-cms.hahabid.com/assets/uploads/profile/noimage.png' : this.state.detail.photoRelawan
    return (
      <View style={{flex: 1}}>
        <Animated.Image
          source={
            this.props.photo
              ? {uri: this.props.photo}
              : require('../assets/icon.png')
          }
          style={{
            width: '100%',
            height: 150,
            zIndex: 2,
            position: 'absolute',
            transform: [{translateY: coverMov}],
          }}
        />
        <Animated.View
          style={{
            width: '100%',
            position: 'absolute',
            backgroundColor: '#121212',
            height: 56,
            zIndex: 13,
            opacity: headerOp,

            alignItems: 'center',
          }}>
          <Animated.View
            style={{
              //opacity: headerContentOp,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}>
            <Button
              transparent
              iconLeft
              light
              onPress={this._goBack.bind(this)}>
              <Icon name="arrow-back" />
            </Button>
            <Text style={{fontSize: 24, color: 'white', flex: 1}}>{}</Text>
            {/* <Button transparent iconRight light>
              <Icon name="md-more" />
            </Button> */}
          </Animated.View>
        </Animated.View>
        <Animated.View
          style={{
            zIndex: 4,
            position: 'absolute',
            top: 135,
            opacity: avatarOp,
            transform: [{translateY: avatarMov}],
          }}>
          <Thumbnail
            large
            source={{
              uri: 'https://data.humdata.org/crisis-tiles/12/2485/1645.png',
            }}
            style={styles.avatarbg}
          />
          <Thumbnail
            large
            source={{uri: this.props.photo}}
            style={styles.avatar}
          />
        </Animated.View>
        <Animated.ScrollView
          scrollEventThrottle={16}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {contentOffset: {y: this.state.scrollY}},
              },
            ],
            {
              useNativeDriver: true,
            },
          )}>
          <View style={StyleSheet.flatten([styles.header, {marginTop: 150}])}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                height: 80,
              }}>
              {/* <Button rounded bordered style={styles.headerButton}>
                <Icon
                  name="ios-mail-outline"
                  style={{ color: "#4286f4", paddingLeft: 8 }}
                />
              </Button> */}
              {/* <Button rounded bordered style={styles.headerButton}>
                <Icon
                  name="ios-notifications-outline"
                  style={{ color: "#4286f4", paddingLeft: 8 }}
                />
              </Button> */}
              {/* <Button
                bordered
                rounded
                primary
                style={StyleSheet.flatten([
                  styles.headerButton,
                  { paddingLeft: 15, paddingRight: 15 }
                ])}
              >
                <Text style={{ color: "#4286f4" }}>Follow</Text>
              </Button> */}
            </View>
          </View>

          <View style={styles.header}>
            <Text style={styles.nameText}>{}</Text>
            <Text style={styles.usernameText}>{this.props.nama}</Text>
            <Text style={styles.bioText}>{this.state.email}</Text>
            {/* <Text style={styles.locationText}>
              <Icon small name="ios-pin-outline" style={{ fontSize: 16 }} />
              {" "}
            </Text>
            <View style={{ flexDirection: "row", marginTop: 10 }}>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{ fontSize: 16, fontWeight: "bold", marginLeft: 14 }}
                >
                  {}
                </Text>
                <Text style={{ fontSize: 16, color: "#555", marginLeft: 5 }}>
                  Following
                </Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{ fontSize: 16, fontWeight: "bold", marginLeft: 30 }}
                >
                  {}
                </Text>
                <Text style={{ fontSize: 16, color: "#555", marginLeft: 5 }}>
                  Followers
                </Text>
              </View>
            </View> */}
          </View>
          <View style={{backgroundColor: 'white', marginTop: 8}}>
            <View
              contentContainerStyle={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}></View>

            <View style={styles.tweet}>
              <View style={{flex: 1}}>
                <View style={styles.inputContainer}>
                  <View>
                    <Text>Nome completo : </Text>
                  </View>
                  <TextInput
                    style={styles.inputs}
                    value={this.state.namalengkap}
                    //editable={false}
                    editable={false}
                    onChangeText={namalengkap => this.setState({namalengkap})}
                    placeholder=""
                    keyboardType="email-address"
                    underlineColorAndroid="transparent"
                  />
                </View>
                <View style={styles.inputContainer}>
                  <View>
                    <Text>Correio eletrónico : </Text>
                  </View>
                  <TextInput
                    style={styles.inputs}
                    value={this.state.email}
                    editable={false}
                    onChangeText={email => this.setState({email})}
                    placeholder=""
                    keyboardType="email-address"
                    underlineColorAndroid="transparent"
                  />
                </View>

                {this.props.fasilitator === null && (
                  <View style={styles.inputContainer}>
                    <View>
                      <Text>Kabupaten : </Text>
                    </View>
                    <TextInput
                      style={styles.inputs}
                      value={this.state.namakabupaten}
                      //onChangeText={(email) => this.setState({ email })}
                      placeholder=""
                      editable={false}
                      keyboardType="email-address"
                      underlineColorAndroid="transparent"
                    />
                    {/* <Picker
										selectedValue={this.state.kabupaten}
										style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
										onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue)}>
										<Picker.Item label="Pilih Kabupaten" value="0" />
										{this.state.kabupatenlist.map((res, i) => (
											<Picker.Item key={i} label={res.msrgName} value={res.msrgId} />
										))}
									</Picker> */}
                  </View>
                )}

                {this.props.fasilitator === null && (
                  <View style={styles.inputContainer}>
                    <View>
                      <Text>Kecamatan : </Text>
                    </View>
                    <TextInput
                      style={styles.inputs}
                      value={this.state.namakecamatan}
                      //onChangeText={(email) => this.setState({ email })}
                      editable={false}
                      placeholder=""
                      keyboardType="email-address"
                      underlineColorAndroid="transparent"
                    />
                    {/* <Picker
										selectedValue={this.state.kecamatan}
										style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
										onValueChange={(itemValue, itemIndex) => this.setState({ kecamatan: itemValue })}>
										<Picker.Item label="Pilih Kecamatan" value="0" />
										{this.state.kecamatanlist.map((res, i) => (
											<Picker.Item key={i} label={res.msdiName} value={res.msdiId} />
										))}
									</Picker> */}
                  </View>
                )}

                <View style={styles.inputContainer}>
                  <View>
                    <Text>Organização : </Text>
                  </View>
                  <TextInput
                    style={styles.inputs}
                    value={this.state.namaorganisasi}
                    editable={false}
                    //onChangeText={(email) => this.setState({ email })}
                    placeholder=""
                    keyboardType="email-address"
                    underlineColorAndroid="transparent"
                  />
                  {/* <Picker
										selectedValue={this.state.organisasi}
										style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
										onValueChange={(itemValue, itemIndex) => this.setState({ organisasi: itemValue })}>
										<Picker.Item label="Pilih Organisasi" value="0" />
										{this.state.organisasilist.map((res, i) => (
											<Picker.Item key={i} label={res.namaOrganisasi} value={res.organisasiId} />
										))}
									</Picker> */}
                </View>
                <View style={styles.inputContainer}>
                  <View>
                    <Text>Celular : </Text>
                  </View>
                  <TextInput
                    style={styles.inputs}
                    value={this.state.handphone}
                    editable={false}
                    onChangeText={handphone => this.setState({handphone})}
                    placeholder=""
                    keyboardType="email-address"
                    underlineColorAndroid="transparent"
                  />
                </View>
                {/* <View style={{ marginLeft: 10 }}><Text style={{ color: GLOBAL.mainColor }}>Isi Password dan Confirm Password Untuk Ganti Password </Text></View> */}
                <View style={styles.inputContainer}>
                  <View>
                    <Text>Nova senha : </Text>
                  </View>
                  <TextInput
                    style={styles.inputs}
                    value={this.state.password}
                    onChangeText={password => this.setState({password})}
                    placeholder=""
                    secureTextEntry
                    underlineColorAndroid="transparent"
                  />
                </View>
                <View style={styles.inputContainer}>
                  <View>
                    <Text>Confirmar senha : </Text>
                  </View>
                  <TextInput
                    style={styles.inputs}
                    secureTextEntry
                    value={this.state.confirmpassword}
                    onChangeText={confirmpassword =>
                      this.setState({confirmpassword})
                    }
                    placeholder=" "
                    underlineColorAndroid="transparent"
                  />
                </View>
                <TouchableOpacity
                  style={[styles.buttonContainer, styles.loginButton]}
                  onPress={this.handleChangePassword}>
                  {this.state.loading ? (
                    <Spinner color="white" />
                  ) : (
                    <Text style={styles.loginText}>Mudar senha</Text>
                  )}
                </TouchableOpacity>
              </View>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: 'grey',
                  }}>
                  ICCID
                </Text>
                <Text
                  selectable
                  style={{
                    color: 'grey',
                  }}>
                  {this.state.iccid}
                </Text>
              </View>
              {this.state.iccid2 !== '' ||
                (this.state.iccid2 !== 'undefined' && (
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color: 'grey',
                      }}>
                      ICCID2
                    </Text>
                    <Text
                      style={{
                        color: 'grey',
                      }}>
                      {this.state.iccid2}
                    </Text>
                  </View>
                ))}
            </View>
          </View>
        </Animated.ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  avatarbg: {
    //marginTop: -95,
    marginLeft: 20,
    padding: 10,
    width: 100,
    height: 100,
    borderRadius: 50,
    zIndex: 12,
    // borderRadius: 180
  },
  avatar: {
    marginLeft: 26,
    marginTop: -95,
    width: 89,
    height: 89,
    borderRadius: 44,
    zIndex: 12,
  },
  headerButton: {
    // alignSelf: "flex-end",
    paddingLeft: 7,
    paddingRight: 7,
    paddingBottom: 3,
    paddingTop: 3,
    marginRight: 8,
  },
  nameText: {
    fontSize: 26,
    fontWeight: '500',
    marginLeft: 14,
  },
  usernameText: {
    color: '#777',
    fontSize: 16,
    marginLeft: 14,
  },
  bioText: {
    fontSize: 16,
    marginLeft: 14,
    marginTop: 10,
    maxHeight: 41,
  },
  locationText: {
    fontSize: 16,
    marginLeft: 14,
    marginTop: 10,
    color: '#555',
  },
  topMargin: {
    // marginTop: 25
  },
  content: {
    padding: 10,
    backgroundColor: 'white',
  },
  heading: {
    fontSize: 32,
    fontWeight: '400',
    marginBottom: 30,
  },
  tweet: {
    paddingTop: 20,
    paddingBottom: 120,
    paddingLeft: 10,
    paddingRight: 10,
    borderBottomColor: '#bbb',
    borderBottomWidth: StyleSheet.hairlineWidth,
    flexDirection: 'column',
  },
  tweetText: {
    marginTop: 10,
    fontSize: 18,
    color: '#555',
  },
  tweetFooter: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  badgeCount: {
    fontSize: 12,
    paddingLeft: 5,
  },
  footerIcons: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#B0E0E6',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#E6ECF0',
    borderRadius: 30,
    borderBottomWidth: 1,
    height: 45,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15,
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  icon: {
    width: 30,
    height: 30,
  },
  inputIcon: {
    marginLeft: 15,
    justifyContent: 'center',
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20,
    width: width * 0.95,
    borderRadius: 30,
  },
  loginButton: {
    backgroundColor: '#3498db',
  },
  fabookButton: {
    backgroundColor: '#3b5998',
  },
  googleButton: {
    backgroundColor: '#ff0000',
  },
  loginText: {
    color: 'white',
  },
  restoreButtonContainer: {
    width: 250,
    marginBottom: 15,
    alignItems: 'flex-end',
  },
  socialButtonContent: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  socialIcon: {
    color: '#FFFFFF',
    marginRight: 5,
  },
});

const mapStateToProps = state => {
  const {
    nama,
    relawanid,
    token,
    photo,
    fasilitator,
    jabatan,
    eposko,
  } = state.anggota;
  return {nama, relawanid, token, photo, fasilitator, jabatan, eposko};
};

export default connect(
  mapStateToProps,
  {},
)(ProfileScreen);
