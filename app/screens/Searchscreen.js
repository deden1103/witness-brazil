import React, { Component } from "react";
import Modal from "react-native-modalbox";
import Dimensions from "Dimensions";
import {
    StyleSheet,
    View,
    Text,
    FlatList,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Platform,
    Alert,
    StatusBar, Image, Picker, TextInput, Linking, LayoutAnimation, ActivityIndicator
} from "react-native";
import {
    Container,
    Content,
    Thumbnail,
    Icon,
    Spinner,
    Fab,
    Button
} from "native-base";
import { connect } from "react-redux";
// import Image from 'react-native-remote-svg';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import AutoHeightImage from 'react-native-auto-height-image';
// import ScrollableTabView, {
//     ScrollableTabBar
// } from "react-native-scrollable-tab-view";
import Hyperlink from 'react-native-hyperlink';
const { width, height } = Dimensions.get('window')



class Searchscreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            newTweetContent: "",
            isModalOpen: false,
            daftarevent: [],
            eventname: '',
            imgforupload: '',
            typeevent: '',
            searchText: '',
            page: 1,
            loadingMore: false,
            alleventforconcat: []
        };
        this._scrollOffset = 0
    }

    componentDidMount() {
        // this.fetchingEvent()
    }



    handleSearch = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}relawan/geteventbyrule?namaevent=${this.state.searchText}`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })
            if (response.data.success == true) {
                this.setState({
                    daftarevent: response.data.data
                })
                // console.log(response)
            } else {
                // console.log(response)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }



    fetchingEvent = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}relawan/geteventbyruleandname?p=${this.state.page}`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })
            if (response.data.success == true) {
                this.setState({
                    daftarevent: response.data.data
                })
                // console.log(response)
            } else {
                // console.log(response)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }


    handleSendEvent = async () => {
        try {
            let data = JSON.stringify({
                eventName: this.state.eventname,
                relawanId: this.props.relawanid

            })
            if (this.state.eventname !== '') {
                let loginResponse = await axios.post(`${GLOBAL.apiUrl}event/sendevent`, data, {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
                if (loginResponse.data.success == true) {
                    this.fetchingEvent()
                    // console.log(loginResponse)
                    this.setState({ eventname: '' })
                    this.closeModal()
                    this.uplod(this.state.imgforupload, loginResponse.data.idhasinput)
                    // this.component._root.scrollToEnd()
                } else {
                    Alert.alert('Submeter um comentário Falha / Você não está logado')
                    // console.log(loginResponse)
                }
            } else {
                Alert.alert('A mensagem não pode estar vazia')
            }
        } catch (e) {
            // console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }
    }

    handleSendLike = async (ei) => {
        // Alert.alert(ei)
        try {
            let data = JSON.stringify({
                eventid: ei
            })

            let loginResponse = await axios.post(`${GLOBAL.apiUrl}relawan/sendlike`, data, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`,
                    'Content-Type': 'application/json',
                }
            })
            if (loginResponse.data.success == true) {
                const myArray = this.state.daftarevent
                objIndex = myArray.findIndex((obj => obj.eventId == ei));
                myArray[objIndex].youlike = !myArray[objIndex].youlike
                myArray[objIndex].likeCount = myArray[objIndex].youlike ? myArray[objIndex].likeCount + 1 : myArray[objIndex].likeCount - 1
                this.setState({ daftarevent: myArray })
                // console.log(loginResponse)
            } else {
                Alert.alert('Like gagal / Anda Belum Login/ anda sudah like laporan ini')
                // console.log(loginResponse)
            }

        } catch (e) {
            // console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }
    }

    handleUploadImage = () => {
        const options = {
            title: 'Foto de no máximo 4MB ',
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {

            }
            else if (response.error) {

            } else if (response.fileSize > 1000000) {
                Alert.alert('arquivo de foto é muito grande')
            }
            else {
                // console.log(response)
                // this.uplod(response)
                this.setState({ imgforupload: response })
            }
        });
    }


    uplod = (datanya, ei) => {
        // console.log(datanya)
        RNFetchBlob.fetch('POST', `${GLOBAL.apiUrl}event/imgnext`, {
            Accept: 'application/json',
            'Authorization': `${ei}`,
            'Content-Type': 'multipart/form-data',
        }, [
                {
                    name: 'pict',
                    filename: datanya.fileName,
                    type: datanya.type,
                    data: datanya.data
                }
            ]
        ).then((resp) => {
            let rspUpload = JSON.parse(resp.data);
            if (rspUpload.success == true) {
                this.fetchingEvent()
                this.setState({ eventname: '' })
            } else {
                Alert.alert('Falha ao atualizar a foto')
            }
        }).catch((err) => {
            // console.log(JSON.parse(err))
            // Alert.alert('Kesalahan Server')
        })
    }

    openModal = () => {
        this.setState({ isModalOpen: !this.state.isModalOpen })
    }

    closeModal = () => {
        this.setState({ isModalOpen: false })
    }

    openLinking = (ph) => {
        // Linking.openURL(`tel:021${800800}`)
        Linking.openURL(`tel:${ph}`)
    }

    openLinking2 = (ur) => {
        Alert.alert(
            'Perhatian',
            'Kunjungi Link',
            [
                { text: 'Cancel', onPress: () => console.log('no') },
                { text: 'Ok', onPress: () => Linking.openURL(ur) }
            ] //, { cancelable: false }
        )

    }

    handleScroll = (event) => {
        const CustomLayoutLinear = {
            duration: 100,
            create: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity },
            update: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity },
            delete: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity }
        }

        const currentOffset = event.nativeEvent.contentOffset.y
        let shouldShow

        if (currentOffset > this._scrollOffset || currentOffset === 0) {//0
            // scroll down or on top of ScrollView
            shouldShow = false
            if (this.isCloseToBottom(event.nativeEvent.layoutMeasurement, event.nativeEvent.contentOffset, event.nativeEvent.contentSize)) {
                this.setState({ page: this.state.page + 1 }, this.handleConcat)
                //this.handleConcat()
            }
        } else {
            // scroll up
            shouldShow = true
        }

        if (shouldShow !== this.state.shouldShow && (!shouldShow || currentOffset > 250)) {//250
            LayoutAnimation.configureNext(CustomLayoutLinear)
            this._scrollOffset = currentOffset
            this.setState({ shouldShow })
        }

        this._scrollOffset = currentOffset
    }

    handleConcat = async () => {
        try {
            this.setState({ loadingMore: true })
            const response = await axios.get(`${GLOBAL.apiUrl}relawan/geteventbyrule?namaevent=${this.state.searchText}&p=${this.state.page}`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })
            if (response.data.success == true) {
                await this.setState({
                    alleventforconcat: response.data.data,
                })
                await this.setState({
                    daftarevent: this.state.daftarevent.concat(this.state.alleventforconcat),

                })
                this.setState({ loadingMore: false })

                // console.log(response)
            } else {
                // console.log(response)
                this.setState({ loadingMore: false })
            }
        }
        catch (e) {
            // console.log(e)
            this.setState({ loadingMore: false })
        }
    }

    isCloseToBottom = (layoutMeasurement, contentOffset, contentSize) => {
        const paddingToBottom = 10;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };


    render() {
        if (this.props.tweetPosted === "success") {
            this.closeModal();
        }
        return (
            <Container>


                <Content onScroll={this.handleScroll} style={{ backgroundColor: "white" }}>

                    <View style={styles.inputContainer}>
                        <Image style={[styles.icon, styles.inputIcon]} source={{ uri: 'https://png.icons8.com/search/androidL/100/000000' }} />
                        <TextInput style={styles.inputs}
                            ref={'txtPassword'}
                            placeholder="Search"
                            underlineColorAndroid='transparent'
                            onChangeText={(searchText) => this.setState({ searchText }, this.handleSearch)} />
                    </View>


                    <View contentContainerStyle={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center"
                    }}>
                    </View>
                    {this.state.daftarevent.map((res, i) => {
                        return (
                            <View key={i} style={{ justifyContent: "flex-start" }}>
                                <View style={styles.tweet}>
                                    <TouchableHighlight
                                        underlayColor="white"
                                        activeOpacity={0.75}>
                                        <View style={{ flex: 1, flexDirection: "row" }}>
                                            <Thumbnail source={{ uri: res.relawan.photoRelawan }} />
                                            <View style={{
                                                flexDirection: "column",
                                                justifyContent: "flex-start"
                                            }}>
                                                <Text style={{
                                                    paddingLeft: 15
                                                }}>
                                                    {
                                                        res.relawan.namaJabatan
                                                    }

                                                </Text>
                                                <Text style={{
                                                    paddingLeft: 15,
                                                    fontWeight: "bold",
                                                    fontSize: 20,
                                                    color: 'black'
                                                }}>
                                                    {res.relawan.namaRelawan}

                                                </Text>

                                                <Text style={{
                                                    paddingLeft: 15,
                                                    color: "#aaa",
                                                    fontSize: 16
                                                }}>
                                                    {res.createDate}
                                                </Text>

                                                <Text style={{
                                                    paddingLeft: 15,
                                                    color: "#aaa",
                                                    fontSize: 16
                                                }}>
                                                    {res.kecamatan == '0' ? ' Semua Kecamatan ' : res.kecamatan}
                                                    {`, `}
                                                    {res.kabupaten == '0' ? ' Semua Kabupaten ' : res.kabupaten}
                                                </Text>
                                            </View>

                                        </View>
                                    </TouchableHighlight>

                                    {
                                        res.isPhotoExist == null || res.isPhotoExist == '' ? null :
                                            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('imageviewer', { imglempar: res.eventPhoto })}>
                                                <View style={{ minHeight: 250, paddingBottom: 10, marginRight: 5, borderBottomColor: 'grey', borderBottomWidth: 0.5 }}>
                                                    {/* <Image source={{ uri: res.eventPhoto }} resizeMode="contain" style={{ flex: 1 }} /> */}
                                                    <AutoHeightImage

                                                        width={width}
                                                        source={{ uri: res.eventPhoto }}
                                                    />
                                                </View>
                                            </TouchableWithoutFeedback>
                                    }

                                    <View style={{
                                        flexDirection: "column",
                                        justifyContent: "flex-start"
                                    }}>
                                        <Text style={{
                                            paddingLeft: 15,
                                            fontSize: 16,
                                            fontWeight: 'bold',
                                            color: "black"
                                        }}>
                                            {(res.type == '1' || res.type == '2') ? `Laporan ${res.namatype} : ` : `Type ${res.namatype} :`}
                                        </Text>
                                        <Hyperlink
                                            linkStyle={{ color: '#2980b9' }}
                                            onPress={(url, text) => this.openLinking2(url)}>
                                            <Text
                                            selectable={true}
                                            style={{
                                                paddingLeft: 25,
                                                color: "#aaa",
                                                fontSize: 16
                                            }}>
                                                {res.eventName}
                                            </Text>
                                        </Hyperlink>
                                        {
                                            (res.type == 1 || res.type == 2) &&
                                            <Text style={{
                                                paddingLeft: 15,
                                                color: "black",
                                                fontSize: 16,
                                                fontWeight: 'bold',
                                            }}>
                                                Usulan :
                                                </Text>
                                        }
                                        {
                                            (res.type == 1 || res.type == 2) &&
                                            <Text style={{
                                                paddingLeft: 25,
                                                color: "#aaa",
                                                fontSize: 16
                                            }}>
                                                {res.usulan}
                                            </Text>
                                        }

                                        {/* <Text style={{
                                            paddingLeft: 15,
                                            color: "black",
                                            fontSize: 16,
                                            fontWeight: 'bold',
                                        }}>
                                            Usulan :
                                                </Text>

                                        <Text style={{
                                            paddingLeft: 25,
                                            color: "#aaa",
                                            fontSize: 16
                                        }}>
                                            {res.usulan}
                                        </Text> */}

                                    </View>

                                    <Text style={styles.tweetText}>{}</Text>

                                    {(this.props.relawanid == res.relawan.relawanId || (this.props.fasilitator !== null)) && <View style={styles.tweetFooter}>
                                        <View style={styles.footerIcons}>
                                            <Button transparent dark onPress={() => this.props.navigation.navigate('eventdetail', { eventIdLempar: res.eventId })}>
                                                <Icon name="ios-text-outline" />
                                                <Text style={styles.badgeCount}>{res.commentCount}</Text>
                                            </Button>
                                        </View>
                                        {
                                            // this.props.fasilitator == null && this.props.jabatan == '0' && this.props.eposko == '0' ? null :
                                            // <View style={styles.footerIcons}>
                                            //     <Button transparent dark onPress={() => this.props.navigation.navigate('eventdetailhusus', { eventIdLempar: res.eventId })}>
                                            //         <Icon name="ios-repeat" />
                                            //         <Text style={styles.badgeCount}>{}</Text>
                                            //     </Button>
                                            // </View>
                                        }

                                        {
                                            // this.props.fasilitator == null && this.props.jabatan == '0' && this.props.eposko == '0' ? null :
                                            // <View style={styles.footerIcons} >
                                            //     <Button transparent dark onPress={() => this.handleSendLike(res.eventId)}>
                                            //         {res.youlike == true ? <Icon name="ios-heart" style={{ color: 'red' }} /> : <Icon name="ios-heart-outline" />}
                                            //         <Text style={styles.badgeCount}>{res.likeCount}</Text>
                                            //     </Button>
                                            // </View>
                                        }

                                        <View style={styles.footerIcons}>
                                            <Button transparent dark onPress={() => this.openLinking(res.relawan.phone)}>
                                                <Icon name="ios-call-outline" />
                                            </Button>
                                        </View>
                                    </View>}



                                </View>
                            </View>
                        )
                    })}

                    {this.state.loadingMore ? <ActivityIndicator size="large" color={GLOBAL.mainColor}
                        style={{ marginBottom: 30, marginTop: 20 }} /> : <View
                            style={{
                                alignSelf: 'center', width: width * 0.5,
                                marginTop: 30, marginBottom: 30, borderBottomColor: GLOBAL.mainColor,
                                borderBottomWidth: 3
                            }}></View>}

                </Content>
            </Container>
        );
    }
}


const styles = StyleSheet.create({
    topMargin: {
        backgroundColor: "white",
        zIndex: -1,
        borderBottomColor: 'grey',
        borderBottomWidth: 0.4
    },
    content: {
        padding: 10,
        backgroundColor: "white"
    },
    heading: {
        fontSize: 32,
        fontWeight: "400",
        marginBottom: 30
    },
    tweet: {
        paddingTop: 20,
        paddingBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        borderBottomColor: "#bbb",
        borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection: "column"
    },
    tweetText: {
        marginTop: 10,
        fontSize: 18,
        color: "#555"
    },
    tweetFooter: {
        flexDirection: "row",
        // justifyContent: "flex-start",
        justifyContent: "space-around",
        padding: 0
    },
    badgeCount: {
        fontSize: 12,
        paddingLeft: 5
    },
    footerIcons: {
        flexDirection: "row",
        alignItems: "center"//center
    },
    modalFooter: {
        backgroundColor: "white",
        elevation: 3,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0.2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        height: 54,
        width: "100%",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        padding: 5
    },
    modal: {
        justifyContent: "flex-start",
        alignItems: "center",
        position: "absolute",
        zIndex: 4,
        elevation: 4,
        // height: Dimensions.get("window").height //- Expo.Constants.statusBarHeight,
        // marginTop: Expo.Constants.statusBarHeight / 2
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#E6ECF0',
        borderRadius: 30,
        borderBottomWidth: 1,
        height: 45,
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        margin: 10,
    },
    icon: {
        width: 30,
        height: 30,
        marginLeft: 20
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#FFFFFF',
        flex: 1,
    }
});

//export default HomeScreen
const mapStateToProps = (state) => {
    const { relawanid, token } = state.anggota;
    return { relawanid, token };
};

export default connect(mapStateToProps, {})(Searchscreen);