import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Dimensions, View, Text,
    StatusBar, Alert,
    TouchableWithoutFeedback, Picker
} from 'react-native';
import {
    Container,
    Content,
    Textarea
} from 'native-base';
import { connect } from 'react-redux';
import { testing, loginsuccess6 } from '../rdx/actions';
import { TextField } from 'react-native-material-textfield';
import DatePicker from "../components/datepicker.js";
import axios from 'axios';
import { HeaderRightBack } from '../particles/HeaderRightBack';
const { width, height } = Dimensions.get('window');


class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            alamat: '',
            alamatSelected: '',
            showForm: false,
            nama: '',
            nik: '',
            tempat: '',
            ttl: '',
            jk: '',
            job: '',
            address: '',
            phone: '',
            namakoperasi: ''
        }

    }



    componentDidMount() {

    }




    fetchingDataAnggota = async () => {
        // try {
        //     const response = await axios.get(`${GLOBAL.apiUrl}api/apianggota/detailanggota`, {
        //         headers: {
        //             'Authorization': `Bearer ${this.props.token}`
        //         }
        //     })
        //     if (response.data.success == true) {
        //         this.setState({
        //             nama: response.data.data.namaAnggota,
        //             nik: response.data.data.ktpAnggota,
        //             tempat: response.data.data.tempatLahir,
        //             ttl: response.data.data.ttlAnggota,
        //             jk: response.data.data.jkAnggota,
        //             job: response.data.data.pekerjaanAnggota,
        //             address: response.data.data.alamatAnggota,
        //             phone: response.data.data.phoneAnggota,
        //             namakoperasi: response.data.data.namaKoperasi
        //         })
        //         console.log(response)
        //     } else {
        //         Alert.alert('anda belum login')
        //         console.log(response)
        //     }
        // }
        // catch (e) {
        //     Alert.alert('kesalahan Server')
        //     console.log(e)
        // }
    }



    handleupdate = async () => {
        // try {
        //     let data = JSON.stringify({
        //         nama: this.state.nama,
        //         noktp: this.state.nik,
        //         tempat: this.state.tempat,
        //         ttl: this.state.ttl,
        //         jk: this.state.jk,
        //         pekerjaan: this.state.job,
        //         alamat: this.state.address,
        //         telp: this.state.phone,
        //         namakoperasi: this.state.namakoperasi
        //     })
        //     const response = await axios.post(`${GLOBAL.apiUrl}api/apianggota/updatedata`, data, {
        //         headers: {
        //             'Authorization': `Bearer ${this.props.token}`,
        //             'Content-Type': 'application/json'
        //         }
        //     })
        //     if (response.data.success == true) {
        //         console.log(response)
        //         Alert.alert('update berhasil')
        //         this.handleLoginGoogle()
        //     } else {
        //         Alert.alert('data belum lengkap')
        //         console.log(response)
        //     }
        // }
        // catch (e) {
        //     Alert.alert('kesalahan Server')
        //     console.log(e)
        // }
    }



    handleLoginGoogle = async () => {
        // try {
        //     let data = JSON.stringify({
        //         email: this.props.email
        //     })
        //     let loginResponse = await axios.post(`${GLOBAL.apiUrl}api/authanggota/logingoogle`, data, {
        //         headers: {
        //             'Content-Type': 'application/json',
        //         }
        //     })
        //     if (loginResponse.data.success == true) {
        //         await this.props.loginsuccess6(loginResponse.data.isAnggota)
        //         this.props.navigation.navigate('home')
        //         console.log(loginResponse)
        //     } else {
        //         console.log(loginResponse)
        //     }


        // } catch (e) {
        //     console.log(e)
        // }
    }





    render() {

        return (
            <Container style={{ backgroundColor: 'transparent' }}>
                {/* <StatusBar
                    backgroundColor={GLOBAL.mainColor}
                    animated
                /> */}
                <HeaderRightBack
                    goBack={() => this.props.navigation.goBack()}
                    txtTitle="Perfil"
                    openDrawer={() => this.props.navigation.openDrawer()}
                />

                <Content style={{ marginBottom: 10 }}>

                    <View style={[GLOBAL.applyShadow, styles.container2]}>

                        <View style={{ width: width * 0.9, paddingLeft: 20, paddingBottom: 30 }}>
                            <TextField
                                label="Nama"
                                value={this.state.nama}
                                onChangeText={(nama) => this.setState({ nama })}
                            />
                            <TextField
                                label="NIK / KTP"
                                value={this.state.nik}
                                onChangeText={(nik) => this.setState({ nik })}
                            />
                            <TextField
                                label="Tempat Lahir"
                                value={this.state.tempat}
                                onChangeText={(tempat) => this.setState({ tempat })}
                            />

                            <DatePicker
                                style={{ width: "40%", marginTop: 8, marginLeft: -18 }}
                                date={this.state.ttl}
                                mode="date"
                                placeholder="Tanggal Lahir"
                                format="DD-MM-YYYY"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                customStyles={{
                                    dateInput: {
                                        marginLeft: 10,
                                        marginTop: 8,
                                        marginRight: 8,
                                        marginBottom: 8,
                                        padding: 8,
                                        alignItems: "flex-start"
                                    },
                                    placeholderText: {
                                        color: "#000000"
                                    }
                                }}
                                onDateChange={date => {
                                    this.setState({ ttl: date });
                                }}
                            />
                            <View style={{ marginTop: 20, marginLeft: -8 }} >
                                <Picker
                                    selectedValue={this.state.jk}
                                    style={{ marginTop: -15, height: 50, width, borderBottomColor: 'red', borderBottomWidth: 0.5 }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ jk: itemValue })}

                                >
                                    <Picker.Item label="Pilih Jenis Kelamin" value="0" />
                                    <Picker.Item label="Laki-laki" value="Laki-laki" />
                                    <Picker.Item label="Perempuan" value="Perempuan" />
                                </Picker>

                            </View >
                            <TextField
                                label="Pekerjaan"
                                value={this.state.job}
                                onChangeText={(job) => this.setState({ job })}
                            />

                            <TextField
                                label="Número de telefone"
                                value={this.state.phone}
                                onChangeText={(phone) => this.setState({ phone })}
                            />

                            <TextField
                                label="Nama Koperasi"
                                value={this.state.namakoperasi}
                                onChangeText={(namakoperasi) => this.setState({ namakoperasi })}
                            />
                        </View>

                    </View >



                    <View style={styles.mainStyle}>
                        <View style={{ width: width * 0.97, alignSelf: 'center' }}>
                            <TouchableWithoutFeedback onPress={this.handleupdate}>
                                <View style={{ backgroundColor: GLOBAL.mainColor, height: 60, justifyContent: 'center', alignItems: 'center' }} onPress={() => Alert.alert('tik')}>
                                    <Text style={{ fontSize: 20, color: 'white' }}>Update</Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>


                </Content>


            </Container >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        minHeight: 150,
        backgroundColor: 'white',
        flexDirection: 'row',
        margin: 4
    },
    container2: {
        flex: 1,
        width,
        minHeight: 100,
        backgroundColor: 'white',
        margin: 4,
        paddingTop: 10
    },
    mainStyle: {
        height: 50,
        backgroundColor: 'black',
        alignSelf: 'center',
        borderRadius: 30,
        // position: 'absolute',
        // bottom: 5,
        flexDirection: 'row'
    }

});

export default Profile
// const mapStateToProps = (state) => {
//     const { token, email } = state.anggota;
//     return { token, email };
// };

// export default connect(mapStateToProps, { testing, loginsuccess6 })(DetailAnggota);
