import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
  Dimensions,
  // Image
  PermissionsAndroid,
} from 'react-native';
// import getTheme from "../native-base-theme/components";
// import platform from "../native-base-theme/variables/platform";
import material from '../native-base-theme/variables/material';
import {NavigationActions} from 'react-navigation';
import {
  Container,
  Button,
  Text,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Title,
  StyleProvider,
  Content,
  Grid,
  Col,
  Row,
  Input,
  Item,
  Form,
  Label,
  Footer,
  FooterTab,
  Spinner,
} from 'native-base';
const {width, height} = Dimensions.get('window');
import {connect} from 'react-redux';
import Image from 'react-native-remote-svg';
import {
  loginsuccess2,
  loginsuccess3,
  loginsuccess4,
  loginsuccess,
  loginsuccess5,
  loginsuccess7,
  loginsuccess8,
  loginsuccess9,
  savephone
} from '../rdx/actions';
import axios from 'axios';
import AwesomeAlert from 'react-native-awesome-alerts';
import OneSignal from 'react-native-onesignal';
import RNSimData from 'react-native-sim-data';
import KeyEvent from 'react-native-keyevent';
// import VersionNumber from 'react-native-version-number';
class Loginscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAlert: false,
      emailorphone: '',
      password: '',
      loading: false,
      secure: true,
      iccid: '',
      iccid2: '',
    };
  }

  /*
      if (loginResponse.data.fasilitator == null && parseInt(loginResponse.data.jabatan) == 0 && parseInt(loginResponse.data.eposko) == 0) {
                            this.props.navigation.navigate('home2')
                        } else if (parseInt(loginResponse.data.jabatan) > 0) {
                            this.props.navigation.navigate('home')
                        } else if (parseInt(loginResponse.data.eposko) > 0) {
                            this.props.navigation.navigate('home3')
                        }
    
    */

  componentDidMount() {
    OneSignal.init('04f752ea-ae43-42a1-8488-75b945eb9447', {
      kOSSettingsKeyAutoPrompt: true,
    });
    // if (this.props.fasilitator == null && parseInt(this.props.jabatan) == 0 && parseInt(this.props.eposko) == 0) {
    //     this.props.navigation.navigate('home2')
    // } else if (parseInt(this.props.jabatan) > 0) {
    //     this.props.navigation.navigate('home')
    // } else if (parseInt(this.props.eposko) > 0) {
    //     this.props.navigation.navigate('home3')
    // }

    this.requestSimPermission();

    // KeyEvent.onKeyDownListener((keyEvent) => {
    //    Alert.alert('down')
    // });
    // KeyEvent.onKeyUpListener((keyEvent) => {
    //     Alert.alert('Up')
    // });
  }

  // componentWillUnmount() {
  //     KeyEvent.removeKeyDownListener();
  //     KeyEvent.removeKeyUpListener();
  //   }

  requestSimPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
        {
          title: 'Tempo Witness Location Permission',
          message:
            'Tempo Witness access to your locations ' +
            'so you can send location.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('You can use the Sim');
        // console.log(RNSimData.getSimInfo().simSerialNumber1);
        this.setState({iccid: RNSimData.getSimInfo().simSerialNumber0});
        // this.setState({iccid2: 8});
        if (RNSimData.getSimInfo().simSerialNumber1) {
          this.setState({iccid2: RNSimData.getSimInfo().simSerialNumber1});
          console.log(RNSimData.getSimInfo().simSerialNumber1);
        }
      } else {
        console.log('Sim permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  toHome = () => {
    this.props.navigation.navigate('ho');
  };
  handleLogin2 = async () => {
    try {
      this.setState({loading: true});
      if (this.state.emailorphone !== '' && this.state.password !== '') {
        let data = JSON.stringify({
          emailorphone: this.state.emailorphone,
          password: this.state.password,
        });
        let loginResponse = await axios.post(
          `${GLOBAL.apiUrl}auth/login`,
          data,
          {
            headers: {
              'Content-Type': 'application/json',
            },
          },
        );
        if (loginResponse.data.success == true) {
          if (
            loginResponse.data.status == 0 ||
            loginResponse.data.status == '0'
          ) {
            Alert.alert('Login falhou, e-mail / palavras-passe erradas');
            this.setState({loading: false});
          } else {
            this.setState({loading: false});
            this.props.loginsuccess2(loginResponse.data.userEmail);
            this.props.savephone(loginResponse.data.userPhone);
            this.props.navigation.navigate('otp');
            this.setState({loading: false});
          }

          this.setState({loading: false});
        } else {
          // console.log(loginResponse)
          Alert.alert('Login falhou, e-mail / palavras-passe erradas');
          this.setState({loading: false});
        }
      } else {
        this.setState({showAlert: !this.state.showAlert, loading: false});
      }
    } catch (e) {
      console.log(e);
      Alert.alert('erro de servidor');
      this.setState({loading: false});
    }
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });
  };

  hideAlert = () => {
    this.setState({
      showAlert: false,
    });
  };

  render() {
    return (
      <Container style={styles.topMargin}>
        {/* <StatusBar

                    backgroundColor='transparent'
                    animated
                /> */}
        <Header noShadow style={{backgroundColor: 'white'}}>
          <Left style={{flex: 1}} />
          <Body style={{flex: 1}}>
            {/* <Icon
                            name="logo-twitter"
                            style={{ alignSelf: "center", color: "#4286f4" }}
                        /> */}
          </Body>
          <Right style={{flex: 1}}>
            {/* <Button transparent>
                            <Text style={{ color: "#4286f4" }}>Sign up</Text>
                        </Button>
                        <Button transparent>
                            <Icon name="more" style={{ color: "#4286f4" }} />
                        </Button> */}
          </Right>
        </Header>
        <Content style={styles.content}>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Image
              resizeMode="contain"
              style={{height: 100, width: 300}}
              source={require('../assets/witness-logo.png')}
            />
          </View>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            {/* <Image source={require('../assets/logo.png')} resizeMode="contain" style={{ width: 100, height: 100 }} /> */}
            <Text style={styles.heading}>Iniciar sessão (Entrar)</Text>
            <View style={styles.inputContainer}>
              <Image
                style={[styles.icon, styles.inputIcon]}
                source={require('../assets/phone.png')}
              />
              <TextInput
                style={styles.inputs}
                onChangeText={emailorphone => this.setState({emailorphone})}
                placeholder="Correio eletrónico"
                value={this.state.emailorphone}
                keyboardType="email-address"
                underlineColorAndroid="transparent"
              />
            </View>
            <View style={styles.inputContainer}>
              <Image
                style={[styles.icon, styles.inputIcon]}
                source={require('../assets/lock.png')}
              />
              <TextInput
                style={styles.inputs}
                onChangeText={password => this.setState({password})}
                placeholder="Password"
                value={this.state.password}
                secureTextEntry={this.state.secure}
                underlineColorAndroid="transparent"
              />
              {/* <View 
                            style={{ 
                                position:'absolute',
                                right:0,
                             }}> */}
              <Icon
                onPress={() => this.setState({secure: !this.state.secure})}
                name={this.state.secure ? 'md-eye' : 'md-eye-off'}
                size={20}
                style={{
                  marginRight: 20,
                  color: this.state.secure ? '#3498db' : 'gray',
                }}
              />
              {/* </View> */}
            </View>

            <TouchableOpacity
              style={[
                styles.buttonContainer,
                styles.loginButton,
                {alignSelf: 'center'},
              ]}
              onPress={this.handleLogin2}>
              {this.state.loading ? (
                <Spinner color="white" />
              ) : (
                <Text style={styles.loginText}>Iniciar sessão (Entrar)</Text>
              )}
            </TouchableOpacity>

            <View
              style={
                {
                  // flexDirection: 'row'
                }
              }>
              {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('forgotpassword')}>
                                <View>
                                    <Text style={{ color: GLOBAL.mainColor }}>Forgot Password ? </Text>
                                </View>
                            </TouchableOpacity> */}
              {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('register')}>
                                <View>
                                    <Text style={{ color: GLOBAL.mainColor }}>Register </Text>
                                </View>
                            </TouchableOpacity> */}

              {/* <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity onPress={this.requestSimPermission}>
                  <Text
                    style={{
                      color: 'grey',
                    }}>
                    ICCID
                  </Text>
                </TouchableOpacity>
                <Text
                  selectable
                  style={{
                    color: 'grey',
                  }}>
                  {this.state.iccid}
                </Text>
              </View> */}

              {
            //   this.state.iccid2 !== null && (
            //     <View
            //       style={{
            //         justifyContent: 'center',
            //         alignItems: 'center',
            //       }}>
            //       <Text
            //         style={{
            //           color: 'grey',
            //         }}>
            //         ICCID2
            //       </Text>
            //       <Text
            //         style={{
            //           color: 'grey',
            //         }}>
            //         {this.state.iccid2}
            //       </Text>
            //     </View>
            //   )
              }
            </View>

            {/* <Item stackedLabel last>
                            <Label>Phone number or email address</Label>
                            <Input
                                value={this.state.emailorphone}
                                onChangeText={(emailorphone) => this.setState({ emailorphone })}
                            />
                        </Item>
                        <Item stackedLabel last>
                           
                            <Label>Password</Label>
                            <Input
                                secureTextEntry={true}
                                value={this.state.password}
                                onChangeText={(password) => this.setState({ password })}
                            />
                        </Item> */}

            {/* <Button
                            onPress={() => this.props.navigation.navigate('forgotpassword')}
                            transparent
                            style={{
                                margin: 15,
                                marginTop: 25,
                                width: "50%",
                                alignSelf: "center"
                            }}>
                            <Text style={{ textAlign: "center", fontSize: 14, color: "#AAA" }}>
                                Forgot password?
                                </Text>
                        </Button>

                        <Button
                            onPress={() => this.props.navigation.navigate('register')}
                            transparent
                            style={{
                                margin: 15,
                                marginTop: 25,
                                width: "50%",
                                alignSelf: "center"
                            }}>
                            <Text style={{ textAlign: "center", fontSize: 14, color: "#AAA" }}>
                                Register
                            </Text>
                        </Button> */}
          </View>
        </Content>
        {/* <Footer style={styles.footer}>
          {this.props.loginStatus === 'ongoing' ? <Spinner /> : null}
          {this.props.loginStatus === 'failed' ? (
            <Text style={{color: '#f92a3f'}}>Login Failed</Text>
          ) : null}
          {/* <Button
                        rounded
                        style={{ backgroundColor: "#4286f4", marginLeft: 20 }}
                        onPress={this.handleLogin2}
                    >
                        {this.state.loading ? <Spinner color="white" /> : <Text>Log in</Text>}
                    </Button>
        </Footer> */}
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title="Aviso"
          message="Todos os dados são necessários!"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={false}
          showConfirmButton={true}
          cancelText="OK"
          confirmText="OK"
          confirmButtonColor="#DD6B55"
          onCancelPressed={() => {
            this.hideAlert();
          }}
          onConfirmPressed={() => {
            this.hideAlert();
          }}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  topMargin: {
    // marginTop: 25
  },
  content: {
    padding: 10,
    backgroundColor: 'white',
    // justifyContent:'center',
    //alignItems:'center'
  },
  heading: {
    fontSize: 32,
    fontWeight: '400',
    marginBottom: 30,
  },
  footer: {
    backgroundColor: 'white',
    elevation: 3,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 0.2},
    shadowOpacity: 0.3,
    shadowRadius: 2,
    height: 60,
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#B0E0E6',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#E6ECF0',
    borderRadius: 30,
    borderBottomWidth: 1,
    height: 45,
    marginBottom: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  icon: {
    width: 30,
    height: 30,
  },
  inputIcon: {
    marginLeft: 15,
    justifyContent: 'center',
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: width * 0.94,
    borderRadius: 30,
  },
  loginButton: {
    backgroundColor: '#3498db',
  },
  fabookButton: {
    backgroundColor: '#3b5998',
  },
  googleButton: {
    backgroundColor: '#ff0000',
  },
  loginText: {
    color: 'white',
  },
  restoreButtonContainer: {
    width: 250,
    marginBottom: 15,
    alignItems: 'flex-end',
  },
  socialButtonContent: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  socialIcon: {
    color: '#FFFFFF',
    marginRight: 5,
  },
});

const mapStateToProps = state => {
  const {token, fasilitator, jabatan, eposko} = state.anggota;
  return {token, fasilitator, jabatan, eposko};
};

export default connect(
  mapStateToProps,
  {
    loginsuccess2,
    loginsuccess3,
    loginsuccess4,
    loginsuccess,
    loginsuccess5,
    loginsuccess7,
    loginsuccess8,
    loginsuccess9,
    savephone
  },
)(Loginscreen);
