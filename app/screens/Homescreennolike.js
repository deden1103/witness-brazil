import React, {Component} from 'react';
import Modal from 'react-native-modalbox';
import Dimensions from 'Dimensions';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Platform,
  Alert,
  ScrollView,
  StatusBar,
  Image,
  Picker,
  Linking,
  LayoutAnimation,
  ActivityIndicator,
  BackHandler,
  ToastAndroid,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import {
  Container,
  Header,
  Body,
  Content,
  Left,
  Title,
  Thumbnail,
  Col,
  Row,
  Grid,
  Icon,
  Spinner,
  Fab,
  Button,
  Footer,
  Input,
  Right,
} from 'native-base';
import {connect} from 'react-redux';
import OneSignal from 'react-native-onesignal';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import {withNavigationFocus, NavigationEvents} from 'react-navigation';
import AutoHeightImage from 'react-native-auto-height-image';
import FastImage from 'react-native-fast-image';
import Hyperlink from 'react-native-hyperlink';
import Video from 'react-native-af-video-player';

import Sound from 'react-native-sound';
import {AudioRecorder, AudioUtils} from 'react-native-audio';
import {Thumbnail as Thm} from 'react-native-thumbnail-video';
import TopButton from '../components/TopButton';
import HTML from 'react-native-render-html';

import {update_list_article} from '../rdx/actions';
import MyFastImage from '../components/MyFastImage';
const {width, height} = Dimensions.get('window');

class Homescreennolike extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      active: false,
      newTweetContent: '',
      isModalOpen: false,
      isModalOpen2: false,
      isModalOpen3: false,
      daftarevent: [],
      eventname: '',
      imgforupload: '',
      typeevent: '',
      osid: '',
      page: 1,
      loadingMore: false,
      alleventforconcat: [],
      kabupaten: '0',
      kecamatan: '0',
      organisasi: '0',
      usulan: '',
      kabupatenlist: [],
      kabupatenlist2: [],
      kecamatanlist: [],
      kecamatanlist2: [],
      organisasilist: [],
      backClickCount: 0,
      gambarUri: '',
      gambarType: '',
      gambarFilename: '',
      letsPlay: false,
      isply: false,
      description: '',
      isLongDescript: false,
      listArticle: [],
      isCollapse: null,
      audioIsPlaying: true,
      audioIsPlayingIndex: '',
      isPlayVideo: null,
    };
    this._scrollOffset = 0;
  }

  _onRefresh = async () => {
    try {
      this.setState({refreshing: true});
      await this.fetchingListArticle();
      await this.setState({refreshing: false, page: 1});
    } catch (e) {
      this.setState({refreshing: false, page: 1});
    }
  };

  handleReadMore = () => {
    this.setState(
      {isLongDescript: !this.state.isLongDescript},
      this.handleUbah,
    );
  };
  handleUbah = () => {
    if (this.state.isLongDescript) {
      this.setState({description: this.state.descript2});
    } else {
      this.setState({description: this.state.descript});
    }
  };

  handleBackButton = () => {
    return true;
  };

  componentDidMount() {
    this.setState({description: this.state.descript});
    this.fetchingListArticle();
  }

  shouldComponentUpdate(np, ns) {
    if (this.props.listArticleRedux !== np.listArticleRedux) {
      return true;
    }
    return true;
  }

  fetchingListArticle = async () => {
    try {
      this.setState({loading: false});
      let response = await fetch(GLOBAL.apiUrl + 'article/list', {
        headers: {
          Authorization: 'Bearer ' + this.props.token,
          Accept: 'application/json',
        },
      });
      let responseJson = await response.json();
      await this.setState({listArticle: responseJson.detail});
      this.props.update_list_article(this.state.listArticle);
    } catch (error) {
      this.setState({loading: false});
      console.error(error);
    }
  };

  handleConcat = async () => {
    try {
      this.setState({loadingMore: true});
      const response = await axios.get(
        `${GLOBAL.apiUrl}article/list?p=${this.state.page}`,
        {
          headers: {
            Authorization: `Bearer ${this.props.token}`,
            Accept: 'application/json',
          },
        },
      );
      if (response.data.success == true) {
        await this.setState({
          alleventforconcat: response.data.detail,
        });
        await this.setState({
          listArticle: this.state.listArticle.concat(
            this.state.alleventforconcat,
          ),
        });
        await this.props.update_list_article(this.state.listArticle);
        this.setState({loadingMore: false});
        console.log(this.props.listArticleRedux);
        // console.log(response)
      } else {
        // console.log(response)
        this.setState({loadingMore: false});
      }
    } catch (e) {
      // console.log(e)
      this.setState({loadingMore: false});
    }
  };

  handleCollapse = (numb) => {
    if (this.state.isCollapse == numb) {
      this.setState({isCollapse: null});
    } else {
      this.setState({isCollapse: numb});
    }
  };

  renderApproveButton = () => {
    return (
      <View
        style={{
          borderColor: '#0a923c',
          borderWidth: 2,
          padding: 10,
          borderRadius: 50,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 10,
        }}>
        <Text style={{color: '#0a923c'}}>Approved</Text>
      </View>
    );
  };

  renderWaitingButton = () => {
    return (
      <View
        style={{
          borderColor: 'red',
          borderWidth: 2,
          padding: 10,
          borderRadius: 50,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 10,
        }}>
        <Text style={{color: 'red'}}>Waiting</Text>
      </View>
    );
  };

  saveOneSignalId = () => {
    let data = JSON.stringify({
      osid: this.state.osid,
    });
    axios
      .post(`${GLOBAL.apiUrl}relawan/updateonesignal`, data, {
        headers: {
          Authorization: `Bearer ${this.props.token}`,
          'Content-Type': 'application/json',
        },
      })
      .then((res) => {
        // console.log(res)
      })
      .catch((e) => {
        // console.log(e)
      });
  };

  handleUploadImage = () => {
    const options = {
      title: 'Foto de no máximo 4MB ',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.fileSize > 10000000) {
        Alert.alert('arquivo de foto é muito grande');
      } else {
        // console.log(response)
        // this.uplod(response)
        this.setState({
          imgforupload: response,
          gambarFilename: response.fileName,
          gambarUri: response.uri,
          gambarType: response.type,
        });
      }
    });
  };

  uplod = (datanya, ei) => {
    // console.log(datanya)
    RNFetchBlob.fetch(
      'POST',
      `${GLOBAL.apiUrl}event/imgnext`,
      {
        Accept: 'application/json',
        Authorization: `${ei}`,
        'Content-Type': 'multipart/form-data',
      },
      [
        {
          name: 'pict',
          filename: datanya.fileName,
          type: datanya.type,
          data: datanya.data,
        },
      ],
    )
      .then((resp) => {
        let rspUpload = JSON.parse(resp.data);
        if (rspUpload.success == true) {
          this.fetchingEvent();
          this.setState({eventname: ''});
        } else {
          Alert.alert('Falha ao atualizar a foto');
        }
      })
      .catch((err) => {
        // console.log(JSON.parse(err))
        // Alert.alert('Kesalahan Server')
      });
  };

  openModal = () => {
    //canvasing
    this.setState({
      isModalOpen: !this.state.isModalOpen,
      typeevent: '3',
      usulan: '',
      imgforupload: '',
      gambarUri: '',
      gambarFilename: '',
      gambarType: '',
      letsPlay: '',
    });
  };
  openModaldua = () => {
    //isu
    this.setState({
      isModalOpen2: !this.state.isModalOpen2,
      typeevent: '1',
      usulan: '',
      imgforupload: '',
      gambarUri: '',
      gambarFilename: '',
      gambarType: '',
      letsPlay: '',
    });
  };

  openModaltiga = () => {
    //broadcast
    this.setState({
      isModalOpen3: !this.state.isModalOpen3,
      typeevent: '',
      usulan: '',
      imgforupload: '',
      gambarUri: '',
      gambarFilename: '',
      gambarType: '',
      letsPlay: '',
    });
    this.fetchDetailKabupaten2();
    //this.fetchDetailKecamatan2()
  };

  closeModal = () => {
    this.setState({
      isModalOpen: false,
      isModalOpen2: false,
      isModalOpen3: false,
    });
  };

  openLinking = (ph) => {
    Linking.openURL(`tel:${ph}`);
  };

  handleScroll = (event) => {
    const CustomLayoutLinear = {
      duration: 100,
      create: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity,
      },
      update: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity,
      },
      delete: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity,
      },
    };

    const currentOffset = event.nativeEvent.contentOffset.y;
    let shouldShow;

    if (currentOffset > this._scrollOffset || currentOffset === 0) {
      shouldShow = false;
      if (
        this.isCloseToBottom(
          event.nativeEvent.layoutMeasurement,
          event.nativeEvent.contentOffset,
          event.nativeEvent.contentSize,
        )
      ) {
        this.setState({page: this.state.page + 1}, this.handleConcat);
      }
    } else {
      shouldShow = true;
    }

    if (
      shouldShow !== this.state.shouldShow &&
      (!shouldShow || currentOffset > 250)
    ) {
      LayoutAnimation.configureNext(CustomLayoutLinear);
      this._scrollOffset = currentOffset;
      this.setState({shouldShow});
    }

    this._scrollOffset = currentOffset;
  };

  isCloseToBottom = (layoutMeasurement, contentOffset, contentSize) => {
    const paddingToBottom = 10;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  handlePlayAudioPress = async (uri, ind) => {
    await this.setState({audioIsPlaying: !this.state.audioIsPlaying});
    this.setState({audioIsPlayingIndex: ind});
    this.handlePlayAudio(uri);
  };

  handlePlayAudio = (uri) => {
    if (this.state.audioIsPlaying) {
      this.handleStopAudio();
    } else {
      Sound.setCategory('Playback');
      this.whoosh = new Sound(uri, Sound.MAIN_BUNDLE, (error) => {
        if (error) {
          console.log('failed to load the sound', error);
          return;
        }
        this.whoosh.play((success) => {
          if (success) {
            console.log('successfully finished playing');
            this.setState({audioIsPlaying: true, audioIsPlayingIndex: ''});
          } else {
            console.log('playback failed due to audio decoding errors');
          }
        });
      });
    }
  };

  handleStopAudio = () => {
    this.whoosh.pause();
    this.setState({audioIsPlaying: true, audioIsPlayingIndex: ''});
  };

  handleOnfocus = async () => {};

  handlePlayVideoWaitingStatus = (ind) => {
    this.setState({isPlayVideo: ind});
  };

  render() {
    if (this.props.tweetPosted === 'success') {
      this.closeModal();
    }
    return (
      <Container>
        <NavigationEvents
          onDidFocus={() => {
            this.handleOnfocus();
          }}
        />

        <Content
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
          onScroll={this.handleScroll}
          style={{backgroundColor: 'white'}}>
          <View
            contentContainerStyle={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}></View>

          <View style={{justifyContent: 'flex-start'}}>
            {/* =============================== */}

            {this.props.listArticleRedux.map((r, i) => (
              <View key={i} style={styles.tweet}>
                <TouchableHighlight underlayColor="white" activeOpacity={0.75}>
                  <View
                    style={{
                      // flex: 1,
                      flexDirection: 'row',
                    }}>
                    <Thumbnail
                      source={{uri: r.userPict}}
                      style={{marginBottom: 15}}
                    />
                    <View
                      style={{
                        flexDirection: 'column',
                        justifyContent: 'flex-start',
                        flexWrap: 'wrap',
                        width: width * 0.7,
                      }}>
                      <Text
                        style={{
                          paddingLeft: 15,
                        }}>
                        {r.userUpload}
                      </Text>
                      <Text
                        style={{
                          paddingLeft: 15,
                          fontWeight: 'bold',
                          fontSize: 20,
                          color: 'black',
                          flexWrap: 'wrap',
                        }}>
                        {r.title}
                      </Text>
                    </View>
                  </View>
                </TouchableHighlight>

                {r.type == 'image' && r.isExist && (
                  <View style={{minHeight: 250, paddingBottom: 10}}>
                    <MyFastImage style={{height: r.height}} urlPict={r.media} />
                  </View>
                )}

                {r.type == 'video' ? (
                  r.artcEmbedURL === null ? (
                    this.state.isPlayVideo == i ? (
                      <Video autoPlay={true} url={r.media} />
                    ) : (
                      <View
                        style={{
                          height: 180,
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'rgba(0, 0, 0, 0.5)',
                        }}>
                        <Image
                          resizeMode="contain"
                          style={{
                            height: 100,
                            width: 300,
                            position: 'absolute',
                            opacity: 0.5,
                          }}
                          source={require('../assets/witness-logo.png')}
                        />
                        <TouchableOpacity
                          onPress={() => this.handlePlayVideoWaitingStatus(i)}>
                          <Icon
                            name="play"
                            style={{
                              color: 'white',
                              fontSize: 50,
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                    )
                  ) : (
                    <Thm url={r.artcEmbedURL} />
                  )
                ) : null}

                {r.type == 'sound' && r.isExist && (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                      alignItems: 'flex-end',
                      width: width * 0.7,
                      alignSelf: 'center',
                      marginTop: 10,
                    }}>
                    <View>
                      <TouchableOpacity
                        onPress={() =>
                          this.handlePlayAudioPress(r.media, r.articleId)
                        }>
                        <View
                          style={{
                            height: 80,
                            width: 80,
                            backgroundColor: 'red',
                            borderRadius: 50,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <Icon
                            type="FontAwesome"
                            name={
                              this.state.audioIsPlayingIndex == r.articleId
                                ? 'stop'
                                : 'play'
                            }
                          />
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                )}

                <View
                  style={{
                    flexDirection: 'column',
                    justifyContent: 'flex-start',
                  }}>
                  <Text
                    style={{
                      paddingLeft: 15,
                      fontSize: 16,
                      fontWeight: 'bold',
                      color: 'black',
                    }}>
                    Detail :
                  </Text>

                  {this.state.isCollapse == i ? (
                    <View style={{alignSelf: 'center', width: width * 0.86}}>
                      <HTML html={r.longDesc == '' ? ' ' : r.longDesc} />
                    </View>
                  ) : (
                    <View style={{alignSelf: 'center', width: width * 0.86}}>
                      <HTML html={r.shortDesc == '' ? ' ' : r.shortDesc} />
                    </View>
                  )}

                  {r.longDesc.length < 150 ? null : (
                    <TouchableWithoutFeedback
                      onPress={() => this.handleCollapse(i)}>
                      <View style={{marginLeft: 25}}>
                        <Text style={{color: 'black', fontWeight: 'bold'}}>
                          {this.state.isCollapse == i
                            ? 'Persingkat'
                            : 'Selengkapnya'}
                        </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                </View>

                <Text style={styles.tweetText}>{}</Text>
                <View style={styles.tweetFooter}>
                  {r.status <= 1
                    ? this.renderWaitingButton()
                    : this.renderApproveButton()}

                  <View style={styles.footerIcons}>
                    <Button
                      style={{
                        backgroundColor: '#9CCEFB',
                        height: 40,
                        width: 50,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      rounded
                      onPress={() =>
                        this.props.navigation.navigate('chat', {
                          artcidlempar: r.articleId,
                        })
                      }>
                      <Icon name="ios-text" style={{width: 30}} />
                    </Button>
                  </View>
                </View>
              </View>
            ))}

            {/* =============================== */}
          </View>

          {this.state.loadingMore ? (
            <ActivityIndicator
              size="large"
              color={GLOBAL.mainColor}
              style={{marginBottom: 30, marginTop: 20}}
            />
          ) : (
            <View
              style={{
                alignSelf: 'center',
                width: width * 0.5,
                marginTop: 30,
                marginBottom: 30,
                borderBottomColor: GLOBAL.mainColor,
                borderBottomWidth: 3,
              }}></View>
          )}
          <View style={{height: 100}} />
        </Content>
        {
          // modal canvasing
        }
        <Modal
          ref={'newTweetModal'}
          backdrop={true}
          style={styles.modal}
          isOpen={this.state.isModalOpen}
          onClosed={this.closeModal.bind(this)}>
          <ScrollView>
            <View
              style={{
                alignSelf: 'flex-start',
                alignItems: 'center',
                flexDirection: 'row',
                padding: 5,
                paddingRight: 10,
              }}>
              <Button transparent onPress={this.closeModal.bind(this)}>
                <Icon name="close" style={{color: 'black', fontSize: 32}} />
              </Button>
              <View style={{flex: 1}} />
              <Thumbnail
                small
                source={{
                  uri: 'https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg',
                }}
              />
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                width: '100%',
              }}>
              <View style={styles.inputContainer}>
                <Picker
                  selectedValue={this.state.typeevent}
                  style={{width: width * 0.95, marginLeft: 10, color: 'grey'}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({typeevent: itemValue})
                  }>
                  <Picker.Item label="Pilih..." value="0" />
                  <Picker.Item label="Target" value="3" />
                  <Picker.Item label="Kendala" value="4" />
                </Picker>
              </View>
              <View style={styles.inputContainer}>
                {/* <View><Text>Kabupaten : </Text></View> */}
                <Picker
                  selectedValue={this.state.kabupaten}
                  style={{width: width * 0.95, marginLeft: 10, color: 'grey'}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.onValueChange(itemValue)
                  }>
                  <Picker.Item label="Pilih Kabupaten" value="0" />
                  {this.state.kabupatenlist.map((res, i) => (
                    <Picker.Item
                      key={i}
                      label={res.msrgName}
                      value={res.msrgId}
                    />
                  ))}
                </Picker>
              </View>
              <View style={styles.inputContainer}>
                {/* <View><Text>Kecamatan : </Text></View> */}
                <Picker
                  selectedValue={this.state.kecamatan}
                  style={{width: width * 0.95, marginLeft: 10, color: 'grey'}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({kecamatan: itemValue})
                  }>
                  <Picker.Item label="Pilih Kecamatan" value="0" />
                  {this.state.kecamatanlist.map((res, i) => (
                    <Picker.Item
                      key={i}
                      label={res.msdiName}
                      value={res.msdiId}
                    />
                  ))}
                </Picker>
              </View>
              <Input
                style={{
                  width: width * 0.95,
                  marginLeft: 10,
                  color: 'grey',
                  borderBottomWidth: 0.5,
                  borderBottomColor: 'grey',
                  // borderBottomColor: 'grey',
                  // borderBottomWidth: 0.5
                }}
                multiline
                placeholder="Laporan... "
                onChangeText={(eventname) => this.setState({eventname})}
              />
              <View
                style={{
                  borderTopColor: 'grey',
                  borderTopWidth: 0.5,
                }}
              />
            </View>
          </ScrollView>
        </Modal>

        {
          //===========================================================================
        }
        <Modal
          ref={'newTweetModal2'}
          backdrop={true}
          style={styles.modal}
          isOpen={this.state.isModalOpen2}
          onClosed={this.closeModal.bind(this)}>
          <ScrollView>
            <View
              style={{
                alignSelf: 'flex-start',
                alignItems: 'center',
                flexDirection: 'row',
                padding: 5,
                paddingRight: 10,
              }}>
              <Button transparent onPress={this.closeModal.bind(this)}>
                <Icon name="close" style={{color: 'black', fontSize: 32}} />
              </Button>
              <View style={{flex: 1}} />
              <Thumbnail
                small
                source={{
                  uri: 'https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg',
                }}
              />
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                width: '100%',
              }}>
              <View style={styles.inputContainer}>
                <Picker
                  selectedValue={this.state.typeevent}
                  style={{width: width * 0.95, marginLeft: 10, color: 'grey'}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({typeevent: itemValue})
                  }>
                  <Picker.Item label="Pilih..." value="0" />
                  <Picker.Item label="Positive" value="1" />
                  <Picker.Item label="Negative" value="2" />
                </Picker>
              </View>
              <View style={styles.inputContainer}>
                {/* <View><Text>Kabupaten : </Text></View> */}
                <Picker
                  selectedValue={this.state.kabupaten}
                  style={{width: width * 0.95, marginLeft: 10, color: 'grey'}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.onValueChange(itemValue)
                  }>
                  <Picker.Item label="Pilih Kabupaten" value="0" />
                  {this.state.kabupatenlist.map((res, i) => (
                    <Picker.Item
                      key={i}
                      label={res.msrgName}
                      value={res.msrgId}
                    />
                  ))}
                </Picker>
              </View>
              <View style={styles.inputContainer}>
                {/* <View><Text>Kecamatan : </Text></View> */}
                <Picker
                  selectedValue={this.state.kecamatan}
                  style={{width: width * 0.95, marginLeft: 10, color: 'grey'}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({kecamatan: itemValue})
                  }>
                  <Picker.Item label="Pilih Kecamatan" value="0" />
                  {this.state.kecamatanlist.map((res, i) => (
                    <Picker.Item
                      key={i}
                      label={res.msdiName}
                      value={res.msdiId}
                    />
                  ))}
                </Picker>
              </View>
              <Input
                style={{
                  width: width * 0.95,
                  marginLeft: 10,
                  color: 'grey',
                  borderBottomWidth: 0.5,
                  borderBottomColor: 'grey',
                }}
                multiline
                placeholder="Laporan... "
                onChangeText={(eventname) => this.setState({eventname})}
              />

              <Input
                style={{
                  width: width * 0.95,
                  marginLeft: 10,
                  color: 'grey',
                  borderBottomWidth: 0.5,
                  borderBottomColor: 'grey',
                }}
                multiline
                placeholder="Usulan... "
                onChangeText={(usulan) => this.setState({usulan})}
              />
            </View>
          </ScrollView>
        </Modal>

        <Modal
          ref={'newTweetModal3'}
          backdrop={true}
          style={styles.modal}
          isOpen={this.state.isModalOpen3}
          onClosed={this.closeModal.bind(this)}>
          <ScrollView>
            <View
              style={{
                alignSelf: 'flex-start',
                alignItems: 'center',
                flexDirection: 'row',
                padding: 5,
                paddingRight: 10,
              }}>
              <Button transparent onPress={this.closeModal.bind(this)}>
                <Icon name="close" style={{color: 'black', fontSize: 32}} />
              </Button>
              <View style={{flex: 1}} />
              <Thumbnail
                small
                source={{
                  uri: 'https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg',
                }}
              />
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                width: '100%',
              }}>
              <View style={styles.inputContainer}>
                <Picker
                  selectedValue={this.state.kabupaten}
                  style={{width: width * 0.95, marginLeft: 10, color: 'grey'}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.onValueChange2(itemValue)
                  }>
                  <Picker.Item label="Semua Kabupaten" value="0" />
                  {this.state.kabupatenlist2.map((res, i) => (
                    <Picker.Item
                      key={i}
                      label={res.msrgName}
                      value={res.msrgId}
                    />
                  ))}
                </Picker>
              </View>

              <View style={styles.inputContainer}>
                <Picker
                  selectedValue={this.state.kecamatan}
                  style={{width: width * 0.95, marginLeft: 10, color: 'grey'}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({kecamatan: itemValue})
                  }>
                  <Picker.Item label="Semua Kecamatan" value="0" />
                  {this.state.kecamatanlist2.map((res, i) => (
                    <Picker.Item
                      key={i}
                      label={res.msdiName}
                      value={res.msdiId}
                    />
                  ))}
                </Picker>
              </View>

              {parseInt(this.props.eposko) > 0 && (
                <View style={styles.inputContainer}>
                  <Picker
                    selectedValue={this.state.organisasi}
                    style={{width: width * 0.95, marginLeft: 10, color: 'grey'}}
                    onValueChange={(itemValue, itemIndex) =>
                      this.setState({organisasi: itemValue})
                    }>
                    <Picker.Item label="Semua Organisasi" value="0" />
                    {this.state.organisasilist.map((res, i) => (
                      <Picker.Item
                        key={i}
                        label={res.namaOrganisasi}
                        value={res.organisasiId}
                      />
                    ))}
                  </Picker>
                </View>
              )}

              <Input
                style={{
                  width: width * 0.95,
                  marginLeft: 10,
                  color: 'grey',
                  borderBottomWidth: 0.5,
                  borderBottomColor: 'grey',
                }}
                multiline
                placeholder="Arahan... "
                onChangeText={(eventname) => this.setState({eventname})}
              />
            </View>
          </ScrollView>
        </Modal>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  topMargin: {
    backgroundColor: 'white',
    zIndex: -1,
    borderBottomColor: 'grey',
    borderBottomWidth: 0.4,
  },
  content: {
    padding: 10,
    backgroundColor: 'white',
  },
  heading: {
    fontSize: 32,
    fontWeight: '400',
    marginBottom: 30,
  },
  tweet: {
    paddingTop: 20,
    paddingBottom: 5,
    paddingLeft: 5,
    paddingRight: 5,
    borderBottomColor: '#bbb',
    borderBottomWidth: StyleSheet.hairlineWidth,
    flexDirection: 'column',
  },
  tweetText: {
    marginTop: 10,
    fontSize: 18,
    color: '#555',
  },
  tweetFooter: {
    flexDirection: 'row',
    // justifyContent: "flex-start",
    justifyContent: 'space-around',
    padding: 0,
  },
  badgeCount: {
    fontSize: 12,
    paddingLeft: 5,
  },
  footerIcons: {
    flexDirection: 'row',
    alignItems: 'center', //center
  },
  modalFooter: {
    backgroundColor: 'white',
    borderTopWidth: 0.4,
    borderTopColor: 'grey',

    height: 54,
    width,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 5,
  },
  modal: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: 'absolute',
    zIndex: 4,
    elevation: 4,
    paddingBottom: 145,
  },
  inputContainer: {
    borderBottomColor: 'grey',
    borderBottomWidth: 0.5,
  },
  floatingButton: {
    position: 'absolute',
    bottom: 0,
    zIndex: 99,
  },
});

const mapStateToProps = (state) => {
  const {relawanid, token, email, fasilitator, eposko, jabatan, userid} =
    state.anggota;
  const {listArticleRedux} = state.data;
  return {
    relawanid,
    token,
    email,
    fasilitator,
    eposko,
    jabatan,
    userid,
    listArticleRedux,
  };
};

export default connect(mapStateToProps, {update_list_article})(
  withNavigationFocus(Homescreennolike),
);
