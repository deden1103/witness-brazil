
import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    Alert,
    TouchableWithoutFeedback,
    ActivityIndicator, AppState
} from 'react-native';
import {
    Container,
    Header,
    Content,
    Left,
    Icon,
    Button,
    Right
} from "native-base";
import Gallery from 'react-native-image-gallery';
class Imageviewer extends Component {
    static navigationOptions = {
        header: null
    };

    constructor() {
        super()
        this.state = {

        }
    }


    render() {
        return (

            <Container style={{
                backgroundColor: "transparent"
            }}>

                <Header style={{
                    backgroundColor: "black"
                }}>
                    <Left>
                        <Button
                            onPress={() => this.props.navigation.goBack()}
                            transparent
                        >
                            <Icon name="arrow-back" style={{ color: 'white' }} />
                        </Button>
                    </Left>
                    <Right />
                </Header>
                <Content style={{ backgroundColor: "transparent" }}>

                    <Gallery
                        // onSingleTapConfirmed={() => this.props.navigation.goBack()}
                        scrollViewStyle={{ marginTop: 50 }}
                        style={{ flex: 1, backgroundColor: 'black', marginTop: -90 }}
                        images={[
                            { source: { uri: this.props.navigation.getParam('imglempar') } }
                        ]}
                    />
                </Content>
            </Container>

        );
    }
}

export default Imageviewer

const styles = {

}

























