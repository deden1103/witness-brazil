
import React, { Component } from 'react';
import {
    Button,
    Container,
    Content,
    Icon
} from 'native-base';

import {
    Text,
    View,
    StatusBar,
    Alert, Dimensions, TouchableOpacity
} from 'react-native';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import { connect } from 'react-redux';
import { InputWhiteBgStyle } from '../particles/SmallParticle';
import FloatingInput from '../components/FloatingInput';
import FloatingButtonThree from '../components/FloatingButtonThree';
import Image from 'react-native-remote-svg';
import { HeaderRightBack } from '../particles/HeaderRightBack';
const { width, height } = Dimensions.get('window')
class Event extends Component {

    constructor() {
        super()
        this.state = {
            isShowReplay: false,
            eventIdActive: null,
            dataevent: [],
            comments: [],
            comm: '',
            rep: '',
            daftarevent: [],
            eventname: '',
            imgforupload: ''
        }
    }


    componentDidMount() {
        this.fetchingEvent()
    }

    // componentDidUpdate() {
    //     this.fetchingDetail()
    // }


    fetchingEvent = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}event/getallevent`)
            if (response.data.success == true) {
                this.setState({
                    daftarevent: response.data.data
                })
                console.log(response)
            } else {
                console.log(response)
            }
        }
        catch (e) {
            console.log(e)
        }
    }


    handleSendEvent = async () => {
        try {
            let data = JSON.stringify({
                eventName: this.state.eventname,
                relawanId: this.props.relawanid

            })
            if (this.state.eventname !== '') {
                let loginResponse = await axios.post(`${GLOBAL.apiUrl}event/sendevent`, data, {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
                if (loginResponse.data.success == true) {
                    this.fetchingEvent()
                    console.log(loginResponse)
                    this.setState({ eventname: '' })
                    this.uplod(this.state.imgforupload, loginResponse.data.idhasinput)
                    // this.component._root.scrollToEnd()
                } else {
                    Alert.alert('Submeter um comentário Falha / Você não está logado')
                    console.log(loginResponse)
                }
            } else {
                Alert.alert('A mensagem não pode estar vazia')
            }
        } catch (e) {
            console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }
    }

    handleUploadImage = () => {
        const options = {
            title: 'Foto de no máximo 4MB ',
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {

            }
            else if (response.error) {

            } else if (response.fileSize > 1000000) {
                Alert.alert('arquivo de foto é muito grande')
            }
            else {
                console.log(response)
                // this.uplod(response)
                this.setState({ imgforupload: response })
            }
        });
    }


    uplod = (datanya, ei) => {
        console.log(datanya)
        RNFetchBlob.fetch('POST', `${GLOBAL.apiUrl}event/imgnext`, {
            Accept: 'application/json',
            'Authorization': `${ei}`,
            'Content-Type': 'multipart/form-data',
        }, [
                {
                    name: 'pict',
                    filename: datanya.fileName,
                    type: datanya.type,
                    data: datanya.data
                }
            ]
        ).then((resp) => {
            let rspUpload = JSON.parse(resp.data);
            if (rspUpload.success == true) {
                this.fetchingEvent()
                this.setState({ eventname: '' })
            } else {
                Alert.alert('Falha ao atualizar a foto')
            }
        }).catch((err) => {
            console.log(JSON.parse(err))
            // Alert.alert('Kesalahan Server')
        })
    }


    handlePostComment = async (evi) => {
        try {
            let data = JSON.stringify({
                eventId: evi,
                commentDesc: this.state.comm,
                relawanId: this.props.relawanid

            })
            if (this.state.comm !== '') {
                let loginResponse = await axios.post(`${GLOBAL.apiUrl}event/sendcomment`, data, {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
                if (loginResponse.data.success == true) {
                    this.fetchingEvent()
                    console.log(loginResponse)
                    this.setState({ comm: '' })

                    // this.component._root.scrollToEnd()
                } else {
                    Alert.alert('Submeter um comentário Falha / Você não está logado')
                    console.log(loginResponse)
                }
            } else {
                Alert.alert('A mensagem não pode estar vazia')
            }
        } catch (e) {
            console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }
    }


    handlePostReplay = async () => {
        try {
            let data = JSON.stringify({
                commentId: this.state.isShowReplay,
                replayDesc: this.state.rep,
                relawanId: this.props.relawanid

            })
            if (this.state.rep !== '') {
                let loginResponse = await axios.post(`${GLOBAL.apiUrl}event/sendreplay`, data, {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
                if (loginResponse.data.success == true) {
                    this.fetchingEvent()
                    console.log(loginResponse)
                    this.setState({ rep: '', isShowReplay: 0 })
                } else {
                    Alert.alert('Submeter um comentário Falha / Você não está logado')
                    console.log(loginResponse)
                }
            } else {
                Alert.alert('A mensagem não pode estar vazia')
            }
        } catch (e) {
            console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }
    }


    renderReplay = (par = []) => {
        return par.map((res, i) => {
            return (
                <View key={i} style={{ alignSelf: 'flex-end', marginTop: 5, minHeight: 50, backgroundColor: '#E9EBEE', width: width * 0.7, borderRadius: 15, justifyContent: 'center', padding: 5 }}>
                    <Text style={{ color: 'black' }}> {res.relawan.namaRelawan} :  {res.replayDesc} </Text>
                    <Text style={{ alignSelf: 'flex-end', color: 'black' }}>{res.createDate}</Text>
                </View>
            )
        })
    }




    renderViewComment = (comments) => {
        return comments.map((val, i) => {
            return (
                <View key={i} style={{ paddingLeft:15,paddingRight:15, marginTop: 20, minHeight: 80, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                    <View style={{ width: 55, height: 55, borderRadius: 60, alignSelf: 'flex-start' }}>
                        <View>
                            <Image source={{ uri: val.relawan.photoRelawan }} style={styles.profile} />
                        </View>
                    </View>
                    <View>
                        <View style={{ minHeight: 60, backgroundColor: '#E9EBEE', width: width * 0.7, borderRadius: 15, justifyContent: 'center', padding: 5 }}>
                            <Text style={{ color: 'black' }}> {val.relawan.namaRelawan} :  {val.commentDesc}</Text>

                            <Text style={{ alignSelf: 'flex-end', color: 'black' }}>{val.createDate}</Text>
                        </View>

                        {this.renderReplay(val.replays)}
                        {this.state.isShowReplay == val.commentId && <View style={{ marginTop: 5, marginBottom: 5, minHeight: 50, width: width * 0.8, borderRadius: 10, justifyContent: 'center', padding: 5 }}>
                            <InputWhiteBgStyle
                                valueInput={this.state.rep}
                                onChangeInput={(rep) => this.setState({ rep })}
                                pHolder="balas komentar..."
                                style={{ backgroundColor: 'transparent', borderColor: 'grey', borderWidth: 1, borderRadius: 30, height: 45 }} />
                        </View>}

                        <TouchableOpacity onPress={() => this.setState({ isShowReplay: val.commentId, eventIdActive: val.eventId })}>
                            <View style={{ marginLeft: 20, paddingBottom: 10, marginTop: 5 }}>
                                <Text style={{ alignSelf: 'flex-end', marginRight: 10, fontSize: 18, color: 'black' }}>Balas</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        })
    }



    render() {
        return (

            <Container style={styles.container}>
                {/* <StatusBar
                    backgroundColor={GLOBAL.mainColor}
                    animated
                /> */}
                <HeaderRightBack
                    goBack={() => this.props.navigation.goBack()}
                    txtTitle="Home"
                    kembaliText="Kembali"
                    openDrawer={() => this.props.navigation.openDrawer()}
                />



                <Content style={{ flex: 1 }}
                    ref={c => (this.component = c)}
                >

                    <View style={{ backgroundColor: 'white', borderColor: 'grey', borderWidth: 0.5, margin: 10, borderRadius: 6 }}>
                        <View style={{ marginTop: 5, height: 150, paddingLeft: 15, justifyContent: 'space-around', width: width * 0.9 }}>
                            <View style={{ marginBottom: 10 }}><Text style={{ fontSize: 18, color: 'black' }}>{`Buat Postingan`}</Text></View>
                            <InputWhiteBgStyle
                                valueInput={this.state.eventname}
                                onChangeInput={(eventname) => this.setState({ eventname })}
                                pHolder="Tulis..."
                                style={{
                                    backgroundColor: 'transparent',
                                    borderColor: 'grey',
                                    borderWidth: 1,
                                    borderRadius: 30,
                                    height: 45,
                                }} />
                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <Text style={{ fontSize: 18, color: 'black', marginRight: 10 }}>{`Tambahkan Photo`}</Text>
                                <TouchableOpacity onPress={this.handleUploadImage}>
                                    <Icon name="ios-camera-outline" style={{ color: 'black' }} />
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity onPress={this.handleSendEvent}>
                                <View style={{}}>
                                    <Text style={{ color: 'black', fontSize: 20, alignSelf: 'flex-end' }}>Enviar</Text>
                                </View>
                            </TouchableOpacity>

                        </View>
                    </View>

                    {this.state.daftarevent.map((res, i) => {
                        return (
                            <View style={{ backgroundColor: 'white', borderColor: 'grey', borderWidth: 0.5, margin: 10, borderRadius: 6 }}>
                                <View style={{ marginTop: 5, minHeight: 30, paddingLeft: 15 }}>
                                    <View><Text style={{ fontSize: 18, color: 'black' }}>{res.relawan.namaRelawan}</Text></View>
                                    <View><Text style={{ color: 'black' }}>{res.eventName}</Text></View>
                                </View>

                                {res.isPhotoExist == null || res.isPhotoExist == '' ? null : <View style={{ height: 200, borderTopColor: 'grey', borderBottomColor: 'grey', borderBottomWidth: 1, borderTopWidth: 1 }}>
                                    <Image source={{ uri: res.eventPhoto }} resizeMode="stretch" style={{ flex: 1 }} />
                                </View>}


                                <View style={{ borderBottomColor: 'grey', borderBottomWidth: 0.5, marginTop: 10, height: 40, paddingLeft: 15, justifyContent: 'center' }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 18, color: 'black', marginRight: 15 }}>Komentar</Text>
                                        {/* <Icon type="FontAwesome" name="comment" style={{ color: 'black', fontSize: 20, marginRight: 6 }} /> */}
                                    </View>
                                </View>
                                <View style={{ marginBottom: 60 }}>

                                    {this.renderViewComment(res.comments)}


                                </View>
                                {
                                    this.state.isShowReplay && this.state.eventIdActive == res.eventId ? <FloatingButtonThree
                                        cancel={() => this.setState({ isShowReplay: 0 })}
                                        kirim={this.handlePostReplay} /> : <FloatingInput
                                            // value={this.state.comm}

                                            onChange={(comm) => this.setState({ comm })}
                                            holder="Tambah Komentar"
                                            toSend={() => this.handlePostComment(res.eventId)} />
                                }
                            </View>
                        )
                    })}


                </Content>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    const { relawanid } = state.anggota;
    return { relawanid };
};

export default connect(mapStateToProps, {})(Event);

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#E9EBEE',
        // marginBottom: 200
    },
    profile: {
        width: 50,
        height: 50,
        borderRadius: 60
    }
}


/**
 * 
 * 
 * 
 * 
 * handlegetComment = async (iii) => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}event/getoneevent?eventid=${iii}`)
            if (response.data.success == true) {
                return response.data.data
            } else {
                console.log(response)
            }
        }
        catch (e) {
            console.log(e)
        }
    }
 *    handleTest = (ii) => {
        return (
            <View >
                <View><Text >{ii}</Text></View>
            </View>
        )
    }

 
 {this.state.comments.map((val, i) => {
                                        return (
                                            <View key={i} style={{ marginTop: 20, minHeight: 80, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                                                <View style={{ width: 55, height: 55, borderRadius: 60, alignSelf: 'flex-start' }}>
                                                    <View>
                                                        <Image source={{ uri: val.relawan.photoRelawan }} style={styles.profile} />
                                                    </View>
                                                </View>
                                                <View>
                                                    <View style={{ minHeight: 60, backgroundColor: '#2b8fef', width: width * 0.8, borderRadius: 15, justifyContent: 'center', padding: 5 }}>
                                                        <Text style={{ color: 'white' }}> {val.relawan.namaRelawan} :  {val.commentDesc}</Text>

                                                        <Text style={{ alignSelf: 'flex-end', color: 'white' }}>{val.createDate}</Text>
                                                    </View>

                                                    {this.renderReplay(val.replays)}
                                                    {this.state.isShowReplay == val.commentId && <View style={{ marginTop: 5, marginBottom: 5, minHeight: 50, width: width * 0.8, borderRadius: 10, justifyContent: 'center', padding: 5 }}>
                                                        <InputWhiteBgStyle
                                                            valueInput={this.state.rep}
                                                            onChangeInput={(rep) => this.setState({ rep })}
                                                            pHolder="balas komentar..."
                                                            style={{ backgroundColor: 'transparent', borderColor: 'grey', borderWidth: 1, borderRadius: 30, height: 45 }} />
                                                    </View>}

                                                    <TouchableOpacity onPress={() => this.setState({ isShowReplay: val.commentId })}>
                                                        <View style={{ marginLeft: 20, paddingBottom: 10, marginTop: 5 }}>
                                                            <Text style={{ alignSelf: 'flex-end', marginRight: 10, fontSize: 18, color: 'black' }}>Balas</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        )
                                    })}


                                     fetchingDetail = async (ii) => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}event/getoneevent?eventid=${ii}`)
            if (response.data.success == true) {
                this.setState({
                    dataevent: this.state.dataevent.push(response.data.data),
                    comments: response.data.data.comments
                })
                console.log(response)
            } else {
                console.log(response)
            }
        }
        catch (e) {
            console.log(e)
        }
    }

 */



//backgroundColor: '#F2F3F5', borderColor: 'grey', borderWidth: 1,
















