import React, { Component } from 'react';
import {
    StyleSheet,
    Dimensions,
    StatusBar,
    Alert,
    View
} from 'react-native';
import {
    Container,
    Content,
    Form,
    Item,
    Label,
    Input,
    Button,
    Text
} from 'native-base';
import axios from 'axios';
import { connect } from 'react-redux';
import { HeaderRightBack } from '../particles/HeaderRightBack';
const { width, height } = Dimensions.get('window');


class Changepassword extends Component {
    constructor(props) {
        super(props)
        this.state = {
            active: false,
            oldPassword: '',
            newPassword: '',
            confirmPassword: '',
        }
        this.token = ''

    }

    async componentDidMount() {

    }

    handleSend = async () => {
        try {
            this.setState({ loading: true })
            if (
                this.state.oldPassword !== '' &&
                this.state.newPassword !== '' &&
                this.state.confirmPassword !== ''
            ) {
                let data = JSON.stringify({
                    old_password: this.state.oldPassword,
                    new_password: this.state.newPassword,
                    confirm_password: this.state.confirmPassword
                })
                let loginResponse = await axios.post(`${GLOBAL.apiUrl}api/apianggota/changepassword`, data, {
                    headers: {
                        'Authorization': `Bearer ${this.props.token}`,
                        'Content-Type': 'application/json',
                    }
                })
                if (loginResponse.data.success == true) {
                    this.setState({ loading: false, oldPassword: '', newPassword: '', confirmPassword: '' })
                    Alert.alert('Senha mudada com sucesso')
                } else {
                    Alert.alert('Falha ao alterar a senha')
                    this.setState({ loading: false })
                }

            } else {
                //this.setState({ showAlert: !this.state.showAlert, loading: false })
                Alert.alert('os dados não podem estar vazios')
            }
        } catch (e) {
            // console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }


    }


    render() {
        return (
            <Container>
                <HeaderRightBack
                    goBack={() => this.props.navigation.goBack()}
                    txtTitle="Lupa Password"
                    openDrawer={() => this.props.navigation.openDrawer()}
                    kecart={() => this.props.navigation.navigate('cart')}
                />
                <Content>
                    {/* <StatusBar
                        backgroundColor={GLOBAL.mainColor}
                        animated
                    /> */}

                    <View style={{ width: width * 0.9 }}>
                        <Form>
                            <Item inlineLabel>
                                <Label>Ganti Password</Label>
                                <Input
                                    value={this.state.oldPassword}
                                    onChangeText={(text) => this.setState({ oldPassword: text })}
                                    secureTextEntry />
                            </Item>
                            <Item inlineLabel >
                                <Label>Edit Password</Label>
                                <Input
                                    value={this.state.newPassword}
                                    onChangeText={(text) => this.setState({ newPassword: text })}
                                    secureTextEntry />
                            </Item>
                            <Item inlineLabel >
                                <Label>Confirmar senha</Label>
                                <Input
                                    value={this.state.confirmPassword}
                                    onChangeText={(text) => this.setState({ confirmPassword: text })}
                                    secureTextEntry />
                            </Item>

                        </Form>

                    </View>

                    <Button block style={{ margin: 15, marginTop: 50, backgroundColor: GLOBAL.mainColor }} onPress={this.handleSend}>
                        <Text style={{ color: 'white' }}>Save</Text>
                    </Button>

                </Content>




            </Container>
        );
    }
}

const styles = StyleSheet.create({


});


const mapStateToProps = (state) => {
    const { token, anggotaid } = state.anggota;
    return { token, anggotaid };
};

export default connect(mapStateToProps, {})(Changepassword);


