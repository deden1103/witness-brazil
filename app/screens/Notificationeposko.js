// ME
import React, { Component } from "react";
import Modal from "react-native-modalbox";
import Dimensions from "Dimensions";
import {
    StyleSheet,
    View,
    Text,
    FlatList,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Platform,
    Alert,
    StatusBar, Image, Picker, RefreshControl
} from "react-native";
import {
    Container,
    Header,
    Body,
    Content,
    Left,
    Title,
    Thumbnail,
    Col,
    Row,
    Grid,
    Icon,
    Spinner,
    Fab,
    Button,
    Footer,
    Input,
    Right
} from "native-base";
import { connect } from "react-redux";
// import Image from 'react-native-remote-svg';
import axios from 'axios';
// import ScrollableTabView, {
//     ScrollableTabBar
// } from "react-native-scrollable-tab-view";
import { countcomment, clearcommentcount, savelastidcomment } from '../rdx/actions'
import { withNavigationFocus,NavigationEvents } from 'react-navigation';
const { width, height } = Dimensions.get('window')



class Notificationsatu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            listnotif: [],
            inisialnotif:[
               
            ],
            listLike: []
        };
    }


    componentDidMount() {
        // this.handleCountComment()
        this.fetchingNotif()
        // this.fetchingNotifInitial()
        // this.fetchingLike()
        // const update = setInterval(() => {
        //     this.fetchingNotif()
        //     // this.fetchingLike()
        // }, 60000)
    }

    // componentDidUpdate() {
    //     // this.fetchingNotif()
    //     //this.fetchingLike()
       
    // }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.isFocused) {
            this.props.clearcommentcount()
           // this.handleSaveLastId()
            return true
        }
        return true
    }

    handleCalculate = () => {
        // Alert.alert(JSON.stringify(this.state.listnotif))
        // let satu = this.state.inisialnotif.length
        // let dua = this.state.listnotif.length
        // if (satu > 0 && dua > 0) {
        //     if (dua > satu) {
        //         let beda = dua - satu
        //         this.props.countcomment(beda)
        //     }

        // }
    }

    // handleCountComment = async () => {
    //     try {
    //         await this.props.countcomment(this.props.token, this.props.lastidcomment)
    //     } catch (e) { }
    // }

    handleSaveLastId = () => {
        this.props.savelastidcomment(0)
    }

    fetchingNotif = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}article/mynotif`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })
            if (response.data.success == true) {
                this.setState({
                    listnotif: response.data.detail,
                }, this.handleCalculate)
                // console.log(response)
            } else {
                // console.log(response)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }

    _onRefresh = async () => {
        try {
            this.setState({ refreshing: true });
            await this.fetchingNotif()
            await this.setState({ refreshing: false });
        } catch (e) { }

    }


    fetchingNotifInitial = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}relawan/notif`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })
            if (response.data.success == true) {
                this.setState({
                    inisialnotif: response.data.data,
                })
                // console.log(response)
            } else {
                // console.log(response)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }


    fetchingLike = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}relawan/getlike`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })
            if (response.data.success == true) {
                this.setState({
                    listlike: response.data.data,
                })
                // console.log(response)
            } else {
                // console.log(response)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }






    render() {
        if (this.props.tweetPosted === "success") {
            this.closeModal();
        }
        return (
            <Container>
                 <NavigationEvents onDidFocus={() => this.fetchingNotif()} />
                <Content
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                    style={{ backgroundColor: "white" }}>

                    {this.state.listnotif.map((res, i) => (


                        <TouchableWithoutFeedback key={i} onPress={() => {}}>
                            <View style={{ flex: 1, paddingLeft: 20, paddingRight: 20, paddingTop: 5, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Icon name="arrow-dropright" style={{ marginRight: 10, alignSelf: 'flex-start' }} />
                                <Text><Text style={{ fontWeight: 'bold' }}>{res.username}</Text> Foi aprovado/a <Text style={{ fontWeight: 'bold' }}>Relatório </Text>{res.artcTitle} Anda</Text>
                            </View>
                        </TouchableWithoutFeedback>

                        // res.type == 'comment' ? <TouchableWithoutFeedback key={i} onPress={() => this.props.navigation.navigate('eventdetail', { eventIdLempar: res.eventId })}>
                        //     <View style={{ flex: 1, paddingLeft: 20, paddingRight: 20, paddingTop: 5, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                        //         <Icon name="arrow-dropright" style={{ marginRight: 10, alignSelf: 'flex-start' }} />
                        //         <Text><Text style={{ fontWeight: 'bold' }}>{res.username}</Text> Telah Mengomentari <Text style={{ fontWeight: 'bold' }}>Relatório</Text> Anda</Text>
                        //     </View>
                        // </TouchableWithoutFeedback> :
                        //     res.type == 'like' ?
                        //         <TouchableWithoutFeedback key={i} onPress={() => this.props.navigation.navigate('eventdetail', { eventIdLempar: res.eventId })}>
                        //             <View style={{ flex: 1, paddingLeft: 20, paddingRight: 20, paddingTop: 5, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                        //                 <Icon name="arrow-dropright" style={{ marginRight: 10, alignSelf: 'flex-start' }} />
                        //                 <Text><Text style={{ fontWeight: 'bold' }}>{res.username}</Text> Menyukai <Text style={{ fontWeight: 'bold' }}>Relatório</Text> Anda</Text>
                        //             </View>
                        //         </TouchableWithoutFeedback> :
                        //         <View key={i} style={{ flex: 1, paddingLeft: 20, paddingRight: 20, paddingTop: 5, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                        //             <Icon name="arrow-dropright" style={{ marginRight: 10, alignSelf: 'flex-start' }} />
                        //             <Text><Text style={{ fontWeight: 'bold' }}>{res.username}</Text>  <Text style={{ fontWeight: 'bold' }}></Text> </Text>
                        //         </View>


                    ))}

                    {/* {this.state.listlike.map((res, i) => (
                        <TouchableWithoutFeedback  key={i} onPress={() => this.props.navigation.navigate('eventdetail', { eventIdLempar: res.eventId })}>
                            <View style={{ flex: 1, paddingLeft: 20,paddingRight:20,paddingTop:5, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Icon name="arrow-dropright" style={{ marginRight: 10, alignSelf: 'flex-start' }} />
                                <Text><Text style={{ fontWeight: 'bold' }}>{res.relawan.namaRelawan}</Text> Menyukai <Text style={{ fontWeight: 'bold' }}>Relatório</Text> Anda</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    ))} */}

                    {/* <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('eventdetail')}>
                            <View style={{ flex: 1, paddingLeft: 20, paddingRight: 20, paddingTop: 5, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Icon name="arrow-dropright" style={{ marginRight: 10, alignSelf: 'flex-start' }} />
                                <Text><Text style={{ fontWeight: 'bold' }}>{'test'}</Text> Telah Mengomentari <Text style={{ fontWeight: 'bold' }}>Relatório</Text> Anda</Text>
                            </View>
                        </TouchableWithoutFeedback>

                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('eventdetail')}>
                            <View style={{ flex: 1, paddingLeft: 20, paddingRight: 20, paddingTop: 5, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Icon name="arrow-dropright" style={{ marginRight: 10, alignSelf: 'flex-start' }} />
                                <Text><Text style={{ fontWeight: 'bold' }}>{'Test'}</Text> Telah Mengomentari <Text style={{ fontWeight: 'bold' }}>Relatório</Text> Anda</Text>
                            </View>
                        </TouchableWithoutFeedback> */}

                </Content>
            </Container>
        );
    }
}


const styles = StyleSheet.create({

})


const mapStateToProps = (state) => {
    const { relawanid, searchshow, token, lastidcomment } = state.anggota;
    return { relawanid, searchshow, token, lastidcomment };
};

export default connect(mapStateToProps, {
    countcomment,
    clearcommentcount,
    savelastidcomment
})(withNavigationFocus(Notificationsatu));