import React, { Component } from "react";
import Modal from "react-native-modalbox";
import Dimensions from "Dimensions";
// import Notification from 'react-native-android-local-notification';
import {
    StyleSheet,
    View,
    Text,
    FlatList,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Platform,Switch,
    Alert, ScrollView, TouchableOpacity,TextInput,
    StatusBar, Image, Picker, Linking, LayoutAnimation, ActivityIndicator, BackHandler, ToastAndroid, RefreshControl
} from "react-native";
import {
    Container,
    Header,
    Body,
    Content,
    Left,
    Title,
    Thumbnail,
    Col,
    Row,
    Grid,
    Icon,
   Textarea,
    Fab,
    Button,
    Footer,
    Input,
    Right
} from "native-base";
import { connect } from "react-redux";
// import Image from 'react-native-remote-svg';
import OneSignal from 'react-native-onesignal';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import { withNavigationFocus } from 'react-navigation';
// import ScrollableTabView, {
//     ScrollableTabBar
// } from "react-native-scrollable-tab-view";
import AutoHeightImage from 'react-native-auto-height-image';
import FastImage from 'react-native-fast-image'
import Hyperlink from 'react-native-hyperlink';
// import YouTube, { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';
import VideoPlayer from './VideoPlayer';
import { Thumbnail as Thm } from 'react-native-thumbnail-video';
import TopButton from '../components/TopButton';
import Sound from 'react-native-sound';
import { AudioRecorder, AudioUtils } from 'react-native-audio';
import { inputArticleList, save_uri_video } from '../rdx/actions';
// import Video from 'react-native-video';
import Video from 'react-native-af-video-player'
import Spinner from 'react-native-loading-spinner-overlay';
import HTML from 'react-native-render-html';
import Upload from 'react-native-background-upload'

import {update_list_article,model_video_open2} from '../rdx/actions';
// const audioRecorderPlayer = new AudioRecorderPlayer();
import MyModal from '../components/MyModal';
const { width, height } = Dimensions.get('window')

// var RNFS = require('react-native-fs');


class Draftscreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            active: false,
            newTweetContent: "",
            isModalOpen: false,
            isModalOpen2: false,
            isModalOpen3: false,
            daftarevent: [],
            eventname: '',
            imgforupload: '',
            typeevent: '',
            osid: '',
            page: 1,
            loadingMore: false,
            alleventforconcat: [],
            kabupaten: '0',
            kecamatan: '0',
            organisasi: '0',
            usulan: '',
            kabupatenlist: [],
            kabupatenlist2: [],
            kecamatanlist: [],
            kecamatanlist2: [],
            organisasilist: [],
            backClickCount: 0,
            gambarUri: '',
            gambarType: '',
            gambarFilename: '',
            voiceFileName:'',
            letsPlay: false,
            isply: false,

            currentTime: 0.0,
            recording: false,
            paused: false,
            stoppedRecording: false,
            finished: false,
            audioPath: '',//AudioUtils.MusicDirectoryPath + '/reportaudio.aac',
            hasPermission: undefined,

            descript: ' TEMPO.CO, Jakarta - Pengaturan waktu yang tepat menjadi salah satu hal yang perlu direncanakan saat ingin m hgugu hygug ugudi hgvhgfvhu uhgv ughf',
            listArticle: [],
            listArticleForPush: {},
            idForEdit : '',
            audioIsPlaying:true,
            audioIsPlayingIndex:'',
            voiceExist:false,
            judul:'',
            desk:'',
            mediaNya:'',
            videoUriForSend :'',
            isLoadingForSpinner : false,
            myArticle:[],
            typeActive : '',
            idDetailArticle:'',
            imagePreviewEdit:'',
        };
        this._scrollOffset = 0

    }

    _onRefresh = async () => {
        try {
            this.setState({ refreshing: true });
            await this.fetchingEvent()
            await this.setState({ refreshing: false, page: 1 });
        } catch (e) { }

    }


   

  

   async componentDidMount() {
       this.props.save_uri_video('')
      this.setState({idDetailArticle:this.props.navigation.getParam('eventIdLempar')})
        // if(this.props.token){
        //     this.fetchingListArticle()
        // }
       
        //   BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        // this.fetchingEvent()
        // this.fetchingDetail()
        // this.fetchDetailKabupaten()
        // this.fetchDetailOrganisasi()
        // this.setState({ letsPlay: '' })
    await this.fetchDetail()
    await this.setState({typeActive : this.state.myArticle[0].type})
    await this.openModalEdit(this.state.typeActive)

       
    }

    fetchingListArticle = async () => {
        try {
            this.setState({ loading: false })
            let response = await fetch(
                GLOBAL.apiUrl + 'article/list',{
                    headers: {
                        'Authorization' : 'Bearer '+this.props.token,
                        'Accept': 'application/json',
                    }
                }
            );
            let responseJson = await response.json();
            await this.setState({ listArticle: responseJson.detail })
            this.props.update_list_article(this.state.listArticle)
        } catch (error) {
            this.setState({ loading: false })
            console.error(error);
        }
    }


    fetchDetail = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}article/detail?id=${this.props.navigation.getParam('eventIdLempar')}`,{
                headers:{
                    'Authorization' : 'Bearer '+this.props.token,
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data'
                }
            })
            if (response.data.success == true) {//this.props.navigation.getParam('eventIdLempar')
                this.setState({
                    myArticle: response.data.detail,
                    judul : response.data.detail[0].title,
                    desk : response.data.detail[0].longDesc,
                    imagePreviewEdit:response.data.detail[0].media,
                    // comments: response.data.data.comments,
                    // relawan: response.data.data.relawan
                })
            } else {
                // console.log(response)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }

    // handleSubmit = async (id,modalName) =>{
    //     try{
    //         let newarrartc = await this.props.myArticle.filter(item => item.articleId == id);
    //         this.setState({
    //         idForEdit:id,
    //         judul : newarrartc[0].title,
    //         desk : newarrartc[0].longDesc,
    //     })

    //    if(modalName == 'image'){
       
    //         // await this.setState({gambarUri:newarrartc[0].media})   
    //         const data = new FormData();
    //         data.append('title',this.state.judul);
    //         data.append('desc', this.state.desk);
    //         if (newarrartc[0].isExist) {
    //             data.append('file', {
    //                 uri: newarrartc[0].media,
    //                 type: 'image/'+ newarrartc[0].ext,
    //                 name: 'laporan'
    //             });
    //         }
    //         let response = await fetch(`${GLOBAL.apiUrl}article/add`, {
    //             headers: {
    //                 'Authorization' : 'Bearer '+this.props.token,
    //                 'Accept': 'application/json',
    //                 'Content-Type': 'multipart/form-data'
    //             },
    //             method: 'post',
    //             body: JSON.stringify(data)
    //         })
    //         const tmp = await response.json()
    //         console.log(tmp)
           
    //     }else if(modalName == 'video'){
       
    //     }else{
        
    //     }
           
    //      }catch(e){
    //            console.log(e)
    //        } 
          

      
    // }

    handleSubmit = async (id,modalName) =>{
        // let newarrartc = await this.props.myArticle.filter(item => item.articleId == id);
        // this.removeArticle(id)

        if(this.state.typeActive == 'image'){
            this.handleSendArticleImage()
            console.log('img')
        // this.setState({
        //     //  idForEdit:id,
        //      judul : newarrartc[0].title,
        //      desk : newarrartc[0].longDesc,
        //      gambarUri : newarrartc[0].media,
        //      gambarType :'image/'+ newarrartc[0].ext,
        //      gambarFilename : 'report.'+newarrartc[0].ext,
        //      imgforupload : newarrartc[0].isExist,
        // },this.handleSendArticleImage)
        }else if(this.state.typeActive == 'sound'){
            console.log('sound')
            this.handleSendArticleAudio()
        //     this.setState({
        //         // idForEdit:id,
        //         judul : newarrartc[0].title,
        //         desk : newarrartc[0].longDesc,
        //         soundPath : newarrartc[0].media,
        //         voiceExist : newarrartc[0].isExist
        //    },this.handleSendArticleAudio)
        }else{
            console.log('video')
            this.handleSendArticleVideo()
            // this.setState({
            //     //  idForEdit:id,
            //      judul : newarrartc[0].title,
            //      desk : newarrartc[0].longDesc,
            //      videoUriForSend:newarrartc[0].media,
            //      videoUriForSendExist : newarrartc[0].isExist
            // },this.handleSendArticleVideo)
        }
       

       
    }


    handleSendArticleAudio = () => {
      
        // this.setState({
        //     isLoadingForSpinner : true
        // })
        // this.closeModal()
        const data = new FormData();
        
            data.append('article_id',this.state.idDetailArticle);
            data.append('title',this.state.judul);
            data.append('desc', this.state.desk);
            // if(this.state.voiceExist){
            //     Notification.create({ subject: 'Tempo Witness', message: 'Uploading...' });
            //     data.append('file', {
            //         uri:this.state.soundPath,
            //         name: 'reportaudio.aac',
            //         type: 'audio/aac',
            //     });
            // }

		fetch(`${GLOBAL.apiUrl}article/edit`, {
			headers: {
                'Authorization' : 'Bearer '+this.props.token,
				'Accept': 'application/json',
				'Content-Type': 'multipart/form-data'
			},
			method: 'post',
			body: data
		})
			.then((response) => response.json())
			.then((responseJson) => {
                if(this.state.voiceExist){
                    this.handleBackgroundUploadSound(this.state.idDetailArticle)
                }

                // console.log(responseJson);
                // this.setState({
                //     isLoadingForSpinner : false
                // })
                this.props.navigation.navigate('home')
                // Notification.create({ subject: 'Tempo Witness', message: 'Upload Selesai' });
			}).catch((e)=>{
                if(this.state.voiceExist){
                    this.handleBackgroundUploadSound(this.state.idDetailArticle)
                }
                // this.setState({
                //     isLoadingForSpinner : false
                // })
                this.props.navigation.navigate('home')
                // Notification.create({ subject: 'Tempo Witness', message: 'Upload Selesai' });
                // Notification.create({ subject: 'Tempo Witness', message: 'Gagal Upload...' });
                console.log(e)
            })
    }
    
    handleSendArticleImage = () => {
        // Notification.create({ subject: 'Tempo Witness', message: 'Uploading...' });
        // this.closeModal()
        // this.setState({
        //     isLoadingForSpinner : true
        // })
        this.props.navigation.navigate('home')
        const data = new FormData();
            data.append('article_id',this.state.idDetailArticle);
            data.append('title',this.state.judul);
            data.append('desc', this.state.desk);
			//  if (this.state.imgforupload) {
            //     Notification.create({ subject: 'Tempo Witness', message: 'Uploading...' });
            //         data.append('file', {
            //             uri: this.state.gambarUri,
            //             type: this.state.gambarType,
            //             name: this.state.gambarFilename
            //         });
            //     }
        console.log(data)
		fetch(`${GLOBAL.apiUrl}article/edit`, {
			headers: {
                'Authorization' : 'Bearer '+this.props.token,
				'Accept': 'application/json',
				'Content-Type': 'multipart/form-data'
			},
			method: 'post',
			body: data
		})
			.then((response) => response.json())
			.then((responseJson) => {
                this.props.navigation.navigate('home')
                if (this.state.imgforupload) {
                   this.handleBackgroundUploadPhoto(this.state.idDetailArticle)
                    }
               
                // console.log(responseJson);
                // this.setState({
                //     isLoadingForSpinner : false
                // })
                // Notification.create({ subject: 'Tempo Witness', message: 'Upload Selesai' });
			}).catch((e)=>{
                if (this.state.imgforupload) {
                    this.handleBackgroundUploadPhoto(this.state.idDetailArticle)
                     }
                // this.props.navigation.navigate('home')
                // this.setState({
                //     isLoadingForSpinner : false
                // })
                // Notification.create({ subject: 'Tempo Witness', message: 'Upload Selesai' });
                // Notification.create({ subject: 'Tempo Witness', message: 'Upload Gagal' });
                console.log(e)
            })
    }
    
    handleSendArticleVideo = () => {
        // Notification.create({ subject: 'Tempo Witness', message: 'Uploading...' });
        // this.closeModal()
        // this.setState({
        //     isLoadingForSpinner : true
        // })
        const data = new FormData();
        data.append('article_id',this.state.idDetailArticle);
            data.append('title',this.state.judul);
            data.append('desc', this.state.desk);
            // if(this.props.uriVid !== ''){
            //     Notification.create({ subject: 'Tempo Witness', message: 'Uploading...' });
            //     data.append('file', {
            //         uri:this.props.uriVid,
            //         name: 'reportvideo.mp4',
            //         type: 'video/mp4',
            //         // name: this.state.gambarFilename
            //     });
            // }
			

		fetch(`${GLOBAL.apiUrl}article/edit`, {
			headers: {
                'Authorization' : 'Bearer '+this.props.token,
				'Accept': 'application/json',
				'Content-Type': 'multipart/form-data'
			},
			method: 'post',
			body: data
		})
			.then((response) => response.json())
			.then((responseJson) => {

                if(this.props.uriVid !== ''){
                    this.handleBackgroundUploadVideo()
                     }
                // this.setState({
                //     isLoadingForSpinner : false
                // })
                this.props.navigation.navigate('home')
                // Notification.create({ subject: 'Tempo Witness', message: 'Upload Selesai' });
				console.log(responseJson);
			}).catch((e)=>{
                 if(this.props.uriVid !== ''){
                    this.handleBackgroundUploadVideo()
                     }
               
                // this.setState({
                //     isLoadingForSpinner : false
                // })
                // this.props.navigation.navigate('home')
                console.log(e)
                // Notification.create({ subject: 'Tempo Witness', message: 'Upload Gagal' });
               // Notification.create({ subject: 'Tempo Witness', message: 'Upload jujul' });
            })
    }
    
    handleBackgroundUploadVideo = () => {

        let file = this.props.uriVid;
            if (Platform.OS == "android") {
            file = this.props.uriVid.replace("file://", "");
            }
        const options = {
            url: 'http://witness-api.hahabid.com/article/add_background',
            path: file,
            method: 'POST',
           // type: 'raw',
            field: 'file',
            type: 'multipart',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'multipart/form-data',
            //   'content-type': 'application/octet-stream', // Customize content-type
              'Authorization': `Bearer ${this.props.token}`,
              'arid': `${this.state.idDetailArticle}`
            },
            // Below are options only supported on Android
            notification: {
              enabled: true
            }
          }
          
          Upload.startUpload(options).then((uploadId) => {
            console.log('Upload started')
            Upload.addListener('progress', uploadId, (data) => {
              console.log(`Progress: ${data.progress}%`)
            })
            Upload.addListener('error', uploadId, (data) => {
              console.log(data)
            })
            Upload.addListener('cancelled', uploadId, (data) => {
              console.log(`Cancelled!`)
              console.log(data)
            })
            Upload.addListener('completed', uploadId, (data) => {
              console.log('Completed!')
              console.log(JSON.parse(data.responseBody))
              let dataResponse = JSON.parse(data.responseBody)
              this.fetchingListArticle()

            })
          }).catch((err) => {
            console.log('Upload error!', err)
          })
    }


    handleBackgroundUploadPhoto = (arid) => {

        let file = this.state.gambarUri;
            if (Platform.OS == "android") {
            file = this.state.gambarUri.replace("file://", "");
            }
        const options = {
            url: 'http://witness-api.hahabid.com/article/add_background',
            path: file,
            method: 'POST',
           // type: 'raw',
            field: 'file',
            type: 'multipart',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'multipart/form-data',
            //   'content-type': 'application/octet-stream', // Customize content-type
              'Authorization': `Bearer ${this.props.token}`,
              'arid': `${arid}`
            },
            // Below are options only supported on Android
            notification: {
              enabled: true
            }
          }
          
          Upload.startUpload(options).then((uploadId) => {
            console.log('Upload started')
            Upload.addListener('progress', uploadId, (data) => {
              console.log(`Progress: ${data.progress}%`)
            })
            Upload.addListener('error', uploadId, (data) => {
              console.log(data)
            })
            Upload.addListener('cancelled', uploadId, (data) => {
              console.log(`Cancelled!`)
              console.log(data)
            })
            Upload.addListener('completed', uploadId, (data) => {
              console.log('Completed!')
              //this.props.navigation.navigate('home')
              console.log(JSON.parse(data.responseBody))
              let dataResponse = JSON.parse(data.responseBody)
                this.fetchingListArticle()

            })
          }).catch((err) => {
            console.log('Upload error!', err)
          })
    }


    handleBackgroundUploadSound = (arid) => {
        /* /storage/emulated/0/Music/PHw05.aac*/
        let file = this.state.suaraTukKirim;
        const options = {
            url: 'http://witness-api.hahabid.com/article/add_background',
            path: file,
            method: 'POST',
           // type: 'raw',
            field: 'file',
            type: 'multipart',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'multipart/form-data',
            //   'content-type': 'application/octet-stream', // Customize content-type
              'Authorization': `Bearer ${this.props.token}`,
              'arid': `${arid}`
            },
            // Below are options only supported on Android
            notification: {
              enabled: true
            }
          }
          
          Upload.startUpload(options).then((uploadId) => {
            console.log('Upload started')
            Upload.addListener('progress', uploadId, (data) => {
              console.log(`Progress: ${data.progress}%`)
            })
            Upload.addListener('error', uploadId, (data) => {
              console.log(data)
            })
            Upload.addListener('cancelled', uploadId, (data) => {
              console.log(`Cancelled!`)
              console.log(data)
            })
            Upload.addListener('completed', uploadId, (data) => {
              console.log('Completed!')
              console.log(JSON.parse(data.responseBody))
              let dataResponse = JSON.parse(data.responseBody)
              this.fetchingListArticle()

            })
          }).catch((err) => {
            console.log('Upload error!', err)
          })
    }



    processorAudio = async () => {
        await this.setState({voiceFileName:this.makeid(5)})
        await this.setState({
            audioPath: AudioUtils.MusicDirectoryPath + '/'+this.state.voiceFileName+'.aac',
        })
        AudioRecorder.requestAuthorization().then((isAuthorised) => {
            this.setState({ hasPermission: isAuthorised });

            if (!isAuthorised) return;

            this.prepareRecordingPath(this.state.audioPath);

            AudioRecorder.onProgress = (data) => {
                this.setState({ currentTime: Math.floor(data.currentTime) });
            };

            AudioRecorder.onFinished = (data) => {
                // Android callback comes in the form of a promise instead.
                if (Platform.OS === 'ios') {
                    this._finishRecording(data.status === "OK", data.audioFileURL, data.audioFileSize);
                }
            };
        });
    }

//=============================== image prossess ===========================
    addArticle = () => {
       
        this.setState({mediaNya:'file:///storage/emulated/0/Pictures/'+this.state.gambarFilename})
    //     var artikel = this.state.listArticleForPush;
    //     artikel['articleId'] = this.makeid(5);
    //     artikel['title'] = this.state.judul;
    //     artikel['shortDesc'] = this.state.desk.substring(0,20);
    //     artikel['longDesc'] = this.state.desk;
    //     artikel['type'] = 'image';
    //     artikel['media'] = 'file:///storage/emulated/0/Pictures/'+this.state.gambarFilename;
    //     artikel['ext'] = this.state.gambarFilename.split('.').pop();
    //     if(this.state.gambarFilename !== ''){
    //         artikel['isExist'] = true;
    //     }else{
    //         artikel['isExist'] = false;
    //     }
    //    // var items = this.props.myArticle;
    //     if(this.state.idForEdit !== ''){
    //         objIndex = items.findIndex((obj => obj.articleId == this.state.idForEdit));
    //         items[objIndex].title = this.state.judul
    //         items[objIndex].shortDesc =  this.state.desk.substring(0,20);
    //         items[objIndex].longDesc =  this.state.desk
           
    //         if(this.state.gambarFilename !== ''){
    //             items[objIndex].media = 'file:///storage/emulated/0/Pictures/'+this.state.gambarFilename;
    //             items[objIndex].isExist = true;
    //         }else{
    //           //  items[objIndex].isExist = false;
    //             items[objIndex].isExist = items[objIndex].isExist ? items[objIndex].isExist : false;
    //         }
    //         // this.props.inputArticleList(JSON.stringify(items))
    //     }else{
    //         items.push(artikel);
    //         // this.props.inputArticleList(JSON.stringify(items))
    //     }
        
        
        this.closeModal()
           
    }

    removeArticle = async (id) => {
        let newarrartc = await this.props.myArticle.filter(item => item.articleId !== id);
        this.props.inputArticleList(JSON.stringify(newarrartc))
        this.closeModal()
    }

    removeOnePressed = (par) => {
        Alert.alert(
            'Atenção!',
            'Tem a certeza de que vai apagar este relatório??',
            [
                { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' },
                { text: 'Yes', onPress: () => this.removeArticle(par) }
            ]
        )
    }

    submitOnPressed = (id,modalname) => {
        Alert.alert(
            'Atenção!',
            'Tem a certeza de que vai apagar este relatório??',
            [
                { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' },
                { text: 'Yes', onPress: () => this.handleSubmit(id,modalname) }
            ]
        )
    }

    openModalEdit = async (modalName) => {
    //     let newarrartc = await this.props.myArticle.filter(item => item.articleId == id);
    //     this.setState({
    //        idForEdit:id,
    //        judul : newarrartc[0].title,
    //        desk : newarrartc[0].longDesc
    //    })

       if(modalName == 'image'){
        this.openModaldua()
       }else if(modalName == 'video'){
        this.openModal()
       }else{
        this.openModaltiga()
       }

       
    }

    //=============================== voice prossess ===========================

    addArticleVoice = () => {
        this.setState({mediaNya: 'file:///storage/emulated/0/Music/'+this.state.voiceFileName+'.aac'})
        // var artikel = this.state.listArticleForPush;
        // artikel['articleId'] = this.state.voiceFileName;
        // artikel['title'] = this.state.judul;
        // artikel['shortDesc'] = this.state.desk.substring(0,20);
        // artikel['longDesc'] = this.state.desk;
        // artikel['type'] = 'sound';
        // // artikel['media'] = 'file:///storage/emulated/0/Music/'+this.state.voiceFileName+'.aac';
        // artikel['media'] = this.state.soundPath;
        // artikel['media2'] = this.state.audioPath;
       
        // artikel['isExist'] = this.state.voiceExist;
        // artikel['ext'] = 'aac'
           
       
        // var items = this.props.myArticle;
        // if(this.state.idForEdit !== ''){
        //     objIndex = items.findIndex((obj => obj.articleId == this.state.idForEdit));
        //     items[objIndex].title = this.state.judul
        //     items[objIndex].shortDesc =  this.state.desk.substring(0,20);
        //     items[objIndex].longDesc =  this.state.desk
        //     if(this.state.soundPath !== undefined || this.state.soundPath !== ''){
        //         items[objIndex].media = this.state.soundPath;
        //         items[objIndex].media2 = this.state.soundPath ? this.state.audioPath :  items[objIndex].media2  ;
        //         items[objIndex].isExist = true
        //     }else{
        //         items[objIndex].isExist = items[objIndex].isExist ? items[objIndex].isExist : this.state.voiceExist;
        //     }
            
        //     // this.props.inputArticleList(JSON.stringify(items))
        // }else{
        //     items.push(artikel);
        //     // this.props.inputArticleList(JSON.stringify(items))
        // }
        
        
        this.closeModal()
           
    }

    //================================ video process ====================================

    addArticleVideo = async () => {
        this.setState({mediaNya:this.props.uriVid})
        // var artikel = this.state.listArticleForPush;
        // artikel['articleId'] =  this.makeid(5);
        // artikel['title'] = this.state.judul;
        // artikel['shortDesc'] = this.state.desk.substring(0,20);
        // artikel['longDesc'] = this.state.desk;
        // artikel['type'] = 'video';
        // // artikel['media'] = 'file:///storage/emulated/0/Music/'+this.state.voiceFileName+'.aac';
        // artikel['media'] = this.props.uriVid;
        // artikel['ext'] ='mp4'
        // if(this.props.uriVid !== ''){
        //     artikel['isExist'] = true;
        // }else{
        //     artikel['isExist'] = false;
        // }
        // var items = this.props.myArticle;
        // if(this.state.idForEdit !== ''){
        //     objIndex = items.findIndex((obj => obj.articleId == this.state.idForEdit));
        //     items[objIndex].title = this.state.judul
        //     items[objIndex].shortDesc =  this.state.desk.substring(0,20);
        //     items[objIndex].longDesc =  this.state.desk
           
        //     if(this.props.uriVid !== ''){
        //         items[objIndex].media = this.props.uriVid;
        //         items[objIndex].isExist = true;
        //     }else{
        //        // items[objIndex].isExist = false;
        //         items[objIndex].isExist = items[objIndex].isExist ? items[objIndex].isExist : false;
        //     }
        // //    await this.props.inputArticleList(JSON.stringify(items))
        //    this.props.save_uri_video('')
        // }else{
        //     items.push(artikel);
        // //    await this.props.inputArticleList(JSON.stringify(items))
        //    this.props.save_uri_video('')
        // }
        
        
        this.closeModal()
           
    }

    //=======================================================================================


     makeid = (length)=>{
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
     }

   

    // setEmailOneSignal = () => {
    //     OneSignal.init("6169230c-f70c-4ecc-88f7-0828c79945ff", { kOSSettingsKeyAutoPrompt: true });
    //     OneSignal.setEmail(this.props.email, (error) => {
    //         //console.log("Sent email with error: ", error);
    //     });
    //     OneSignal.getPermissionSubscriptionState((subscriptionState) => {
    //         this.setState({ osid: subscriptionState.userId }, this.saveOneSignalId)
    //     })
    // }


    saveOneSignalId = () => {
        let data = JSON.stringify({
            osid: this.state.osid
        })
        axios.post(`${GLOBAL.apiUrl}relawan/updateonesignal`, data, {
            headers: {
                'Authorization': `Bearer ${this.props.token}`,
                'Content-Type': 'application/json',
            }
        }).then((res) => {
            // console.log(res)
        }).catch((e) => {
            // console.log(e)
        })
    }


 




    handleUploadImage = () => {
        const options = {
            title: 'Foto de no máximo 4MB ',
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.launchCamera(options, (response) => {
            if (response.didCancel) {

            }
            else if (response.error) {

            } else if (response.fileSize > 10000000) {
                Alert.alert('arquivo de foto é muito grande')
            }
            else {
                // console.log(response)
                // this.uplod(response)
                this.setState({
                    imgforupload: response,
                    gambarFilename: response.fileName,
                    gambarUri: response.uri,
                    gambarType: response.type
                })
            }
        });
    }


    uplod = (datanya, ei) => {
        // console.log(datanya)
        RNFetchBlob.fetch('POST', `${GLOBAL.apiUrl}event/imgnext`, {
            Accept: 'application/json',
            'Authorization': `${ei}`,
            'Content-Type': 'multipart/form-data',
        }, [
                {
                    name: 'pict',
                    filename: datanya.fileName,
                    type: datanya.type,
                    data: datanya.data
                }
            ]
        ).then((resp) => {
            let rspUpload = JSON.parse(resp.data);
            if (rspUpload.success == true) {
                this.fetchingEvent()
                this.setState({ eventname: '' })
            } else {
                Alert.alert('Falha ao atualizar a foto')
            }
        }).catch((err) => {
            // console.log(JSON.parse(err))
            // Alert.alert('Kesalahan Server')
        })
    }

    openModal = () => {//canvasing
        this.setState({
            isModalOpen: !this.state.isModalOpen,
            typeevent: '3',
            usulan: '',
            imgforupload: '',
            gambarUri: '',
            gambarFilename: '',
            gambarType: '',
            letsPlay: ''
        })

        // setTimeout(()=>{
        //     this.props.navigation.navigate('recordvideo')
        // },200)
    }
    openModaldua = () => {//isu
        // this.handleUploadImage()
        this.setState({
            isModalOpen2: !this.state.isModalOpen2,
            typeevent: '1',
            usulan: '',
            imgforupload: '',
            gambarUri: '',
            gambarFilename: '',
            gambarType: '',
            letsPlay: ''
        })
       
    }

    openModaltiga = async () => {//broadcast
       await this.setState({
            isModalOpen3: !this.state.isModalOpen3,
            typeevent: '',
            usulan: '',
            imgforupload: '',
            gambarUri: '',
            gambarFilename: '',
            gambarType: '',
            letsPlay: '',
            //voiceFileName:this.makeid(5),
           // audioPath: AudioUtils.MusicDirectoryPath + '/'+this.makeid(5)+'.aac',
        })

        this.processorAudio()

        // await this.setState({
        //     audioPath: AudioUtils.MusicDirectoryPath + '/'+this.state.voiceFileName+'.aac',
        // })
       // this.fetchDetailKabupaten2()
        //this.fetchDetailKecamatan2()
    }

    closeModal = () => {
        this.setState({ 
            isModalOpen: false, 
            isModalOpen2: false, 
            isModalOpen3: false,
            // gambarFilename:'',
            // voiceFileName:'',
            // voiceExist:false,
            // judul:'',
            // desk:'',
            // idForEdit:'',
            // soundPath:'',
            // audioPath:'',

        })
    }

    openLinking = (ph) => {
        // Linking.openURL(`tel:021${800800}`)
        Linking.openURL(`tel:${ph}`)
    }

    // openLinking2 = (ur) => {
    //     Alert.alert(
    //         'Perhatian',
    //         'Kunjungi Link',
    //         [
    //             { text: 'Cancel', onPress: () => console.log('no') },
    //             { text: 'Ok', onPress: () => Linking.openURL(ur) }
    //         ] //, { cancelable: false }
    //     )

    // }

    handleScroll = (event) => {
        const CustomLayoutLinear = {
            duration: 100,
            create: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity },
            update: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity },
            delete: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity }
        }

        const currentOffset = event.nativeEvent.contentOffset.y
        let shouldShow

        if (currentOffset > this._scrollOffset || currentOffset === 0) {//0
            // scroll down or on top of ScrollView
            shouldShow = false
            if (this.isCloseToBottom(event.nativeEvent.layoutMeasurement, event.nativeEvent.contentOffset, event.nativeEvent.contentSize)) {
                this.setState({ page: this.state.page + 1 }, this.handleConcat)
                //Alert.alert('sampai bawah')
                //this.handleConcat()
            }
        } else {
            // scroll up
            shouldShow = true
        }

        if (shouldShow !== this.state.shouldShow && (!shouldShow || currentOffset > 250)) {//250
            LayoutAnimation.configureNext(CustomLayoutLinear)
            this._scrollOffset = currentOffset
            this.setState({ shouldShow })
        }

        this._scrollOffset = currentOffset
    }

    handleConcat = async () => {
        try {
            this.setState({ loadingMore: true })
            const response = await axios.get(`${GLOBAL.apiUrl}relawan/geteventbyrule?p=${this.state.page}`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })
            if (response.data.success == true) {
                await this.setState({
                    alleventforconcat: response.data.data,
                })
                await this.setState({
                    daftarevent: this.state.daftarevent.concat(this.state.alleventforconcat),

                })
                this.setState({ loadingMore: false })

                // console.log(response)
            } else {
                // console.log(response)
                this.setState({ loadingMore: false })
            }
        }
        catch (e) {
            // console.log(e)
            this.setState({ loadingMore: false })
        }
    }

    isCloseToBottom = (layoutMeasurement, contentOffset, contentSize) => {
        const paddingToBottom = 10;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };


    openLinking2 = async (ur) => {
        let cek = await this.isYoutubeUrl(ur)
        if (cek == true) {
            let vId = await this.getId(ur)
            Alert.alert(
                'Perhatian',
                'Lihat Video ?',
                [
                    { text: 'Cancel', onPress: () => console.log('no') },
                    { text: 'Ok', onPress: () => Linking.openURL(ur) }
                ] //, { cancelable: false }
            )
        } else {
            Alert.alert(
                'Perhatian',
                'Kunjungi Link ?',
                [
                    { text: 'Cancel', onPress: () => console.log('no') },
                    { text: 'Ok', onPress: () => Linking.openURL(ur) }
                ] //, { cancelable: false }
            )
        }
    }



    isYoutubeUrl = (url) => {
        //var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/
        var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        if (url.match(p)) {
            return true;
        }
        return false;
    }



    getId = (url) => {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }



    handleIconYoutubeClick = (pa) => {
        if (pa == 'paused') {
            this.setState({ letsPlay: '' })
        }
    }


    //==================================== function audios=================

    prepareRecordingPath(audioPath) {
        AudioRecorder.prepareRecordingAtPath(audioPath, {
            SampleRate: 22050,
            Channels: 1,
            AudioQuality: "Low",
            AudioEncoding: "aac",
            AudioEncodingBitRate: 32000
        });
    }

    _renderButton(title, onPress, active) {
        var style = (active) ? styles.activeButtonText : styles.buttonText;

        return (
            <TouchableHighlight style={styles.button} onPress={onPress}>
                <Text style={style}>
                    {title}
                </Text>
            </TouchableHighlight>
        );
    }

    _renderPauseButton(onPress, active) {
        var style = (active) ? styles.activeButtonText : styles.buttonText;
        var title = this.state.paused ? "RESUME" : "PAUSE";
        return (
            <TouchableHighlight style={styles.button} onPress={onPress}>
                <Text style={style}>
                    {title}
                </Text>
            </TouchableHighlight>
        );
    }

    async _pause() {
        if (!this.state.recording) {
            console.warn('Can\'t pause, not recording!');
            return;
        }

        try {
            const filePath = await AudioRecorder.pauseRecording();
            this.setState({ paused: true });
        } catch (error) {
            console.error(error);
        }
    }

    async _resume() {
        if (!this.state.paused) {
            console.warn('Can\'t resume, not paused!');
            return;
        }

        try {
            await AudioRecorder.resumeRecording();
            this.setState({ paused: false });
        } catch (error) {
            console.error(error);
        }
    }

    async _stop() {
        if (!this.state.recording) {
            console.warn('Can\'t stop, not recording!');
            return;
        }

        this.setState({ stoppedRecording: true, recording: false, paused: false });

        try {
            const filePath = await AudioRecorder.stopRecording();

            if (Platform.OS === 'android') {
                this._finishRecording(true, filePath);
            }
            return filePath;
        } catch (error) {
            console.error(error);
        }
    }

    async _play() {
        if (this.state.recording) {
            await this._stop();
        }

        // These timeouts are a hacky workaround for some issues with react-native-sound.
        // See https://github.com/zmxv/react-native-sound/issues/89.
        setTimeout(() => {
            var sound = new Sound(this.state.audioPath, '', (error) => {
                if (error) {
                    console.log('failed to load the sound', error);
                }
            });

            setTimeout(() => {
                sound.play((success) => {
                    if (success) {
                        console.log('successfully finished playing');
                    } else {
                        console.log('playback failed due to audio decoding errors');
                    }
                });
            }, 100);
        }, 100);
    }

    async _record() {
        if (this.state.recording) {
            console.warn('Already recording!');
            return;
        }

        if (!this.state.hasPermission) {
            console.warn('Can\'t record, no permission granted!');
            return;
        }

        if (this.state.stoppedRecording) {
            this.prepareRecordingPath(this.state.audioPath);
        }

        this.setState({ recording: true, paused: false });

        try {
            const filePath = await AudioRecorder.startRecording();
        } catch (error) {
            console.error(error);
        }
    }

    _finishRecording(didSucceed, filePath, fileSize) {
        this.setState({ finished: didSucceed, 
            soundPath:`file://${AudioUtils.MusicDirectoryPath}/${this.state.voiceFileName}.aac`, 
            voiceExist:true,
            suaraTukKirim:filePath
        });
        console.log(`Finished recording of duration ${this.state.currentTime} seconds at path: ${filePath} and size of ${fileSize || 0} bytes`);
    }

    handlePlayAudio = (uri)=>{
        if(this.state.audioIsPlaying){
            this.handleStopAudio()
        }else{
            Sound.setCategory('Playback');
            this.whoosh = new Sound(uri, Sound.MAIN_BUNDLE, (error) => {
                if (error) {
                    console.log('failed to load the sound', error);
                    return;
                }
                this.whoosh.play((success) => {
                    if (success) {
                    console.log('successfully finished playing');
                    this.setState({audioIsPlaying:true,audioIsPlayingIndex:''})
                    } else {
                    console.log('playback failed due to audio decoding errors');
                    }
                });
                });
        }
       

    }

    handlePlayAudioPress = async (uri,ind)=>{
       await this.setState({audioIsPlaying : !this.state.audioIsPlaying})
       this.setState({audioIsPlayingIndex : ind})
       this.handlePlayAudio(uri)
    }

    handleStopAudio = ()=>{
        this.whoosh.pause();
    }



    //==================================== function audios end ===============
    fancyTimeFormat = (time) => {
        // Hours, minutes and seconds
        var hrs = ~~(time / 3600);
        var mins = ~~((time % 3600) / 60);
        var secs = ~~time % 60;

        // Output like "1:01" or "4:03:59" or "123:03:59"
        var ret = "";

        if (hrs > 0) {
            ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
        }

        ret += "" + mins + ":" + (secs < 10 ? "0" : "");
        ret += "" + secs;
        return ret;
    }

    render() {
        if (this.props.tweetPosted === "success") {
            this.closeModal();
        }
        return (
            <Container>
                 <Header style={{
                    backgroundColor: "white", borderBottomColor: 'black',
                    borderBottomWidth: 0.6
                }}>
                    <Left>
                        <Button
                            onPress={() => this.props.navigation.goBack()}
                            transparent>
                            <Icon name="arrow-back" style={{ color: 'blue' }} />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={{ color: 'black' }}>Edit</Title>
                    </Body>
                    <Right />
                </Header>
                {/* <StatusBar
                    backgroundColor='transparent'

                /> */}
                {/* <Header style={styles.topMargin}>
                    <Left>
                        <Thumbnail small source={{ uri: 'http://relawan-cms.hahabid.com/assets/uploads/profile/noimage.png' }} />
                    </Left>
                    <Body>
                        <Title style={{ color: "#121212" }}>Home</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={this.openModal}>
                            <Icon name="md-create" style={{ color: "#4286f4" }} />
                        </Button>
                    </Right>
                </Header> */}
                <Spinner
                    visible={this.state.isLoadingForSpinner}
                    textContent={'Loading...'}
                    textStyle={{color:'white'}}
                    />

                <Content
                    // refreshControl={
                    //     <RefreshControl
                    //         refreshing={this.state.refreshing}
                    //         onRefresh={this._onRefresh}
                    //     />
                    // }
                    // onScroll={this.handleScroll} style={{ backgroundColor: "white" }}
                    >

                      
                   

                    <View contentContainerStyle={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center"
                    }}>
                    </View>



                    <View style={{ justifyContent: "flex-start" }}>

                      {
                        //    this.props.myArticle.length == 0 && 
                        //    <TopButton
                        //         openImage={this.openModaldua}
                        //         openVideo={this.openModal}
                        //         openVoice={this.openModaltiga}
                        //       />
                        }


                        {/* =============================== */}
                        {
                           this.state.myArticle && this.state.myArticle.map((r,i) => (
                                <View style={styles.tweet}>
                                <TouchableHighlight
                                    underlayColor="white"
                                    activeOpacity={0.75}>
                                    <View style={{ flex: 1, flexDirection: "row" }}>
                                        {/* <Thumbnail source={{ uri: this.props.photo }} /> */}
                                        <View style={{
                                            flexDirection: "column",
                                            justifyContent: "flex-start"
                                        }}>
                                            <Text style={{
                                                paddingLeft: 15
                                            }}>
                                    
                                            </Text>
                                            <Text style={{
                                                paddingLeft: 15,
                                                fontWeight: "bold",
                                                fontSize: 20,
                                                color: 'black'
                                            }}>
    
                                               {this.state.judul}
                                            </Text>
    
                                            <Text style={{
                                                paddingLeft: 15,
                                                color: "#aaa",
                                                fontSize: 16
                                            }}>
    
                                            </Text>
    
                                            <Text style={{
                                                paddingLeft: 15,
                                                color: "#aaa",
                                                fontSize: 16
                                            }}>
    
                                            </Text>
    
                                        </View>
    
                                    </View>
                                </TouchableHighlight>
                                            
                                            {
                                                (r.type == 'image' && r.isExist) && 
                                                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('imageviewer', { imglempar: '' })}>
                                                <View style={{ minHeight: 250, paddingBottom: 10 }}>
                                                    {/* <Image source={{ uri: res.eventPhoto }} resizeMode="contain" style={{ flex: 1 }} /> */}
                                                    {/* <AutoHeightImage
                
                                                        width={width}
                                                        source={{uri:r.media}}
                                                    /> */}

                                                     <FastImage
                                                        style={{ height: r.height }}
                                                        source={{
                                                            uri: this.state.gambarUri ? this.state.gambarUri : r.media,
                                                            cache:FastImage.cacheControl.web,
                                                            priority: FastImage.priority.normal,
                                                        }}
                                                        resizeMode={FastImage.resizeMode.contain}
                                                    />
            
                                                 
            
                                                </View>
                                            </TouchableWithoutFeedback>
                                            }


                                            {
                                                (r.type == 'video' && r.isExist) && 
                                            //     <Video source={{uri:r.media}}  
                                            //     ref={(ref) => {
                                            //         this.player = ref
                                            //     }}    
                                            //     fullscreenOrientation="portrait"
                                            //     controls={true}   
                                            //     posterResizeMode="stretch" 
                                            //     resizeMode="cover"            
                                            //     // posterResizeMode="contain"                               
                                            //     // onBuffer={this.onBuffer}               
                                            //     // onError={this.videoError}              
                                            //    style={{height:200,width:width*0.98}}
                                            //      />
                                            <Video url={ this.props.uriVid ? this.props.uriVid : r.media} />
                                            }

                                            {
                                                (r.type == 'sound' && r.isExist) && 
                                                <View style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-around',
                                                    alignItems: 'flex-end',
                                                    width: width * 0.7,
                                                    alignSelf: 'center',
                                                    marginTop: 10
                                                }}>
                    
                                                    <View>
                                                        <TouchableOpacity onPress={()=>this.handlePlayAudioPress(r.media,r.articleId)}>
                                                            <View style={{
                                                                height: 80,
                                                                width: 80,
                                                                backgroundColor: 'red',
                                                                borderRadius: 50,
                                                                justifyContent: 'center',
                                                                alignItems: 'center'
                        
                                                            }}>
                                                                <Icon  type="FontAwesome" name={this.state.audioIsPlayingIndex == r.articleId ? 'stop' : 'play'} />
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                        
                                                   
                                                </View>
                                            }
    
                              
                               
    
                                <View style={{
                                    flexDirection: "column",
                                    justifyContent: "flex-start"
                                }}>
                                    <Text style={{
                                        paddingLeft: 15,
                                        fontSize: 16,
                                        fontWeight: 'bold',
                                        color: 'black'
                                    }}>
    
                                        description :
    
                                    </Text>
                                    <View style={{alignSelf:'center', width:width*0.86}}><HTML html={this.state.desk}  /></View>
                                    {/* <Hyperlink
                                        linkStyle={{ color: '#2980b9' }}
                                        onPress={() => console.log('no')}>
                                        <Text
                                            selectable={true}
                                            style={{
                                                paddingLeft: 25,
                                                color: "#aaa",
                                                fontSize: 16
                                            }}>
                                            {this.state.desk}
                                        </Text>
                                        <View style={{ marginLeft: 25 }}>
                                            <Text style={{ color: 'black', fontWeight: 'bold' }}>Read More</Text>
                                        </View>
    
    
    
                                    </Hyperlink> */}
    
                                </View>
    
                                <Text style={styles.tweetText}>{}</Text>
                                <View style={styles.tweetFooter}>
                                    {/* <View style={{
                                        borderColor: 'red',
                                        borderWidth: 2,
                                        padding: 10,
                                        borderRadius: 50,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginBottom: 10
                                    }}>
                                        <Text style={{ color: 'red' }}>Waiting</Text>
                                    </View> */}
                                    {/* <View style={styles.footerIcons}>
                                        <Button transparent dark onPress={() => this.props.navigation.navigate('eventdetail', { eventIdLempar: '' })}>
                                            <Icon name="ios-text-outline" />
                                            <Text style={styles.badgeCount}>{'1'}</Text>
                                        </Button>
                                    </View> */}
                                    {/* <View style={styles.footerIcons}>
                                                <Button transparent dark onPress={() => this.props.navigation.navigate('eventdetailhusus', { eventIdLempar: res.eventId })}>
                                                    <Icon name="ios-repeat" />
                                                    <Text style={styles.badgeCount}>{}</Text>
                                                </Button>
                                            </View>
                                            <View style={styles.footerIcons} >
                                                <Button transparent dark onPress={() => this.handleSendLike(res.eventId)}>
                                                    {res.youlike == true ? <Icon name="ios-heart" style={{ color: 'red' }} /> : <Icon name="ios-heart-outline" />}
                                                    <Text style={styles.badgeCount}>{res.likeCount}</Text>
                                                </Button>
                                            </View> */}
                                    <View style={styles.footerIcons}>
    
                                        <Button
                                         style={{ backgroundColor: "green", height: 40, width: 50, justifyContent: 'center', alignItems: 'center' }}
                                        rounded onPress={() => this.openModalEdit(r.type)}>
                                            <Icon name="ios-create" />
                                        </Button>
                                    </View>

                                   

                                    <View style={styles.footerIcons}>
                                        <Button
                                            onPress={() => this.submitOnPressed(r.articleId,r.type)}
                                            rounded
                                            style={{ color: "#4286f4", height: 40, width: 94, justifyContent: 'center', alignItems: 'center' }}>
                                            <Text style={{ color: "white" }}>Update</Text>
                                        </Button>
    
                                      
                                    </View>
    
                                     {/* <View style={styles.footerIcons}>
                                      
                                         <Button
                                            onPress={() => this.removeOnePressed(r.articleId)}
                                            rounded
                                            style={{ backgroundColor: "red", height: 40, width: 50, justifyContent: 'center', alignItems: 'center' }}>
                                            <Icon name="ios-trash-outline" />
                                        </Button>
                                        
                                    </View> */}
                                </View>
                            </View>
                         ))
                        }

                        {/* =============================== */}

                    </View>






                    {
                        // this.state.loadingMore ? <ActivityIndicator size="large" color={GLOBAL.mainColor}
                        // style={{ marginBottom: 30, marginTop: 20 }} /> : <View
                        //     style={{
                        //         alignSelf: 'center', width: width * 0.5,
                        //         marginTop: 30, marginBottom: 30, borderBottomColor: GLOBAL.mainColor,
                        //         borderBottomWidth: 3
                        //     }}></View>
                            }
                    <View style={{ height: 100 }} />
                </Content>
                {
                    // modal canvasing
                }
                <Modal
                    ref={"newTweetModal"}
                    backdrop={true}
                    style={styles.modal}
                    // isOpen={this.state.isModalOpen}
                    isOpen={this.state.isModalOpen}
                    onClosed={this.closeModal.bind(this)}>

                    <ScrollView
                     keyboardShouldPersistTaps="always"
                    >

                        <View
                            style={{
                                alignSelf: "flex-start",
                                alignItems: "center",
                                flexDirection: "row",
                                padding: 5,
                                paddingRight: 10
                            }}>
                            <Button transparent onPress={this.closeModal.bind(this)}>
                                <Icon name="close" style={{ color: "black", fontSize: 32 }} />
                            </Button>
                            <View style={{ flex: 1 }} />
                            <Thumbnail
                                small
                                source={{
                                    uri:
                                        "https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg"
                                }} />
                        </View>
                        <View
                            style={{
                                flex: 1,
                                justifyContent: "flex-start",
                                alignItems: "flex-start",
                                width: "100%",

                            }} >
                            {/* <View style={styles.inputContainer}>
                                <Picker
                                    selectedValue={this.state.typeevent}
                                    style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ typeevent: itemValue })}>
                                    <Picker.Item label="Pilih..." value="0" />
                                    <Picker.Item label="Category 1" value="3" />
                                    <Picker.Item label="Category 2" value="4" />
                                </Picker>
                            </View> */}
                            {/* <View style={styles.inputContainer}>
                                <View><Text>Kabupaten : </Text></View>
                                <Picker
                                    selectedValue={this.state.kabupaten}
                                    style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                    onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue)}>
                                    <Picker.Item label="Pilih Kabupaten" value="0" />
                                    {this.state.kabupatenlist.map((res, i) => (
                                        <Picker.Item key={i} label={res.msrgName} value={res.msrgId} />
                                    ))}
                                </Picker>
                            </View>
                            <View style={styles.inputContainer}>
                                <View><Text>Kecamatan : </Text></View>
                                <Picker
                                    selectedValue={this.state.kecamatan}
                                    style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ kecamatan: itemValue })}>
                                    <Picker.Item label="Pilih Kecamatan" value="0" />
                                    {this.state.kecamatanlist.map((res, i) => (
                                        <Picker.Item key={i} label={res.msdiName} value={res.msdiId} />
                                    ))}
                                </Picker>
                            </View> */}
                            <Input
                                style={{
                                    width: width * 0.95, marginLeft: 10, color: 'grey', borderBottomWidth: 0.5, borderBottomColor: 'grey'
                                    // borderBottomColor: 'grey',
                                    // borderBottomWidth: 0.5

                                }}
                                multiline
                                placeholder="Judul... "
                                value={this.state.judul}
                                onChangeText={judul => this.setState({ judul })} />
                            <View style={{
                                borderTopColor: 'grey',
                                borderTopWidth: 0.5
                            }} />

                            <Input
                                style={{
                                    width: width * 0.95, marginLeft: 10, color: 'grey'

                                }}
                                multiline
                                value={this.state.desk}
                                placeholder="Description... "
                                onChangeText={desk => this.setState({ desk })} />
                        </View>
                        <View style={{justifyContent:'center',alignItems:'center',marginTop:15}}>
                            {/* <Text style={{color:'black', fontSize:20}}>Last Video</Text> */}
                        {/* <Video 
                            source={{uri:this.state.imagePreviewEdit}}  
                            ref={(ref) => {
                                this.player = ref
                            }}    
                            fullscreenOrientation="portrait"
                            controls={true}   
                            posterResizeMode="stretch" 
                            resizeMode="cover"                              
                            // onBuffer={this.onBuffer}               
                            // onError={this.videoError}              
                            style={{height:200,width:width*0.98}}
                            /> */}
                             <Video url={this.state.imagePreviewEdit} />
                        </View>
                        {/* <View style={styles.modalFooter}>
                            <Button transparent small onPress={this.handleUploadImage}>
                                <Icon name="ios-image" />
                            </Button>
                            <Button transparent small onPress={this.handleUploadImage}>
                                <Text>Attachment Photo</Text>
                            </Button>


                            <View style={{ flex: 1 }} />

                            <Button
                                onPress={this.handleSendEvent}
                                rounded
                                style={{ color: "#4286f4", height: 40, width: 94, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: "white" }}>Kirim</Text>
                            </Button>
                        </View> */}
                    </ScrollView>
                </Modal>

                {
                    //===========================================================================
                }
                <Modal
                    ref={"newTweetModal2"}
                    backdrop={true}
                    style={styles.modal}
                    isOpen={this.state.isModalOpen2}
                    onClosed={this.closeModal.bind(this)}>
                    <ScrollView 
                     keyboardShouldPersistTaps="always"
                    >
                        <View
                            style={{
                                alignSelf: "flex-start",
                                alignItems: "center",
                                flexDirection: "row",
                                padding: 5,
                                paddingRight: 10
                            }}>
                            <Button transparent onPress={this.closeModal.bind(this)}>
                                <Icon name="close" style={{ color: "black", fontSize: 32 }} />
                            </Button>
                            <View style={{ flex: 1 }} />
                            <Thumbnail
                                small
                                source={{
                                    uri:
                                        "https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg"
                                }} />
                        </View>
                        <View
                            style={{
                                flex: 1,
                                justifyContent: "flex-start",
                                alignItems: "flex-start",
                                width: "100%",

                            }} >
                            {/* <View style={styles.inputContainer}>
                                <Picker
                                    selectedValue={this.state.typeevent}
                                    style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ typeevent: itemValue })}>
                                    <Picker.Item label="Pilih..." value="0" />
                                    <Picker.Item label="category 1" value="1" />
                                    <Picker.Item label="category 2" value="2" />
                                </Picker>
                            </View> */}
                            {/* <View style={styles.inputContainer}>
                                <View><Text>Kabupaten : </Text></View>
                                <Picker
                                    selectedValue={this.state.kabupaten}
                                    style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                    onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue)}>
                                    <Picker.Item label="Pilih Kabupaten" value="0" />
                                    {this.state.kabupatenlist.map((res, i) => (
                                        <Picker.Item key={i} label={res.msrgName} value={res.msrgId} />
                                    ))}
                                </Picker>
                            </View>
                            <View style={styles.inputContainer}>
                                <View><Text>Kecamatan : </Text></View>
                                <Picker
                                    selectedValue={this.state.kecamatan}
                                    style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ kecamatan: itemValue })}>
                                    <Picker.Item label="Pilih Kecamatan" value="0" />
                                    {this.state.kecamatanlist.map((res, i) => (
                                        <Picker.Item key={i} label={res.msdiName} value={res.msdiId} />
                                    ))}
                                </Picker>
                            </View> */}
                            <Input
                                style={{
                                    width: width * 0.95, marginLeft: 10, color: 'grey', borderBottomWidth: 0.5, borderBottomColor: 'grey'
                                }}
                                multiline
                                placeholder="Judul..."
                                value={this.state.judul}
                                onChangeText={judul => this.setState({ judul })} />

                            <Input
                                style={{
                                    width: width * 0.95, marginLeft: 10, color: 'grey', borderBottomWidth: 0.5, borderBottomColor: 'grey'

                                }}
                                multiline
                                placeholder="Description... "
                                value={this.state.desk}
                                onChangeText={desk => this.setState({ desk })} />
                        </View>

                         <View style={{justifyContent:'center',alignItems:'center',marginTop:15}}>
                         {/* <Text  style={{color:'black', fontSize:20}}>Last Photo</Text> */}
                             {/* <AutoHeightImage
                                    width={width}
                                    source={{uri:this.state.imagePreviewEdit}}
                                /> */}

                                  <FastImage
                                                        style={{ height: 300 }}
                                                        source={{
                                                            uri: this.state.imagePreviewEdit,
                                                            cache:FastImage.cacheControl.web,
                                                            priority: FastImage.priority.normal,
                                                        }}
                                                        resizeMode={FastImage.resizeMode.contain}
                                                    />

                        </View>
                        {/* <View style={styles.modalFooter}>
                            <Button transparent small onPress={this.handleUploadImage}>
                                <Icon name="ios-image" />
                            </Button>
                            <Button transparent small onPress={this.handleUploadImage}>
                                <Text>Attachment Photo</Text>
                            </Button>


                            <View style={{ flex: 1 }} />

                            <Button
                                onPress={this.handleSendEvent}
                                rounded
                                style={{ color: "#4286f4", height: 40, width: 94, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: "white" }}>Kirim</Text>
                            </Button>
                        </View> */}
                    </ScrollView>
                </Modal>


                <Modal
                    //Audio
                    ref={"newTweetModal3"}
                    backdrop={true}
                    style={styles.modal}
                    isOpen={this.state.isModalOpen3}
                    onClosed={this.closeModal.bind(this)}>
                    <ScrollView
                     keyboardShouldPersistTaps="always"
                    >
                        <View
                            style={{
                                alignSelf: "flex-start",
                                alignItems: "center",
                                flexDirection: "row",
                                padding: 5,
                                paddingRight: 10
                            }}>
                            <Button transparent onPress={this.closeModal.bind(this)}>
                                <Icon name="close" style={{ color: "black", fontSize: 32 }} />
                            </Button>
                            <View style={{ flex: 1 }} />
                            <Thumbnail
                                small
                                source={{
                                    uri:
                                        "https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg"
                                }} />
                        </View>
                        <View
                            style={{
                                flex: 1,
                                justifyContent: "flex-start",
                                alignItems: "flex-start",
                                width: "100%",

                            }} >
                            {/* <View style={styles.inputContainer}>
                                <Picker
                                    selectedValue={this.state.typeevent}
                                    style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ typeevent: itemValue })}>
                                    <Picker.Item label="Pilih..." value="0" />
                                    <Picker.Item label="category 1" value="1" />
                                    <Picker.Item label="category 2" value="2" />
                                </Picker>
                            </View> */}
                            {/* <View style={styles.inputContainer}>
                                <View><Text>Kabupaten : </Text></View>
                                <Picker
                                    selectedValue={this.state.kabupaten}
                                    style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                    onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue)}>
                                    <Picker.Item label="Pilih Kabupaten" value="0" />
                                    {this.state.kabupatenlist.map((res, i) => (
                                        <Picker.Item key={i} label={res.msrgName} value={res.msrgId} />
                                    ))}
                                </Picker>
                            </View>
                            <View style={styles.inputContainer}>
                                <View><Text>Kecamatan : </Text></View>
                                <Picker
                                    selectedValue={this.state.kecamatan}
                                    style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ kecamatan: itemValue })}>
                                    <Picker.Item label="Pilih Kecamatan" value="0" />
                                    {this.state.kecamatanlist.map((res, i) => (
                                        <Picker.Item key={i} label={res.msdiName} value={res.msdiId} />
                                    ))}
                                </Picker>
                            </View> */}
                            <Input
                                style={{
                                    width: width * 0.95, marginLeft: 10, color: 'grey', borderBottomWidth: 0.5, borderBottomColor: 'grey'
                                }}
                                multiline
                                placeholder="Judul..."
                                value={this.state.judul}
                                onChangeText={judul => this.setState({ judul })} />

                            <Input
                                style={{
                                    width: width * 0.95, marginLeft: 10, color: 'grey', borderBottomWidth: 0.5, borderBottomColor: 'grey'

                                }}
                                multiline
                                placeholder="Description... "
                                value={this.state.desk}
                                onChangeText={desk => this.setState({ desk })} />
                        </View>
                        {/* <View style={styles.modalFooter}>
                            <Button transparent small onPress={this.handleUploadImage}>
                                <Icon name="ios-image" />
                            </Button>
                            <Button transparent small onPress={this.handleUploadImage}>
                                <Text>Attachment Photo</Text>
                            </Button>


                            <View style={{ flex: 1 }} />

                            <Button
                                onPress={this.handleSendEvent}
                                rounded
                                style={{ color: "#4286f4", height: 40, width: 94, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: "white" }}>Kirim</Text>
                            </Button>
                        </View> */}


                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                            alignItems: 'flex-end',
                            width: width * 0.7,
                            alignSelf: 'center',
                            marginTop: 10
                        }}>
                            <TouchableOpacity onPress={() => this._play()}>
                                <View>
                                    <Icon name="play" />
                                </View>
                            </TouchableOpacity>

                            <View>
                                <View style={{ alignSelf: 'center' }}><Text style={{
                                    color: 'red',
                                    fontWeight: 'bold',
                                    fontSize: 20
                                }}>{this.fancyTimeFormat(this.state.currentTime)}</Text></View>
                                <TouchableOpacity onPress={() => this._record()}>
                                    <View style={{
                                        height: 80,
                                        width: 80,
                                        backgroundColor: 'red',
                                        borderRadius: 50,
                                        justifyContent: 'center',
                                        alignItems: 'center'

                                    }}>
                                        <Icon name="mic" />
                                    </View>
                                </TouchableOpacity>
                                {/* <TouchableOpacity onPress={() => this._stop()}>
                                    <View style={{alignSelf:'center'}}>
                                        <Icon type="FontAwesome" name="times" />
                                    </View>
                                </TouchableOpacity> */}
                            </View>

                            <TouchableOpacity onPress={() => this._stop()}>
                                <View>
                                    <Icon type="FontAwesome" name="stop" />
                                </View>
                            </TouchableOpacity>
                        </View>


                        {/* <View>
            {this._renderButton("RECORD", () => {this._record()}, this.state.recording )}
            {this._renderButton("PLAY", () => {this._play()} )}
            {this._renderButton("STOP", () => {this._stop()} )}
            {this._renderButton("PAUSE", () => {this._pause()} )}
            {this._renderPauseButton(() => {this.state.paused ? this._resume() : this._pause()})}
            <Text style={styles.progressText}>{this.state.currentTime}s</Text>
          </View> */}










                    </ScrollView>
                </Modal>


                {
                    this.state.isModalOpen == true || this.state.isModalOpen2 == true || this.state.isModalOpen3 == true ?
                        <View style={[styles.modalFooter, styles.floatingButton]}>
                            <Button transparent small>
                                <Icon name={this.state.isModalOpen == true ? 'videocam' : this.state.isModalOpen2 == true ? 'ios-image' : ''} />
                            </Button>

                            {
                                this.state.isModalOpen == true ?
                                    <Button transparent small onPress={() => this.props.navigation.navigate('recordvideoTwo',{vivid:this.props.navigation.getParam('eventIdLempar')})}>
                                        <Text>Attachment Video</Text>
                                    </Button>
                                    : this.state.isModalOpen2 == true
                                        ?
                                        <Button transparent small onPress={this.handleUploadImage}>
                                            <Text>Attachment Photo</Text>
                                        </Button> :
                                        <Button transparent small onPress={this.onStartRecord}>
                                            <Text> </Text>
                                        </Button>
                            }




                            <View style={{ flex: 1 }} />


                            {
                                this.state.isModalOpen == true ?
                                <Button
                                onPress={this.addArticleVideo}
                                rounded
                                style={{ color: "#4286f4", height: 40, width: 94, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: "white" }}>Next </Text>
                              </Button>
                                    : this.state.isModalOpen2 == true
                                        ?
                                        <Button
                                onPress={this.addArticle}
                                rounded
                                style={{ color: "#4286f4", height: 40, width: 94, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: "white" }}>Next </Text>
                            </Button> :
                                        <Button
                                        onPress={this.addArticleVoice}
                                        rounded
                                        style={{ color: "#4286f4", height: 40, width: 94, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: "white" }}>Next</Text>
                                    </Button>
                            }

                            
                           

                        </View>
                        : null
                    // <Fab
                    //     active={this.state.active}
                    //     direction="up"
                    //     containerStyle={{}}
                    //     style={{ backgroundColor: '#5067FF' }}
                    //     position="bottomRight"
                    //     onPress={() => this.setState({ active: !this.state.active })}>
                    //     <Icon name="md-create" />


                    //     <Button style={{ backgroundColor: '#3B5998' }} onPress={this.openModaltiga}>
                    //         <Icon name="mic" />
                    //         {/* <Text style={{ color: "white" }}>Can</Text> */}
                    //     </Button>

                    //     <Button style={{ backgroundColor: '#DD5144' }} onPress={this.openModaldua}>
                    //         <Icon name="image" />
                    //         {/* <Text style={{ color: "white" }}>Isu</Text> */}
                    //     </Button>

                    //     <Button style={{ backgroundColor: '#3B5998' }} onPress={this.openModal}>
                    //         <Icon name="videocam" />
                    //         {/* <Text style={{ color: "white" }}>Can</Text> */}
                    //     </Button>
                    // </Fab>
                    // <Fab
                    //     position="bottomRight"
                    //     style={{ backgroundColor: "#4286f4" }}
                    //     onPress={this.openModal}
                    //     ref={"FAB"}
                    // >
                    //     <Icon name="md-create" />
                    // </Fab>
                }




                

            </Container >
        );
    }
}


const styles = StyleSheet.create({
    topMargin: {
        backgroundColor: "white",
        zIndex: -1,
        borderBottomColor: 'grey',
        borderBottomWidth: 0.4
    },
    content: {
        padding: 10,
        backgroundColor: "white"
    },
    heading: {
        fontSize: 32,
        fontWeight: "400",
        marginBottom: 30
    },
    tweet: {
        paddingTop: 20,
        paddingBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        borderBottomColor: "#bbb",
        borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection: "column"
    },
    tweetText: {
        marginTop: 10,
        fontSize: 18,
        color: "#555"
    },
    tweetFooter: {
        flexDirection: "row",
        // justifyContent: "flex-start",
        justifyContent: "space-around",
        padding: 0
    },
    badgeCount: {
        fontSize: 12,
        paddingLeft: 5
    },
    footerIcons: {
        flexDirection: "row",
        alignItems: "center"//center
    },
    modalFooter: {
        backgroundColor: "white",
        borderTopWidth: 0.4,
        borderTopColor: 'grey',
        // elevation: 3,
        // shadowColor: "#000",
        // shadowOffset: { width: 0, height: 0.2 },
        //  shadowOpacity: 0.3,
        // shadowRadius: 2,
        height: 54,
        width,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        padding: 5
    },
    modal: {
        justifyContent: "flex-start",
        alignItems: "center",
        position: "absolute",
        zIndex: 4,
        elevation: 4,
        paddingBottom: 145
        // height: Dimensions.get("window").height //- Expo.Constants.statusBarHeight,
        // marginTop: Expo.Constants.statusBarHeight / 2
    },
    inputContainer: {
        borderBottomColor: 'grey',
        borderBottomWidth: 0.5
    },
    floatingButton: {
        position: 'absolute',
        bottom: 0,
        zIndex: 99
    },
    fab: {
        height: 50,
        width: width * 0.7,
        alignSelf: 'center',
        // borderRadius: 200,
        position: 'absolute',
        bottom: 20,
        // left: 20,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    fabitem: {
        height: 50,
        width: 50,
        borderRadius: 200,
        // position: 'absolute',
        // bottom: 20,
        // left: 20,
        justifyContent: 'center',
        alignItems: 'center',
    }

});

//export default HomeScreen
const mapStateToProps = (state) => {
    const { relawanid, token, email, fasilitator, eposko, jabatan,myArticle,photo } = state.anggota;
    const {uriVid,modalVideoIsOpen2 } = state.auth;
    return { relawanid, token, email, fasilitator, eposko, jabatan,myArticle,photo,uriVid,modalVideoIsOpen2 };
};

export default connect(mapStateToProps, {inputArticleList,save_uri_video,update_list_article,model_video_open2})(withNavigationFocus(Draftscreen));
