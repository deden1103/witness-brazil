import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Icon } from 'native-base';
import { RNCamera } from 'react-native-camera';
import { connect } from "react-redux";
var RNFS = require('react-native-fs');
import {
  save_uri_video
} from '../rdx/actions';
class Recordvideo extends Component {
  constructor() {
    super()
    this.state = {
      recording: '',
      processing: '',
      uriV: '',
      backCamera: true
    }
  }
  async startRecording() {
    if (this.camera) {
      try {
        const promise = this.camera.recordAsync({
          // mute: false,
          // maxDuration: 5,
          quality: RNCamera.Constants.VideoQuality['4:3'],
        });

        if (promise) {
          this.setState({ recording: true });
          const data = await promise;
          this.props.save_uri_video(data.uri)


          //   const datar = new FormData();
          //   datar.append('title','koko');
          //   datar.append('desc',JSON.stringify(data.uri));
          //   datar.append('file', {
          //         uri:data.uri,
          //         name: 'reportvideo.mp4',
          //         type: 'video/mp4',

          //     });

          //  const sending = await fetch(`http://witness-api.hahabid.com/article/add`, {
          //     headers: {
          //               'Authorization' : 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJlbWFpbCI6ImRldkBnbWFpbC5jb20iLCJwaG9uZSI6IjA4NTY4NzE1Njc4MSIsImtvbXVuaXRhc2lkIjoiMiIsImxvZ2dlZCI6dHJ1ZSwiaWF0IjoxNTU5MDEzODkzLCJleHAiOjE1NjE2MDU4OTN9.QkOiK5mU-f1c-Iik7vqFGthmosYizbeAKQZdd0Z6nJU',
          //       'Accept': 'application/json',
          //       'Content-Type': 'multipart/form-data'
          //     },
          //     method: 'post',
          //     body: datar
          //   })



          this.setState({ recording: false });
          console.warn('takeVideo', data);
        }
      } catch (e) {
        console.log(e);
      }
    }
  }

  stopRecording() {
    this.camera.stopRecording();
    // this.props.navigation.navigate('home', { uriVideo: this.state.uriV })
    this.setState({ recording: false })
  }

  handleSelfie = () => {
    this.setState({ backCamera: !this.state.backCamera })
  }

  render() {
    const { recording, processing } = this.state;
    let button = (
      <TouchableOpacity
        onPress={this.startRecording.bind(this)}
        style={styles.capture}
      >
        <Text style={{ fontSize: 14 }}> RECORD </Text>
      </TouchableOpacity>
    );

    if (recording) {
      button = (
        <TouchableOpacity
          onPress={this.stopRecording.bind(this)}
          style={styles.capture}
        >
          <Text style={{ fontSize: 14 }}> STOP </Text>
        </TouchableOpacity>
      );
    }

    if (processing) {
      button = (
        <View style={styles.capture}>
          <ActivityIndicator animating size={18} />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View
          style={{ flex: 0, flexDirection: "row", justifyContent: "center", marginTop: 20 }}
        >
          <TouchableOpacity onPress={this.handleSelfie}>
            <View style={{}}>
              <Icon name="reverse-camera" style={{ color: "white", fontSize: 40 }} />
            </View>
          </TouchableOpacity>
        </View>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={this.state.backCamera ? RNCamera.Constants.Type.back : RNCamera.Constants.Type.front}
          flashMode={RNCamera.Constants.FlashMode.on}
          permissionDialogTitle={"Permission to use camera"}
          permissionDialogMessage={
            "We need your permission to use your camera phone"
          }
        />
        <View
          style={{ flex: 0, flexDirection: "row", justifyContent: "center" }}
        >
          {button}

          <TouchableOpacity
            onPress={()=>{
             this.handleGoback()
            }}
            style={styles.capture}
          >
            <Text style={{ fontSize: 14 }}> Kembali </Text>
          </TouchableOpacity>


          {/* <TouchableOpacity onPress={this.handleSelfie}>
            <View style={{}}>
              <Icon name="reverse-camera" style={{ color: "white" }} />
            </View>
          </TouchableOpacity> */}
        </View>
      </View>
    );
  }

  handleGoback = async () =>{
    await this.stopRecording.bind(this)
    this.props.navigation.goBack()
  }

  takePicture = async function () {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});

// export default Recordvideo
const mapStateToProps = (state) => {
  const { token, fasilitator, jabatan, eposko } = state.anggota;
  return { token, fasilitator, jabatan, eposko };
};

export default connect(mapStateToProps, {
  save_uri_video
})(Recordvideo);
/*
 <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          onGoogleVisionBarcodesDetected={({ barcodes }) => {
            console.log(barcodes);
          }}
        />
        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
            <Text style={{ fontSize: 14, color: 'red' }}> Record </Text>
          </TouchableOpacity>
        </View>
      </View>
 */