import React, { Component } from "react";
import {
    StyleSheet,
    View,
    TextInput,
    TouchableOpacity,
    TouchableHighlight,
    Alert,
    Text,
    Dimensions,
    processColor,
    ScrollView
} from "react-native";

import {
    Container,
    Content,
    Header,
    Body,
    Left,
    Title,
    Thumbnail,
    Col,
    Row,
    Grid,
    Icon,
    Spinner,
    Fab,
    Button,
    Footer,
    Input,
    Right
} from "native-base";
const { width, height } = Dimensions.get('window')
import { connect } from "react-redux";
import Image from 'react-native-remote-svg';
import axios from 'axios';
// import { PieChart } from 'react-native-charts-wrapper';
import { StackNavigator, SafeAreaView } from 'react-navigation';
// import Piechart from '../components/Piechart';
import DatePicker from "../components/datepicker.js";
import AutoHeightImage from 'react-native-auto-height-image';
class Report extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nilai: null,
            testPositif: 2,
            chartTwoIsShow: false,
            issueid: null,
            kabupatenid: null,
            kecamatanid: null,
            issuewithid: null,
            issuewithidandkabupatenid: null,
            isitable: [],
            tanggal: '',
            pieorganisasi: null,
            piekabupaten: null
        }
        this.data = null
    }

    async componentDidMount() {
        await this.fetchingPieOne()
        this.fetchingOrganisasi()
        this.fetchingKabupaten()
        // console.log(this.state.nilai)
        //  this.fetchingForTabale()

    }

    //chart/issue2?tgl=2018-12-14&issueid=1&kabupatenid=3175&kecamatanid=3175060

    fetchingPieOne = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}chart/issue2?tgl=${this.state.tanggal}`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })

            if (response.data.success == true) {
                let dt = {
                    dataSets: [{
                        values: response.data.data,//.dataSets[0].values
                        label: '',
                        config: {
                            colors: [processColor('#8CEAFF'), processColor('#FF8C9D'),processColor('#FFD08C'), processColor('#8CEAFF'), processColor('#FF8C9D'), processColor('#8CEAFF'), processColor('#FF8C9D')],
                            valueTextSize: 12,
                            valueTextColor: processColor('green'),
                            sliceSpace: 5,
                            selectionShift: 13,
                            xValuePosition: "OUTSIDE_SLICE",
                            yValuePosition: "OUTSIDE_SLICE",
                            valueFormatter: "#.#",
                            // valueFormatter: "#.#'%'",
                            valueLineColor: processColor('green'),
                            valueLinePart1Length: 0.5
                        }
                    }],
                }
                this.setState({
                    nilai: dt
                })
                // console.log(response.data.data)
            } else {
                // console.log(response.data.data)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }

    handleSelectissueid = (event) => {
        let entry = event.nativeEvent
        if (!event.nativeEvent.value) {
            this.setState({ issuewithid: null, isitable: [], issuewithidandkabupatenid: null })
        } else {
            this.setState({ issuewithid: null, isitable: [], issuewithidandkabupatenid: null })
            this.setState({ issueid: entry.data.id }, this.fetchingIssueWithId)
        }
    }

    fetchingIssueWithId = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}chart/issue2?tgl=${this.state.tanggal}&issueid=${this.state.issueid}`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })

            if (response.data.success == true) {
                let dt = {
                    dataSets: [{
                        values: response.data.data,//.dataSets[0].values
                        label: '',
                        config: {
                            colors: [processColor('#8CEAFF'),processColor('#FFD08C'), processColor('#8CEAFF'), processColor('#FF8C9D'), processColor('#8CEAFF'), processColor('#FF8C9D'), processColor('#FF8C9D')],
                            valueTextSize: 12,
                            valueTextColor: processColor('green'),
                            sliceSpace: 5,
                            selectionShift: 13,
                            xValuePosition: "OUTSIDE_SLICE",
                            yValuePosition: "OUTSIDE_SLICE",
                            valueFormatter: "#.#",
                            // valueFormatter: "#.#'%'",
                            valueLineColor: processColor('green'),
                            valueLinePart1Length: 0.5
                        }
                    }],
                }
                this.setState({
                    issuewithid: dt
                })
                // console.log(response.data.data)
            } else {
                // console.log(response.data.data)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }

    handleSelectissueIdAndKabupatenId = (event) => {
        let entry = event.nativeEvent
        if (!event.nativeEvent.value) {
            this.setState({ issuewithidandkabupatenid: null })
        } else {
            this.setState({ kabupatenid: entry.data.id }, this.fetchingIssueWithIdAndKabupatenId)
        }
    }

    fetchingIssueWithIdAndKabupatenId = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}chart/issue2?tgl=${this.state.tanggal}&issueid=${this.state.issueid}&kabupatenid=${this.state.kabupatenid}`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })

            if (response.data.success == true) {
                let dt = {
                    dataSets: [{
                        values: response.data.data,//.dataSets[0].values
                        label: '',
                        config: {
                            colors: [processColor('#8CEAFF'), processColor('#FF8C9D'),processColor('#FFD08C'), processColor('#8CEAFF'), processColor('#FF8C9D'), processColor('#8CEAFF'), processColor('#FF8C9D')],
                            valueTextSize: 12,
                            valueTextColor: processColor('green'),
                            sliceSpace: 5,
                            selectionShift: 13,
                            xValuePosition: "OUTSIDE_SLICE",
                            yValuePosition: "OUTSIDE_SLICE",
                            valueFormatter: "#.#",
                            // valueFormatter: "#.#'%'",
                            valueLineColor: processColor('green'),
                            valueLinePart1Length: 0.5
                        }
                    }],
                }
                this.setState({
                    issuewithidandkabupatenid: dt
                })
                // console.log(response.data.data)
            } else {
                // console.log(response.data.data)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }


    handlefortable = (event) => {
        let entry = event.nativeEvent
        if (!event.nativeEvent.value) {
            // console.log('koso')
        } else {
            this.setState({ kecamatanid: entry.data.id }, this.fetchingForTabale)
        }
    }

    fetchingForTabale = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}chart/issue2?tgl=${this.state.tanggal}&issueid=${this.state.issueid}&kabupatenid=${this.state.kabupatenid}&kecamatanid=${this.state.kecamatanid}`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })

            if (response.data.success == true) {
                this.setState({
                    isitable: response.data.data
                })
                // console.log(response.data.data)
            } else {
                // console.log(response.data.data)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }

    //============================================================

    fetchingOrganisasi = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}chart/organisasi2?tgl=${this.state.tanggal}`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })

            if (response.data.success == true) {
                let dt = {
                    dataSets: [{
                        values: response.data.data,//.dataSets[0].values
                        label: '',
                        config: {
                            colors: [processColor('#FFD08C'), processColor('#8CEAFF'), processColor('#FF8C9D'), processColor('#8CEAFF'), processColor('#FF8C9D')],
                            valueTextSize: 12,
                            valueTextColor: processColor('green'),
                            sliceSpace: 5,
                            selectionShift: 13,
                            xValuePosition: "OUTSIDE_SLICE",
                            yValuePosition: "OUTSIDE_SLICE",
                            valueFormatter: "#.#",
                            // valueFormatter: "#.#'%'",
                            valueLineColor: processColor('green'),
                            valueLinePart1Length: 0.5
                        }
                    }],
                }
                this.setState({
                    pieorganisasi: dt
                })
                // console.log(response.data.data)
            } else {
                // console.log(response.data.data)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }
    //========================================================


    fetchingKabupaten = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}chart/kabupaten2?tgl=${this.state.tanggal}`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })

            if (response.data.success == true) {
                let dt = {
                    dataSets: [{
                        values: response.data.data,//.dataSets[0].values
                        label: '',
                        config: {
                            colors: [processColor('#ff00e7'), processColor('#8CEAFF'), processColor('#FFD08C'), processColor('#8CEAFF'), processColor('#FF8C9D')],
                            valueTextSize: 12,
                            valueTextColor: processColor('green'),
                            sliceSpace: 5,
                            selectionShift: 13,
                            xValuePosition: "OUTSIDE_SLICE",
                            yValuePosition: "OUTSIDE_SLICE",
                            valueFormatter: "#.#",
                            // valueFormatter: "#.#'%'",
                            valueLineColor: processColor('green'),
                            valueLinePart1Length: 0.5
                        }
                    }],
                }
                this.setState({
                    piekabupaten: dt
                })
                // console.log(response.data.data)
            } else {
                // console.log(response.data.data)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }















    onchangedate = async (date) => {
        await this.setState({ tanggal: date });
        await this.fetchingPieOne()
        await this.fetchingOrganisasi()
        await this.fetchingKabupaten()
    }

    handleSelect = (event) => {
        // let entry = event.nativeEvent
        // if (entry == null) {
        //     this.setState({ ...this.state, selectedEntry: null })
        // } else {
        //     this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
        // }

        // console.log(entry.data.id)
    }




    render() {
        if (this.state.nilai == null) {
            return null
        }
        return (
            <Container style={{}}>
                <View>
                    <DatePicker
                        style={{ width: "100%", backgroundColor: 'transparent', borderBottomColor: 'grey', borderBottomWidth: 0.5 }}
                        date={this.state.tanggal}
                        mode="date"
                        placeholder="Pilih Tanggal"
                        // format="DD-MM-YYYY"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        showIcon={true}
                        customStyles={{
                            dateInput: {
                                marginLeft: 10,
                                marginTop: 8,
                                marginRight: 8,
                                marginBottom: 8,
                                padding: 8,
                                alignItems: "flex-start"
                            },
                            placeholderText: {
                                color: "#000000"
                            }
                        }}
                        onDateChange={date => {
                            this.onchangedate(date);
                        }}
                    />
                </View>
                <Content>

                    <View style={{ minHeight: 300 }}>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', marginTop: 15 }}>

                            <Text style={{ fontSize: 18 }}>
                                Chart Issue Negative & Positive
                            </Text>
                        </View>
                        {/* <Piechart
                            datanya={this.state.nilai}
                            selectPie={this.handleSelectissueid}
                        /> */}
                    </View>
                    {
                        this.state.issuewithid == null ? null :
                            <View style={{ minHeight: 250 }}>
                                <Piechart
                                    datanya={this.state.issuewithid}
                                    selectPie={this.handleSelectissueIdAndKabupatenId}
                                />
                            </View>
                    }

                    {
                        this.state.issuewithidandkabupatenid == null ? null :
                            <View style={{ minHeight: 250 }}>
                                <Piechart
                                    datanya={this.state.issuewithidandkabupatenid}
                                    selectPie={this.handlefortable}
                                />
                            </View>
                    }
                    {
                        // this.state.isitable.length < 1 ? null :
                        //     <View style={{ minHeight: 200, marginTop: 20 }}>
                        //         <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#E6ECF0' }}>
                        //             <View style={{ flex: 0.25, borderCOlor: 'grey', borderWidth: 0.5, alignItems: 'center' }}><Text>Nama Laporan</Text></View>
                        //             <View style={{ flex: 0.25, borderCOlor: 'grey', borderWidth: 0.5, alignItems: 'center' }}><Text>Photo Laporan</Text></View>
                        //             <View style={{ flex: 0.25, borderCOlor: 'grey', borderWidth: 0.5, alignItems: 'center' }}><Text>Nama Relawan</Text></View>
                        //             <View style={{ flex: 0.25, borderCOlor: 'grey', borderWidth: 0.5, alignItems: 'center' }}><Text>Organização</Text></View>
                        //         </View>
                        //         {this.state.isitable.map((res, i) => (
                        //             <View key={i} style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                        //                 <View style={{
                        //                     flex: 0.25, borderCOlor: 'grey', borderWidth: 0.5
                        //                 }}><Text style={{ marginLeft: 15 }}>{res.eventName}</Text></View>
                        //                 <View style={{ flex: 0.25, borderCOlor: 'grey', borderWidth: 0.5, alignItems: 'center' }}>
                        //                     <Image style={{ width: 50, height: 50 }} source={{ uri: res.eventPhoto }} />
                        //                 </View>
                        //                 <View style={{ flex: 0.25, borderCOlor: 'grey', borderWidth: 0.5, alignItems: 'center' }}><Text>{res.relawanId}</Text></View>
                        //                 <View style={{ flex: 0.25, borderCOlor: 'grey', borderWidth: 0.5, alignItems: 'center' }}><Text>{res.eventName}</Text></View>
                        //             </View>
                        //         ))}
                        //     </View>
                    }

                    {
                        this.state.isitable.length < 1 ? null :
                            this.state.isitable.map((res, i) => (
                                <View key={i} style={{ justifyContent: "flex-start", borderBottomColor: 'grey', borderBottomWidth: 0.5 }}>
                                    <View style={styles.tweet}>
                                        <TouchableHighlight
                                            underlayColor="white"
                                            activeOpacity={0.75}>
                                            <View style={{ flex: 1, flexDirection: "row" }}>
                                                <Thumbnail source={{ uri: '' }} />
                                                <View style={{
                                                    flexDirection: "column",
                                                    justifyContent: "flex-start"
                                                }}>
                                                    <Text style={{
                                                        paddingLeft: 15
                                                    }}>
                                                        {

                                                        }

                                                    </Text>
                                                    <Text style={{
                                                        paddingLeft: 15,
                                                        fontWeight: "bold",
                                                        fontSize: 20,
                                                        color: 'black'
                                                    }}>
                                                        {res.relawanId}

                                                    </Text>

                                                    <Text style={{
                                                        paddingLeft: 15,
                                                        color: "#aaa",
                                                        fontSize: 16
                                                    }}>
                                                        {}
                                                    </Text>

                                                    <Text style={{
                                                        paddingLeft: 15
                                                    }}>
                                                        {`Organisasi : ${res.organisasi}`}
                                                    </Text>
                                                </View>

                                            </View>
                                        </TouchableHighlight>

                                        {res.isPhotoExist && <View style={{ minHeight: 250, margin: 10, paddingBottom: 10 }}>
                                            {/* <Image source={{ uri: res.eventPhoto }} resizeMode="contain" style={{ flex: 1 }} /> */}
                                            <AutoHeightImage
                                                style={{ flex: 1 }}
                                                width={width * 0.9}
                                                source={{ uri: res.eventPhoto }}
                                            />
                                        </View>}




                                        <View style={{
                                            flexDirection: "column",
                                            justifyContent: "flex-start"
                                        }}>
                                            <Text style={{
                                                paddingLeft: 15,
                                                fontSize: 16,
                                                fontWeight: 'bold',
                                                color: "black"
                                            }}>
                                                {`Laporan : ${res.eventName}`}
                                            </Text>

                                            <Text style={{
                                                paddingLeft: 25,
                                                color: "#aaa",
                                                fontSize: 16
                                            }}>
                                                {}
                                            </Text>


                                            <Text style={{
                                                paddingLeft: 15,
                                                color: "black",
                                                fontSize: 16,
                                                fontWeight: 'bold',
                                            }}>

                                            </Text>

                                            <Text style={{
                                                paddingLeft: 25,
                                                color: "#aaa",
                                                fontSize: 16
                                            }}>
                                                {}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            ))

                    }



                    <View style={{ borderBottomColor: 'grey', borderBottomWidth: 0.5 }} />

                    {
                        this.state.pieorganisasi == null ? null : <View style={{ height: 300 }}>
                            <View style={{ justifyContent: 'center', alignSelf: 'center', marginTop: 15 }}>
                                <Text style={{ fontSize: 18 }}>
                                    Chart Koordinator
                            </Text>
                            </View>
                            <Piechart
                                datanya={this.state.pieorganisasi}
                                selectPie={this.handleSelect}
                            />
                        </View>
                    }
                    <View style={{ borderBottomColor: 'grey', borderBottomWidth: 0.5 }} />
                    {
                        this.state.piekabupaten == null ? null :
                            <View style={{ height: 300 }}>
                                <View style={{ justifyContent: 'center', alignSelf: 'center', marginTop: 15 }}>
                                    <Text style={{ fontSize: 18 }}>
                                        Chart Laporan per Kabupaten
                            </Text>
                                </View>
                                <Piechart
                                    datanya={this.state.piekabupaten}
                                    selectPie={this.handleSelect}
                                />
                            </View>
                    }

                    <View style={{ borderBottomColor: 'grey', borderBottomWidth: 0.5 }} />


                </Content>
            </Container>
        );
    }
}


const styles = StyleSheet.create({

});

const mapStateToProps = (state) => {
    const { token, fasilitator } = state.anggota;
    return { token, fasilitator };
};

export default connect(mapStateToProps, {})(Report);
/*
  this.setState({ nilai: {
                    dataSets: [{
                        values: [
                            { value: 45, label: 'Positive', id: 2 },
                            { value: 21, label: 'Negative', id: 4 }
                        ],
                        label: '',
                        config: {
                            colors: [processColor('#8CEAFF'), processColor('#FF8C9D')],
                            valueTextSize: 20,
                            valueTextColor: processColor('green'),
                            sliceSpace: 5,
                            selectionShift: 13,
                            // xValuePosition: "OUTSIDE_SLICE",
                            // yValuePosition: "OUTSIDE_SLICE",
                            valueFormatter: "#.#'%'",
                            valueLineColor: processColor('green'),
                            valueLinePart1Length: 0.5
                        }
                    }],
                } })


               let xxx = {}
                let clone = {
                    dataSets: [{
                        values: [
                            { value: 45, label: 'Positive', id: 2 },
                            { value: 21, label: 'Negative', id: 4 }
                        ],
                        label: '',
                        config: {
                            colors: [processColor('#8CEAFF'), processColor('#FF8C9D')],
                            valueTextSize: 20,
                            valueTextColor: processColor('green'),
                            sliceSpace: 5,
                            selectionShift: 13,
                            xValuePosition: "OUTSIDE_SLICE",
                            yValuePosition: "OUTSIDE_SLICE",
                            valueFormatter: "#.#'%'",
                            valueLineColor: processColor('green'),
                            valueLinePart1Length: 0.5
                        }
                    }],
                }


                clone.dataSets[0].values = response.data
                 this.setState({
                nilai:{
                    dataSets: [{
                        values: [
                            { value: 3, label: 'Positive', id: 2 },
                            { value: 1, label: 'Negative', id: 4 }
                        ],
                        label: '',
                        config: {
                            colors: [processColor('#8CEAFF'), processColor('#FF8C9D')],
                            valueTextSize: 20,
                            valueTextColor: processColor('green'),
                            sliceSpace: 5,
                            selectionShift: 13,
                            xValuePosition: "OUTSIDE_SLICE",
                            yValuePosition: "OUTSIDE_SLICE",
                            valueFormatter: "#.#",
                            // valueFormatter: "#.#'%'",
                            valueLineColor: processColor('green'),
                            valueLinePart1Length: 0.5
                        }
                    }],
                }
            })

*/