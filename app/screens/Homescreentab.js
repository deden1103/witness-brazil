import React, { Component } from "react";
import Modal from "react-native-modalbox";
import { createMaterialTopTabNavigator, createBottomTabNavigator } from 'react-navigation'
import Dimensions from "Dimensions";
import {
    StyleSheet,
    View,
    Text,
    FlatList,
    TouchableHighlight,
    Platform,
    Alert,
    StatusBar
} from "react-native";
import {
    Container,
    Header,
    Body,
    Content,
    Left,
    Title,
    Thumbnail,
    Col,
    Row,
    Grid,
    Icon,
    Spinner,
    Fab,
    Button,
    Footer,
    Input,
    Right,
    Tabs,
    Tab,
    TabHeading,
    ScrollableTab
} from "native-base";
import { connect } from "react-redux";
import Image from 'react-native-remote-svg';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
// import ScrollableTabView, {
//     ScrollableTabBar
// } from "react-native-scrollable-tab-view";
const { width, height } = Dimensions.get('window')
import Homescreen from './Homescreen'
import Profile from './Profile'
import Forgotpassword from './Forgotpassword'


const MyTab = createMaterialTopTabNavigator({
    a: { screen: Homescreen },
    b: { screen: Profile },
    c: { screen: Profile },
    d: { screen: Forgotpassword }
},
    {
        tabBarOptions: {
            labelStyle: {
                fontSize: 12,
            },
            style: {
                backgroundColor: 'white',
            },
            showIcon: true,
            showLabel: false,
            indicatorStyle: { backgroundColor: 'red', height: 4 }
        },
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                isColor = focused ? 'red' : 'grey'
                if (routeName === 'a') {
                    iconName = `ios-home-outline`;
                    isColor = focused ?  'red' : 'grey'
                } else if (routeName === 'b') {
                    iconName = `ios-text-outline`;
                    isColor = focused ?  'red' : 'grey'
                }
                else if (routeName === 'c') {
                    iconName = `ios-search-outline`;
                    isColor = focused ?  'red' : 'grey'
                }
                else if (routeName === 'd') {
                    iconName = `ios-notifications-outline`;
                    isColor = focused ?  'red' : 'grey'
                }
                return <Icon name={iconName} color={tintColor} style={{ color: isColor }} />
            },

        }),

    }
)

class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newTweetContent: "",
            isModalOpen: false,
            daftarevent: [],
            eventname: '',
            imgforupload: '',
            currentTab: 0
        };
    }

    componentDidMount() {
        // this.fetchingEvent()
    }

    // componentDidUpdate() {
    //     console.log(this.props)
    // }

    fetchingEvent = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}event/getallevent`)
            if (response.data.success == true) {
                this.setState({
                    daftarevent: response.data.data
                })
                console.log(response)
            } else {
                console.log(response)
            }
        }
        catch (e) {
            console.log(e)
        }
    }


    handleSendEvent = async () => {
        try {
            let data = JSON.stringify({
                eventName: this.state.eventname,
                relawanId: this.props.relawanid

            })
            if (this.state.eventname !== '') {
                let loginResponse = await axios.post(`${GLOBAL.apiUrl}event/sendevent`, data, {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
                if (loginResponse.data.success == true) {
                    this.fetchingEvent()
                    console.log(loginResponse)
                    this.setState({ eventname: '' })
                    this.closeModal()
                    this.uplod(this.state.imgforupload, loginResponse.data.idhasinput)
                    // this.component._root.scrollToEnd()
                } else {
                    Alert.alert('Kirim komentar Gagal / Anda Belum Login')
                    console.log(loginResponse)
                }
            } else {
                Alert.alert('A mensagem não pode estar vazia')
            }
        } catch (e) {
            console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }
    }

    handleUploadImage = () => {
        const options = {
            title: 'Foto de no máximo 4MB ',
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {

            }
            else if (response.error) {

            } else if (response.fileSize > 1000000) {
                Alert.alert('arquivo de foto é muito grande')
            }
            else {
                console.log(response)
                // this.uplod(response)
                this.setState({ imgforupload: response })
            }
        });
    }


    uplod = (datanya, ei) => {
        console.log(datanya)
        RNFetchBlob.fetch('POST', `${GLOBAL.apiUrl}event/imgnext`, {
            Accept: 'application/json',
            'Authorization': `${ei}`,
            'Content-Type': 'multipart/form-data',
        }, [
                {
                    name: 'pict',
                    filename: datanya.fileName,
                    type: datanya.type,
                    data: datanya.data
                }
            ]
        ).then((resp) => {
            let rspUpload = JSON.parse(resp.data);
            if (rspUpload.success == true) {
                this.fetchingEvent()
                this.setState({ eventname: '' })
            } else {
                Alert.alert('Falha ao atualizar a foto')
            }
        }).catch((err) => {
            console.log(JSON.parse(err))
            // Alert.alert('Kesalahan Server')
        })
    }

    openModal = () => {
        this.setState({ isModalOpen: !this.state.isModalOpen })
    }

    closeModal = () => {
        this.setState({ isModalOpen: false })
    }


    render() {
        if (this.props.tweetPosted === "success") {
            this.closeModal();
        }
        return (
            <Container style={{ marginTop: 20 }}>
                <Header style={styles.topMargin}>
                    <Left>
                        <Thumbnail small source={{ uri: 'http://relawan-cms.hahabid.com/assets/uploads/profile/noimage.png' }} />
                    </Left>
                    <Body>
                        <Title style={{ color: "#121212" }}>Home</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={this.openModal}>
                            <Icon name="md-create" style={{ color: "#4286f4" }} />
                        </Button>
                    </Right>
                </Header>



                <MyTab />



                <Modal
                    ref={"newTweetModal"}
                    backdrop={true}
                    style={styles.modal}
                    isOpen={this.state.isModalOpen}
                    onClosed={this.closeModal.bind(this)}>
                    <View
                        style={{
                            alignSelf: "flex-start",
                            alignItems: "center",
                            flexDirection: "row",
                            padding: 5,
                            paddingRight: 10
                        }}>
                        <Button transparent onPress={this.closeModal.bind(this)}>
                            <Icon name="close" style={{ color: "black", fontSize: 32 }} />
                        </Button>
                        <View style={{ flex: 1 }} />
                        <Thumbnail
                            small
                            source={{
                                uri:
                                    "https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg"
                            }} />
                    </View>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: "flex-start",
                            alignItems: "flex-start",
                            width: "100%",

                        }} >
                        <Input
                            style={{
                                flex: 1,
                                width: "100%",
                                fontSize: 24,
                                alignContent: "flex-start",
                                justifyContent: "flex-start",
                                textAlignVertical: "top",
                                margin: 5,

                            }}
                            multiline
                            placeholder="Posting..."
                            onChangeText={eventname => this.setState({ eventname })} />
                    </View>
                    <View style={styles.modalFooter}>
                        <Button transparent small onPress={this.handleUploadImage}>
                            <Icon name="ios-image" />
                        </Button>
                        <Button transparent small onPress={this.handleUploadImage}>
                            <Text>Attachment Photo</Text>
                        </Button>


                        <View style={{ flex: 1 }} />

                        <Button
                            onPress={this.handleSendEvent}
                            rounded
                            style={{ color: "#4286f4", height: 40, width: 94, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: "white" }}>Enviar</Text>
                        </Button>
                    </View>
                </Modal>

               

            </Container>
        );
    }
}


const styles = StyleSheet.create({
    topMargin: {
        backgroundColor: "white",
        zIndex: -1,
        borderBottomColor: 'grey',
        borderBottomWidth: 0.4
    },
    content: {
        padding: 10,
        backgroundColor: "white"
    },
    heading: {
        fontSize: 32,
        fontWeight: "400",
        marginBottom: 30
    },
    tweet: {
        paddingTop: 20,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
        borderBottomColor: "#bbb",
        borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection: "column"
    },
    tweetText: {
        marginTop: 10,
        fontSize: 18,
        color: "#555"
    },
    tweetFooter: {
        flexDirection: "row",
        // justifyContent: "flex-start",
        justifyContent: "space-around",
        padding: 0
    },
    badgeCount: {
        fontSize: 12,
        paddingLeft: 5
    },
    footerIcons: {
        flexDirection: "row",
        alignItems: "center"//center
    },
    modalFooter: {
        backgroundColor: "white",
        elevation: 3,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0.2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        height: 54,
        width: "100%",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        padding: 5
    },
    modal: {
        justifyContent: "flex-start",
        alignItems: "center",
        position: "absolute",
        zIndex: 4,
        elevation: 4,
        // height: Dimensions.get("window").height //- Expo.Constants.statusBarHeight,
        // marginTop: Expo.Constants.statusBarHeight / 2
    }
});

//export default HomeScreen
const mapStateToProps = (state) => {
    const { relawanid } = state.anggota;
    return { relawanid };
};

export default connect(mapStateToProps, {})(HomeScreen);