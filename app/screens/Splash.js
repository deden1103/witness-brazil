import React, { Component } from 'react'
import { StyleSheet, StatusBar, View, Text, Linking, Alert, AsyncStorage, ActivityIndicator, PermissionsAndroid } from 'react-native'
import { Container } from 'native-base'
import { connect } from "react-redux";
import Geolocation from '@react-native-community/geolocation';
// import VersionNumber from 'react-native-version-number';
import { save_latitude, save_longitude } from '../rdx/actions';
import RNSimData from 'react-native-sim-data'
import axios from 'axios';
class Splash extends Component {
    constructor(props) {
        super(props)
        this.state = {
            latitude: '',
            longitude: '',
            error: ''
        }
    }

    // navigateToAuth() {
    //     this.props.navigation.navigate("lo")
    // }

    // navigateToHome() {
    //     this.props.navigation.navigate("ho")
    // }



    componentDidMount() {
       this.requestSimPermission()
        // if (this.props.token) {
        //     this.props.navigation.navigate("ho")
        // } else {
        //     this.props.navigation.navigate("lo")
        // }
        // console.log(this.props.token)
        // setTimeout(()=>{ this.props.navigation.navigate("login")},1000)
        // Alert.alert(VersionNumber.appVersion)

        this.checkVersion()
        // this.requestCameraPermission()
        // this.fetchingDetailCheckSim()

    }

    fetchingDetailCheckSim = async () => {
		try {
			const response = await axios.get(`${GLOBAL.apiUrl}user/detail`, {
				headers: {
					'Authorization': `Bearer ${this.props.token}`
				}
			})
			if (response.data.success == true) {
                // Alert.alert(response.data.detail.ukomPhone)
				// this.setState({
				// 	//detail: response.data.detail,
				// 	namalengkap: response.data.detail.ukomName,
				// 	email: response.data.detail.ukomEmail,
				// 	organisasi: response.data.detail.ukomKomtId,
				// 	handphone: response.data.detail.ukomPhone,
				// 	namaorganisasi: response.data.detail.ukomKomtId,
				// })
				// console.log(response.data.detail)
			} else {
				// console.log(response)
			}
		}
		catch (e) {
			// console.log(e)
		}
	}

    requestCameraPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Tempo Witness Location Permission',
                    message:
                        'Tempo Witness access to your locations ' +
                        'so you can send location.',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use the GPS');
                this.getLotLang()
            } else {
                console.log('GPS permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }

    requestSimPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
                {
                    title: 'Tempo Witness Location Permission',
                    message:
                        'Tempo Witness access to your locations ' +
                        'so you can send location.',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use the Sim');
                console.log(RNSimData.getSimInfo())
            } else {
                console.log('GPS permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }

    /*
    
    {
        carrierName0: "XL Axiata"
        countryCode0: "id"
        deviceId0: "868698040227714"
        displayName0: "SIM1"
        getConstants: ƒ ()
        isDataRoaming0: false
        isNetworkRoaming0: false
        mcc0: 510
        mnc0: 11
        phoneNumber0: "083807345096"
        simSerialNumber0: "8962111432101332199F"
        simSlotIndex0: 0
        subscriptionId0: 1
    }

    */


    getLotLang = async () => {
        try {
            // const tesloc = await navigator.geolocation.getCurrentPosition((position) => {
                const tesloc = Geolocation.getCurrentPosition((position) => {
                this.props.save_latitude(position.coords.latitude)
                this.props.save_longitude(position.coords.longitude)
                // console.log('lat = '+position.coords.latitude)
                // console.log('long = '+position.coords.longitude)
                // this.setState({position: {longitude: position.longitude, latitude: position.latitude}});
            }, (error) => {
                // alert(JSON.stringify('Gagal Access Lokasi'))
                // this.props.navigation.navigate('app')
            }, {
                    enableHighAccuracy: false,
                    timeout: 65000,
                    // maximumAge: 1000
                });
            // console.log(tesloc)
            // this.props.navigation.navigate('app')
        }
        catch (err) {
            // this.props.navigation.navigate('app')
            console.log(err);
        }
    }


    setItem = (url) => {
        Alert.alert(
            'Perhatian !',
            'Update Versi Terbaru ?',
            [
                { text: 'Update', onPress: () => this.openLinking(url) }
            ] //, { cancelable: false }
        )
    }


    openLinking = (url) => {
        Linking.openURL(url)
        //this.props.navigation.navigate('addSchedule', { pasienId: this.props.navigation.getParam('idd') })
    }


    /*
    
                      if (parseInt(loginResponse.data.eposko) > 0) {
                            this.props.navigation.navigate('home3')
                        }
                        else if (parseInt(loginResponse.data.jabatan) > 0) {
                            this.props.navigation.navigate('home')
                        }
                        else if (loginResponse.data.fasilitator !== null) {
                            this.props.navigation.navigate('home3')
                        }
                        else {
                            this.props.navigation.navigate('home2')
                        }
    
    */


    checkVersion = async () => {
        try {
            this.requestCameraPermission()
            //     const versi = VersionNumber.appVersion
            //     const response = await axios.get(`http://cms.deltaone.id/welcome`)
            //     if (response) {
            //         if (versi == response.data.version) { // (versi == response.data.version) //versi == "1.0.5"
            //             // if (this.props.fasilitator == null && parseInt(this.props.jabatan) == 0 && parseInt(this.props.eposko) == 0) {
            //             //     this.props.navigation.navigate('home2')
            //             // } else if (parseInt(this.props.jabatan) > 0) {
            //             //     this.props.navigation.navigate('home')
            //             // } else if (parseInt(this.props.eposko) > 0) {
            //             //     this.props.navigation.navigate('home3')
            //             // } else {
            //             //     this.props.navigation.navigate('login')
            //             // }
            //             if (this.props.token == '') {
            //                 this.props.navigation.navigate('login')
            //             } else {
            //                 if (parseInt(this.props.eposko) > 0) {
            //                     this.props.navigation.navigate('home')
            //                 }
            //                 else if (parseInt(this.props.jabatan) > 0) {
            //                     this.props.navigation.navigate('home')
            //                 }
            //                 else if (this.props.fasilitator !== null) {
            //                     this.props.navigation.navigate('home')
            //                 }
            //                 else {
            //                     this.props.navigation.navigate('home')
            //                 }
            //             }


            //         } else {
            //             this.setItem(response.data.url)
            //             // console.log(response.data.version)
            //             //console.log(versi)
            //             // this.props.navigation.navigate('login')
            //             // if (this.props.fasilitator == null && parseInt(this.props.jabatan) == 0 && parseInt(this.props.eposko) == 0) {
            //             //     this.props.navigation.navigate('home2')
            //             // } else if (parseInt(this.props.jabatan) > 0) {
            //             //     this.props.navigation.navigate('home')
            //             // } else if (parseInt(this.props.eposko) > 0) {
            //             //     this.props.navigation.navigate('home3')
            //             // } else {
            //             //     this.props.navigation.navigate('login')
            //             // }

            //             if (this.props.token == '') {
            //                 this.props.navigation.navigate('login')
            //             } else {
            //                 if (parseInt(this.props.eposko) > 0) {
            //                     this.props.navigation.navigate('home')
            //                 }
            //                 else if (parseInt(this.props.jabatan) > 0) {
            //                     this.props.navigation.navigate('home')
            //                 }
            //                 else if (this.props.fasilitator !== null) {
            //                     this.props.navigation.navigate('home')
            //                 }
            //                 else {
            //                     this.props.navigation.navigate('home')
            //                 }
            //             }
            //         }
            //     } else {
            //         // console.log(response)
            //         //this.props.navigation.navigate('login')
            //         // if (this.props.fasilitator == null && parseInt(this.props.jabatan) == 0 && parseInt(this.props.eposko) == 0) {
            //         //     this.props.navigation.navigate('home2')
            //         // } else if (parseInt(this.props.jabatan) > 0) {
            //         //     this.props.navigation.navigate('home')
            //         // } else if (parseInt(this.props.eposko) > 0) {
            //         //     this.props.navigation.navigate('home3')
            //         // } else {
            //         //     this.props.navigation.navigate('login')
            //         // }
            //         if (this.props.token == '') {
            //             this.props.navigation.navigate('login')
            //         } else {
            //             if (parseInt(this.props.eposko) > 0) {
            //                 this.props.navigation.navigate('home')
            //             }
            //             else if (parseInt(this.props.jabatan) > 0) {
            //                 this.props.navigation.navigate('home')
            //             }
            //             else if (this.props.fasilitator !== null) {
            //                 this.props.navigation.navigate('home')
            //             }
            //             else {
            //                 this.props.navigation.navigate('home')
            //             }
            //         }

            //     }
            if (this.props.token == '') {
                this.props.navigation.navigate('lo')
            } else {
                this.props.navigation.navigate('app')
            }
        }
        catch (e) {
            this.props.navigation.navigate('lo')
            // console.log(e)
        }
    }


    render() {
        return (
            <Container style={styles.container}>
                <View>
                    {/* <Text>{this.props.token}</Text> */}
                    {/* <Image
                        source={require('../assets/logo.png')}

                        style={{ width: 188, height: 170, alignSelf: 'center' }} /> */}
                    <ActivityIndicator size="large" color={GLOBAL.mainColor} />
                </View>
            </Container>
        )
    }
}
// <Container style={[styles.container, { backgroundColor: GLOBAL.mainColor }]}>

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    }
})

const mapStateToProps = (state) => {
    const { relawanid, token, fasilitator, jabatan, eposko } = state.anggota;
    return { relawanid, token, fasilitator, jabatan, eposko };
};

export default connect(mapStateToProps, { save_latitude, save_longitude })(Splash);