import React, { Component } from "react";
import Modal from "react-native-modalbox";
import Dimensions from "Dimensions";
import {
    StyleSheet,
    View,
    Text,
    FlatList,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Platform,
    Alert, ScrollView,
    StatusBar, Image, Picker, Linking, LayoutAnimation, ActivityIndicator, RefreshControl
} from "react-native";
import {
    Container,
    Header,
    Body,
    Content,
    Left,
    Title,
    Thumbnail,
    Col,
    Row,
    Grid,
    Icon,
    Spinner,
    Fab,
    Button,
    Footer,
    Input,
    Right
} from "native-base";
import { connect } from "react-redux";
// import Image from 'react-native-remote-svg';
import OneSignal from 'react-native-onesignal';
import { broadcastcount, savelastidbroadcast, clearbroadcastcount } from '../rdx/actions';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
// import ScrollableTabView, {
//     ScrollableTabBar
// } from "react-native-scrollable-tab-view";
import AutoHeightImage from 'react-native-auto-height-image';
import { withNavigationFocus } from 'react-navigation';
import Hyperlink from 'react-native-hyperlink';
// import YouTube, { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';
import VideoPlayer from './VideoPlayer';
import { Thumbnail as Thm } from 'react-native-thumbnail-video';
const { width, height } = Dimensions.get('window')



class Homescreenbroadcast extends Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            active: false,
            newTweetContent: "",
            isModalOpen: false,
            isModalOpen2: false,
            daftarevent: [
                {
                    "eventId": 0,
                    "eventName": "Belum ada data",
                    "kabupaten": null,
                    "kecamatan": null,
                    "type": "",
                    "usulan": "",
                    "isPhotoExist": "",
                    "eventPhoto": "",
                    "createDate": "",
                    "commentCount": "",
                    "likeCount": "",
                    "youlike": "",
                    "relawan": "",
                    "comments": []
                }
            ],
            eventname: '',
            imgforupload: '',
            typeevent: '',
            osid: '',
            page: 1,
            loadingMore: false,
            alleventforconcat: [],
            kabupaten: '',
            kecamatan: '',
            usulan: '',
            kabupatenlist: [],
            kecamatanlist: [],
            letsPlay: ''
        };
        this._scrollOffset = 0
    }

    componentDidMount() {
        // this.fetchingEvent()
        // this.handleCountBroadcast()
        // this.fetchingDetail()
        // this.fetchDetailKabupaten()
        // this.setState({ letsPlay: '' })

    }


    // componentDidUpdate() {
    //     this.fetchingEvent()
    // }

    // setEmailOneSignal = () => {
    //     OneSignal.init("6169230c-f70c-4ecc-88f7-0828c79945ff", { kOSSettingsKeyAutoPrompt: true });
    //     OneSignal.setEmail(this.props.email, (error) => {
    //         //console.log("Sent email with error: ", error);
    //     });
    //     OneSignal.getPermissionSubscriptionState((subscriptionState) => {
    //         this.setState({ osid: subscriptionState.userId }, this.saveOneSignalId)
    //     })
    // }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.isFocused) {
            this.props.clearbroadcastcount()
            this.handleSaveLastId()
        }
        return true
    }


    saveOneSignalId = () => {
        let data = JSON.stringify({
            osid: this.state.osid
        })
        axios.post(`${GLOBAL.apiUrl}relawan/updateonesignal`, data, {
            headers: {
                'Authorization': `Bearer ${this.props.token}`,
                'Content-Type': 'application/json',
            }
        }).then((res) => {
            // console.log(res)
        }).catch((e) => {
            // console.log(e)
        })
    }


    fetchingDetail = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}relawan/detail`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })
            if (response.data.success == true) {
                this.setState({
                    kabupaten: response.data.detail.kabupaten,
                    kecamatan: response.data.detail.kecamatan
                })
                if (response.data.detail.osid == '' || response.data.detail.osid == null) {
                    // console.log(response)
                    this.setEmailOneSignal()

                } else {
                    // console.log('tidak dijalankan coy')
                    // console.log(response)
                }
            } else {
                Alert.alert('response failed')
            }
        }
        catch (e) {
            //  Alert.alert('Kesalahan Server Detail')
            // console.log(e)
        }
    }

    onValueChange = async  value => {
        await this.setState({ kabupaten: value })
        await this.fetchDetailKecamatan(this.state.kabupaten)
    }


    fetchDetailKabupaten = async () => {
        try {
            const response = await axios.get(`http://cms.deltaone.id/data_kecamatan/api_kabupaten`)
            if (response) {
                this.setState({
                    kabupatenlist: response.data
                })
                // console.log(response)
                await this.fetchDetailKecamatan(this.state.kabupaten)
            } else {
                // console.log(response)
            }
            //console.log(response)
        }
        catch (e) {
            // console.log(e)
        }
    }



    fetchDetailKecamatan = async (id) => {
        try {
            const response = await axios.get(`http://cms.deltaone.id/data_kecamatan/api/${id}`)
            if (response) {
                this.setState({
                    kecamatanlist: response.data
                })
            } else {
                // console.log(response)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }


    fetchingEvent = async () => {
        try {
            const response = await axios.get(`${GLOBAL.apiUrl}relawan/getbroadcastbyrule`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })
            if (response.data.success == true) {
                this.setState({
                    daftarevent: response.data.data
                })

                console.log(response)
            } else {
                console.log(response)
            }
        }
        catch (e) {
            // console.log(e)
        }
    }

    handleCountBroadcast = async () => {
        try {
            await this.props.broadcastcount(this.props.token, this.props.lastidbroadcast)
            //  await this.fetchingEvent()
        } catch (e) { }
    }


    handleSaveLastId = () => {
        this.props.savelastidbroadcast(this.state.daftarevent[0].eventId)
    }


    handleSendEvent = async () => {
        try {
            let data = JSON.stringify({
                eventName: this.state.eventname,
                type: this.state.typeevent,
                relawanId: this.props.relawanid,
                kabupaten: this.state.kabupaten,
                kecamatan: this.state.kecamatan,
                usulan: this.state.usulan

            })
            if (this.state.eventname !== '') {
                let loginResponse = await axios.post(`${GLOBAL.apiUrl}event/sendevent`, data, {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
                if (loginResponse.data.success == true) {
                    this.fetchingEvent()
                    // console.log(loginResponse)
                    this.setState({ eventname: '' })
                    this.closeModal()
                    this.uplod(this.state.imgforupload, loginResponse.data.idhasinput)
                    // this.component._root.scrollToEnd()
                } else {
                    Alert.alert('Kirim komentar Gagal / Anda Belum Login')
                    // console.log(loginResponse)
                }
            } else {
                Alert.alert('A mensagem não pode estar vazia')
            }
        } catch (e) {
            // console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }
    }


    handleSendEventBroadcast = async () => {
        try {
            let data = JSON.stringify({
                eventName: this.state.eventname,
                type: this.state.typeevent,
                relawanId: this.props.relawanid,
                kabupaten: this.state.kabupaten,
                kecamatan: this.state.kecamatan,
                usulan: this.state.usulan

            })
            if (this.state.eventname !== '') {
                let loginResponse = await axios.post(`${GLOBAL.apiUrl}event/sendevent`, data, {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
                if (loginResponse.data.success == true) {
                    this.fetchingEvent()
                    // console.log(loginResponse)
                    this.setState({ eventname: '' })
                    this.closeModal()
                    this.uplod(this.state.imgforupload, loginResponse.data.idhasinput)
                    // this.component._root.scrollToEnd()
                } else {
                    Alert.alert('Kirim komentar Gagal / Anda Belum Login')
                    // console.log(loginResponse)
                }
            } else {
                Alert.alert('A mensagem não pode estar vazia')
            }
        } catch (e) {
            // console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }
    }


    handleSendLike = async (ei) => {
        // Alert.alert(ei)
        try {

            let data = JSON.stringify({
                eventid: ei
            })

            let loginResponse = await axios.post(`${GLOBAL.apiUrl}relawan/sendlike`, data, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`,
                    'Content-Type': 'application/json',
                }
            })
            if (loginResponse.data.success == true) {
                const myArray = this.state.daftarevent
                objIndex = myArray.findIndex((obj => obj.eventId == ei));
                myArray[objIndex].youlike = !myArray[objIndex].youlike
                myArray[objIndex].likeCount = myArray[objIndex].youlike ? myArray[objIndex].likeCount + 1 : myArray[objIndex].likeCount - 1
                this.setState({ daftarevent: myArray })
                // console.log(loginResponse)
            } else {
                Alert.alert('Like gagal / Anda Belum Login/ anda sudah like laporan ini')
                // console.log(loginResponse)
            }

        } catch (e) {
            // console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }
    }

    handleUploadImage = () => {
        const options = {
            title: 'Foto de no máximo 4MB ',
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {

            }
            else if (response.error) {

            } else if (response.fileSize > 10000000) {
                Alert.alert('arquivo de foto é muito grande')
            }
            else {
                // console.log(response)
                // this.uplod(response)
                this.setState({ imgforupload: response })
            }
        });
    }


    uplod = (datanya, ei) => {
        // console.log(datanya)
        RNFetchBlob.fetch('POST', `${GLOBAL.apiUrl}event/imgnext`, {
            Accept: 'application/json',
            'Authorization': `${ei}`,
            'Content-Type': 'multipart/form-data',
        }, [
                {
                    name: 'pict',
                    filename: datanya.fileName,
                    type: datanya.type,
                    data: datanya.data
                }
            ]
        ).then((resp) => {
            let rspUpload = JSON.parse(resp.data);
            if (rspUpload.success == true) {
                this.fetchingEvent()
                this.setState({ eventname: '' })
            } else {
                Alert.alert('Falha ao atualizar a foto')
            }
        }).catch((err) => {
            // console.log(JSON.parse(err))
            // Alert.alert('Kesalahan Server')
        })
    }

    openModal = () => {
        this.setState({ isModalOpen: !this.state.isModalOpen })
    }
    openModaldua = () => {
        this.setState({ isModalOpen2: !this.state.isModalOpen2 })
    }

    closeModal = () => {
        this.setState({ isModalOpen: false, isModalOpen2: false })
    }

    openLinking = () => {
        Linking.openURL(`tel:021${800800}`)
    }




    handleScroll = (event) => {
        const CustomLayoutLinear = {
            duration: 100,
            create: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity },
            update: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity },
            delete: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity }
        }

        const currentOffset = event.nativeEvent.contentOffset.y
        let shouldShow

        if (currentOffset > this._scrollOffset || currentOffset === 0) {//0
            // scroll down or on top of ScrollView
            shouldShow = false
            if (this.isCloseToBottom(event.nativeEvent.layoutMeasurement, event.nativeEvent.contentOffset, event.nativeEvent.contentSize)) {
                this.setState({ page: this.state.page + 1 }, this.handleConcat)
                //Alert.alert('sampai bawah')
                //this.handleConcat()
            }
        } else {
            // scroll up
            shouldShow = true
        }

        if (shouldShow !== this.state.shouldShow && (!shouldShow || currentOffset > 250)) {//250
            LayoutAnimation.configureNext(CustomLayoutLinear)
            this._scrollOffset = currentOffset
            this.setState({ shouldShow })
        }

        this._scrollOffset = currentOffset
    }

    handleConcat = async () => {
        try {
            this.setState({ loadingMore: true })
            const response = await axios.get(`${GLOBAL.apiUrl}relawan/getbroadcastbyrule?p=${this.state.page}`, {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            })
            if (response.data.success == true) {
                await this.setState({
                    alleventforconcat: response.data.data,
                })
                await this.setState({
                    daftarevent: this.state.daftarevent.concat(this.state.alleventforconcat),

                })
                this.setState({ loadingMore: false })

                // console.log(response)
            } else {
                // console.log(response)
                this.setState({ loadingMore: false })
            }
        }
        catch (e) {
            // console.log(e)
            this.setState({ loadingMore: false })
        }
    }

    isCloseToBottom = (layoutMeasurement, contentOffset, contentSize) => {
        const paddingToBottom = 10;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };

    _onRefresh = async () => {
        try {
            this.setState({ refreshing: true });
            await this.fetchingEvent()
            await this.setState({ refreshing: false, page: 1 });
        } catch (e) { }

    }


    openLinking2 = async (ur) => {
        let cek = await this.isYoutubeUrl(ur)
        if (cek == true) {
            let vId = await this.getId(ur)
            Alert.alert(
                'Perhatian',
                'Lihat Video ?',
                [
                    { text: 'Cancel', onPress: () => console.log('no') },
                    { text: 'Ok', onPress: () => Linking.openURL(ur) }
                ] //, { cancelable: false }this.props.navigation.navigate('videoPlayer', { vidId: vId })
            )
        } else {
            Alert.alert(
                'Perhatian',
                'Kunjungi Link ?',
                [
                    { text: 'Cancel', onPress: () => console.log('no') },
                    { text: 'Ok', onPress: () => Linking.openURL(ur) }
                ] //, { cancelable: false }
            )
        }


    }

    isYoutubeUrl = (url) => {
        //var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/
        var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        if (url.match(p)) {
            return true;
        }
        return false;
    }



    getId = (url) => {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }


    /*
    var videoId = getId('http://www.youtube.com/watch?v=zbYf5_S7oJo');
    
    var iframeMarkup = '<iframe width="560" height="315" src="//www.youtube.com/embed/' 
        + videoId + '" frameborder="0" allowfullscreen></iframe>';

    */

    handleIconYoutubeClick = (pa) => {
        if (pa == 'paused') {
            this.setState({ letsPlay: '' })
        }
    }


    render() {
        if (this.props.tweetPosted === "success") {
            this.closeModal();
        }
        return (
            <Container>
                {/* <StatusBar
                    backgroundColor='transparent'

                /> */}
                {/* <Header style={styles.topMargin}>
                    <Left>
                        <Thumbnail small source={{ uri: 'http://relawan-cms.hahabid.com/assets/uploads/profile/noimage.png' }} />
                    </Left>
                    <Body>
                        <Title style={{ color: "#121212" }}>Home</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={this.openModal}>
                            <Icon name="md-create" style={{ color: "#4286f4" }} />
                        </Button>
                    </Right>
                </Header> */}

                <Content
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                    onScroll={this.handleScroll} style={{ backgroundColor: "white" }}>
                    <View contentContainerStyle={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center"
                    }}>
                    </View>
                    {this.state.daftarevent.map((res, i) => {

                        // var jabatan = ''
                        // if (res.relawan.eposko == '0' && res.relawan.jabatan == '0' && (res.relawan.fasilitator == null || res.relawan.fasilitator !== null)) {
                        //     jabatan = 'Relawan'
                        // } else if (res.relawan.eposko == '0' && res.relawan.jabatan == '1' && res.relawan.fasilitator == null) {
                        //     jabatan = 'Koordinator'
                        // }
                        // else if (res.relawan.eposko == '1' && res.relawan.jabatan == '0' && res.relawan.fasilitator == null) {
                        //     jabatan = 'Eposko'
                        // }


                        return (
                            <View key={i} style={{ justifyContent: "flex-start" }}>
                                <View style={styles.tweet}>
                                    <TouchableHighlight
                                        underlayColor="white"
                                        activeOpacity={0.75}>
                                        <View style={{ flex: 1, flexDirection: "row" }}>
                                            <Thumbnail source={{ uri: res.relawan.photoRelawan }} />
                                            <View style={{
                                                flexDirection: "column",
                                                justifyContent: "flex-start"
                                            }}>
                                                <Text style={{
                                                    paddingLeft: 15,
                                                    fontWeight: "bold",
                                                    fontSize: 20,
                                                    color: 'black'
                                                }}>
                                                    {res.relawan.namaJabatan}

                                                </Text>

                                                <Text style={{
                                                    paddingLeft: 15,
                                                    color: "#aaa",
                                                    fontSize: 16
                                                }}>
                                                    {res.createDate}
                                                </Text>

                                                <Text style={{
                                                    paddingLeft: 15,
                                                    color: "#aaa",
                                                    fontSize: 16
                                                }}>
                                                    {/* {`${res.kecamatan} , ${res.kabupaten}`} */}

                                                    {res.kecamatan == 0 ? ' Semua Kecamatan ' : res.kecamatan}
                                                    {res.eventId > 0 ? ` , ` : null}
                                                    {res.kabupaten == 0 ? ' Semua Kabupaten ' : res.kabupaten}


                                                </Text>
                                            </View>

                                        </View>
                                    </TouchableHighlight>

                                    {
                                        res.isPhotoExist == null || res.isPhotoExist == '' ? null :
                                            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('imageviewer', { imglempar: res.eventPhoto })}>
                                                <View style={{ minHeight: 250, paddingBottom: 10, marginRight: 5 }}>
                                                    {/* <Image source={{ uri: res.eventPhoto }} resizeMode="contain" style={{ flex: 1 }} /> */}
                                                    <AutoHeightImage
                                                        style={{}}
                                                        width={width}
                                                        source={{ uri: res.eventPhoto }}
                                                    />
                                                </View>
                                            </TouchableWithoutFeedback>
                                    }

                                    {
                                        (this.isYoutubeUrl(res.eventName) && this.state.letsPlay !== i) ? <Thm
                                            imageHeight={250}
                                            onPress={() => this.setState({ letsPlay: i })}
                                            url={`${res.eventName}`} />
                                            :
                                            (this.isYoutubeUrl(res.eventName) == false && this.state.letsPlay !== i) ? null
                                                : <VideoPlayer onPress={(e) => this.handleIconYoutubeClick(e.state)} url={this.getId(res.eventName)} />
                                    }


                                    {res.eventId > 0 ? <View style={{
                                        flexDirection: "column",
                                        justifyContent: "flex-start"
                                    }}>
                                        <Text style={{
                                            paddingLeft: 15,
                                            fontSize: 16,
                                            fontWeight: 'bold',
                                            color: 'black'
                                        }}>
                                            Arahan :

                                                </Text>
                                        <Hyperlink
                                            linkStyle={{ color: '#2980b9' }}
                                            onPress={(url, text) => this.openLinking2(url)}>
                                            <Text
                                                selectable={true}
                                                style={{
                                                    paddingLeft: 25,
                                                    color: "#aaa",
                                                    fontSize: 16
                                                }}>
                                                {res.eventName}
                                            </Text>
                                        </Hyperlink>

                                    </View> : null}



                                    <Text style={styles.tweetText}>{}</Text>

                                </View>
                            </View>
                        )
                    })}
                    {this.state.loadingMore ? <ActivityIndicator size="large" color={GLOBAL.mainColor}
                        style={{ marginBottom: 30, marginTop: 20 }} /> : <View
                            style={{
                                alignSelf: 'center', width: width * 0.5,
                                marginTop: 30, marginBottom: 30, borderBottomColor: GLOBAL.mainColor,
                                borderBottomWidth: 3
                            }}></View>}
                    <View style={{ height: 100 }} />
                </Content>

                <Modal
                    ref={"newTweetModal"}
                    backdrop={true}
                    style={styles.modal}
                    isOpen={this.state.isModalOpen}
                    onClosed={this.closeModal.bind(this)}>
                    <ScrollView>


                        <View
                            style={{
                                alignSelf: "flex-start",
                                alignItems: "center",
                                flexDirection: "row",
                                padding: 5,
                                paddingRight: 10
                            }}>
                            <Button transparent onPress={this.closeModal.bind(this)}>
                                <Icon name="close" style={{ color: "black", fontSize: 32 }} />
                            </Button>
                            <View style={{ flex: 1 }} />
                            <Thumbnail
                                small
                                source={{
                                    uri:
                                        "https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg"
                                }} />
                        </View>
                        <View
                            style={{
                                flex: 1,
                                justifyContent: "flex-start",
                                alignItems: "flex-start",
                                width: "100%",

                            }} >
                            <Picker
                                selectedValue={this.state.typeevent}
                                style={{ height: 60, width: width * 0.969 }}
                                onValueChange={(itemValue, itemIndex) => this.setState({ typeevent: itemValue })}>
                                {/* <Picker.Item label="Alat Kampanye" value="Alat Kampanye" />
                            <Picker.Item label="Usulan" value="Usulan" />
                            <Picker.Item label="Isu" value="Isu" />
                            <Picker.Item label=" - Positive" value="Positive" />
                            <Picker.Item label=" - Negative" value="Negative" />
                            <Picker.Item label="Canvasing" value="Canvasing" /> */}
                                <Picker.Item label=" - Target" value="3" />
                                <Picker.Item label=" - Kendala" value="4" />

                            </Picker>
                            <View style={styles.inputContainer}>
                                {/* <View><Text>Kabupaten : </Text></View> */}
                                <Picker
                                    selectedValue={this.state.kabupaten}
                                    style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                    onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue)}>
                                    <Picker.Item label="Pilih Kabupaten" value="0" />
                                    {this.state.kabupatenlist.map((res, i) => (
                                        <Picker.Item key={i} label={res.msrgName} value={res.msrgId} />
                                    ))}
                                </Picker>
                            </View>
                            <View style={styles.inputContainer}>
                                {/* <View><Text>Kecamatan : </Text></View> */}
                                <Picker
                                    selectedValue={this.state.kecamatan}
                                    style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ kecamatan: itemValue })}>
                                    <Picker.Item label="Pilih Kecamatan" value="0" />
                                    {this.state.kecamatanlist.map((res, i) => (
                                        <Picker.Item key={i} label={res.msdiName} value={res.msdiId} />
                                    ))}
                                </Picker>
                            </View>
                            <Input
                                style={{
                                    flex: 1,
                                    width: "100%",
                                    fontSize: 24,
                                    alignContent: "flex-start",
                                    justifyContent: "flex-start",
                                    textAlignVertical: "top",
                                    margin: 5,

                                }}
                                multiline
                                placeholder="Laporan... "
                                onChangeText={eventname => this.setState({ eventname })} />

                            <Input
                                style={{
                                    flex: 1,
                                    width: "100%",
                                    fontSize: 24,
                                    alignContent: "flex-start",
                                    justifyContent: "flex-start",
                                    textAlignVertical: "top",
                                    margin: 5,

                                }}
                                multiline
                                placeholder="Usulan... "
                                onChangeText={usulan => this.setState({ usulan })} />
                        </View>
                        <View style={styles.modalFooter}>
                            <Button transparent small onPress={this.handleUploadImage}>
                                <Icon name="ios-image" />
                            </Button>
                            <Button transparent small onPress={this.handleUploadImage}>
                                <Text>Attachment Photo</Text>
                            </Button>


                            <View style={{ flex: 1 }} />

                            <Button
                                onPress={this.handleSendEvent}
                                rounded
                                style={{ color: "#4286f4", height: 40, width: 94, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: "white" }}>Enviar</Text>
                            </Button>
                        </View>
                    </ScrollView>
                </Modal>

                {
                    //===========================================================================
                }
                <Modal
                    ref={"newTweetModal2"}
                    backdrop={true}
                    style={styles.modal}
                    isOpen={this.state.isModalOpen2}
                    onClosed={this.closeModal.bind(this)}>
                    <ScrollView>
                        <View
                            style={{
                                alignSelf: "flex-start",
                                alignItems: "center",
                                flexDirection: "row",
                                padding: 5,
                                paddingRight: 10
                            }}>
                            <Button transparent onPress={this.closeModal.bind(this)}>
                                <Icon name="close" style={{ color: "black", fontSize: 32 }} />
                            </Button>
                            <View style={{ flex: 1 }} />
                            <Thumbnail
                                small
                                source={{
                                    uri:
                                        "https://i1.wallpaperscraft.ru/image/betmen_art_minimalizm_107658_300x240.jpg"
                                }} />
                        </View>
                        <View
                            style={{
                                flex: 1,
                                justifyContent: "flex-start",
                                alignItems: "flex-start",
                                width: "100%",

                            }} >
                            <Picker
                                selectedValue={this.state.typeevent}
                                style={{ height: 60, width: width * 0.969 }}
                                onValueChange={(itemValue, itemIndex) => this.setState({ typeevent: itemValue })}>
                                {/* <Picker.Item label="Alat Kampanye" value="Alat Kampanye" />
                            <Picker.Item label="Usulan" value="Usulan" />
                            <Picker.Item label="Isu" value="Isu" /> */}
                                <Picker.Item label=" - Positive" value="1" />
                                <Picker.Item label=" - Negative" value="2" />
                                {/* <Picker.Item label="Canvasing" value="Canvasing" />
                            <Picker.Item label=" - Target" value="Target" />
                            <Picker.Item label=" - Kendala" value="Kendala" /> */}

                            </Picker>
                            <View style={styles.inputContainer}>
                                {/* <View><Text>Kabupaten : </Text></View> */}
                                <Picker
                                    selectedValue={this.state.kabupaten}
                                    style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                    onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue)}>
                                    <Picker.Item label="Pilih Kabupaten" value="0" />
                                    {this.state.kabupatenlist.map((res, i) => (
                                        <Picker.Item key={i} label={res.msrgName} value={res.msrgId} />
                                    ))}
                                </Picker>
                            </View>
                            <View style={styles.inputContainer}>
                                {/* <View><Text>Kecamatan : </Text></View> */}
                                <Picker
                                    selectedValue={this.state.kecamatan}
                                    style={{ width: width * 0.95, marginLeft: 10, color: 'grey' }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ kecamatan: itemValue })}>
                                    <Picker.Item label="Pilih Kecamatan" value="0" />
                                    {this.state.kecamatanlist.map((res, i) => (
                                        <Picker.Item key={i} label={res.msdiName} value={res.msdiId} />
                                    ))}
                                </Picker>
                            </View>
                            <Input
                                style={{
                                    flex: 1,
                                    width: "100%",
                                    fontSize: 24,
                                    alignContent: "flex-start",
                                    justifyContent: "flex-start",
                                    textAlignVertical: "top",
                                    margin: 5,

                                }}
                                multiline
                                placeholder="Laporan... "
                                onChangeText={eventname => this.setState({ eventname })} />
                            <Input
                                style={{
                                    flex: 1,
                                    width: "100%",
                                    fontSize: 24,
                                    alignContent: "flex-start",
                                    justifyContent: "flex-start",
                                    textAlignVertical: "top",
                                    margin: 5,

                                }}
                                multiline
                                placeholder="Usulan... "
                                onChangeText={usulan => this.setState({ usulan })} />
                        </View>
                        <View style={styles.modalFooter}>
                            <Button transparent small onPress={this.handleUploadImage}>
                                <Icon name="ios-image" />
                            </Button>
                            <Button transparent small onPress={this.handleUploadImage}>
                                <Text>Attachment Photo</Text>
                            </Button>


                            <View style={{ flex: 1 }} />

                            <Button
                                onPress={this.handleSendEvent}
                                rounded
                                style={{ color: "#4286f4", height: 40, width: 94, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: "white" }}>Enviar</Text>
                            </Button>
                        </View>
                    </ScrollView>
                </Modal>






                {
                    // this.state.isModalOpen == true || this.state.isModalOpen2 == true ? null :
                    //     <Fab
                    //         active={this.state.active}
                    //         direction="up"
                    //         containerStyle={{}}
                    //         style={{ backgroundColor: '#5067FF' }}
                    //         position="bottomRight"
                    //         onPress={() => this.setState({ active: !this.state.active })}>
                    //         <Icon name="md-create" />
                    //         <Button style={{ backgroundColor: '#3B5998' }} onPress={this.openModal}>
                    //             {/* <Icon name="logo-facebook" /> */}
                    //             <Text style={{ color: "white" }}>Can</Text>
                    //         </Button>
                    //         <Button style={{ backgroundColor: '#DD5144' }} onPress={this.openModaldua}>
                    //             {/* <Icon name="logo-facebook" /> */}
                    //             <Text style={{ color: "white" }}>Isu</Text>
                    //         </Button>
                    //     </Fab>
                    // <Fab
                    //     position="bottomRight"
                    //     style={{ backgroundColor: "#4286f4" }}
                    //     onPress={this.openModal}
                    //     ref={"FAB"}
                    // >
                    //     <Icon name="md-create" />
                    // </Fab>
                }

            </Container>
        );
    }
}


const styles = StyleSheet.create({
    topMargin: {
        backgroundColor: "white",
        zIndex: -1,
        borderBottomColor: 'grey',
        borderBottomWidth: 0.4
    },
    content: {
        padding: 10,
        backgroundColor: "white"
    },
    heading: {
        fontSize: 32,
        fontWeight: "400",
        marginBottom: 30
    },
    tweet: {
        paddingTop: 20,
        paddingBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        borderBottomColor: "#bbb",
        borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection: "column"
    },
    tweetText: {
        marginTop: 10,
        fontSize: 18,
        color: "#555"
    },
    tweetFooter: {
        flexDirection: "row",
        // justifyContent: "flex-start",
        justifyContent: "space-around",
        padding: 0
    },
    badgeCount: {
        fontSize: 12,
        paddingLeft: 5
    },
    footerIcons: {
        flexDirection: "row",
        alignItems: "center"//center
    },
    modalFooter: {
        backgroundColor: "white",
        elevation: 3,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0.2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        height: 54,
        width: "100%",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        padding: 5
    },
    modal: {
        justifyContent: "flex-start",
        alignItems: "center",
        position: "absolute",
        zIndex: 4,
        elevation: 4,
        // height: Dimensions.get("window").height //- Expo.Constants.statusBarHeight,
        // marginTop: Expo.Constants.statusBarHeight / 2
    }
});

//export default HomeScreen
const mapStateToProps = (state) => {
    const { relawanid, token, email, lastidbroadcast } = state.anggota;
    //const { lastidbroadcast } = state.cartpersist;
    return { relawanid, token, email, lastidbroadcast };
};

export default connect(mapStateToProps, { broadcastcount, savelastidbroadcast, clearbroadcastcount })(withNavigationFocus(Homescreenbroadcast));

        // this.props.savelastidbroadcast(this.state.daftarevent[0].eventId)
        // if (this.state.daftarevent[0].eventId == this.props.lastidbroadcast) {
        //     // this.props.clearbroadcastcount()
        //     console.log('sama')
        // } else {
        //     console.log('tidak sama')
        // await this.props.savelastidbroadcast(this.state.daftarevent[0].eventId)
        // }
        // this.props.savelastidbroadcast(this.state.daftarevent[0].eventId)
        // console.log(this.props.lastidbroadcast)