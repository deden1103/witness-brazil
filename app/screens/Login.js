
import React, { Component } from 'react';
import {
    Button,
    Icon,
    Container,
    Content
} from 'native-base';

import {
    Text,
    View,
    StatusBar,
    Alert,
    TouchableWithoutFeedback,
    ActivityIndicator,
    Dimensions, ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import axios from 'axios';
import Image from 'react-native-remote-svg';
import * as Animatable from 'react-native-animatable';
import { TextField } from 'react-native-material-textfield';
import AwesomeAlert from 'react-native-awesome-alerts';
import Style from '../style/MyStyle';
import { loginsuccess3,loginsuccess4,loginsuccess,loginsuccess7 } from '../rdx/actions';
const { width, height } = Dimensions.get('window')


class Login extends Component {
    static navigationOptions = {
        header: null
    };

    constructor() {
        super()
        this.state = {
            secure: true,
            aniType: 'bounceInDown',
            loading: false,
            emailorphone: '',
            password: '',
            showAlert: false,
            osId: '',

        }

    }

    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };



    handleLogin2 = async () => {
        try {
            this.setState({ loading: true })
            if (this.state.emailorphone !== '' && this.state.password !== '') {
                let data = JSON.stringify({
                    emailorphone: this.state.emailorphone,
                    password: this.state.password
                })
                let loginResponse = await axios.post(`${GLOBAL.apiUrl}authrelawan/login`, data, {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
                if (loginResponse.data.success == true) {
                    this.setState({ loading: false })
                    this.props.loginsuccess3(loginResponse.data.relawanId)
                    this.props.loginsuccess4(loginResponse.data.photoRelawan)
                    this.props.loginsuccess(loginResponse.data.namaRelawan)
                    this.props.loginsuccess7(loginResponse.data.fasilitator)
                    this.props.navigation.navigate('event')
                } else {
                    Alert.alert('Login falhou, e-mail / palavras-passe erradas')
                    this.setState({ loading: false })
                }

            } else {
                this.setState({ showAlert: !this.state.showAlert, loading: false })
            }
        } catch (e) {
            // console.log(e)
            Alert.alert('erro de servidor')
            this.setState({ loading: false })
        }


    }



    handleSee = () => {
        this.setState({ secure: !this.state.secure })
    }



    render() {
        return (
            <Container>
                <Content>
                    <ImageBackground
                        style={{ flex: 1, width, height }}
                        resizeMode='cover'
                        source={require('../assets/bglogin.jpg')}>
                        <View style={styles.container}>
                            {/* <StatusBar
                                translucent
                                backgroundColor='transparent'
                                animated
                            /> */}


                            <Animatable.View
                                animation={this.state.aniType}
                                delay={1200}
                                duration={700}
                                ref={(ref) => { this.animationView = ref; }}
                                style={styles.viewForm}>

                                {/* <Image
                                    source={require('../assets/logo.png')}
                                    style={{ width: 188, height: 170, alignSelf: 'center' }} /> */}



                                <View style={{ marginTop: 20 }}>
                                    <View style={{ flexDirection: 'row', marginBottom: 20 }}>
                                        <View >
                                            <Icon active />
                                            <Icon name='mail' onPress={this.handleSee} style={{ color: 'grey', marginRight: 15 }} />
                                        </View>
                                        <View style={{ width: width * 0.7, marginTop: -30 }}>
                                            <TextField
                                                label='Correio eletrónico'
                                                value={this.state.emailorphone}
                                                onChangeText={(text) => this.setState({ emailorphone: text })}

                                            />
                                        </View>

                                    </View>

                                    <View style={{ flexDirection: 'row' }}>
                                        <View >
                                            <Icon active />
                                            <Icon name='lock' onPress={this.handleSee} style={{ color: 'grey', marginRight: 19 }} />
                                        </View>
                                        <View style={{ width: width * 0.7, marginTop: -30 }}>
                                            <TextField
                                                label='Password'
                                                value={this.state.password}
                                                onChangeText={(password) => this.setState({ password })}
                                                secureTextEntry={this.state.secure}
                                            />
                                        </View>
                                        <View>
                                            <Icon name={this.state.secure ? 'eye' : 'eye-off'} onPress={this.handleSee} />
                                        </View>
                                    </View>
                                </View>


                                {this.state.getError && <Text style={{ color: 'red', alignSelf: 'center', marginTop: 10 }}>Eror</Text>}

                                <Button block style={{ margin: 15, marginTop: 35, backgroundColor: GLOBAL.mainColor }} onPress={this.handleLogin2}>
                                    {this.state.loading == true ? <ActivityIndicator size="large" color="white" /> : <Text style={{ color: 'white' }}>{`Login`}</Text>}
                                </Button>

                                {/* <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('forgotPassword')}>
                                    <View style={styles.forgotPasswordStyleView}>
                                        <Text style={[styles.forgotPasswordStyleText, { color: GLOBAL.mainColor }]}>
                                            Forgot Password
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback> */}

                            </Animatable.View>

                        </View >
                    </ImageBackground>
                </Content>
                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title="Aviso"
                    message="E-mail / Senha Necessário!"
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={false}
                    showConfirmButton={true}
                    cancelText="OK"
                    confirmText="OK"
                    confirmButtonColor="#DD6B55"
                    onCancelPressed={() => {
                        this.hideAlert()
                    }}
                    onConfirmPressed={() => {
                        this.hideAlert()
                    }}
                />
            </Container>

        );
    }
}

const mapStateToProps = (state) => {
    const { count } = state.auth;
    return { count };
};

export default connect(mapStateToProps, { loginsuccess3,loginsuccess4,loginsuccess,loginsuccess7 })(Login);


const styles = {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'

    },
    titleStyle: {
        alignSelf: 'center',
    },

    viewForm: {
        width: Style.CARD_WIDTH
    },

    buttonStyle: {
        alignSelf: 'center',
        width: Style.CARD_WIDTH,
        marginTop: 20
    },

    inputStyle: {
        marginTop: 20
    },

    forgotPasswordStyleView: {
        justifyContent: 'center'
    },
    forgotPasswordStyleText: {
        color: GLOBAL.mainColor,
        alignSelf: 'center'
    },
    noHaveView: {
        marginTop: 20,
        justifyContent: 'center',
        flexDirection: 'row'
    }
}
