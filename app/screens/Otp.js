import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
  Dimensions,
  // Image
  PermissionsAndroid,
} from 'react-native';
// import getTheme from "../native-base-theme/components";
// import platform from "../native-base-theme/variables/platform";
import material from '../native-base-theme/variables/material';
import {NavigationActions} from 'react-navigation';
import {
  Container,
  Button,
  Text,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Title,
  StyleProvider,
  Content,
  Grid,
  Col,
  Row,
  Input,
  Item,
  Form,
  Label,
  Footer,
  FooterTab,
  Spinner,
} from 'native-base';
const {width, height} = Dimensions.get('window');
import {connect} from 'react-redux';
import Image from 'react-native-remote-svg';
import {
  loginsuccess2,
  loginsuccess3,
  loginsuccess4,
  loginsuccess,
  loginsuccess5,
  loginsuccess7,
  loginsuccess8,
  loginsuccess9,
} from '../rdx/actions';
import axios from 'axios';
import AwesomeAlert from 'react-native-awesome-alerts';
import OneSignal from 'react-native-onesignal';
import RNSimData from 'react-native-sim-data';
import KeyEvent from 'react-native-keyevent';
import OtpInputs from 'react-native-otp-inputs';
// import VersionNumber from 'react-native-version-number';
class Loginscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAlert: false,
      emailorphone: '',
      password: '',
      loading: false,
      secure: true,
      iccid: '',
      iccid2: '',
      kodeotp: '',
    };
  }

  /*
      if (loginResponse.data.fasilitator == null && parseInt(loginResponse.data.jabatan) == 0 && parseInt(loginResponse.data.eposko) == 0) {
                            this.props.navigation.navigate('home2')
                        } else if (parseInt(loginResponse.data.jabatan) > 0) {
                            this.props.navigation.navigate('home')
                        } else if (parseInt(loginResponse.data.eposko) > 0) {
                            this.props.navigation.navigate('home3')
                        }
    
    */

  componentDidMount() {
    OneSignal.init('04f752ea-ae43-42a1-8488-75b945eb9447', {
      kOSSettingsKeyAutoPrompt: true,
    });
    // if (this.props.fasilitator == null && parseInt(this.props.jabatan) == 0 && parseInt(this.props.eposko) == 0) {
    //     this.props.navigation.navigate('home2')
    // } else if (parseInt(this.props.jabatan) > 0) {
    //     this.props.navigation.navigate('home')
    // } else if (parseInt(this.props.eposko) > 0) {
    //     this.props.navigation.navigate('home3')
    // }

    this.requestSimPermission();

    // KeyEvent.onKeyDownListener((keyEvent) => {
    //    Alert.alert('down')
    // });
    // KeyEvent.onKeyUpListener((keyEvent) => {
    //     Alert.alert('Up')
    // });
  }

  // componentWillUnmount() {
  //     KeyEvent.removeKeyDownListener();
  //     KeyEvent.removeKeyUpListener();
  //   }

  requestSimPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
        {
          title: 'Tempo Witness Location Permission',
          message:
            'Tempo Witness access to your locations ' +
            'so you can send location.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('You can use the Sim');
        // console.log(RNSimData.getSimInfo().simSerialNumber1);
        this.setState({iccid: RNSimData.getSimInfo().simSerialNumber0});
        // this.setState({iccid2: 8});
        if (RNSimData.getSimInfo().simSerialNumber1) {
          this.setState({iccid2: RNSimData.getSimInfo().simSerialNumber1});
          console.log(RNSimData.getSimInfo().simSerialNumber1);
        }
      } else {
        console.log('Sim permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  toHome = () => {
    this.props.navigation.navigate('ho');
  };
  handleLogin2 = async () => {
    try {
      this.setState({loading: true});

      let data = JSON.stringify({
        emailorphone: this.props.email,
        phone: this.props.phone,
        otp: this.state.kodeotp,
      });
      let loginResponse = await axios.post(
        `${GLOBAL.apiUrl}auth/loginotp`,
        data,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      if (loginResponse.data.success == true) {
        if (
          loginResponse.data.status == 0 ||
          loginResponse.data.status == '0'
        ) {
          Alert.alert('Atenção!', 'Login falhou, e-mail / palavras-passe erradas');
          this.setState({loading: false});
        } else {
          this.setState({loading: false});
          this.props.loginsuccess3(loginResponse.data.userId);
          this.props.loginsuccess2(loginResponse.data.userEmail);
          this.props.loginsuccess4(loginResponse.data.userPict);
          this.props.loginsuccess(loginResponse.data.userFullName);
          this.props.loginsuccess5(loginResponse.data.token);

          this.props.navigation.navigate('home');
          this.setState({loading: false});
        }

        this.setState({loading: false});
      } else {
        Alert.alert('Atenção!', 'Falha no login, código Otp inválido.');
        this.setState({loading: false});
      }
    } catch (e) {
      console.log(e);
      Alert.alert('erro de servidor');
      this.setState({loading: false});
    }
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });
  };

  hideAlert = () => {
    this.setState({
      showAlert: false,
    });
  };

  render() {
    return (
      <Container style={styles.topMargin}>
        {/* <StatusBar

                    backgroundColor='transparent'
                    animated
                /> */}
        <Header noShadow style={{backgroundColor: 'white'}}>
          <Left style={{flex: 1}}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('lo');
              }}>
              <Icon
                name="chevron-left"
                type="FontAwesome"
                style={{color: 'black'}}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{flex: 1}}>
            {/* <Icon
                            name="logo-twitter"
                            style={{ alignSelf: "center", color: "#4286f4" }}
                        /> */}
          </Body>
          <Right style={{flex: 1}}>
            {/* <Button transparent>
                            <Text style={{ color: "#4286f4" }}>Sign up</Text>
                        </Button>
                        <Button transparent>
                            <Icon name="more" style={{ color: "#4286f4" }} />
                        </Button> */}
          </Right>
        </Header>
        <Content style={styles.content}>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Image
              resizeMode="contain"
              style={{height: 100, width: 300}}
              source={require('../assets/witness-logo.png')}
            />
          </View>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            {/* <Image source={require('../assets/logo.png')} resizeMode="contain" style={{ width: 100, height: 100 }} /> */}
            {/* <Text style={styles.heading}>{this.props.email}</Text>
            <Text style={styles.heading}>{this.props.phone}</Text> */}
            <Text style={styles.heading}>Entrar o código OTP</Text>

            <OtpInputs
              handleChange={code => this.setState({kodeotp: code})}
              keyboardType="default"
              numberOfInputs={4}
              inputContainerStyles={{
                justifyContent: 'center',
                alignItems: 'center',
              }}
              inputStyles={{
                backgroundColor: '#e0e0e0',
                margin: 10,
                borderRadius: 4,
                paddingLeft: 15,
                paddingRight: 5,
                fontSize: 20,
                fontWeight: 'bold',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            />

            <View style={{height: 80}} />
            <TouchableOpacity
              style={[
                styles.buttonContainer,
                styles.loginButton,
                {alignSelf: 'center'},
              ]}
              onPress={this.handleLogin2}>
              {this.state.loading ? (
                <Spinner color="white" />
              ) : (
                <Text style={styles.loginText}>Enviar</Text>
              )}
            </TouchableOpacity>

            <View
              style={
                {
                  // flexDirection: 'row'
                }
              }>
              {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('forgotpassword')}>
                                <View>
                                    <Text style={{ color: GLOBAL.mainColor }}>Forgot Password ? </Text>
                                </View>
                            </TouchableOpacity> */}
              {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('register')}>
                                <View>
                                    <Text style={{ color: GLOBAL.mainColor }}>Register </Text>
                                </View>
                            </TouchableOpacity> */}
            </View>

            {/* <Item stackedLabel last>
                            <Label>Phone number or email address</Label>
                            <Input
                                value={this.state.emailorphone}
                                onChangeText={(emailorphone) => this.setState({ emailorphone })}
                            />
                        </Item>
                        <Item stackedLabel last>
                           
                            <Label>Password</Label>
                            <Input
                                secureTextEntry={true}
                                value={this.state.password}
                                onChangeText={(password) => this.setState({ password })}
                            />
                        </Item> */}

            {/* <Button
                            onPress={() => this.props.navigation.navigate('forgotpassword')}
                            transparent
                            style={{
                                margin: 15,
                                marginTop: 25,
                                width: "50%",
                                alignSelf: "center"
                            }}>
                            <Text style={{ textAlign: "center", fontSize: 14, color: "#AAA" }}>
                                Forgot password?
                                </Text>
                        </Button>

                        <Button
                            onPress={() => this.props.navigation.navigate('register')}
                            transparent
                            style={{
                                margin: 15,
                                marginTop: 25,
                                width: "50%",
                                alignSelf: "center"
                            }}>
                            <Text style={{ textAlign: "center", fontSize: 14, color: "#AAA" }}>
                                Register
                            </Text>
                        </Button> */}
          </View>
        </Content>
        <Footer style={styles.footer}>
          {this.props.loginStatus === 'ongoing' ? <Spinner /> : null}
          {this.props.loginStatus === 'failed' ? (
            <Text style={{color: '#f92a3f'}}>Login Failed</Text>
          ) : null}
          {/* <Button
                        rounded
                        style={{ backgroundColor: "#4286f4", marginLeft: 20 }}
                        onPress={this.handleLogin2}
                    >
                        {this.state.loading ? <Spinner color="white" /> : <Text>Log in</Text>}
                    </Button> */}
        </Footer>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title="Aviso"
          message="Todos os dados são necessários!"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={false}
          showConfirmButton={true}
          cancelText="OK"
          confirmText="OK"
          confirmButtonColor="#DD6B55"
          onCancelPressed={() => {
            this.hideAlert();
          }}
          onConfirmPressed={() => {
            this.hideAlert();
          }}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  topMargin: {
    // marginTop: 25
  },
  content: {
    padding: 10,
    backgroundColor: 'white',
    // justifyContent:'center',
    //alignItems:'center'
  },
  heading: {
    fontSize: 32,
    fontWeight: '400',
    marginBottom: 30,
  },
  footer: {
    backgroundColor: 'white',
    elevation: 3,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 0.2},
    shadowOpacity: 0.3,
    shadowRadius: 2,
    height: 60,
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#B0E0E6',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#E6ECF0',
    borderRadius: 30,
    borderBottomWidth: 1,
    height: 45,
    marginBottom: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  icon: {
    width: 30,
    height: 30,
  },
  inputIcon: {
    marginLeft: 15,
    justifyContent: 'center',
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: width * 0.94,
    borderRadius: 30,
  },
  loginButton: {
    backgroundColor: '#3498db',
  },
  fabookButton: {
    backgroundColor: '#3b5998',
  },
  googleButton: {
    backgroundColor: '#ff0000',
  },
  loginText: {
    color: 'white',
  },
  restoreButtonContainer: {
    width: 250,
    marginBottom: 15,
    alignItems: 'flex-end',
  },
  socialButtonContent: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  socialIcon: {
    color: '#FFFFFF',
    marginRight: 5,
  },
});

const mapStateToProps = state => {
  const {token, fasilitator, jabatan, eposko, email, phone} = state.anggota;
  return {token, fasilitator, jabatan, eposko, email, phone};
};

export default connect(
  mapStateToProps,
  {
    loginsuccess2,
    loginsuccess3,
    loginsuccess4,
    loginsuccess,
    loginsuccess5,
    loginsuccess7,
    loginsuccess8,
    loginsuccess9,
  },
)(Loginscreen);
