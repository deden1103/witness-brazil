/**
 * @format
 */
import React from 'react'
import { AppRegistry } from 'react-native';
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { persistStore, persistReducer } from 'redux-persist'
// import storage from 'redux-persist/lib/storage'
import { PersistGate } from 'redux-persist/integration/react'
import StyleGlobal from './app/global/styleGlobal'
import reducer from './app/rdx/reducers'
import AppNavigator from './app/AppNavigator'
import AsyncStorage from '@react-native-community/async-storage';
// base url api
//GLOBAL.apiUrl = 'https://witness-api.tempo.co/'
// GLOBAL.apiUrl = 'http://witness-api.hahabid.com/'
GLOBAL.apiUrl = 'https://witness-api-85yfn.ondigitalocean.app/'

StyleGlobal.loadStyle()

const persistConfig = {
    key: 'root',
    storage:AsyncStorage,
    whitelist: ['data','anggota','cartpersist']
}

const persistedReducer = persistReducer(persistConfig, reducer)
let store = createStore(persistedReducer, applyMiddleware(thunk))
let persistor = persistStore(store)

const App = () => (
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <AppNavigator />
        </PersistGate>
    </Provider>
)

console.disableYellowBox = true
AppRegistry.registerComponent('koinnu', () => App);

